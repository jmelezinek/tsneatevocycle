(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
const BalancingGameController_1 = require("./BalancingGameController");
class BalancingBot {
    constructor(_individual) {
        this._individual = _individual;
        this.start = function () {
            window.clearInterval(this.timer);
            this.game.gameReset();
            let fitnessPromise = new Promise((resolve, reject) => {
                this.timer = window.setInterval(this.update.bind(this, resolve), this.game.getTstep());
            });
            this.game.gameStart();
            return fitnessPromise;
        };
        this.update = function (resolve) {
            if (this.game.isGameover() || this.game.isGamewon()) {
                window.clearInterval(this.timer);
                resolve(this.game.getTime());
            }
            this.game.setOutputs(this.evaluate());
        };
        this._game = BalancingGameController_1.default.getInstance();
    }
    //
    // GETTERST and SETTERS
    //
    get game() {
        return this._game;
    }
    get timer() {
        return this._timer;
    }
    set timer(value) {
        this._timer = value;
    }
    get individual() {
        return this._individual;
    }
}
exports.BalancingBot = BalancingBot;
/**
 * @class
 * @description
 * used with sigmoid NodeGenes (output is (0;1))
 * 4 inputs - standard
 * 1 output - force and direction (value and sign)
 */
class NeatSingleOutputBalancingBot extends BalancingBot {
    evaluate() {
        let neatIndiv = this.individual;
        let outputs = neatIndiv.evaluateNetwork(this.game.getInputs());
        // translate neat output to game output // 0 -> same; -1 -> left; +1 -> right
        return [outputs[0] - 0.5];
    }
}
exports.NeatSingleOutputBalancingBot = NeatSingleOutputBalancingBot;
/**
 * @class
 * @description
 * used with binaryStep NodeGenes (output is 0/1)
 * 4 inputs - standard
 * 2 outputs - which button to press
 */
class NeatTwoOutputsBalancingBot extends BalancingBot {
    evaluate() {
        let neatIndiv = this.individual;
        let outputs = neatIndiv.evaluateNetwork(this.game.getInputs());
        // translate neat output to game output // 0 -> same; -1 -> left; +1 -> right
        if (outputs[0] == 1 && outputs[1] == 1 // both buttons down - incorrect output
            || outputs[0] == 0 && outputs[1] == 0) {
            return [0]; // keep same
        }
        if (outputs[0] == 1) {
            return [-1]; // left
        }
        if (outputs[1] == 1) {
            return [1]; // right
        }
        throw "Unregonized options";
    }
}
exports.NeatTwoOutputsBalancingBot = NeatTwoOutputsBalancingBot;
},{"./BalancingGameController":2}],2:[function(require,module,exports){
"use strict";
/// <reference path="../../node_modules/@types/jquery/index.d.ts" />
/// <reference path="../../node_modules/@types/angular/index.d.ts" />
class BalancingGameController {
    constructor() {
        this._angularCtrl = angular.element($('#game')).controller();
    }
    static getInstance() {
        if (!BalancingGameController._instance) {
            BalancingGameController._instance = new BalancingGameController();
        }
        return BalancingGameController._instance;
    }
    getInputs() {
        return [this.angularCtrl.getCartPos(), this.angularCtrl.getCartVel(), this.angularCtrl.getPoleAngle(), this.angularCtrl.getPoleVel()];
    }
    /**
     * @description Translates outputs to actions.
     * @param values
     */
    setOutputs(values) {
        if (values[0] < 0) {
            this.angularCtrl.pushLeft();
        }
        else if (values[0] == 0) {
            this.angularCtrl.pushSame();
        }
        else if (values[0] > 0) {
            this.angularCtrl.pushRight();
        }
    }
    getTime() {
        return this.angularCtrl.config.time;
    }
    getTstep() {
        return this.angularCtrl.config.tstep;
    }
    isGameover() {
        return !this.angularCtrl.fulfillConstraints();
    }
    isGamewon() {
        return this.angularCtrl.fulfillGame();
    }
    gameStart() {
        this.angularCtrl.start();
    }
    gameReset() {
        this.angularCtrl.reset();
    }
    //
    // GETTERS and SETTERS
    //
    get angularCtrl() {
        return this._angularCtrl;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BalancingGameController;
},{}],3:[function(require,module,exports){
"use strict";
const BaseNeatIndividual_1 = require("../lib/model/BaseNeatIndividual");
const BalancingBot_1 = require("./BalancingBot");
class NeatIndividual extends BaseNeatIndividual_1.BaseNeatIndividual {
    evaluateFitness() {
        let bot = new BalancingBot_1.NeatSingleOutputBalancingBot(this);
        return bot.start()
            .then((res) => {
            this._fitness = res;
            return res;
        });
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = NeatIndividual;
},{"../lib/model/BaseNeatIndividual":8,"./BalancingBot":1}],4:[function(require,module,exports){
"use strict";
/// <reference path="../../node_modules/@types/angular/index.d.ts" />
// TODO do not import .ts rather import .d.ts
const EvoCycle_1 = require("../lib/EvoCycle");
const BaseNeatIndividual_1 = require("../lib/model/BaseNeatIndividual");
const NeatIndividual_1 = require("./NeatIndividual");
// first load jq if not loaded
window.addEventListener('load', function () {
    if (window.jQuery) {
        console.log("WARNING: jQuery already loaded.");
        TsNeatEvoCycleControllerLoader.init();
    }
    else {
        console.log("WARNING: Loading jQuery first.");
        var url = "../jquery.min.js"; // TODO url
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {
                let jqscript = document.createElement("script");
                jqscript.type = "text/javascript";
                jqscript.src = url;
                document.head.appendChild(jqscript);
                TsNeatEvoCycleControllerLoader.init();
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
});
class TsNeatEvoCycleControllerLoader {
    constructor() { }
    static init() {
        TsNeatEvoCycleControllerLoader.loadAll().then(function () {
            // create init population and EvoCycle
            let initPopulation = BaseNeatIndividual_1.default.createInitPopulation(50, NeatIndividual_1.default, [4, 1]);
            let evocycle = new EvoCycle_1.default(initPopulation);
            // init controller with model
            let evoCycleController = angular.element($('#tsneatevocycle-controller')).controller();
            evoCycleController.setModel(evocycle);
        });
    }
    static loadAll() {
        // TODO insert from http://neat.melezinek.cz/dist/* (or github) for online purposes
        // var libBase = "http://neat.melezinek.cz/dist/lib/";
        let libBase = "../lib/";
        let script2 = $.getScript(libBase + "controller.js"); // loaded by ajax then executed
        let css2 = $.ajax({
            url: libBase + "controller.css",
            success: function (data) {
                $('<link>').attr({
                    href: libBase + "controller.css",
                    type: 'text/css',
                    rel: 'stylesheet'
                }).appendTo('head');
            },
            dataType: 'html'
        });
        let script1 = $.getScript(libBase + "controller_vendors.min.js"); // loaded by ajax then executed
        let css1 = $.ajax({
            url: libBase + "controller_vendors.css",
            success: function (data) {
                $('<link>').attr({
                    href: libBase + "controller_vendors.css",
                    type: 'text/css',
                    rel: 'stylesheet'
                }).appendTo('head');
            },
            dataType: 'html'
        });
        return Promise.all([script1, css1, script2, css2]).then((values) => {
            return $.ajax({
                url: libBase + "controller.html",
                success: function (data) {
                    $('body').append(data);
                },
                dataType: 'html'
            });
        });
    }
}
exports.TsNeatEvoCycleControllerLoader = TsNeatEvoCycleControllerLoader;
},{"../lib/EvoCycle":6,"../lib/model/BaseNeatIndividual":8,"./NeatIndividual":3}],5:[function(require,module,exports){
"use strict";
class Config {
}
Config.log = {
    functions: false,
    cycle: false,
    generations: false,
    nextFitness: false
};
Config.cycle = {
    continue: false,
    running: false
};
Config.general = {
    alwaysEvaluateFitness: false
};
Config.mutationOptions = {
    mutateOffsprings: true,
    mutateByCloning: true,
    individualTopology: {
        chance: 0.25,
        addNode: {
            chance: 0.03
        },
        addConnection: {
            chance: 0.05
        },
        addNodeXORaddConnection: true,
    },
    individualWeights: {
        chance: 0.8,
        weights: {
            chance: 1,
            mutateSingle: {
                chance: 1,
                stdev: 1
            }
        },
        thresholds: {
            chance: 1,
            mutateSingle: {
                chance: 1,
                stdev: 1
            }
        }
    }
};
Config.crossoverOptions = {
    offspringRatio: 0.8,
    tournamentRatio: 0.5
};
/**
 * distance function coefficient
 * d = (c_e*E)/N + (c_d*D)/N + c_m*W;
 * 3.0 = 1.0 ...... 1.0 ...... 0.4 - values from NEAT paper capter 4.1
 */
Config.speciation = {
    excessCoef: 1,
    disjointCoef: 1,
    matchingCoef: 0.4,
    distanceThreshold: 3.0
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Config;
},{}],6:[function(require,module,exports){
"use strict";
const Config_1 = require("./Config");
const BaseNeatIndividual_1 = require("./model/BaseNeatIndividual");
const Species_1 = require("./model/Species");
/**
 * @class
 * @description Main library class
 */
class EvoCycle {
    constructor(individuals) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.constructor");
        }
        this.config = Config_1.default;
        this.generationCounter = 0;
        this._observers = [];
        this.population = individuals;
        this.species = [new Species_1.default(individuals)];
        this.offsprings = [];
        this.mutants = [];
    }
    addObserver(observer) {
        this._observers.push(observer);
    }
    //////////////////// EVOLUTION FUNCTIONS ////////////////////
    continue(singleGeneration = false) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.continue - " + "running: " + Config_1.default.cycle.running + "; continue: " + Config_1.default.cycle.continue + "; singleGeneration: " + singleGeneration);
        }
        if (!this.population) {
            throw "EvoCycle has to be inicialized first with init population! See EvoCycle.init(individuals: Individuals)";
        }
        if (Config_1.default.cycle.running) {
            console.warn("EvoCycle.continue is already running");
            return; // prevent multiple call
        }
        if (!singleGeneration && !Config_1.default.cycle.continue) {
            console.warn("EvoCycle.continue cannot continue EvoCycle.config.cycle.continue flag is false");
            return; // do not continue
        }
        Config_1.default.cycle.running = true;
        this.doReduction().then((population) => {
            if (Config_1.default.log.functions) {
                console.log("EvoCycle.continue doReduction-then");
            }
            this.population = population;
            this.generationCounter++;
            if (Config_1.default.log.generations) {
                console.log("GENERATION");
                EvoCycle.print(this.population);
            }
            this._observers.forEach((observer) => {
                observer.notifyDoneReduction(this);
            });
            this.step(); // continue with cycle
        });
    }
    // step whole new generation
    step() {
        if (Config_1.default.log.cycle) {
            console.log("population - NEXT STEP:");
            console.dir(this.population);
            EvoCycle.print(this.population);
        }
        // classify individuals
        this.doSpeciation();
        if (Config_1.default.log.cycle) {
            console.log("species - doSpeciation:");
            console.dir(this.species);
        }
        // select individuals to be parents for breeding
        this.doSelection();
        if (Config_1.default.log.cycle) {
            console.log("parents - doSelection:");
            console.dir(this.parents); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this.parents.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // generate new individuals by crossover
        this.doCrossover();
        if (Config_1.default.log.cycle) {
            console.log("offsprings - doCrossover:");
            console.dir(this.offsprings); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this.offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // change individuals by mutations
        this.doMutation();
        if (Config_1.default.log.cycle) {
            console.log("population and offsprings - doMutation:");
            console.dir(this.population);
            console.dir(this.offsprings);
            EvoCycle.print(this.population);
            EvoCycle.print(this.offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // this.population = this.population.concat(offsprings); // TODO az v redukci
        Config_1.default.cycle.running = false;
        this.continue();
    }
    filterDuplicates(individials) {
        return individials.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        });
    }
    doSpeciation() {
        // random species representatives
        let i = this.species.length;
        while (i--) {
            if (this.species[i].clear() == false) {
                this.species.splice(i, 1); // remove this species
            }
        }
        // remove species which can be merged
        // this is not in specification but sounds like a good idea
        // i = this.species.length;
        // while(i-- > 1) { // WARNING iterating in reverse because of splice (removing items)
        //     if(this.species[i].representative.distanceTo(this.species[i-1].representative) < Config.speciation.distanceThreshold) {
        //         this.species[i].individuals = []; // empty the species // still in population so it will be classify again
        //         this.species.splice(i, 1); // remove this species
        //     }
        // }
        // classify individuals
        for (let key in this.population) {
            let indiv = this.population[key];
            let placed = false;
            for (let i = 0; i < this.species.length; i++) {
                if (indiv == this.species[i].representative) {
                    placed = true;
                    break;
                }
            }
            if (placed) {
                continue;
            }
            for (let i = 0; i < this.species.length; i++) {
                // if (indiv != this.species[i].representative)
                // place in existing class
                if (indiv.distanceTo(this.species[i].representative) < Config_1.default.speciation.distanceThreshold) {
                    this.species[i].add(indiv);
                    placed = true;
                    break;
                }
            }
            if (placed) {
                continue;
            }
            else {
                this.species.push(new Species_1.default([indiv]));
            }
        }
        this._observers.forEach((observer) => {
            observer.notifyDoneSpeciation(this, this.species);
        });
    }
    /**
     * Simple tournament selection implementation.
     * Randomly chooses (with repetition) k individuals and picks the best one of the tournament. Repeats until whole population is filled.
     */
    doSelection() {
        // TODO tournament by species
        // TODO how much to crossover vs how much to mutate
        // TODO taky mutovat asi nejen rodice ale vsechno co projde
        // protoze kdyz vypnu crossover uplne tak by nikdy nemutovali
        // nejde mutovat jenom potomky nebo jenom rodice protoze kdyz neni zapnutej crossover tak jsou tyhle skupiny prazdny
        // jak velkou cast populace vygenerovat pres crossover
        let offspringSize = Math.round(this.population.length * Config_1.default.crossoverOptions.offspringRatio);
        // zjistit kolik z jaky specie podle shared fitness
        let sumSharedFittness = this.species.reduce((sum, curr) => {
            return sum + curr.getSharedFitness();
        }, 0);
        this.parents = [];
        for (let key in this.species) {
            let spec = this.species[key];
            let specSize = spec.individuals.length;
            let speciOffspringSize = Math.round(spec.getSharedFitness() / sumSharedFittness * offspringSize);
            speciOffspringSize = speciOffspringSize > 0 ? speciOffspringSize : 1; // atleast one offspring
            // this.parents[key].push(null);
            let tournamentSize = Math.round(specSize * Config_1.default.crossoverOptions.tournamentRatio);
            tournamentSize = tournamentSize < 1 ? 1 : tournamentSize;
            tournamentSize = tournamentSize > specSize ? specSize : tournamentSize;
            // by tournament selection select as many individuals
            // as it is needed for crossover (2 times as much as offspring size)
            let winners = this.parents[key] = [];
            while (winners.length < speciOffspringSize * 2) {
                // select k random individuals (with repetition)
                let contestants = [];
                while (contestants.length < tournamentSize) {
                    let r = Math.floor(Math.random() * specSize);
                    contestants.push(spec.individuals[r]);
                }
                // sort them and select first as winner
                contestants.sort(BaseNeatIndividual_1.BaseNeatIndividual.compare);
                winners.push(contestants[0]);
            }
        }
        // let tournSize = this.tournamentSize;
        // let popSize = choices.length;
        // if (tournSize < 1 || tournSize > popSize) {
        //     throw new RangeError("Tournament size must be greater than 1 and less than or equal to population size: " +
        //         "tournamentSize = " + tournSize + ", populationSize = " + popSize);
        // }
        //
        // // by tournament selection select as many individuals
        // // as it is needed for crossover (2 times as much as offspring size)
        // let winners: Individuals = [];
        // while (winners.length < this.offspringSize * 2) {
        //     // select k random individuals (with repetition)
        //     let contestants: Individuals = [];
        //     while (contestants.length < tournSize) {
        //         let r = Math.floor(Math.random() * popSize);
        //         contestants.push(choices[r]);
        //     }
        //
        //     // sort them and select first as winner
        //     contestants.sort(BaseNeatIndividual.compare);
        //     winners.push(contestants[0]);
        // }
        //
        // return winners;
    }
    /**
     * Crossover
     */
    doCrossover() {
        this.offsprings = [];
        for (let key in this.parents) {
            let parents = this.parents[key];
            let offsprings = this.offsprings[key] = [];
            for (let i = 0; i < this.parents[key].length; i += 2) {
                offsprings.push(parents[i].breed(parents[i + 1]));
            }
        }
    }
    doMutation() {
        for (let key in this.population) {
            let indiv = this.population[key];
            this.mutants = [];
            if (Config_1.default.mutationOptions.mutateByCloning) {
                let clone = new indiv.constructor(indiv);
                let wasMutated = clone.mutate();
                if (wasMutated) {
                    this.mutants.push(clone);
                } // else will be forgotten
            }
            else {
                indiv.mutate();
            }
        }
        if (Config_1.default.mutationOptions.mutateOffsprings) {
            let allOffsprings = this.offsprings.reduce((a, b) => {
                return a.concat(b);
            });
            for (let key in allOffsprings) {
                let indiv = allOffsprings[key];
                indiv.mutate();
            }
        }
    }
    ;
    doReduction() {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.doReduction");
        }
        let allIndividials = [].concat.apply(this.population, this.offsprings).concat(this.mutants);
        // evaluateFitness if needed
        return this.evaluateAllFitness(allIndividials).then((population) => {
            if (Config_1.default.log.functions) {
                console.log("EvoCycle.doReduction evaluateAllFitness-then");
            }
            // sort all individuals and returns the fittest
            allIndividials.sort(BaseNeatIndividual_1.BaseNeatIndividual.compare); // in-place
            let eliminated = allIndividials.splice(this.population.length); // in-place
            this.population = allIndividials;
            for (let i = 0; i < eliminated.length; i++) {
                eliminated[i].eliminated = true;
            }
            return this.population; // chained promise
        });
    }
    evaluateAllFitness(population) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.EvaluateAllFitness");
        }
        // CANNOT use Promise.All, needs to be evaluated sequentially
        var allDonePromise = new Promise((resolve, reject) => {
            this.evaluateNextFitness(population, 0, resolve); // init promise recursion
        });
        return allDonePromise;
    }
    evaluateNextFitness(population, current, resolve) {
        if ((Config_1.default.log.functions && Config_1.default.log.nextFitness)
            || Config_1.default.log.nextFitness) {
            console.log("EvoCycle.evaluateNextFitness");
        }
        // end condition
        if (current >= population.length) {
            resolve(population);
        }
        else {
            // recursion
            if (population[current].fitness && !Config_1.default.general.alwaysEvaluateFitness) {
                // do not evaluate if fitness is known, call next immediately
                this.evaluateNextFitness(population, current + 1, resolve);
            }
            else {
                // evaluate unknown fitness, then call next
                population[current].evaluateFitness().then((res) => {
                    this._observers.forEach((observer) => {
                        observer.notifyDoneEvaluateNextFitness(this, population[current]);
                    });
                    this.evaluateNextFitness(population, current + 1, resolve);
                });
            }
        }
    }
    //////////////////// POPULATION FUNCTIONS ////////////////////
    getMaxAvgMinFitness() {
        let max;
        let sum = 0;
        let min;
        for (let i = 0; i < this.population.length; i++) {
            let f = this.population[i].fitness;
            if (max === undefined || max < f) {
                max = f;
            }
            if (min === undefined || min > f) {
                min = f;
            }
            sum += f;
        }
        return [max, sum / this.population.length, min];
    }
    static print(individuals) {
        var arr = individuals.map((item) => {
            return item.toString();
        });
        console.dir(arr);
    }
    //////////////////// GETTERS and SETTERS ////////////////////
    // toudnamentSize
    get tournamentSize() {
        return this._tournamentSize;
    }
    set tournamentSize(value) {
        this._tournamentSize = value;
    }
    getTournamentSize() {
        return this.tournamentSize;
    }
    setTournamentSize(value) {
        this.tournamentSize = value;
    }
    // offsipringSize
    get offspringSize() {
        return this._offspringSize;
    }
    set offspringSize(value) {
        this._offspringSize = value;
    }
    getOffspringSize() {
        return this.offspringSize;
    }
    setOffspringSize(value) {
        this.offspringSize = value;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = EvoCycle;
},{"./Config":5,"./model/BaseNeatIndividual":8,"./model/Species":11}],7:[function(require,module,exports){
"use strict";
class MyMath {
    constructor() { }
    /**
     * @description returns a gaussian random function with the given mean and stdev.
     * @author http://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve#answer-35599181
     * @viz https://en.wikipedia.org/wiki/Marsaglia_polar_method
     * @param mean (mu)
     * @param stdev (sigma) standard deviation
     * @returns {()=>number}
     */
    static gaussian(mean, stdev) {
        var y2;
        var use_last = false;
        return function () {
            var y1;
            if (use_last) {
                y1 = y2;
                use_last = false;
            }
            else {
                var x1, x2, w;
                do {
                    x1 = 2.0 * Math.random() - 1.0;
                    x2 = 2.0 * Math.random() - 1.0;
                    w = x1 * x1 + x2 * x2;
                } while (w >= 1.0);
                w = Math.sqrt((-2.0 * Math.log(w)) / w);
                y1 = x1 * w;
                y2 = x2 * w;
                use_last = true;
            }
            var retval = mean + stdev * y1;
            // if(retval > 0)
            //     return retval;
            // return -retval;
            return retval;
        };
    }
    // public static randomNormal = MyMath.gaussian(0, 1);
    static randomNormal(stdev) {
        return MyMath.gaussian(0, stdev);
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MyMath;
},{}],8:[function(require,module,exports){
"use strict";
const NodeGene_1 = require("./NodeGene");
const ConnectGene_1 = require("./ConnectGene");
const NodeGene_2 = require("./NodeGene");
const MyMath_1 = require("../MyMath");
const Config_1 = require("../Config");
class BaseNeatIndividual {
    constructor() {
        this._id = ++BaseNeatIndividual._idCounter;
        this._genome = [{}, {}];
        this.eliminated = false;
        if (arguments.length == 1 && arguments[0] instanceof BaseNeatIndividual) {
            this.copyConstructor(arguments[0]);
        }
        else {
            this.normalConstructor.apply(this, arguments);
        }
    }
    /**
     * @description copy constructor
     */
    copyConstructor(that) {
        this._inputGenes = [];
        this._outputGenes = [];
        // copy nodeGenes
        for (let key in that.nodeGenes) {
            this.addNodeGene(new NodeGene_1.default(that.nodeGenes[key]));
        }
        // add same connections
        for (let key in that.connectGenes) {
            let thatConnectNode = that.connectGenes[key];
            this.addConnection(this.nodeGenes[thatConnectNode.inNode.innov], this.nodeGenes[thatConnectNode.outNode.innov], thatConnectNode.weight, thatConnectNode.enabled, thatConnectNode.innov);
        }
    }
    /**
     * @description args constructor
     */
    normalConstructor() {
        let inputsLength;
        let outputsLength;
        if (typeof arguments[0] === "number" && typeof arguments[1] === "number") {
            inputsLength = arguments[0];
            outputsLength = arguments[1];
            this._inputGenes = [];
            this._outputGenes = [];
            for (let i = 0; i < inputsLength; i++) {
                this.addNodeGene(new NodeGene_1.default(NodeGene_2.NodeGeneType.Input));
            }
            for (let o = 0; o < outputsLength; o++) {
                this.addNodeGene(new NodeGene_1.default(NodeGene_2.NodeGeneType.Output));
            }
        }
        else if (Array.isArray(arguments[0]) && Array.isArray(arguments[1])) {
            inputsLength = arguments[0].length;
            outputsLength = arguments[1].length;
            this._inputGenes = arguments[0];
            this._outputGenes = arguments[1];
        }
        else {
            throw "Unexpected parameters";
        }
        for (let i = 0; i < inputsLength; i++) {
            for (let o = 0; o < outputsLength; o++) {
                this.addConnection(this._inputGenes[i], this._outputGenes[o]);
            }
        }
    }
    /**
     * @description merges on top of this individual (does not change matching nodes)
     * @param that
     */
    merge(that) {
        // copy nodeGenes which do not exists yet
        for (let key in that.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];
            if (!thisNode) {
                this.addNodeGene(new NodeGene_1.default(thatNode));
            }
        }
        // add connections which do not exists yet
        for (let key in that.connectGenes) {
            let thisConn = this.connectGenes[key];
            let thatConn = that.connectGenes[key];
            if (!thisConn) {
                this.addConnection(this.nodeGenes[thatConn.inNode.innov], this.nodeGenes[thatConn.outNode.innov], thatConn.weight, thatConn.enabled, thatConn.innov);
            }
        }
    }
    static createInitPopulation(populationSize, IndividualClass, firstIndividualConstructorArgs) {
        let population = [];
        let first;
        if (typeof firstIndividualConstructorArgs !== "undefined") {
            first = new IndividualClass(...firstIndividualConstructorArgs);
        }
        else {
            first = new IndividualClass();
        }
        population.push(first);
        for (let i = 1; i < populationSize; i++) {
            // randomize weights
            let next = new IndividualClass(first);
            for (let key in next.connectGenes) {
                let connection = next.connectGenes[key];
                connection.weight = 2 * Math.random() - 1; // TODO config? randomWeightFunc
            }
            population.push(next); // copies of first so same innov numbers
        }
        return population;
    }
    addNodeGene(nodeGene) {
        this.nodeGenes[nodeGene.innov] = nodeGene;
        if (nodeGene.type == NodeGene_2.NodeGeneType.Input) {
            this._inputGenes.push(nodeGene);
        }
        if (nodeGene.type == NodeGene_2.NodeGeneType.Output) {
            this._outputGenes.push(nodeGene);
        }
    }
    addConnectGene(connectGene) {
        this.connectGenes[connectGene.innov] = connectGene;
    }
    addConnection(inNode, outNode, weight, enabled, innov) {
        weight = typeof weight != "undefined" ? weight : 2 * Math.random() - 1; // TODO config? randomWeightFunc
        this.addConnectGene(new ConnectGene_1.default(inNode, outNode, weight, enabled, innov));
    }
    breed(partner) {
        let better;
        let worse;
        if (this.fitness > partner.fitness) {
            better = this;
            worse = partner;
        }
        else {
            better = partner;
            worse = this;
        }
        // make copy of better
        let offspring = new better.constructor(better); // compact way
        // inherit randomly weight for shared genes
        for (let key in offspring.connectGenes) {
            let connOffspring = offspring.connectGenes[key];
            let connWorse = worse.connectGenes[key];
            if (connWorse) {
                let which = 0.5 > Math.random();
                connOffspring.weight = which ? connOffspring.weight : connWorse.weight; // TODO weightCrossoverFunc
                connOffspring.enabled = which ? connOffspring.enabled : connWorse.enabled;
            }
        }
        /** TODO
         * this part is not totally clear for me - from http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf:
         * "In this case, equal fitnesses are assumed, so the disjoint and excess genes are also inherited randomly."
         * How can be disjoint and excess genes inherrited randomly when they are only in one of the parent?
         */
        // if same fitnesses, also inherit (copy) genes from other parent
        if (this.fitness == partner.fitness) {
            offspring.merge(worse);
        }
        // inherit randomly thresholds - imagine as another in connection
        for (let key in offspring.nodeGenes) {
            let nodeOffspring = offspring.nodeGenes[key];
            let nodeWorse = worse.nodeGenes[key];
            if (nodeWorse) {
                nodeOffspring.threshold = 0.5 > Math.random() ? nodeOffspring.threshold : nodeWorse.threshold;
            }
        }
        // chance to enable connection genes
        for (let key in offspring.connectGenes) {
            let connOffspring = offspring.connectGenes[key];
            connOffspring.enabled = 0.25 > Math.random() ? true : connOffspring.enabled; // TODO editable chance
        }
        return offspring;
    }
    mutate() {
        let isMutated = false;
        let topologyMutOptOf = Config_1.default.mutationOptions.individualTopology;
        let weightsMutOptOf = Config_1.default.mutationOptions.individualWeights;
        if (topologyMutOptOf.chance > Math.random()) {
            let xorChance; // 2 for false, <0;1) for addNode, <1;2) for addConnection
            if (topologyMutOptOf.addNodeXORaddConnection) {
                xorChance = Math.random() * 2; // Math.random() never equals 1 => xorChance never equals 2 which is reserved for xorChance false
            }
            else {
                xorChance = 2;
            }
            if (xorChance === 2 || (xorChance >= 0 && xorChance < 1)) {
                if (topologyMutOptOf.addNode.chance > Math.random()) {
                    this.mutateAddNode();
                    isMutated = true;
                }
            }
            if (xorChance === 2 || (xorChance >= 1 && xorChance < 2)) {
                if (topologyMutOptOf.addConnection.chance > Math.random()) {
                    this.mutateAddConnection();
                    isMutated = true;
                }
            }
        }
        if (weightsMutOptOf.chance > Math.random()) {
            if (weightsMutOptOf.weights.chance > Math.random()) {
                this.mutateWeights();
                isMutated = true;
            }
            if (weightsMutOptOf.thresholds.chance > Math.random()) {
                this.mutateThresholds();
                isMutated = true;
            }
        }
        this._fitness = null;
        return isMutated;
    }
    mutateAddConnection() {
        let n1 = this.getRandomNodeGene();
        let n2;
        do {
            n2 = this.getRandomNodeGene();
        } while (n1.id == n2.id);
        if (!this.areConnected(n1, n2)) {
            return this.addConnection(n1, n2);
        }
        else {
            return this.mutateAddConnection();
        }
    }
    mutateAddNode() {
        let edge;
        do {
            edge = this.getRandomConnectGene();
        } while (edge.enabled == false);
        edge.enabled = false;
        let inNode = edge.inNode;
        let outNode = edge.outNode;
        let innerNode = new NodeGene_1.default();
        this.addNodeGene(innerNode);
        this.addConnection(inNode, innerNode, 1);
        this.addConnection(innerNode, outNode, edge.weight);
    }
    mutateWeights() {
        for (let key in this.connectGenes) {
            if (Config_1.default.mutationOptions.individualWeights.weights.mutateSingle.chance > Math.random()) {
                let connection = this.connectGenes[key];
                connection.weight += MyMath_1.default.randomNormal(Config_1.default.mutationOptions.individualWeights.weights.mutateSingle.stdev)();
            }
        }
    }
    mutateThresholds() {
        for (let key in this.nodeGenes) {
            if (Config_1.default.mutationOptions.individualWeights.thresholds.mutateSingle.chance > Math.random()) {
                let node = this.nodeGenes[key];
                node.threshold += MyMath_1.default.randomNormal(Config_1.default.mutationOptions.individualWeights.thresholds.mutateSingle.stdev)();
            }
        }
    }
    evaluateNetwork(inputs) {
        let outputs = [];
        // evaluate input nodes (recursion end condition)
        for (let i = 0; i < this.inputGenes.length; i++) {
            this.inputGenes[i].evaluateOutput(inputs[i]);
        }
        // evaluate network from output nodes by recursion
        for (let key in this.outputGenes) {
            outputs.push(this.outputGenes[key].evaluateOutput());
        }
        // reset network evaluation for next evaluation
        for (let key in this.nodeGenes) {
            this.nodeGenes[key].resetOutput();
        }
        return outputs;
    }
    toString() {
        let nodeGenesString = "";
        for (let key in this.nodeGenes) {
            nodeGenesString += "\t" + this.nodeGenes[key].toString() + ";\n";
        }
        let connectGenesString = "";
        for (let key in this.connectGenes) {
            connectGenesString += "\t" + this.connectGenes[key].toString() + ";\n";
        }
        return "BaseNeatIndividual = {\n" +
            "\tid: " + this.id + ";\n" +
            "\tfitness: " + this.fitness + ";\n" +
            nodeGenesString +
            connectGenesString
            + "}";
    }
    static nodeGeneToText(nodeGene) {
        return `innov: ${nodeGene.innov}
                threshold: ${nodeGene.threshold}`;
    }
    static connectGeneToText(connectGeneGene) {
        return `innov: ${connectGeneGene.innov}
                weight: ${connectGeneGene.weight.toFixed(2)}
                ${connectGeneGene.enabled ? "" : "DISABLED"}`;
    }
    toGOJS() {
        let json = {};
        json["nodeKeyProperty"] = "id";
        json["nodeDataArray"] = [];
        for (let key in this.nodeGenes) {
            let nodeGene = this.nodeGenes[key];
            json["nodeDataArray"].push({
                id: nodeGene.id,
                text: BaseNeatIndividual.nodeGeneToText(nodeGene)
            });
        }
        json["linkDataArray"] = [];
        for (let key in this.connectGenes) {
            let connectGene = this.connectGenes[key];
            json["linkDataArray"].push({
                from: connectGene.inNode.id,
                to: connectGene.outNode.id,
                text: BaseNeatIndividual.connectGeneToText(connectGene)
            });
        }
        // json["nodeDataArray"] = [{ "id": 0, "loc": "120 120", "text": "XXX" }];
        // json["linkDataArray"] = [{ "from": 0, "to": 0, "text": "up or timer", "curviness": -20 }];
        return json;
    }
    //
    // GET functions
    //
    /**
     * @complexity O(#NG)
     * TODO often function and O(n) complexity, could be eliminated if array of NodeGenes is used, but how about other functions (crossover), probably easy by array functions
     * pick random from stream - complexity O(n)
     * problem that object is used not array
     * size will not help
     * @see next solution
     */
    getRandomNodeGene() {
        let result;
        let count = 0;
        for (let key in this.nodeGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.nodeGenes[key];
            }
        }
        return result;
    }
    // /**
    //  * TODO question is if Object.keys() is O(1) or O(n)
    //  * @returns {any}
    //  */
    // private getRandomNodeGeneXXX() {
    //     var keys = Object.keys(this.nodeGenes); // O(n) so makes no sence
    //     return this.nodeGenes[keys[ keys.length * Math.random() << 0]];
    // }
    /**
     * TODO same problem as for getRandomNdeGene
     * @complexity O(#CG)
     */
    getRandomConnectGene() {
        let result;
        let count = 0;
        for (let key in this.connectGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.connectGenes[key];
            }
        }
        return result;
    }
    /**
     * @complexity O(#CG) but probably cannot be better
     */
    areConnected(n1, n2) {
        for (let key in this.connectGenes) {
            // check
            let edge = this.connectGenes[key];
            if (edge.inNode.id == n1.id && edge.outNode.id == n2.id
                || edge.inNode.id == n2.id && edge.outNode.id == n1.id) {
                return true;
            }
        }
        return false;
    }
    static compare(first, second) {
        return second.fitness - first.fitness;
    }
    distanceTo(that) {
        let c_e = Config_1.default.speciation.excessCoef;
        let c_d = Config_1.default.speciation.disjointCoef;
        let c_m = Config_1.default.speciation.matchingCoef;
        let N = 0; // #genes in longer
        let D = 0; // #genes disjoint
        let E = 0; // #genes excess
        let W = 0; // average weight differences of matching genes W
        let M = 0; // #genes matching
        let thisArray = this.getConnectGenesAsArray();
        let thatArray = that.getConnectGenesAsArray();
        let i = 0;
        let j = 0;
        while (i < thisArray.length && j < thatArray.length) {
            let thisConn = thisArray[i];
            let thatConn = thatArray[j];
            if (thisConn.innov == thatConn.innov) {
                W += Math.abs(thisConn.weight - thatConn.weight);
                M++;
                i++;
                j++;
            }
            else {
                D++;
                if (thisConn.innov < thatConn.innov) {
                    j++;
                }
                else {
                    i++;
                }
            }
        }
        E = i < thisArray.length ? thisArray.length - i : (j < thatArray.length ? thatArray.length - j : 0); // excess
        N = thisArray.length > thatArray.length ? thisArray.length : thatArray.length;
        // count thresholds also as input connection
        for (let key in this.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];
            if (thatNode) {
                W += Math.abs(thisNode.threshold - thatNode.threshold);
                M++;
            }
        }
        W = W / M;
        return (c_e * E) / N + (c_d * D) / N + c_m * W;
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get fitness() {
        return this._fitness;
    }
    get nodeGenes() {
        return this._genome[0];
    }
    get connectGenes() {
        return this._genome[1];
    }
    getConnectGenesAsArray() {
        let res = [];
        for (let key in this.connectGenes) {
            res.push(this.connectGenes[key]);
        }
        return res.sort((a, b) => { return b.innov - a.innov; });
    }
    get inputGenes() {
        return this._inputGenes;
    }
    get outputGenes() {
        return this._outputGenes;
    }
}
BaseNeatIndividual._idCounter = 0;
exports.BaseNeatIndividual = BaseNeatIndividual;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BaseNeatIndividual;
},{"../Config":5,"../MyMath":7,"./ConnectGene":9,"./NodeGene":10}],9:[function(require,module,exports){
"use strict";
class ConnectGene {
    // public inNode: NodeGene;
    // public outNode: NodeGene;
    // public weight: number;
    constructor(inNode, outNode, weight, enabled, innov) {
        this.inNode = inNode;
        this.outNode = outNode;
        this.weight = weight;
        this._id = ++ConnectGene._idCounter;
        this._innov = typeof innov != "undefined" ? innov : ++ConnectGene._innovCounter;
        this.enabled = typeof enabled != "undefined" ? enabled : true;
        this.outNode.addInConnection(this);
    }
    toString() {
        return "ConnecGene = {id: " + this.id + "; innov: " + this.innov + "; in: " + this.inNode.id + "; out: " + this.outNode.id + "; weight: " + this.weight.toFixed(2) + "; enabled: " + this.enabled + "}";
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get innov() {
        return this._innov;
    }
}
ConnectGene._idCounter = 0;
ConnectGene._innovCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ConnectGene;
},{}],10:[function(require,module,exports){
"use strict";
class NodeGene {
    constructor() {
        this._id = ++NodeGene._idCounter;
        if (arguments.length == 1 && arguments[0] instanceof NodeGene) {
            this.copyConstructor(arguments[0]);
        }
        else {
            this.normalConstructor(arguments[0], arguments[1]);
        }
    }
    copyConstructor(that) {
        // does not copy connectGenes - copied from caller
        this._innov = that._innov;
        this._inConnections = [];
        this.threshold = that.threshold;
        this.output = that.output;
        this.lastOutput = that.lastOutput;
        this._state = that._state;
        this._type = that._type;
        this._activationFunc = that._activationFunc;
    }
    normalConstructor(type, activationFunc) {
        this._innov = ++NodeGene._innovCounter;
        this._inConnections = [];
        this.threshold = 0;
        this.output = null;
        this.lastOutput = 0;
        this._state = NodeGeneState.New;
        this._type = typeof type !== "undefined" ? arguments[0] : NodeGeneType.Hidden;
        this._activationFunc = typeof activationFunc !== "undefined" ? activationFunc : softStep;
    }
    addInConnection(inConnection) {
        this.inConnections.push(inConnection);
    }
    evaluateOutput(input) {
        if (this.state == NodeGeneState.Open) {
            return this.lastOutput;
        }
        if (this.state == NodeGeneState.Closed) {
            return this.output;
        }
        // other - evaluate recursively
        this._state = NodeGeneState.Open;
        let sum = 0;
        // input nodes - sum over inputs, then over recurrent connections
        if (this.type == NodeGeneType.Input && arguments.length == 1) {
            sum += input;
            sum += this.inConnections.reduce((sum, current) => {
                if (current.enabled) {
                    return sum + current.inNode.lastOutput * current.weight; // NO recursion - recurrent connection
                }
                else {
                    return sum;
                }
            }, 0);
            sum += this.threshold;
        }
        else {
            // hidden, output nodes - sum over in connections
            sum += this.inConnections.reduce((sum, current) => {
                if (current.enabled) {
                    return sum + current.inNode.evaluateOutput() * current.weight; // recursion
                }
                else {
                    return sum;
                }
            }, 0);
            sum += this.threshold;
        }
        this._state = NodeGeneState.Closed;
        this.output = this.activationFunc(sum);
        return this.output;
    }
    resetOutput() {
        this.lastOutput = this.output;
        this.output = null;
        this._state = NodeGeneState.New;
    }
    toString() {
        return "NodeGene = {id: " + this.id + "; innov: " + this.innov + "; type:" + this.type + "; threshold: " + this.threshold.toFixed(2) + "}";
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get innov() {
        return this._innov;
    }
    get type() {
        return this._type;
    }
    get activationFunc() {
        return this._activationFunc;
    }
    get inConnections() {
        return this._inConnections;
    }
    get state() {
        return this._state;
    }
}
NodeGene._idCounter = 0;
NodeGene._innovCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = NodeGene;
var NodeGeneType;
(function (NodeGeneType) {
    NodeGeneType[NodeGeneType["Input"] = 0] = "Input";
    NodeGeneType[NodeGeneType["Hidden"] = 1] = "Hidden";
    NodeGeneType[NodeGeneType["Output"] = 2] = "Output";
})(NodeGeneType = exports.NodeGeneType || (exports.NodeGeneType = {}));
var NodeGeneState;
(function (NodeGeneState) {
    NodeGeneState[NodeGeneState["New"] = 0] = "New";
    NodeGeneState[NodeGeneState["Open"] = 1] = "Open";
    NodeGeneState[NodeGeneState["Closed"] = 2] = "Closed";
})(NodeGeneState = exports.NodeGeneState || (exports.NodeGeneState = {}));
// TODO somehow better, or class and static fieldy
var NodeGeneActFunc;
(function (NodeGeneActFunc) {
})(NodeGeneActFunc = exports.NodeGeneActFunc || (exports.NodeGeneActFunc = {}));
function softStep(x) {
    return 1 / (1 + Math.exp(-x));
}
exports.softStep = softStep;
function binaryStep(x) {
    return x >= 0 ? 1 : 0;
}
exports.binaryStep = binaryStep;
},{}],11:[function(require,module,exports){
"use strict";
class Species {
    constructor(individuals) {
        this._id = ++Species._idCounter;
        this.individuals = typeof individuals != "undefined" ? individuals : [];
    }
    add(individual) {
        this.individuals.push(individual);
    }
    /**
     * keeps single random representative
     */
    clear() {
        let representative = null;
        let count = 0;
        for (let key in this.individuals) {
            if (!this.individuals[key].eliminated) {
                if (Math.random() < 1 / ++count) {
                    representative = this.individuals[key];
                }
            }
        }
        if (representative == null) {
            this.individuals = [];
            return false;
        }
        else {
            this.individuals = [representative];
            return true;
        }
    }
    /**
     * average fitness over species
     */
    getSharedFitness() {
        return this.individuals.reduce((sum, curr) => { return sum + curr.fitness; }, 0) / this.individuals.length;
    }
    isEmpty() {
        return this.individuals.length == 0;
    }
    get representative() {
        return this.individuals[0];
    }
    get id() {
        return this._id;
    }
}
Species._idCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Species;
},{}]},{},[4])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJCYWxhbmNpbmdCb3QudHMiLCJCYWxhbmNpbmdHYW1lQ29udHJvbGxlci50cyIsIk5lYXRJbmRpdmlkdWFsLnRzIiwiaW5kZXgudHMiLCIuLi9saWIvQ29uZmlnLnRzIiwiLi4vbGliL0V2b0N5Y2xlLnRzIiwiLi4vbGliL015TWF0aC50cyIsIi4uL2xpYi9tb2RlbC9CYXNlTmVhdEluZGl2aWR1YWwudHMiLCIuLi9saWIvbW9kZWwvQ29ubmVjdEdlbmUudHMiLCIuLi9saWIvbW9kZWwvTm9kZUdlbmUudHMiLCIuLi9saWIvbW9kZWwvU3BlY2llcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUNFQSx1RUFBZ0U7QUFFaEU7SUFNSSxZQUEyQixXQUEyQjtRQUEzQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFJL0MsVUFBSyxHQUFHO1lBQ1gsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUV0QixJQUFJLGNBQWMsR0FBRyxJQUFJLE9BQU8sQ0FDNUIsQ0FBQyxPQUFPLEVBQUUsTUFBTTtnQkFDWixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUMzRixDQUFDLENBQUMsQ0FBQztZQUVQLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFdEIsTUFBTSxDQUFDLGNBQWMsQ0FBQztRQUMxQixDQUFDLENBQUM7UUFFTSxXQUFNLEdBQUcsVUFBUyxPQUFZO1lBQ2xDLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNqQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQ2pDLENBQUM7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUM7UUF4QkUsSUFBSSxDQUFDLEtBQUssR0FBRyxpQ0FBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2RCxDQUFDO0lBMkJELEVBQUU7SUFDRix1QkFBdUI7SUFDdkIsRUFBRTtJQUVGLElBQWMsSUFBSTtRQUNkLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFjLEtBQUs7UUFDZixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRUQsSUFBYyxLQUFLLENBQUMsS0FBYTtRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBYyxVQUFVO1FBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7Q0FFSjtBQXZERCxvQ0F1REM7QUFFRDs7Ozs7O0dBTUc7QUFDSCxrQ0FBMEMsU0FBUSxZQUFZO0lBQ2hELFFBQVE7UUFFZCxJQUFJLFNBQVMsR0FBMkMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN4RSxJQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztRQUUvRCw2RUFBNkU7UUFDN0UsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0lBQzlCLENBQUM7Q0FDSjtBQVRELG9FQVNDO0FBRUQ7Ozs7OztHQU1HO0FBQ0gsZ0NBQXdDLFNBQVEsWUFBWTtJQUM5QyxRQUFRO1FBRWQsSUFBSSxTQUFTLEdBQTJDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDeEUsSUFBSSxPQUFPLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFFL0QsNkVBQTZFO1FBQzdFLEVBQUUsQ0FBQSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyx1Q0FBdUM7ZUFDdEUsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVk7UUFDNUIsQ0FBQztRQUNELEVBQUUsQ0FBQSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO1FBQ3hCLENBQUM7UUFDRCxFQUFFLENBQUEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7UUFDeEIsQ0FBQztRQUVELE1BQU0scUJBQXFCLENBQUM7SUFDaEMsQ0FBQztDQUNKO0FBcEJELGdFQW9CQzs7O0FDMUdELG9FQUFvRTtBQUNwRSxxRUFBcUU7QUFHckU7SUFLSTtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNqRSxDQUFDO0lBRU0sTUFBTSxDQUFDLFdBQVc7UUFDckIsRUFBRSxDQUFBLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHVCQUF1QixDQUFDLFNBQVMsR0FBRyxJQUFJLHVCQUF1QixFQUFFLENBQUM7UUFDdEUsQ0FBQztRQUNELE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUM7SUFDN0MsQ0FBQztJQUVNLFNBQVM7UUFDWixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7SUFDMUksQ0FBQztJQUVEOzs7T0FHRztJQUNJLFVBQVUsQ0FBQyxNQUFxQjtRQUNuQyxFQUFFLENBQUEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2hDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLE9BQU87UUFDVixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3hDLENBQUM7SUFFTSxRQUFRO1FBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUN6QyxDQUFDO0lBRU0sVUFBVTtRQUNiLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUNsRCxDQUFDO0lBRU0sU0FBUztRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUFFTSxTQUFTO1FBQ1osSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRU0sU0FBUztRQUNaLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELEVBQUU7SUFDRixzQkFBc0I7SUFDdEIsRUFBRTtJQUVGLElBQWMsV0FBVztRQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0NBRUo7O0FBbEVELDBDQWtFQzs7O0FDdEVELHdFQUFtRTtBQUNuRSxpREFBNEQ7QUFFNUQsb0JBQW9DLFNBQVEsdUNBQWtCO0lBRW5ELGVBQWU7UUFFbEIsSUFBSSxHQUFHLEdBQUcsSUFBSSwyQ0FBNEIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVqRCxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRTthQUNiLElBQUksQ0FBQyxDQUFDLEdBQVc7WUFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztZQUNwQixNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ2YsQ0FBQyxDQUFDLENBQUM7SUFFWCxDQUFDO0NBQ0o7O0FBYkQsaUNBYUM7OztBQ2hCRCxxRUFBcUU7QUFHckUsNkNBQTZDO0FBQzdDLDhDQUF1QztBQUN2Qyx3RUFBaUU7QUFDakUscURBQThDO0FBRTlDLDhCQUE4QjtBQUM5QixNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO0lBQzVCLEVBQUUsQ0FBQyxDQUFPLE1BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUMvQyw4QkFBOEIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBQUMsSUFBSSxDQUFDLENBQUM7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFFOUMsSUFBSSxHQUFHLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxXQUFXO1FBQ3pDLElBQUksT0FBTyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7UUFDbkMsT0FBTyxDQUFDLGtCQUFrQixHQUFHO1lBQ3pCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksY0FBYyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFBLENBQUM7Z0JBRXBFLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2hELFFBQVEsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7Z0JBQ2xDLFFBQVEsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO2dCQUNuQixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFFcEMsOEJBQThCLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQixPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbkIsQ0FBQztBQUVMLENBQUMsQ0FBQyxDQUFDO0FBRUg7SUFDSSxnQkFBdUIsQ0FBQztJQUVqQixNQUFNLENBQUMsSUFBSTtRQUNkLDhCQUE4QixDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQztZQUUxQyxzQ0FBc0M7WUFDdEMsSUFBSSxjQUFjLEdBQUcsNEJBQWtCLENBQUMsb0JBQW9CLENBQUMsRUFBRSxFQUFFLHdCQUFjLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6RixJQUFJLFFBQVEsR0FBRyxJQUFJLGtCQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7WUFFNUMsNkJBQTZCO1lBQzdCLElBQUksa0JBQWtCLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3ZGLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxNQUFNLENBQUMsT0FBTztRQUNsQixtRkFBbUY7UUFDbkYsc0RBQXNEO1FBQ3RELElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQztRQUV4QixJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLCtCQUErQjtRQUNyRixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ2QsR0FBRyxFQUFFLE9BQU8sR0FBRyxnQkFBZ0I7WUFDL0IsT0FBTyxFQUFFLFVBQVUsSUFBSTtnQkFDbkIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDYixJQUFJLEVBQUUsT0FBTyxHQUFHLGdCQUFnQjtvQkFDaEMsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLEdBQUcsRUFBRSxZQUFZO2lCQUNwQixDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3hCLENBQUM7WUFDRCxRQUFRLEVBQUUsTUFBTTtTQUNuQixDQUFDLENBQUM7UUFFSCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsK0JBQStCO1FBQ2pHLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDZCxHQUFHLEVBQUUsT0FBTyxHQUFHLHdCQUF3QjtZQUN2QyxPQUFPLEVBQUUsVUFBVSxJQUFJO2dCQUNuQixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUNiLElBQUksRUFBRSxPQUFPLEdBQUcsd0JBQXdCO29CQUN4QyxJQUFJLEVBQUUsVUFBVTtvQkFDaEIsR0FBRyxFQUFFLFlBQVk7aUJBQ3BCLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDeEIsQ0FBQztZQUNELFFBQVEsRUFBRSxNQUFNO1NBQ25CLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNO1lBQzNELE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNWLEdBQUcsRUFBRSxPQUFPLEdBQUcsaUJBQWlCO2dCQUNoQyxPQUFPLEVBQUUsVUFBVSxJQUFJO29CQUNuQixDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQixDQUFDO2dCQUNELFFBQVEsRUFBRSxNQUFNO2FBQ25CLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKO0FBekRELHdFQXlEQzs7O0FDNUZEOztBQUNrQixVQUFHLEdBQUc7SUFDaEIsU0FBUyxFQUFFLEtBQUs7SUFDaEIsS0FBSyxFQUFFLEtBQUs7SUFDWixXQUFXLEVBQUUsS0FBSztJQUNsQixXQUFXLEVBQUUsS0FBSztDQUNyQixDQUFDO0FBRVksWUFBSyxHQUFHO0lBQ2xCLFFBQVEsRUFBRSxLQUFLO0lBQ2YsT0FBTyxFQUFFLEtBQUs7Q0FDakIsQ0FBQztBQUVZLGNBQU8sR0FBRztJQUNwQixxQkFBcUIsRUFBRSxLQUFLO0NBQy9CLENBQUM7QUFFWSxzQkFBZSxHQUFHO0lBQzVCLGdCQUFnQixFQUFFLElBQUk7SUFDdEIsZUFBZSxFQUFFLElBQUk7SUFDckIsa0JBQWtCLEVBQUU7UUFDaEIsTUFBTSxFQUFFLElBQUk7UUFDWixPQUFPLEVBQUU7WUFDTCxNQUFNLEVBQUUsSUFBSTtTQUNmO1FBQ0QsYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLElBQUk7U0FDZjtRQUNELHVCQUF1QixFQUFFLElBQUk7S0FDaEM7SUFDRCxpQkFBaUIsRUFBRTtRQUNmLE1BQU0sRUFBRSxHQUFHO1FBQ1gsT0FBTyxFQUFFO1lBQ0wsTUFBTSxFQUFFLENBQUM7WUFDVCxZQUFZLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsS0FBSyxFQUFFLENBQUM7YUFDWDtTQUNKO1FBQ0QsVUFBVSxFQUFFO1lBQ1IsTUFBTSxFQUFFLENBQUM7WUFDVCxZQUFZLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsS0FBSyxFQUFFLENBQUM7YUFDWDtTQUNKO0tBQ0o7Q0FDSixDQUFDO0FBRVksdUJBQWdCLEdBQUc7SUFDN0IsY0FBYyxFQUFFLEdBQUc7SUFDbkIsZUFBZSxFQUFFLEdBQUc7Q0FDdkIsQ0FBQztBQUVGOzs7O0dBSUc7QUFDVyxpQkFBVSxHQUFHO0lBQ3ZCLFVBQVUsRUFBRSxDQUFDO0lBQ2IsWUFBWSxFQUFHLENBQUM7SUFDaEIsWUFBWSxFQUFFLEdBQUc7SUFDakIsaUJBQWlCLEVBQUUsR0FBRztDQUN6QixDQUFDOztBQWhFTix5QkFrRUM7OztBQ2xFRCxxQ0FBOEI7QUFDOUIsbUVBQTJFO0FBQzNFLDZDQUFzQztBQWF0Qzs7O0dBR0c7QUFDSDtJQWdCSSxZQUFZLFdBQXdCO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUVyQixJQUFJLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQztRQUM5QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxpQkFBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVNLFdBQVcsQ0FBQyxRQUEwQjtRQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsNkRBQTZEO0lBRXRELFFBQVEsQ0FBQyxtQkFBNEIsS0FBSztRQUM3QyxFQUFFLENBQUMsQ0FBQyxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEdBQUcsV0FBVyxHQUFHLGdCQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxjQUFjLEdBQUcsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLHNCQUFzQixHQUFHLGdCQUFnQixDQUFDLENBQUM7UUFDbEssQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSx3R0FBd0csQ0FBQztRQUNuSCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFDckQsTUFBTSxDQUFDLENBQUMsd0JBQXdCO1FBQ3BDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLENBQUMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM5QyxPQUFPLENBQUMsSUFBSSxDQUFDLGdGQUFnRixDQUFDLENBQUM7WUFDL0YsTUFBTSxDQUFDLENBQUMsa0JBQWtCO1FBQzlCLENBQUM7UUFFRCxnQkFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBRTVCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0NBQW9DLENBQUMsQ0FBQztZQUN0RCxDQUFDO1lBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7WUFDN0IsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFFekIsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEMsQ0FBQztZQUVELElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBMEI7Z0JBQy9DLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUN0QyxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLHNCQUFzQjtRQUN2QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0QkFBNEI7SUFDckIsSUFBSTtRQUVQLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFFRCx1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFRCxnREFBZ0Q7UUFDaEQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsMEVBQTBFO1lBQ3JHLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDcEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDUixDQUFDO1FBRUQsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixFQUFFLENBQUMsQ0FBQyxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLDBFQUEwRTtZQUN4RyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3ZDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ1IsQ0FBQztRQUVELGtDQUFrQztRQUNsQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEIsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7WUFDdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDN0IsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDaEMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN2QyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNSLENBQUM7UUFFRCw2RUFBNkU7UUFDN0UsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVPLGdCQUFnQixDQUFDLFdBQXdCO1FBQzdDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJO1lBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTyxZQUFZO1FBQ2hCLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztRQUM1QixPQUFPLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDVCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLHNCQUFzQjtZQUNyRCxDQUFDO1FBQ0wsQ0FBQztRQUVELHFDQUFxQztRQUNyQywyREFBMkQ7UUFDM0QsMkJBQTJCO1FBQzNCLHNGQUFzRjtRQUN0Riw4SEFBOEg7UUFDOUgscUhBQXFIO1FBQ3JILDREQUE0RDtRQUM1RCxRQUFRO1FBQ1IsSUFBSTtRQUVKLHVCQUF1QjtRQUN2QixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUM5QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRWpDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNuQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzNDLEVBQUUsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQzFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ2QsS0FBSyxDQUFDO2dCQUNWLENBQUM7WUFDTCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDVCxRQUFRLENBQUM7WUFDYixDQUFDO1lBRUQsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUMzQywrQ0FBK0M7Z0JBQy9DLDBCQUEwQjtnQkFDMUIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDekYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzNCLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ2QsS0FBSyxDQUFDO2dCQUNWLENBQUM7WUFDTCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDVCxRQUFRLENBQUM7WUFDYixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxpQkFBTyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUEwQjtZQUMvQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7O09BR0c7SUFDSyxXQUFXO1FBR2YsNkJBQTZCO1FBQzdCLG1EQUFtRDtRQUVuRCwyREFBMkQ7UUFDM0QsNkRBQTZEO1FBQzdELG9IQUFvSDtRQUVwSCxzREFBc0Q7UUFDdEQsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxnQkFBTSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBRWhHLG1EQUFtRDtRQUNuRCxJQUFJLGlCQUFpQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUk7WUFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN6QyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFTixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNsQixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTdCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBRXZDLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxpQkFBaUIsR0FBRyxhQUFhLENBQUMsQ0FBQztZQUNqRyxrQkFBa0IsR0FBRyxrQkFBa0IsR0FBRyxDQUFDLEdBQUcsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLENBQUMsd0JBQXdCO1lBRTlGLGdDQUFnQztZQUVoQyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxnQkFBTSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3BGLGNBQWMsR0FBRyxjQUFjLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxjQUFjLENBQUM7WUFDekQsY0FBYyxHQUFHLGNBQWMsR0FBRyxRQUFRLEdBQUcsUUFBUSxHQUFHLGNBQWMsQ0FBQztZQUV2RSxxREFBcUQ7WUFDckQsb0VBQW9FO1lBQ3BFLElBQUksT0FBTyxHQUFnQixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNsRCxPQUFPLE9BQU8sQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQzdDLGdEQUFnRDtnQkFDaEQsSUFBSSxXQUFXLEdBQWdCLEVBQUUsQ0FBQztnQkFDbEMsT0FBTyxXQUFXLENBQUMsTUFBTSxHQUFHLGNBQWMsRUFBRSxDQUFDO29CQUN6QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsQ0FBQztvQkFDN0MsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLENBQUM7Z0JBRUQsdUNBQXVDO2dCQUN2QyxXQUFXLENBQUMsSUFBSSxDQUFDLHVDQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUM3QyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLENBQUM7UUFFTCxDQUFDO1FBRUQsdUNBQXVDO1FBQ3ZDLGdDQUFnQztRQUNoQyw4Q0FBOEM7UUFDOUMsa0hBQWtIO1FBQ2xILDhFQUE4RTtRQUM5RSxJQUFJO1FBQ0osRUFBRTtRQUNGLHdEQUF3RDtRQUN4RCx1RUFBdUU7UUFDdkUsaUNBQWlDO1FBQ2pDLG9EQUFvRDtRQUNwRCx1REFBdUQ7UUFDdkQseUNBQXlDO1FBQ3pDLCtDQUErQztRQUMvQyx1REFBdUQ7UUFDdkQsd0NBQXdDO1FBQ3hDLFFBQVE7UUFDUixFQUFFO1FBQ0YsOENBQThDO1FBQzlDLG9EQUFvRDtRQUNwRCxvQ0FBb0M7UUFDcEMsSUFBSTtRQUNKLEVBQUU7UUFDRixrQkFBa0I7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssV0FBVztRQUVmLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzNCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFaEMsSUFBSSxVQUFVLEdBQWdCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3hELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNuRCxVQUFVLENBQUMsSUFBSSxDQUNYLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUNuQyxDQUFDO1lBQ04sQ0FBQztRQUNMLENBQUM7SUFFTCxDQUFDO0lBRU8sVUFBVTtRQUVkLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsRUFBRSxDQUFBLENBQUMsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxLQUFLLEdBQXVCLElBQVUsS0FBSyxDQUFDLFdBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFcEUsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUVoQyxFQUFFLENBQUEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNaLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixDQUFDLENBQUMseUJBQXlCO1lBQy9CLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDbkIsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDNUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7WUFDSCxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixJQUFJLEtBQUssR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRS9CLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNuQixDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFBQSxDQUFDO0lBRU0sV0FBVztRQUNmLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFFRCxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRTVGLDRCQUE0QjtRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVU7WUFDM0QsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQ2hFLENBQUM7WUFFRCwrQ0FBK0M7WUFDL0MsY0FBYyxDQUFDLElBQUksQ0FBQyx1Q0FBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFdBQVc7WUFDNUQsSUFBSSxVQUFVLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsV0FBVztZQUMzRSxJQUFJLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQztZQUVqQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDekMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDcEMsQ0FBQztZQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsa0JBQWtCO1FBQzlDLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUVPLGtCQUFrQixDQUFDLFVBQXVCO1FBQzlDLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1FBQy9DLENBQUM7UUFFRCw2REFBNkQ7UUFFN0QsSUFBSSxjQUFjLEdBQUcsSUFBSSxPQUFPLENBQWMsQ0FBQyxPQUFPLEVBQUUsTUFBTTtZQUMxRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLHlCQUF5QjtRQUMvRSxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxjQUFjLENBQUM7SUFDMUIsQ0FBQztJQUVPLG1CQUFtQixDQUFDLFVBQXVCLEVBQUUsT0FBZSxFQUFFLE9BQVk7UUFDOUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLElBQUksZ0JBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO2VBQzdDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1FBQ2hELENBQUM7UUFFRCxnQkFBZ0I7UUFDaEIsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFFSixZQUFZO1lBQ1osRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGdCQUFNLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztnQkFDdkUsNkRBQTZEO2dCQUM3RCxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLE9BQU8sR0FBRyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDL0QsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLDJDQUEyQztnQkFDM0MsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUc7b0JBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBeUI7d0JBQzlDLFFBQVEsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ3RFLENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsT0FBTyxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDL0QsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBQ0wsQ0FBQztJQUVMLENBQUM7SUFFRCw4REFBOEQ7SUFFdkQsbUJBQW1CO1FBQ3RCLElBQUksR0FBVyxDQUFDO1FBQ2hCLElBQUksR0FBRyxHQUFXLENBQUMsQ0FBQztRQUNwQixJQUFJLEdBQVcsQ0FBQztRQUVoQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDOUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFFbkMsRUFBRSxDQUFDLENBQUMsR0FBRyxLQUFLLFNBQVMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNaLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQztZQUVELEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDYixDQUFDO1FBRUQsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0sTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUF3QjtRQUN4QyxJQUFJLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBd0I7WUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELDZEQUE2RDtJQUU3RCxpQkFBaUI7SUFDakIsSUFBWSxjQUFjO1FBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxJQUFZLGNBQWMsQ0FBQyxLQUFhO1FBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxpQkFBaUI7UUFDcEIsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVNLGlCQUFpQixDQUFDLEtBQWE7UUFDbEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVELGlCQUFpQjtJQUNqQixJQUFZLGFBQWE7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELElBQVksYUFBYSxDQUFDLEtBQWE7UUFDbkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVNLGdCQUFnQjtRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRU0sZ0JBQWdCLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0NBRUo7O0FBL2NELDJCQStjQzs7O0FDbGVEO0lBRUksZ0JBQXVCLENBQUM7SUFFeEI7Ozs7Ozs7T0FPRztJQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBWSxFQUFFLEtBQWE7UUFDOUMsSUFBSSxFQUFVLENBQUM7UUFDZixJQUFJLFFBQVEsR0FBWSxLQUFLLENBQUM7UUFDOUIsTUFBTSxDQUFDO1lBQ0gsSUFBSSxFQUFVLENBQUM7WUFDZixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNYLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ1IsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNyQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxFQUFVLEVBQUUsRUFBVSxFQUFFLENBQVMsQ0FBQztnQkFDdEMsR0FBRyxDQUFDO29CQUNBLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQztvQkFDL0IsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO29CQUMvQixDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUMxQixDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsRUFBRTtnQkFDbkIsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDcEIsQ0FBQztZQUVELElBQUksTUFBTSxHQUFHLElBQUksR0FBRyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQy9CLGlCQUFpQjtZQUNqQixxQkFBcUI7WUFDckIsa0JBQWtCO1lBRWxCLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDbEIsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQUVELHNEQUFzRDtJQUUvQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQVk7UUFDbkMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Q0FDSjs7QUEvQ0QseUJBK0NDOzs7QUMvQ0QseUNBQWtDO0FBQ2xDLCtDQUF3QztBQUV4Qyx5Q0FBd0M7QUFFeEMsc0NBQStCO0FBQy9CLHNDQUErQjtBQVUvQjtJQWVJO1FBQ0ksSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLGtCQUFrQixDQUFDLFVBQVUsQ0FBQztRQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBRXhCLEVBQUUsQ0FBQSxDQUFDLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsWUFBWSxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNsRCxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ssZUFBZSxDQUFDLElBQXdCO1FBQzVDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBRXZCLGlCQUFpQjtRQUNqQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksa0JBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCxDQUFDO1FBRUQsdUJBQXVCO1FBQ3ZCLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FDZCxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFDN0MsZUFBZSxDQUFDLE1BQU0sRUFDdEIsZUFBZSxDQUFDLE9BQU8sRUFDdkIsZUFBZSxDQUFDLEtBQUssQ0FDeEIsQ0FBQztRQUNOLENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSyxpQkFBaUI7UUFDckIsSUFBSSxZQUFvQixDQUFDO1FBQ3pCLElBQUksYUFBcUIsQ0FBQztRQUUxQixFQUFFLENBQUEsQ0FBQyxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN0RSxZQUFZLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLGFBQWEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFFdkIsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLGtCQUFRLENBQUMsdUJBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELENBQUM7WUFFRCxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksa0JBQVEsQ0FBQyx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDeEQsQ0FBQztRQUVMLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSxZQUFZLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNuQyxhQUFhLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLHVCQUF1QixDQUFDO1FBQ2xDLENBQUM7UUFFRCxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ25DLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEUsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssS0FBSyxDQUFDLElBQXdCO1FBRWxDLHlDQUF5QztRQUN6QyxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLFFBQVEsR0FBYSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdDLElBQUksUUFBUSxHQUFhLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFN0MsRUFBRSxDQUFBLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNYLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxrQkFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDN0MsQ0FBQztRQUNMLENBQUM7UUFFRCwwQ0FBMEM7UUFDMUMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXRDLEVBQUUsQ0FBQSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsYUFBYSxDQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUN0QyxRQUFRLENBQUMsTUFBTSxFQUNmLFFBQVEsQ0FBQyxPQUFPLEVBQ2hCLFFBQVEsQ0FBQyxLQUFLLENBQ2pCLENBQUM7WUFDTixDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsb0JBQW9CLENBQUMsY0FBc0IsRUFBRSxlQUEyRCxFQUFFLDhCQUEyQztRQUMvSixJQUFJLFVBQVUsR0FBeUIsRUFBRSxDQUFDO1FBQzFDLElBQUksS0FBeUIsQ0FBQztRQUM5QixFQUFFLENBQUEsQ0FBQyxPQUFPLDhCQUE4QixLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDdkQsS0FBSyxHQUFHLElBQUksZUFBZSxDQUFDLEdBQUcsOEJBQThCLENBQUMsQ0FBQztRQUNuRSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixLQUFLLEdBQUcsSUFBSSxlQUFlLEVBQUUsQ0FBQztRQUNsQyxDQUFDO1FBRUQsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQWMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3JDLG9CQUFvQjtZQUNwQixJQUFJLElBQUksR0FBRyxJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUV0QyxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLGdDQUFnQztZQUM3RSxDQUFDO1lBRUQsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHdDQUF3QztRQUNuRSxDQUFDO1FBRUQsTUFBTSxDQUFDLFVBQVUsQ0FBRTtJQUN2QixDQUFDO0lBRU8sV0FBVyxDQUFDLFFBQWtCO1FBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQztRQUMxQyxFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLHVCQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBQ0QsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckMsQ0FBQztJQUNMLENBQUM7SUFFTyxjQUFjLENBQUMsV0FBd0I7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsV0FBVyxDQUFDO0lBQ3ZELENBQUM7SUFFTyxhQUFhLENBQUMsTUFBZSxFQUFFLE9BQWdCLEVBQUUsTUFBZ0IsRUFBRSxPQUFpQixFQUFFLEtBQWM7UUFDeEcsTUFBTSxHQUFHLE9BQU8sTUFBTSxJQUFJLFdBQVcsR0FBRyxNQUFNLEdBQUcsQ0FBQyxHQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxnQ0FBZ0M7UUFDdEcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLHFCQUFXLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUlNLEtBQUssQ0FBQyxPQUEyQjtRQUVwQyxJQUFJLE1BQTBCLENBQUM7UUFDL0IsSUFBSSxLQUF5QixDQUFDO1FBRTlCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDaEMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNkLEtBQUssR0FBRyxPQUFPLENBQUM7UUFDcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNqQixLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLENBQUM7UUFFRCxzQkFBc0I7UUFDdEIsSUFBSSxTQUFTLEdBQXVCLElBQVUsTUFBTSxDQUFDLFdBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLGNBQWM7UUFFekYsMkNBQTJDO1FBQzNDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksYUFBYSxHQUFnQixTQUFTLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdELElBQUksU0FBUyxHQUFnQixLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXJELEVBQUUsQ0FBQSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsSUFBSSxLQUFLLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFFaEMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLEdBQUcsYUFBYSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsMkJBQTJCO2dCQUNuRyxhQUFhLENBQUMsT0FBTyxHQUFHLEtBQUssR0FBRyxhQUFhLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUM7WUFDOUUsQ0FBQztRQUNMLENBQUM7UUFFRDs7OztXQUlHO1FBQ0gsaUVBQWlFO1FBQ2pFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDbEMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixDQUFDO1FBRUQsaUVBQWlFO1FBQ2pFLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLElBQUksYUFBYSxHQUFhLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkQsSUFBSSxTQUFTLEdBQWEsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUUvQyxFQUFFLENBQUEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNYLGFBQWEsQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxhQUFhLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7WUFDbEcsQ0FBQztRQUNMLENBQUM7UUFFRCxvQ0FBb0M7UUFDcEMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDcEMsSUFBSSxhQUFhLEdBQWdCLFNBQVMsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFN0QsYUFBYSxDQUFDLE9BQU8sR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsdUJBQXVCO1FBQ3hHLENBQUM7UUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxNQUFNO1FBRVQsSUFBSSxTQUFTLEdBQVksS0FBSyxDQUFDO1FBRS9CLElBQUksZ0JBQWdCLEdBQUcsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUM7UUFDakUsSUFBSSxlQUFlLEdBQUcsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7UUFFL0QsRUFBRSxDQUFBLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxTQUFpQixDQUFDLENBQUMsMERBQTBEO1lBQ2pGLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztnQkFDM0MsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxpR0FBaUc7WUFDcEksQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDbEIsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDbEQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUNyQixTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixDQUFDO1lBQ0wsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7b0JBQzNCLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3JCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztZQUV4QyxFQUFFLENBQUEsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3JCLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQztZQUVELEVBQUUsQ0FBQSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN4QixTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFFckIsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRU8sbUJBQW1CO1FBRXZCLElBQUksRUFBRSxHQUFhLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzVDLElBQUksRUFBWSxDQUFDO1FBRWpCLEdBQUcsQ0FBQztZQUNBLEVBQUUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUNsQyxDQUFDLFFBQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFO1FBRXhCLEVBQUUsQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFTyxhQUFhO1FBQ2pCLElBQUksSUFBaUIsQ0FBQztRQUV0QixHQUFHLENBQUM7WUFDQSxJQUFJLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUE7UUFDdEMsQ0FBQyxRQUFPLElBQUksQ0FBQyxPQUFPLElBQUksS0FBSyxFQUFFO1FBRS9CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDekIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUMzQixJQUFJLFNBQVMsR0FBRyxJQUFJLGtCQUFRLEVBQUUsQ0FBQztRQUUvQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTyxhQUFhO1FBQ2pCLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQy9CLEVBQUUsQ0FBQSxDQUFDLGdCQUFNLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RGLElBQUksVUFBVSxHQUFnQixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyRCxVQUFVLENBQUMsTUFBTSxJQUFJLGdCQUFNLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztZQUNwSCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTyxnQkFBZ0I7UUFDcEIsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsRUFBRSxDQUFBLENBQUMsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDekYsSUFBSSxJQUFJLEdBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLFNBQVMsSUFBSSxnQkFBTSxDQUFDLFlBQVksQ0FBQyxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDcEgsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sZUFBZSxDQUFDLE1BQWdCO1FBQ25DLElBQUksT0FBTyxHQUFhLEVBQUUsQ0FBQztRQUUzQixpREFBaUQ7UUFDakQsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFFRCxrREFBa0Q7UUFDbEQsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDOUIsT0FBTyxDQUFDLElBQUksQ0FDUixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUVELCtDQUErQztRQUMvQyxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RDLENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFTSxRQUFRO1FBRVgsSUFBSSxlQUFlLEdBQVcsRUFBRSxDQUFDO1FBQ2pDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzVCLGVBQWUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxLQUFLLENBQUM7UUFDckUsQ0FBQztRQUVELElBQUksa0JBQWtCLEdBQVcsRUFBRSxDQUFDO1FBQ3BDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQzNCLGtCQUFrQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxHQUFHLEtBQUssQ0FBQztRQUMvRSxDQUFDO1FBRUQsTUFBTSxDQUFDLDBCQUEwQjtZQUN6QixRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFLO1lBQzFCLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUs7WUFDcEMsZUFBZTtZQUNmLGtCQUFrQjtjQUNwQixHQUFHLENBQUM7SUFDZCxDQUFDO0lBRU8sTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFrQjtRQUM1QyxNQUFNLENBQUMsVUFBVSxRQUFRLENBQUMsS0FBSzs2QkFDVixRQUFRLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVPLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxlQUE0QjtRQUN6RCxNQUFNLENBQUMsVUFBVSxlQUFlLENBQUMsS0FBSzswQkFDcEIsZUFBZSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2tCQUN6QyxlQUFlLENBQUMsT0FBTyxHQUFHLEVBQUUsR0FBRyxVQUFVLEVBQUUsQ0FBQTtJQUN6RCxDQUFDO0lBRU0sTUFBTTtRQUNULElBQUksSUFBSSxHQUFTLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxJQUFJLENBQUM7UUFFL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMzQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLFFBQVEsR0FBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDZixJQUFJLEVBQUUsa0JBQWtCLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQzthQUNwRCxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMzQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLFdBQVcsR0FBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzNCLEVBQUUsRUFBRSxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzFCLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUM7YUFDMUQsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELDBFQUEwRTtRQUMxRSw2RkFBNkY7UUFFN0YsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsRUFBRTtJQUNGLGdCQUFnQjtJQUNoQixFQUFFO0lBRUY7Ozs7Ozs7T0FPRztJQUNLLGlCQUFpQjtRQUNyQixJQUFJLE1BQWdCLENBQUM7UUFDckIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsTUFBTTtJQUNOLHVEQUF1RDtJQUN2RCxvQkFBb0I7SUFDcEIsTUFBTTtJQUNOLG1DQUFtQztJQUNuQyx3RUFBd0U7SUFDeEUsc0VBQXNFO0lBQ3RFLElBQUk7SUFFSjs7O09BR0c7SUFDSyxvQkFBb0I7UUFDeEIsSUFBSSxNQUFtQixDQUFDO1FBQ3hCLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNwQyxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssWUFBWSxDQUFDLEVBQVksRUFBRSxFQUFZO1FBRTNDLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLFFBQVE7WUFDUixJQUFJLElBQUksR0FBZ0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFO21CQUNoRCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRU0sTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUF5QixFQUFFLE1BQTBCO1FBQ3ZFLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUM7SUFDMUMsQ0FBQztJQUVNLFVBQVUsQ0FBQyxJQUF3QjtRQUN0QyxJQUFJLEdBQUcsR0FBRyxnQkFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7UUFDdkMsSUFBSSxHQUFHLEdBQUcsZ0JBQU0sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO1FBQ3pDLElBQUksR0FBRyxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUN6QyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxtQkFBbUI7UUFDOUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsa0JBQWtCO1FBQzdCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLGdCQUFnQjtRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxpREFBaUQ7UUFFNUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsa0JBQWtCO1FBRTdCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBRTlDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNWLE9BQU0sQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNqRCxJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxRQUFRLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTVCLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUVqRCxDQUFDLEVBQUUsQ0FBQztnQkFDSixDQUFDLEVBQUUsQ0FBQztnQkFDSixDQUFDLEVBQUUsQ0FBQztZQUNSLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFFSixDQUFDLEVBQUUsQ0FBQztnQkFDSixFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxDQUFDLEVBQUUsQ0FBQztnQkFDUixDQUFDO2dCQUFDLElBQUksQ0FBRSxDQUFDO29CQUNMLENBQUMsRUFBRSxDQUFDO2dCQUNSLENBQUM7WUFDTCxDQUFDO1FBRUwsQ0FBQztRQUNELENBQUMsR0FBRyxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUztRQUM3RyxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQztRQUU5RSw0Q0FBNEM7UUFDNUMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRW5DLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3ZELENBQUMsRUFBRSxDQUFDO1lBQ1IsQ0FBQztRQUNMLENBQUM7UUFFRCxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVWLE1BQU0sQ0FBQyxDQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBQyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUdELEVBQUU7SUFDRixzQkFBc0I7SUFDdEIsRUFBRTtJQUVGLElBQVcsRUFBRTtRQUNULE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRUQsSUFBVyxTQUFTO1FBQ2hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxJQUFXLFlBQVk7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVNLHNCQUFzQjtRQUN6QixJQUFJLEdBQUcsR0FBdUIsRUFBRSxDQUFDO1FBRWpDLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3JDLENBQUM7UUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU0sTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxJQUFZLFVBQVU7UUFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQUVELElBQVksV0FBVztRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDOztBQXRqQmMsNkJBQVUsR0FBVyxDQUFDLENBQUM7QUFIMUMsZ0RBMGpCQzs7QUFFRCxrQkFBZSxrQkFBa0IsQ0FBQzs7O0FDeGtCbEM7SUFPSSwyQkFBMkI7SUFDM0IsNEJBQTRCO0lBQzVCLHlCQUF5QjtJQUV6QixZQUFtQixNQUFnQixFQUFTLE9BQWlCLEVBQVMsTUFBYyxFQUFFLE9BQWlCLEVBQUUsS0FBYztRQUFwRyxXQUFNLEdBQU4sTUFBTSxDQUFVO1FBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBVTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDaEYsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLEtBQUssSUFBSSxXQUFXLEdBQUcsS0FBSyxHQUFHLEVBQUUsV0FBVyxDQUFDLGFBQWEsQ0FBQztRQUNoRixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sT0FBTyxJQUFJLFdBQVcsR0FBRyxPQUFPLEdBQUUsSUFBSSxDQUFDO1FBQzdELElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFTSxRQUFRO1FBQ1gsTUFBTSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsYUFBYSxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO0lBQzVNLENBQUM7SUFFRCxFQUFFO0lBQ0Ysc0JBQXNCO0lBQ3RCLEVBQUU7SUFFRixJQUFXLEVBQUU7UUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNwQixDQUFDO0lBRUQsSUFBVyxLQUFLO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7QUE3QmMsc0JBQVUsR0FBVyxDQUFDLENBQUM7QUFFdkIseUJBQWEsR0FBVyxDQUFDLENBQUM7O0FBTDdDLDhCQWtDQzs7O0FDbENEO0lBbUJJO1FBQ0ksSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLFFBQVEsQ0FBQyxVQUFVLENBQUM7UUFFakMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELENBQUM7SUFDTCxDQUFDO0lBRU8sZUFBZSxDQUFDLElBQWM7UUFDbEMsa0RBQWtEO1FBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUV6QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUVsQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBRXhCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoRCxDQUFDO0lBRU8saUJBQWlCLENBQUMsSUFBbUIsRUFBRSxjQUE0QjtRQUN2RSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQztRQUN2QyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUV6QixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUVwQixJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLElBQUksS0FBSyxXQUFXLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7UUFFOUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLGNBQWMsS0FBSyxXQUFXLEdBQUcsY0FBYyxHQUFHLFFBQVEsQ0FBQztJQUM3RixDQUFDO0lBRU0sZUFBZSxDQUFDLFlBQXlCO1FBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFTSxjQUFjLENBQUMsS0FBYztRQUNoQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzNCLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7UUFFRCwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO1FBRWpDLElBQUksR0FBRyxHQUFXLENBQUMsQ0FBQztRQUVwQixpRUFBaUU7UUFDakUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzRCxHQUFHLElBQUksS0FBSyxDQUFDO1lBRWIsR0FBRyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUM1QixDQUFDLEdBQVcsRUFBRSxPQUFvQjtnQkFDOUIsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLHNDQUFzQztnQkFDbkcsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNmLENBQUM7WUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFVixHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixpREFBaUQ7WUFDakQsR0FBRyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUM1QixDQUFDLEdBQVcsRUFBRSxPQUFvQjtnQkFDOUIsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsWUFBWTtnQkFDL0UsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNmLENBQUM7WUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDVixHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO1FBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRU0sV0FBVztRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUM7SUFDcEMsQ0FBQztJQUVNLFFBQVE7UUFDWCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxlQUFlLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQy9JLENBQUM7SUFFRCxFQUFFO0lBQ0Ysc0JBQXNCO0lBQ3RCLEVBQUU7SUFFRixJQUFXLEVBQUU7UUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNwQixDQUFDO0lBRUQsSUFBVyxLQUFLO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELElBQVcsSUFBSTtRQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFXLGNBQWM7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDaEMsQ0FBQztJQUVELElBQWMsYUFBYTtRQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQsSUFBYyxLQUFLO1FBQ2YsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7QUE3SWMsbUJBQVUsR0FBVyxDQUFDLENBQUM7QUFFdkIsc0JBQWEsR0FBVyxDQUFDLENBQUM7O0FBTDdDLDJCQWlKQztBQUVELElBQVksWUFJWDtBQUpELFdBQVksWUFBWTtJQUNwQixpREFBSyxDQUFBO0lBQ0wsbURBQU0sQ0FBQTtJQUNOLG1EQUFNLENBQUE7QUFDVixDQUFDLEVBSlcsWUFBWSxHQUFaLG9CQUFZLEtBQVosb0JBQVksUUFJdkI7QUFFRCxJQUFZLGFBSVg7QUFKRCxXQUFZLGFBQWE7SUFDckIsK0NBQUcsQ0FBQTtJQUNILGlEQUFJLENBQUE7SUFDSixxREFBTSxDQUFBO0FBQ1YsQ0FBQyxFQUpXLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBSXhCO0FBRUQsa0RBQWtEO0FBQ2xELElBQVksZUFFWDtBQUZELFdBQVksZUFBZTtBQUUzQixDQUFDLEVBRlcsZUFBZSxHQUFmLHVCQUFlLEtBQWYsdUJBQWUsUUFFMUI7QUFHRCxrQkFBeUIsQ0FBUztJQUM5QixNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2xDLENBQUM7QUFGRCw0QkFFQztBQUVELG9CQUEyQixDQUFTO0lBQ2hDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDMUIsQ0FBQztBQUZELGdDQUVDOzs7QUM3S0Q7SUFNSSxZQUFZLFdBQXlCO1FBQ2pDLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxXQUFXLElBQUksV0FBVyxHQUFHLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDNUUsQ0FBQztJQUVNLEdBQUcsQ0FBQyxVQUE4QjtRQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSxLQUFLO1FBRVIsSUFBSSxjQUFjLEdBQXVCLElBQUksQ0FBQztRQUM5QyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUMvQixFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDbkMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQzlCLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMzQyxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUEsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxnQkFBZ0I7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksT0FBTyxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7SUFDL0csQ0FBQztJQUVNLE9BQU87UUFDVixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFRCxJQUFXLGNBQWM7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELElBQUksRUFBRTtRQUNGLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7O0FBckRnQixrQkFBVSxHQUFXLENBQUMsQ0FBQzs7QUFINUMsMEJBeURDIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImltcG9ydCBOZWF0SW5kaXZpZHVhbCBmcm9tIFwiLi9OZWF0SW5kaXZpZHVhbFwiO1xyXG5pbXBvcnQge0Jhc2VOZWF0SW5kaXZpZHVhbH0gZnJvbSBcIi4uL2xpYi9tb2RlbC9CYXNlTmVhdEluZGl2aWR1YWxcIjtcclxuaW1wb3J0IEJhbGFuY2luZ0dhbWVDb250cm9sbGVyIGZyb20gXCIuL0JhbGFuY2luZ0dhbWVDb250cm9sbGVyXCI7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFsYW5jaW5nQm90IHtcclxuXHJcbiAgICAvLyBwcml2YXRlIF9pbmRpdmlkdWFsOiBCYXNlSW5kaXZpZHVhbDtcclxuICAgIHByb3RlY3RlZCBfZ2FtZTogQmFsYW5jaW5nR2FtZUNvbnRyb2xsZXI7XHJcbiAgICBwcm90ZWN0ZWQgX3RpbWVyOiBudW1iZXI7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgX2luZGl2aWR1YWw6IE5lYXRJbmRpdmlkdWFsKSB7XHJcbiAgICAgICAgdGhpcy5fZ2FtZSA9IEJhbGFuY2luZ0dhbWVDb250cm9sbGVyLmdldEluc3RhbmNlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXJ0ID0gZnVuY3Rpb24oKSA6IFByb21pc2U8bnVtYmVyPiB7XHJcbiAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwodGhpcy50aW1lcik7XHJcblxyXG4gICAgICAgIHRoaXMuZ2FtZS5nYW1lUmVzZXQoKTtcclxuXHJcbiAgICAgICAgbGV0IGZpdG5lc3NQcm9taXNlID0gbmV3IFByb21pc2UoXHJcbiAgICAgICAgICAgIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGltZXIgPSB3aW5kb3cuc2V0SW50ZXJ2YWwodGhpcy51cGRhdGUuYmluZCh0aGlzLCByZXNvbHZlKSwgdGhpcy5nYW1lLmdldFRzdGVwKCkpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5nYW1lLmdhbWVTdGFydCgpO1xyXG5cclxuICAgICAgICByZXR1cm4gZml0bmVzc1Byb21pc2U7XHJcbiAgICB9O1xyXG5cclxuICAgIHByaXZhdGUgdXBkYXRlID0gZnVuY3Rpb24ocmVzb2x2ZTogYW55KSB7XHJcbiAgICAgICAgaWYodGhpcy5nYW1lLmlzR2FtZW92ZXIoKSB8fCB0aGlzLmdhbWUuaXNHYW1ld29uKCkpIHtcclxuICAgICAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwodGhpcy50aW1lcik7XHJcbiAgICAgICAgICAgIHJlc29sdmUodGhpcy5nYW1lLmdldFRpbWUoKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ2FtZS5zZXRPdXRwdXRzKHRoaXMuZXZhbHVhdGUoKSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBldmFsdWF0ZSgpOiBudW1iZXJbXTtcclxuXHJcbiAgICAvL1xyXG4gICAgLy8gR0VUVEVSU1QgYW5kIFNFVFRFUlNcclxuICAgIC8vXHJcblxyXG4gICAgcHJvdGVjdGVkIGdldCBnYW1lKCk6IEJhbGFuY2luZ0dhbWVDb250cm9sbGVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZ2FtZTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgZ2V0IHRpbWVyKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RpbWVyO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBzZXQgdGltZXIodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3RpbWVyID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldCBpbmRpdmlkdWFsKCk6IE5lYXRJbmRpdmlkdWFsIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faW5kaXZpZHVhbDtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbi8qKlxyXG4gKiBAY2xhc3NcclxuICogQGRlc2NyaXB0aW9uXHJcbiAqIHVzZWQgd2l0aCBzaWdtb2lkIE5vZGVHZW5lcyAob3V0cHV0IGlzICgwOzEpKVxyXG4gKiA0IGlucHV0cyAtIHN0YW5kYXJkXHJcbiAqIDEgb3V0cHV0IC0gZm9yY2UgYW5kIGRpcmVjdGlvbiAodmFsdWUgYW5kIHNpZ24pXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgTmVhdFNpbmdsZU91dHB1dEJhbGFuY2luZ0JvdCBleHRlbmRzIEJhbGFuY2luZ0JvdCB7XHJcbiAgICBwcm90ZWN0ZWQgZXZhbHVhdGUoKTogbnVtYmVyW10ge1xyXG5cclxuICAgICAgICBsZXQgbmVhdEluZGl2OiBCYXNlTmVhdEluZGl2aWR1YWwgPSA8QmFzZU5lYXRJbmRpdmlkdWFsPnRoaXMuaW5kaXZpZHVhbDtcclxuICAgICAgICBsZXQgb3V0cHV0cyA9IG5lYXRJbmRpdi5ldmFsdWF0ZU5ldHdvcmsodGhpcy5nYW1lLmdldElucHV0cygpKTtcclxuXHJcbiAgICAgICAgLy8gdHJhbnNsYXRlIG5lYXQgb3V0cHV0IHRvIGdhbWUgb3V0cHV0IC8vIDAgLT4gc2FtZTsgLTEgLT4gbGVmdDsgKzEgLT4gcmlnaHRcclxuICAgICAgICByZXR1cm4gW291dHB1dHNbMF0gLSAwLjVdO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogQGNsYXNzXHJcbiAqIEBkZXNjcmlwdGlvblxyXG4gKiB1c2VkIHdpdGggYmluYXJ5U3RlcCBOb2RlR2VuZXMgKG91dHB1dCBpcyAwLzEpXHJcbiAqIDQgaW5wdXRzIC0gc3RhbmRhcmRcclxuICogMiBvdXRwdXRzIC0gd2hpY2ggYnV0dG9uIHRvIHByZXNzXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgTmVhdFR3b091dHB1dHNCYWxhbmNpbmdCb3QgZXh0ZW5kcyBCYWxhbmNpbmdCb3Qge1xyXG4gICAgcHJvdGVjdGVkIGV2YWx1YXRlKCk6IG51bWJlcltdIHtcclxuXHJcbiAgICAgICAgbGV0IG5lYXRJbmRpdjogQmFzZU5lYXRJbmRpdmlkdWFsID0gPEJhc2VOZWF0SW5kaXZpZHVhbD50aGlzLmluZGl2aWR1YWw7XHJcbiAgICAgICAgbGV0IG91dHB1dHMgPSBuZWF0SW5kaXYuZXZhbHVhdGVOZXR3b3JrKHRoaXMuZ2FtZS5nZXRJbnB1dHMoKSk7XHJcblxyXG4gICAgICAgIC8vIHRyYW5zbGF0ZSBuZWF0IG91dHB1dCB0byBnYW1lIG91dHB1dCAvLyAwIC0+IHNhbWU7IC0xIC0+IGxlZnQ7ICsxIC0+IHJpZ2h0XHJcbiAgICAgICAgaWYob3V0cHV0c1swXSA9PSAxICYmIG91dHB1dHNbMV0gPT0gMSAvLyBib3RoIGJ1dHRvbnMgZG93biAtIGluY29ycmVjdCBvdXRwdXRcclxuICAgICAgICAgICAgfHwgb3V0cHV0c1swXSA9PSAwICYmIG91dHB1dHNbMV0gPT0gMCkgeyAvLyBib3RoIGJ1dHRvbnMgdXAgLSBpbmNvcnJlY3Qgb3V0cHV0XHJcbiAgICAgICAgICAgIHJldHVybiBbMF07IC8vIGtlZXAgc2FtZVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZihvdXRwdXRzWzBdID09IDEpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFstMV07IC8vIGxlZnRcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYob3V0cHV0c1sxXSA9PSAxKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbMV07IC8vIHJpZ2h0XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aHJvdyBcIlVucmVnb25pemVkIG9wdGlvbnNcIjtcclxuICAgIH1cclxufSIsIi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuLi8uLi9ub2RlX21vZHVsZXMvQHR5cGVzL2pxdWVyeS9pbmRleC5kLnRzXCIgLz5cclxuLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4uLy4uL25vZGVfbW9kdWxlcy9AdHlwZXMvYW5ndWxhci9pbmRleC5kLnRzXCIgLz5cclxuZGVjbGFyZSB2YXIgYW5ndWxhcjogYW55OyAvLyBub3QgcmVhbGx5IGltcG9ydGluZywganVzdCBuZWVkIC5kLnRzIGhlcmVcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJhbGFuY2luZ0dhbWVDb250cm9sbGVyIHtcclxuXHJcbiAgICBwcm90ZWN0ZWQgc3RhdGljIF9pbnN0YW5jZTogQmFsYW5jaW5nR2FtZUNvbnRyb2xsZXI7XHJcbiAgICBwcm90ZWN0ZWQgX2FuZ3VsYXJDdHJsOiBhbnk7XHJcblxyXG4gICAgcHJvdGVjdGVkIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuX2FuZ3VsYXJDdHJsID0gYW5ndWxhci5lbGVtZW50KCQoJyNnYW1lJykpLmNvbnRyb2xsZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldEluc3RhbmNlKCkge1xyXG4gICAgICAgIGlmKCFCYWxhbmNpbmdHYW1lQ29udHJvbGxlci5faW5zdGFuY2UpIHtcclxuICAgICAgICAgICAgQmFsYW5jaW5nR2FtZUNvbnRyb2xsZXIuX2luc3RhbmNlID0gbmV3IEJhbGFuY2luZ0dhbWVDb250cm9sbGVyKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBCYWxhbmNpbmdHYW1lQ29udHJvbGxlci5faW5zdGFuY2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldElucHV0cygpOiBBcnJheTxudW1iZXI+IHtcclxuICAgICAgICByZXR1cm4gW3RoaXMuYW5ndWxhckN0cmwuZ2V0Q2FydFBvcygpLCB0aGlzLmFuZ3VsYXJDdHJsLmdldENhcnRWZWwoKSwgdGhpcy5hbmd1bGFyQ3RybC5nZXRQb2xlQW5nbGUoKSwgdGhpcy5hbmd1bGFyQ3RybC5nZXRQb2xlVmVsKCldO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlc2NyaXB0aW9uIFRyYW5zbGF0ZXMgb3V0cHV0cyB0byBhY3Rpb25zLlxyXG4gICAgICogQHBhcmFtIHZhbHVlc1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc2V0T3V0cHV0cyh2YWx1ZXM6IEFycmF5PG51bWJlcj4pIHtcclxuICAgICAgICBpZih2YWx1ZXNbMF0gPCAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYW5ndWxhckN0cmwucHVzaExlZnQoKTtcclxuICAgICAgICB9IGVsc2UgaWYodmFsdWVzWzBdID09IDApIHtcclxuICAgICAgICAgICAgdGhpcy5hbmd1bGFyQ3RybC5wdXNoU2FtZSgpO1xyXG4gICAgICAgIH0gZWxzZSBpZih2YWx1ZXNbMF0gPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYW5ndWxhckN0cmwucHVzaFJpZ2h0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUaW1lKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYW5ndWxhckN0cmwuY29uZmlnLnRpbWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFRzdGVwKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYW5ndWxhckN0cmwuY29uZmlnLnRzdGVwO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBpc0dhbWVvdmVyKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAhdGhpcy5hbmd1bGFyQ3RybC5mdWxmaWxsQ29uc3RyYWludHMoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaXNHYW1ld29uKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFuZ3VsYXJDdHJsLmZ1bGZpbGxHYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdhbWVTdGFydCgpIHtcclxuICAgICAgICB0aGlzLmFuZ3VsYXJDdHJsLnN0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdhbWVSZXNldCgpIHtcclxuICAgICAgICB0aGlzLmFuZ3VsYXJDdHJsLnJlc2V0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy9cclxuICAgIC8vIEdFVFRFUlMgYW5kIFNFVFRFUlNcclxuICAgIC8vXHJcblxyXG4gICAgcHJvdGVjdGVkIGdldCBhbmd1bGFyQ3RybCgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hbmd1bGFyQ3RybDtcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQge0Jhc2VOZWF0SW5kaXZpZHVhbH0gZnJvbSBcIi4uL2xpYi9tb2RlbC9CYXNlTmVhdEluZGl2aWR1YWxcIjtcclxuaW1wb3J0IHtOZWF0U2luZ2xlT3V0cHV0QmFsYW5jaW5nQm90fSBmcm9tIFwiLi9CYWxhbmNpbmdCb3RcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE5lYXRJbmRpdmlkdWFsIGV4dGVuZHMgQmFzZU5lYXRJbmRpdmlkdWFsIHtcclxuXHJcbiAgICBwdWJsaWMgZXZhbHVhdGVGaXRuZXNzKCk6IFByb21pc2U8bnVtYmVyPiB7XHJcblxyXG4gICAgICAgIGxldCBib3QgPSBuZXcgTmVhdFNpbmdsZU91dHB1dEJhbGFuY2luZ0JvdCh0aGlzKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGJvdC5zdGFydCgpXHJcbiAgICAgICAgICAgIC50aGVuKChyZXM6IG51bWJlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZml0bmVzcyA9IHJlcztcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXM7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi4vLi4vbm9kZV9tb2R1bGVzL0B0eXBlcy9hbmd1bGFyL2luZGV4LmQudHNcIiAvPlxyXG5kZWNsYXJlIHZhciBhbmd1bGFyOiBhbnk7IC8vIG5vdCByZWFsbHkgaW1wb3J0aW5nLCBqdXN0IG5lZWQgLmQudHMgaGVyZVxyXG5cclxuLy8gVE9ETyBkbyBub3QgaW1wb3J0IC50cyByYXRoZXIgaW1wb3J0IC5kLnRzXHJcbmltcG9ydCBFdm9DeWNsZSBmcm9tIFwiLi4vbGliL0V2b0N5Y2xlXCI7XHJcbmltcG9ydCBCYXNlTmVhdEluZGl2aWR1YWwgZnJvbSBcIi4uL2xpYi9tb2RlbC9CYXNlTmVhdEluZGl2aWR1YWxcIjtcclxuaW1wb3J0IE5lYXRJbmRpdmlkdWFsIGZyb20gXCIuL05lYXRJbmRpdmlkdWFsXCI7XHJcblxyXG4vLyBmaXJzdCBsb2FkIGpxIGlmIG5vdCBsb2FkZWRcclxud2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBmdW5jdGlvbigpIHtcclxuICAgIGlmICgoPGFueT53aW5kb3cpLmpRdWVyeSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiV0FSTklORzogalF1ZXJ5IGFscmVhZHkgbG9hZGVkLlwiKTtcclxuICAgICAgICBUc05lYXRFdm9DeWNsZUNvbnRyb2xsZXJMb2FkZXIuaW5pdCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIldBUk5JTkc6IExvYWRpbmcgalF1ZXJ5IGZpcnN0LlwiKTtcclxuXHJcbiAgICAgICAgdmFyIHVybCA9IFwiLi4vanF1ZXJ5Lm1pbi5qc1wiOyAvLyBUT0RPIHVybFxyXG4gICAgICAgIHZhciB4bWxodHRwID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICAgICAgeG1saHR0cC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBpZiAoeG1saHR0cC5yZWFkeVN0YXRlID09IFhNTEh0dHBSZXF1ZXN0LkRPTkUgJiYgeG1saHR0cC5zdGF0dXMgPT0gMjAwKXtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQganFzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic2NyaXB0XCIpO1xyXG4gICAgICAgICAgICAgICAganFzY3JpcHQudHlwZSA9IFwidGV4dC9qYXZhc2NyaXB0XCI7XHJcbiAgICAgICAgICAgICAgICBqcXNjcmlwdC5zcmMgPSB1cmw7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKGpxc2NyaXB0KTtcclxuXHJcbiAgICAgICAgICAgICAgICBUc05lYXRFdm9DeWNsZUNvbnRyb2xsZXJMb2FkZXIuaW5pdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB4bWxodHRwLm9wZW4oXCJHRVRcIiwgdXJsLCB0cnVlKTtcclxuICAgICAgICB4bWxodHRwLnNlbmQoKTtcclxuICAgIH1cclxuXHJcbn0pO1xyXG5cclxuZXhwb3J0IGNsYXNzIFRzTmVhdEV2b0N5Y2xlQ29udHJvbGxlckxvYWRlciB7XHJcbiAgICBwcml2YXRlIGNvbnN0cnVjdG9yKCkge31cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgVHNOZWF0RXZvQ3ljbGVDb250cm9sbGVyTG9hZGVyLmxvYWRBbGwoKS50aGVuKGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgICAgIC8vIGNyZWF0ZSBpbml0IHBvcHVsYXRpb24gYW5kIEV2b0N5Y2xlXHJcbiAgICAgICAgICAgIGxldCBpbml0UG9wdWxhdGlvbiA9IEJhc2VOZWF0SW5kaXZpZHVhbC5jcmVhdGVJbml0UG9wdWxhdGlvbig1MCwgTmVhdEluZGl2aWR1YWwsIFs0LCAxXSk7XHJcbiAgICAgICAgICAgIGxldCBldm9jeWNsZSA9IG5ldyBFdm9DeWNsZShpbml0UG9wdWxhdGlvbik7XHJcblxyXG4gICAgICAgICAgICAvLyBpbml0IGNvbnRyb2xsZXIgd2l0aCBtb2RlbFxyXG4gICAgICAgICAgICBsZXQgZXZvQ3ljbGVDb250cm9sbGVyID0gYW5ndWxhci5lbGVtZW50KCQoJyN0c25lYXRldm9jeWNsZS1jb250cm9sbGVyJykpLmNvbnRyb2xsZXIoKTtcclxuICAgICAgICAgICAgZXZvQ3ljbGVDb250cm9sbGVyLnNldE1vZGVsKGV2b2N5Y2xlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHN0YXRpYyBsb2FkQWxsKCk6IFByb21pc2U8YW55PiB7XHJcbiAgICAgICAgLy8gVE9ETyBpbnNlcnQgZnJvbSBodHRwOi8vbmVhdC5tZWxlemluZWsuY3ovZGlzdC8qIChvciBnaXRodWIpIGZvciBvbmxpbmUgcHVycG9zZXNcclxuICAgICAgICAvLyB2YXIgbGliQmFzZSA9IFwiaHR0cDovL25lYXQubWVsZXppbmVrLmN6L2Rpc3QvbGliL1wiO1xyXG4gICAgICAgIGxldCBsaWJCYXNlID0gXCIuLi9saWIvXCI7XHJcblxyXG4gICAgICAgIGxldCBzY3JpcHQyID0gJC5nZXRTY3JpcHQobGliQmFzZSArIFwiY29udHJvbGxlci5qc1wiKTsgLy8gbG9hZGVkIGJ5IGFqYXggdGhlbiBleGVjdXRlZFxyXG4gICAgICAgIGxldCBjc3MyID0gJC5hamF4KHsgLy8gbG9hZCBpdCBmaXJzdCBieSBhamF4IHRoZW4gYXBwZW5kXHJcbiAgICAgICAgICAgIHVybDogbGliQmFzZSArIFwiY29udHJvbGxlci5jc3NcIixcclxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICQoJzxsaW5rPicpLmF0dHIoe1xyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY6IGxpYkJhc2UgKyBcImNvbnRyb2xsZXIuY3NzXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQvY3NzJyxcclxuICAgICAgICAgICAgICAgICAgICByZWw6ICdzdHlsZXNoZWV0J1xyXG4gICAgICAgICAgICAgICAgfSkuYXBwZW5kVG8oJ2hlYWQnKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGF0YVR5cGU6ICdodG1sJ1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgc2NyaXB0MSA9ICQuZ2V0U2NyaXB0KGxpYkJhc2UgKyBcImNvbnRyb2xsZXJfdmVuZG9ycy5taW4uanNcIik7IC8vIGxvYWRlZCBieSBhamF4IHRoZW4gZXhlY3V0ZWRcclxuICAgICAgICBsZXQgY3NzMSA9ICQuYWpheCh7IC8vIGxvYWQgaXQgZmlyc3QgYnkgYWpheCB0aGVuIGFwcGVuZFxyXG4gICAgICAgICAgICB1cmw6IGxpYkJhc2UgKyBcImNvbnRyb2xsZXJfdmVuZG9ycy5jc3NcIixcclxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICQoJzxsaW5rPicpLmF0dHIoe1xyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY6IGxpYkJhc2UgKyBcImNvbnRyb2xsZXJfdmVuZG9ycy5jc3NcIixcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dC9jc3MnLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbDogJ3N0eWxlc2hlZXQnXHJcbiAgICAgICAgICAgICAgICB9KS5hcHBlbmRUbygnaGVhZCcpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkYXRhVHlwZTogJ2h0bWwnXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBQcm9taXNlLmFsbChbc2NyaXB0MSwgY3NzMSwgc2NyaXB0MiwgY3NzMl0pLnRoZW4oKHZhbHVlcyk9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiAkLmFqYXgoeyAvLyBsYXN0bHkgbG9hZCBodG1sIGFsc28gYnkgYWpheFxyXG4gICAgICAgICAgICAgICAgdXJsOiBsaWJCYXNlICsgXCJjb250cm9sbGVyLmh0bWxcIixcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnYm9keScpLmFwcGVuZChkYXRhKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ2h0bWwnXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59IiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29uZmlnIHtcclxuICAgIHB1YmxpYyBzdGF0aWMgbG9nID0ge1xyXG4gICAgICAgIGZ1bmN0aW9uczogZmFsc2UsXHJcbiAgICAgICAgY3ljbGU6IGZhbHNlLFxyXG4gICAgICAgIGdlbmVyYXRpb25zOiBmYWxzZSxcclxuICAgICAgICBuZXh0Rml0bmVzczogZmFsc2VcclxuICAgIH07XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBjeWNsZSA9IHtcclxuICAgICAgICBjb250aW51ZTogZmFsc2UsXHJcbiAgICAgICAgcnVubmluZzogZmFsc2VcclxuICAgIH07XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZW5lcmFsID0ge1xyXG4gICAgICAgIGFsd2F5c0V2YWx1YXRlRml0bmVzczogZmFsc2VcclxuICAgIH07XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBtdXRhdGlvbk9wdGlvbnMgPSB7XHJcbiAgICAgICAgbXV0YXRlT2Zmc3ByaW5nczogdHJ1ZSxcclxuICAgICAgICBtdXRhdGVCeUNsb25pbmc6IHRydWUsXHJcbiAgICAgICAgaW5kaXZpZHVhbFRvcG9sb2d5OiB7XHJcbiAgICAgICAgICAgIGNoYW5jZTogMC4yNSxcclxuICAgICAgICAgICAgYWRkTm9kZToge1xyXG4gICAgICAgICAgICAgICAgY2hhbmNlOiAwLjAzXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGFkZENvbm5lY3Rpb246IHtcclxuICAgICAgICAgICAgICAgIGNoYW5jZTogMC4wNVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBhZGROb2RlWE9SYWRkQ29ubmVjdGlvbjogdHJ1ZSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGluZGl2aWR1YWxXZWlnaHRzOiB7XHJcbiAgICAgICAgICAgIGNoYW5jZTogMC44LFxyXG4gICAgICAgICAgICB3ZWlnaHRzOiB7XHJcbiAgICAgICAgICAgICAgICBjaGFuY2U6IDEsXHJcbiAgICAgICAgICAgICAgICBtdXRhdGVTaW5nbGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBjaGFuY2U6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgc3RkZXY6IDFcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGhyZXNob2xkczoge1xyXG4gICAgICAgICAgICAgICAgY2hhbmNlOiAxLFxyXG4gICAgICAgICAgICAgICAgbXV0YXRlU2luZ2xlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2hhbmNlOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0ZGV2OiAxXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgY3Jvc3NvdmVyT3B0aW9ucyA9IHtcclxuICAgICAgICBvZmZzcHJpbmdSYXRpbzogMC44LFxyXG4gICAgICAgIHRvdXJuYW1lbnRSYXRpbzogMC41XHJcbiAgICB9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogZGlzdGFuY2UgZnVuY3Rpb24gY29lZmZpY2llbnRcclxuICAgICAqIGQgPSAoY19lKkUpL04gKyAoY19kKkQpL04gKyBjX20qVztcclxuICAgICAqIDMuMCA9IDEuMCAuLi4uLi4gMS4wIC4uLi4uLiAwLjQgLSB2YWx1ZXMgZnJvbSBORUFUIHBhcGVyIGNhcHRlciA0LjFcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBzcGVjaWF0aW9uID0ge1xyXG4gICAgICAgIGV4Y2Vzc0NvZWY6IDEsXHJcbiAgICAgICAgZGlzam9pbnRDb2VmOiAgMSxcclxuICAgICAgICBtYXRjaGluZ0NvZWY6IDAuNCxcclxuICAgICAgICBkaXN0YW5jZVRocmVzaG9sZDogMy4wXHJcbiAgICB9O1xyXG5cclxufVxyXG4iLCJpbXBvcnQgQ29uZmlnIGZyb20gXCIuL0NvbmZpZ1wiO1xyXG5pbXBvcnQge0Jhc2VOZWF0SW5kaXZpZHVhbCwgSW5kaXZpZHVhbHN9IGZyb20gXCIuL21vZGVsL0Jhc2VOZWF0SW5kaXZpZHVhbFwiO1xyXG5pbXBvcnQgU3BlY2llcyBmcm9tIFwiLi9tb2RlbC9TcGVjaWVzXCI7XHJcblxyXG4vKipcclxuICogQGludGVyZmFjZVxyXG4gKiBAZGVzY3JpcHRpb24gRXZvQ3ljbGUgaXMgb2JzZXJ2YWJsZSBmcm9tIG91dHNpZGUuIFRoaXMgaXMgaXRzIG9ic2VydmVyIGludGVyZmFjZS5cclxuICogRG9uJ3QgZm9yZ2V0IHRoYXQgZXZhbHVhdGluZyBmaXRuZXNzIHJldHVybnMgUHJvbWlzZSAtIGFzeW5jaHJvbm91cy5cclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgRXZvQ3ljbGVPYnNlcnZlciB7XHJcbiAgICBub3RpZnlEb25lUmVkdWN0aW9uKGV2b2N5Y2xlOiBFdm9DeWNsZSk6IHZvaWRcclxuICAgIG5vdGlmeURvbmVTcGVjaWF0aW9uKGV2b2N5Y2xlOiBFdm9DeWNsZSwgc3BlY2llczogU3BlY2llc1tdKTogdm9pZFxyXG4gICAgbm90aWZ5RG9uZUV2YWx1YXRlTmV4dEZpdG5lc3MoZXZvY3ljbGU6IEV2b0N5Y2xlLCBpbmRpdmlkdWFsOiBCYXNlTmVhdEluZGl2aWR1YWwpOiB2b2lkXHJcbn1cclxuXHJcbi8qKlxyXG4gKiBAY2xhc3NcclxuICogQGRlc2NyaXB0aW9uIE1haW4gbGlicmFyeSBjbGFzc1xyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXZvQ3ljbGUge1xyXG5cclxuICAgIHB1YmxpYyBjb25maWc6IENvbmZpZyB8IGFueTtcclxuICAgIHByaXZhdGUgX3RvdXJuYW1lbnRTaXplOiBudW1iZXI7IC8vIGdldFNlbGVjdGlvbk9wdGlvbnNcclxuICAgIHByaXZhdGUgX29mZnNwcmluZ1NpemU6IG51bWJlcjtcclxuXHJcbiAgICBwdWJsaWMgcG9wdWxhdGlvbjogSW5kaXZpZHVhbHM7XHJcbiAgICBwdWJsaWMgZ2VuZXJhdGlvbkNvdW50ZXI6IG51bWJlcjtcclxuXHJcbiAgICBwcml2YXRlIHNwZWNpZXM6IFNwZWNpZXNbXTtcclxuICAgIHByaXZhdGUgcGFyZW50czogSW5kaXZpZHVhbHNbXTtcclxuICAgIHByaXZhdGUgb2Zmc3ByaW5nczogSW5kaXZpZHVhbHNbXTtcclxuICAgIHByaXZhdGUgbXV0YW50czogSW5kaXZpZHVhbHM7XHJcblxyXG4gICAgcHJpdmF0ZSBfb2JzZXJ2ZXJzOiBFdm9DeWNsZU9ic2VydmVyW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoaW5kaXZpZHVhbHM6IEluZGl2aWR1YWxzKSB7XHJcbiAgICAgICAgaWYgKENvbmZpZy5sb2cuZnVuY3Rpb25zKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZvQ3ljbGUuY29uc3RydWN0b3JcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmNvbmZpZyA9IENvbmZpZztcclxuICAgICAgICB0aGlzLmdlbmVyYXRpb25Db3VudGVyID0gMDtcclxuICAgICAgICB0aGlzLl9vYnNlcnZlcnMgPSBbXTtcclxuXHJcbiAgICAgICAgdGhpcy5wb3B1bGF0aW9uID0gaW5kaXZpZHVhbHM7XHJcbiAgICAgICAgdGhpcy5zcGVjaWVzID0gW25ldyBTcGVjaWVzKGluZGl2aWR1YWxzKV07XHJcbiAgICAgICAgdGhpcy5vZmZzcHJpbmdzID0gW107XHJcbiAgICAgICAgdGhpcy5tdXRhbnRzID0gW107XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFkZE9ic2VydmVyKG9ic2VydmVyOiBFdm9DeWNsZU9ic2VydmVyKSB7XHJcbiAgICAgICAgdGhpcy5fb2JzZXJ2ZXJzLnB1c2gob2JzZXJ2ZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vIEVWT0xVVElPTiBGVU5DVElPTlMgLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuXHJcbiAgICBwdWJsaWMgY29udGludWUoc2luZ2xlR2VuZXJhdGlvbjogYm9vbGVhbiA9IGZhbHNlKSB7XHJcbiAgICAgICAgaWYgKENvbmZpZy5sb2cuZnVuY3Rpb25zKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZvQ3ljbGUuY29udGludWUgLSBcIiArIFwicnVubmluZzogXCIgKyBDb25maWcuY3ljbGUucnVubmluZyArIFwiOyBjb250aW51ZTogXCIgKyBDb25maWcuY3ljbGUuY29udGludWUgKyBcIjsgc2luZ2xlR2VuZXJhdGlvbjogXCIgKyBzaW5nbGVHZW5lcmF0aW9uKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5wb3B1bGF0aW9uKSB7XHJcbiAgICAgICAgICAgIHRocm93IFwiRXZvQ3ljbGUgaGFzIHRvIGJlIGluaWNpYWxpemVkIGZpcnN0IHdpdGggaW5pdCBwb3B1bGF0aW9uISBTZWUgRXZvQ3ljbGUuaW5pdChpbmRpdmlkdWFsczogSW5kaXZpZHVhbHMpXCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoQ29uZmlnLmN5Y2xlLnJ1bm5pbmcpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiRXZvQ3ljbGUuY29udGludWUgaXMgYWxyZWFkeSBydW5uaW5nXCIpO1xyXG4gICAgICAgICAgICByZXR1cm47IC8vIHByZXZlbnQgbXVsdGlwbGUgY2FsbFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFzaW5nbGVHZW5lcmF0aW9uICYmICFDb25maWcuY3ljbGUuY29udGludWUpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiRXZvQ3ljbGUuY29udGludWUgY2Fubm90IGNvbnRpbnVlIEV2b0N5Y2xlLmNvbmZpZy5jeWNsZS5jb250aW51ZSBmbGFnIGlzIGZhbHNlXCIpO1xyXG4gICAgICAgICAgICByZXR1cm47IC8vIGRvIG5vdCBjb250aW51ZVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgQ29uZmlnLmN5Y2xlLnJ1bm5pbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICB0aGlzLmRvUmVkdWN0aW9uKCkudGhlbigocG9wdWxhdGlvbikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoQ29uZmlnLmxvZy5mdW5jdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZvQ3ljbGUuY29udGludWUgZG9SZWR1Y3Rpb24tdGhlblwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5wb3B1bGF0aW9uID0gcG9wdWxhdGlvbjtcclxuICAgICAgICAgICAgdGhpcy5nZW5lcmF0aW9uQ291bnRlcisrO1xyXG5cclxuICAgICAgICAgICAgaWYgKENvbmZpZy5sb2cuZ2VuZXJhdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiR0VORVJBVElPTlwiKTtcclxuICAgICAgICAgICAgICAgIEV2b0N5Y2xlLnByaW50KHRoaXMucG9wdWxhdGlvbik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuX29ic2VydmVycy5mb3JFYWNoKChvYnNlcnZlcjogRXZvQ3ljbGVPYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubm90aWZ5RG9uZVJlZHVjdGlvbih0aGlzKVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc3RlcCgpOyAvLyBjb250aW51ZSB3aXRoIGN5Y2xlXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gc3RlcCB3aG9sZSBuZXcgZ2VuZXJhdGlvblxyXG4gICAgcHVibGljIHN0ZXAoKSB7XHJcblxyXG4gICAgICAgIGlmIChDb25maWcubG9nLmN5Y2xlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicG9wdWxhdGlvbiAtIE5FWFQgU1RFUDpcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKHRoaXMucG9wdWxhdGlvbik7XHJcbiAgICAgICAgICAgIEV2b0N5Y2xlLnByaW50KHRoaXMucG9wdWxhdGlvbik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjbGFzc2lmeSBpbmRpdmlkdWFsc1xyXG4gICAgICAgIHRoaXMuZG9TcGVjaWF0aW9uKCk7XHJcbiAgICAgICAgaWYgKENvbmZpZy5sb2cuY3ljbGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJzcGVjaWVzIC0gZG9TcGVjaWF0aW9uOlwiKTtcclxuICAgICAgICAgICAgY29uc29sZS5kaXIodGhpcy5zcGVjaWVzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHNlbGVjdCBpbmRpdmlkdWFscyB0byBiZSBwYXJlbnRzIGZvciBicmVlZGluZ1xyXG4gICAgICAgIHRoaXMuZG9TZWxlY3Rpb24oKTtcclxuICAgICAgICBpZiAoQ29uZmlnLmxvZy5jeWNsZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInBhcmVudHMgLSBkb1NlbGVjdGlvbjpcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKHRoaXMucGFyZW50cyk7IC8vIGlmIHRoaXMgY2hhbmdlZCBsYXRlciAobXV0YXRpb24pLCBuZXcgdmFsdWUgbWF5IGJlIGRpc3BsYXllZCBpbiBjb25zb2xlXHJcbiAgICAgICAgICAgIEV2b0N5Y2xlLnByaW50KHRoaXMucGFyZW50cy5yZWR1Y2UoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhLmNvbmNhdChbXSkuY29uY2F0KGIpO1xyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBnZW5lcmF0ZSBuZXcgaW5kaXZpZHVhbHMgYnkgY3Jvc3NvdmVyXHJcbiAgICAgICAgdGhpcy5kb0Nyb3Nzb3ZlcigpO1xyXG4gICAgICAgIGlmIChDb25maWcubG9nLmN5Y2xlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwib2Zmc3ByaW5ncyAtIGRvQ3Jvc3NvdmVyOlwiKTtcclxuICAgICAgICAgICAgY29uc29sZS5kaXIodGhpcy5vZmZzcHJpbmdzKTsgLy8gaWYgdGhpcyBjaGFuZ2VkIGxhdGVyIChtdXRhdGlvbiksIG5ldyB2YWx1ZSBtYXkgYmUgZGlzcGxheWVkIGluIGNvbnNvbGVcclxuICAgICAgICAgICAgRXZvQ3ljbGUucHJpbnQodGhpcy5vZmZzcHJpbmdzLnJlZHVjZSgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGEuY29uY2F0KFtdKS5jb25jYXQoYik7XHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGNoYW5nZSBpbmRpdmlkdWFscyBieSBtdXRhdGlvbnNcclxuICAgICAgICB0aGlzLmRvTXV0YXRpb24oKTtcclxuICAgICAgICBpZiAoQ29uZmlnLmxvZy5jeWNsZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInBvcHVsYXRpb24gYW5kIG9mZnNwcmluZ3MgLSBkb011dGF0aW9uOlwiKTtcclxuICAgICAgICAgICAgY29uc29sZS5kaXIodGhpcy5wb3B1bGF0aW9uKTtcclxuICAgICAgICAgICAgY29uc29sZS5kaXIodGhpcy5vZmZzcHJpbmdzKTtcclxuICAgICAgICAgICAgRXZvQ3ljbGUucHJpbnQodGhpcy5wb3B1bGF0aW9uKTtcclxuICAgICAgICAgICAgRXZvQ3ljbGUucHJpbnQodGhpcy5vZmZzcHJpbmdzLnJlZHVjZSgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGEuY29uY2F0KFtdKS5jb25jYXQoYik7XHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHRoaXMucG9wdWxhdGlvbiA9IHRoaXMucG9wdWxhdGlvbi5jb25jYXQob2Zmc3ByaW5ncyk7IC8vIFRPRE8gYXogdiByZWR1a2NpXHJcbiAgICAgICAgQ29uZmlnLmN5Y2xlLnJ1bm5pbmcgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmNvbnRpbnVlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBmaWx0ZXJEdXBsaWNhdGVzKGluZGl2aWRpYWxzOiBJbmRpdmlkdWFscykge1xyXG4gICAgICAgIHJldHVybiBpbmRpdmlkaWFscy5maWx0ZXIoZnVuY3Rpb24gKGl0ZW0sIHBvcywgc2VsZikge1xyXG4gICAgICAgICAgICByZXR1cm4gc2VsZi5pbmRleE9mKGl0ZW0pID09IHBvcztcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZG9TcGVjaWF0aW9uKCkge1xyXG4gICAgICAgIC8vIHJhbmRvbSBzcGVjaWVzIHJlcHJlc2VudGF0aXZlc1xyXG4gICAgICAgIGxldCBpID0gdGhpcy5zcGVjaWVzLmxlbmd0aDtcclxuICAgICAgICB3aGlsZSAoaS0tKSB7IC8vIFdBUk5JTkcgaXRlcmF0aW5nIGluIHJldmVyc2UgYmVjYXVzZSBvZiBzcGxpY2UgKHJlbW92aW5nIGl0ZW1zKVxyXG4gICAgICAgICAgICBpZiAodGhpcy5zcGVjaWVzW2ldLmNsZWFyKCkgPT0gZmFsc2UpIHsgLy8ga2VlcHMgc2luZ2xlIHJlcHJlc2VudGF0aXZlIGFuZCByZXR1cm5zIGZhbHNlIGlmIGVtcHR5XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNwZWNpZXMuc3BsaWNlKGksIDEpOyAvLyByZW1vdmUgdGhpcyBzcGVjaWVzXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHJlbW92ZSBzcGVjaWVzIHdoaWNoIGNhbiBiZSBtZXJnZWRcclxuICAgICAgICAvLyB0aGlzIGlzIG5vdCBpbiBzcGVjaWZpY2F0aW9uIGJ1dCBzb3VuZHMgbGlrZSBhIGdvb2QgaWRlYVxyXG4gICAgICAgIC8vIGkgPSB0aGlzLnNwZWNpZXMubGVuZ3RoO1xyXG4gICAgICAgIC8vIHdoaWxlKGktLSA+IDEpIHsgLy8gV0FSTklORyBpdGVyYXRpbmcgaW4gcmV2ZXJzZSBiZWNhdXNlIG9mIHNwbGljZSAocmVtb3ZpbmcgaXRlbXMpXHJcbiAgICAgICAgLy8gICAgIGlmKHRoaXMuc3BlY2llc1tpXS5yZXByZXNlbnRhdGl2ZS5kaXN0YW5jZVRvKHRoaXMuc3BlY2llc1tpLTFdLnJlcHJlc2VudGF0aXZlKSA8IENvbmZpZy5zcGVjaWF0aW9uLmRpc3RhbmNlVGhyZXNob2xkKSB7XHJcbiAgICAgICAgLy8gICAgICAgICB0aGlzLnNwZWNpZXNbaV0uaW5kaXZpZHVhbHMgPSBbXTsgLy8gZW1wdHkgdGhlIHNwZWNpZXMgLy8gc3RpbGwgaW4gcG9wdWxhdGlvbiBzbyBpdCB3aWxsIGJlIGNsYXNzaWZ5IGFnYWluXHJcbiAgICAgICAgLy8gICAgICAgICB0aGlzLnNwZWNpZXMuc3BsaWNlKGksIDEpOyAvLyByZW1vdmUgdGhpcyBzcGVjaWVzXHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIC8vIGNsYXNzaWZ5IGluZGl2aWR1YWxzXHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMucG9wdWxhdGlvbikge1xyXG4gICAgICAgICAgICBsZXQgaW5kaXYgPSB0aGlzLnBvcHVsYXRpb25ba2V5XTtcclxuXHJcbiAgICAgICAgICAgIGxldCBwbGFjZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnNwZWNpZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChpbmRpdiA9PSB0aGlzLnNwZWNpZXNbaV0ucmVwcmVzZW50YXRpdmUpIHtcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAocGxhY2VkKSB7IC8vIHNraXBwaW5nIHJlcHJlc2VudGF0aXZlc1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zcGVjaWVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBpZiAoaW5kaXYgIT0gdGhpcy5zcGVjaWVzW2ldLnJlcHJlc2VudGF0aXZlKVxyXG4gICAgICAgICAgICAgICAgLy8gcGxhY2UgaW4gZXhpc3RpbmcgY2xhc3NcclxuICAgICAgICAgICAgICAgIGlmIChpbmRpdi5kaXN0YW5jZVRvKHRoaXMuc3BlY2llc1tpXS5yZXByZXNlbnRhdGl2ZSkgPCBDb25maWcuc3BlY2lhdGlvbi5kaXN0YW5jZVRocmVzaG9sZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3BlY2llc1tpXS5hZGQoaW5kaXYpO1xyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChwbGFjZWQpIHsgLy8gY29udGludWUgd2l0aCBuZXh0XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHsgLy8gbmV3IGNsYXNzXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNwZWNpZXMucHVzaChuZXcgU3BlY2llcyhbaW5kaXZdKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX29ic2VydmVycy5mb3JFYWNoKChvYnNlcnZlcjogRXZvQ3ljbGVPYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5ub3RpZnlEb25lU3BlY2lhdGlvbih0aGlzLCB0aGlzLnNwZWNpZXMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2ltcGxlIHRvdXJuYW1lbnQgc2VsZWN0aW9uIGltcGxlbWVudGF0aW9uLlxyXG4gICAgICogUmFuZG9tbHkgY2hvb3NlcyAod2l0aCByZXBldGl0aW9uKSBrIGluZGl2aWR1YWxzIGFuZCBwaWNrcyB0aGUgYmVzdCBvbmUgb2YgdGhlIHRvdXJuYW1lbnQuIFJlcGVhdHMgdW50aWwgd2hvbGUgcG9wdWxhdGlvbiBpcyBmaWxsZWQuXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZG9TZWxlY3Rpb24oKSB7XHJcblxyXG5cclxuICAgICAgICAvLyBUT0RPIHRvdXJuYW1lbnQgYnkgc3BlY2llc1xyXG4gICAgICAgIC8vIFRPRE8gaG93IG11Y2ggdG8gY3Jvc3NvdmVyIHZzIGhvdyBtdWNoIHRvIG11dGF0ZVxyXG5cclxuICAgICAgICAvLyBUT0RPIHRha3kgbXV0b3ZhdCBhc2kgbmVqZW4gcm9kaWNlIGFsZSB2c2VjaG5vIGNvIHByb2pkZVxyXG4gICAgICAgIC8vIHByb3RvemUga2R5eiB2eXBudSBjcm9zc292ZXIgdXBsbmUgdGFrIGJ5IG5pa2R5IG5lbXV0b3ZhbGlcclxuICAgICAgICAvLyBuZWpkZSBtdXRvdmF0IGplbm9tIHBvdG9ta3kgbmVibyBqZW5vbSByb2RpY2UgcHJvdG96ZSBrZHl6IG5lbmkgemFwbnV0ZWogY3Jvc3NvdmVyIHRhayBqc291IHR5aGxlIHNrdXBpbnkgcHJhemRueVxyXG5cclxuICAgICAgICAvLyBqYWsgdmVsa291IGNhc3QgcG9wdWxhY2UgdnlnZW5lcm92YXQgcHJlcyBjcm9zc292ZXJcclxuICAgICAgICBsZXQgb2Zmc3ByaW5nU2l6ZSA9IE1hdGgucm91bmQodGhpcy5wb3B1bGF0aW9uLmxlbmd0aCAqIENvbmZpZy5jcm9zc292ZXJPcHRpb25zLm9mZnNwcmluZ1JhdGlvKTtcclxuXHJcbiAgICAgICAgLy8gemppc3RpdCBrb2xpayB6IGpha3kgc3BlY2llIHBvZGxlIHNoYXJlZCBmaXRuZXNzXHJcbiAgICAgICAgbGV0IHN1bVNoYXJlZEZpdHRuZXNzID0gdGhpcy5zcGVjaWVzLnJlZHVjZSgoc3VtLCBjdXJyKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBzdW0gKyBjdXJyLmdldFNoYXJlZEZpdG5lc3MoKTtcclxuICAgICAgICB9LCAwKTtcclxuXHJcbiAgICAgICAgdGhpcy5wYXJlbnRzID0gW107XHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMuc3BlY2llcykge1xyXG4gICAgICAgICAgICBsZXQgc3BlYyA9IHRoaXMuc3BlY2llc1trZXldO1xyXG5cclxuICAgICAgICAgICAgbGV0IHNwZWNTaXplID0gc3BlYy5pbmRpdmlkdWFscy5sZW5ndGg7XHJcblxyXG4gICAgICAgICAgICBsZXQgc3BlY2lPZmZzcHJpbmdTaXplID0gTWF0aC5yb3VuZChzcGVjLmdldFNoYXJlZEZpdG5lc3MoKSAvIHN1bVNoYXJlZEZpdHRuZXNzICogb2Zmc3ByaW5nU2l6ZSk7XHJcbiAgICAgICAgICAgIHNwZWNpT2Zmc3ByaW5nU2l6ZSA9IHNwZWNpT2Zmc3ByaW5nU2l6ZSA+IDAgPyBzcGVjaU9mZnNwcmluZ1NpemUgOiAxOyAvLyBhdGxlYXN0IG9uZSBvZmZzcHJpbmdcclxuXHJcbiAgICAgICAgICAgIC8vIHRoaXMucGFyZW50c1trZXldLnB1c2gobnVsbCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgdG91cm5hbWVudFNpemUgPSBNYXRoLnJvdW5kKHNwZWNTaXplICogQ29uZmlnLmNyb3Nzb3Zlck9wdGlvbnMudG91cm5hbWVudFJhdGlvKTtcclxuICAgICAgICAgICAgdG91cm5hbWVudFNpemUgPSB0b3VybmFtZW50U2l6ZSA8IDEgPyAxIDogdG91cm5hbWVudFNpemU7XHJcbiAgICAgICAgICAgIHRvdXJuYW1lbnRTaXplID0gdG91cm5hbWVudFNpemUgPiBzcGVjU2l6ZSA/IHNwZWNTaXplIDogdG91cm5hbWVudFNpemU7XHJcblxyXG4gICAgICAgICAgICAvLyBieSB0b3VybmFtZW50IHNlbGVjdGlvbiBzZWxlY3QgYXMgbWFueSBpbmRpdmlkdWFsc1xyXG4gICAgICAgICAgICAvLyBhcyBpdCBpcyBuZWVkZWQgZm9yIGNyb3Nzb3ZlciAoMiB0aW1lcyBhcyBtdWNoIGFzIG9mZnNwcmluZyBzaXplKVxyXG4gICAgICAgICAgICBsZXQgd2lubmVyczogSW5kaXZpZHVhbHMgPSB0aGlzLnBhcmVudHNba2V5XSA9IFtdO1xyXG4gICAgICAgICAgICB3aGlsZSAod2lubmVycy5sZW5ndGggPCBzcGVjaU9mZnNwcmluZ1NpemUgKiAyKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBzZWxlY3QgayByYW5kb20gaW5kaXZpZHVhbHMgKHdpdGggcmVwZXRpdGlvbilcclxuICAgICAgICAgICAgICAgIGxldCBjb250ZXN0YW50czogSW5kaXZpZHVhbHMgPSBbXTtcclxuICAgICAgICAgICAgICAgIHdoaWxlIChjb250ZXN0YW50cy5sZW5ndGggPCB0b3VybmFtZW50U2l6ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogc3BlY1NpemUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlc3RhbnRzLnB1c2goc3BlYy5pbmRpdmlkdWFsc1tyXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gc29ydCB0aGVtIGFuZCBzZWxlY3QgZmlyc3QgYXMgd2lubmVyXHJcbiAgICAgICAgICAgICAgICBjb250ZXN0YW50cy5zb3J0KEJhc2VOZWF0SW5kaXZpZHVhbC5jb21wYXJlKTtcclxuICAgICAgICAgICAgICAgIHdpbm5lcnMucHVzaChjb250ZXN0YW50c1swXSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBsZXQgdG91cm5TaXplID0gdGhpcy50b3VybmFtZW50U2l6ZTtcclxuICAgICAgICAvLyBsZXQgcG9wU2l6ZSA9IGNob2ljZXMubGVuZ3RoO1xyXG4gICAgICAgIC8vIGlmICh0b3VyblNpemUgPCAxIHx8IHRvdXJuU2l6ZSA+IHBvcFNpemUpIHtcclxuICAgICAgICAvLyAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoXCJUb3VybmFtZW50IHNpemUgbXVzdCBiZSBncmVhdGVyIHRoYW4gMSBhbmQgbGVzcyB0aGFuIG9yIGVxdWFsIHRvIHBvcHVsYXRpb24gc2l6ZTogXCIgK1xyXG4gICAgICAgIC8vICAgICAgICAgXCJ0b3VybmFtZW50U2l6ZSA9IFwiICsgdG91cm5TaXplICsgXCIsIHBvcHVsYXRpb25TaXplID0gXCIgKyBwb3BTaXplKTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyAvLyBieSB0b3VybmFtZW50IHNlbGVjdGlvbiBzZWxlY3QgYXMgbWFueSBpbmRpdmlkdWFsc1xyXG4gICAgICAgIC8vIC8vIGFzIGl0IGlzIG5lZWRlZCBmb3IgY3Jvc3NvdmVyICgyIHRpbWVzIGFzIG11Y2ggYXMgb2Zmc3ByaW5nIHNpemUpXHJcbiAgICAgICAgLy8gbGV0IHdpbm5lcnM6IEluZGl2aWR1YWxzID0gW107XHJcbiAgICAgICAgLy8gd2hpbGUgKHdpbm5lcnMubGVuZ3RoIDwgdGhpcy5vZmZzcHJpbmdTaXplICogMikge1xyXG4gICAgICAgIC8vICAgICAvLyBzZWxlY3QgayByYW5kb20gaW5kaXZpZHVhbHMgKHdpdGggcmVwZXRpdGlvbilcclxuICAgICAgICAvLyAgICAgbGV0IGNvbnRlc3RhbnRzOiBJbmRpdmlkdWFscyA9IFtdO1xyXG4gICAgICAgIC8vICAgICB3aGlsZSAoY29udGVzdGFudHMubGVuZ3RoIDwgdG91cm5TaXplKSB7XHJcbiAgICAgICAgLy8gICAgICAgICBsZXQgciA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIHBvcFNpemUpO1xyXG4gICAgICAgIC8vICAgICAgICAgY29udGVzdGFudHMucHVzaChjaG9pY2VzW3JdKTtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gICAgIC8vIHNvcnQgdGhlbSBhbmQgc2VsZWN0IGZpcnN0IGFzIHdpbm5lclxyXG4gICAgICAgIC8vICAgICBjb250ZXN0YW50cy5zb3J0KEJhc2VOZWF0SW5kaXZpZHVhbC5jb21wYXJlKTtcclxuICAgICAgICAvLyAgICAgd2lubmVycy5wdXNoKGNvbnRlc3RhbnRzWzBdKTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLy9cclxuICAgICAgICAvLyByZXR1cm4gd2lubmVycztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyb3Nzb3ZlclxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGRvQ3Jvc3NvdmVyKCkge1xyXG5cclxuICAgICAgICB0aGlzLm9mZnNwcmluZ3MgPSBbXTtcclxuICAgICAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5wYXJlbnRzKSB7XHJcbiAgICAgICAgICAgIGxldCBwYXJlbnRzID0gdGhpcy5wYXJlbnRzW2tleV07XHJcblxyXG4gICAgICAgICAgICBsZXQgb2Zmc3ByaW5nczogSW5kaXZpZHVhbHMgPSB0aGlzLm9mZnNwcmluZ3Nba2V5XSA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucGFyZW50c1trZXldLmxlbmd0aDsgaSArPSAyKSB7XHJcbiAgICAgICAgICAgICAgICBvZmZzcHJpbmdzLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50c1tpXS5icmVlZChwYXJlbnRzW2kgKyAxXSlcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZG9NdXRhdGlvbigpIHtcclxuXHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMucG9wdWxhdGlvbikge1xyXG4gICAgICAgICAgICBsZXQgaW5kaXYgPSB0aGlzLnBvcHVsYXRpb25ba2V5XTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubXV0YW50cyA9IFtdO1xyXG4gICAgICAgICAgICBpZihDb25maWcubXV0YXRpb25PcHRpb25zLm11dGF0ZUJ5Q2xvbmluZykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGNsb25lOiBCYXNlTmVhdEluZGl2aWR1YWwgPSBuZXcgKDxhbnk+aW5kaXYuY29uc3RydWN0b3IpKGluZGl2KTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgd2FzTXV0YXRlZCA9IGNsb25lLm11dGF0ZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmKHdhc011dGF0ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm11dGFudHMucHVzaChjbG9uZSk7XHJcbiAgICAgICAgICAgICAgICB9IC8vIGVsc2Ugd2lsbCBiZSBmb3Jnb3R0ZW5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGluZGl2Lm11dGF0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoQ29uZmlnLm11dGF0aW9uT3B0aW9ucy5tdXRhdGVPZmZzcHJpbmdzKSB7XHJcbiAgICAgICAgICAgIGxldCBhbGxPZmZzcHJpbmdzID0gdGhpcy5vZmZzcHJpbmdzLnJlZHVjZSgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGEuY29uY2F0KGIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgZm9yIChsZXQga2V5IGluIGFsbE9mZnNwcmluZ3MpIHtcclxuICAgICAgICAgICAgICAgIGxldCBpbmRpdiA9IGFsbE9mZnNwcmluZ3Nba2V5XTtcclxuXHJcbiAgICAgICAgICAgICAgICBpbmRpdi5tdXRhdGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcHJpdmF0ZSBkb1JlZHVjdGlvbigpOiBQcm9taXNlPEluZGl2aWR1YWxzPiB7XHJcbiAgICAgICAgaWYgKENvbmZpZy5sb2cuZnVuY3Rpb25zKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZvQ3ljbGUuZG9SZWR1Y3Rpb25cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgYWxsSW5kaXZpZGlhbHMgPSBbXS5jb25jYXQuYXBwbHkodGhpcy5wb3B1bGF0aW9uLCB0aGlzLm9mZnNwcmluZ3MpLmNvbmNhdCh0aGlzLm11dGFudHMpO1xyXG5cclxuICAgICAgICAvLyBldmFsdWF0ZUZpdG5lc3MgaWYgbmVlZGVkXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZXZhbHVhdGVBbGxGaXRuZXNzKGFsbEluZGl2aWRpYWxzKS50aGVuKChwb3B1bGF0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChDb25maWcubG9nLmZ1bmN0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdm9DeWNsZS5kb1JlZHVjdGlvbiBldmFsdWF0ZUFsbEZpdG5lc3MtdGhlblwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gc29ydCBhbGwgaW5kaXZpZHVhbHMgYW5kIHJldHVybnMgdGhlIGZpdHRlc3RcclxuICAgICAgICAgICAgYWxsSW5kaXZpZGlhbHMuc29ydChCYXNlTmVhdEluZGl2aWR1YWwuY29tcGFyZSk7IC8vIGluLXBsYWNlXHJcbiAgICAgICAgICAgIGxldCBlbGltaW5hdGVkID0gYWxsSW5kaXZpZGlhbHMuc3BsaWNlKHRoaXMucG9wdWxhdGlvbi5sZW5ndGgpOyAvLyBpbi1wbGFjZVxyXG4gICAgICAgICAgICB0aGlzLnBvcHVsYXRpb24gPSBhbGxJbmRpdmlkaWFscztcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZWxpbWluYXRlZC5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgZWxpbWluYXRlZFtpXS5lbGltaW5hdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucG9wdWxhdGlvbjsgLy8gY2hhaW5lZCBwcm9taXNlXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZXZhbHVhdGVBbGxGaXRuZXNzKHBvcHVsYXRpb246IEluZGl2aWR1YWxzKTogUHJvbWlzZTxJbmRpdmlkdWFscz4ge1xyXG4gICAgICAgIGlmIChDb25maWcubG9nLmZ1bmN0aW9ucykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2b0N5Y2xlLkV2YWx1YXRlQWxsRml0bmVzc1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIENBTk5PVCB1c2UgUHJvbWlzZS5BbGwsIG5lZWRzIHRvIGJlIGV2YWx1YXRlZCBzZXF1ZW50aWFsbHlcclxuXHJcbiAgICAgICAgdmFyIGFsbERvbmVQcm9taXNlID0gbmV3IFByb21pc2U8SW5kaXZpZHVhbHM+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ldmFsdWF0ZU5leHRGaXRuZXNzKHBvcHVsYXRpb24sIDAsIHJlc29sdmUpOyAvLyBpbml0IHByb21pc2UgcmVjdXJzaW9uXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBhbGxEb25lUHJvbWlzZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGV2YWx1YXRlTmV4dEZpdG5lc3MocG9wdWxhdGlvbjogSW5kaXZpZHVhbHMsIGN1cnJlbnQ6IG51bWJlciwgcmVzb2x2ZTogYW55KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKChDb25maWcubG9nLmZ1bmN0aW9ucyAmJiBDb25maWcubG9nLm5leHRGaXRuZXNzKVxyXG4gICAgICAgICAgICB8fCBDb25maWcubG9nLm5leHRGaXRuZXNzKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZvQ3ljbGUuZXZhbHVhdGVOZXh0Rml0bmVzc1wiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGVuZCBjb25kaXRpb25cclxuICAgICAgICBpZiAoY3VycmVudCA+PSBwb3B1bGF0aW9uLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXNvbHZlKHBvcHVsYXRpb24pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAvLyByZWN1cnNpb25cclxuICAgICAgICAgICAgaWYgKHBvcHVsYXRpb25bY3VycmVudF0uZml0bmVzcyAmJiAhQ29uZmlnLmdlbmVyYWwuYWx3YXlzRXZhbHVhdGVGaXRuZXNzKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBkbyBub3QgZXZhbHVhdGUgaWYgZml0bmVzcyBpcyBrbm93biwgY2FsbCBuZXh0IGltbWVkaWF0ZWx5XHJcbiAgICAgICAgICAgICAgICB0aGlzLmV2YWx1YXRlTmV4dEZpdG5lc3MocG9wdWxhdGlvbiwgY3VycmVudCArIDEsIHJlc29sdmUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gZXZhbHVhdGUgdW5rbm93biBmaXRuZXNzLCB0aGVuIGNhbGwgbmV4dFxyXG4gICAgICAgICAgICAgICAgcG9wdWxhdGlvbltjdXJyZW50XS5ldmFsdWF0ZUZpdG5lc3MoKS50aGVuKChyZXMpPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX29ic2VydmVycy5mb3JFYWNoKChvYnNlcnZlcjpFdm9DeWNsZU9ic2VydmVyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5vdGlmeURvbmVFdmFsdWF0ZU5leHRGaXRuZXNzKHRoaXMsIHBvcHVsYXRpb25bY3VycmVudF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXZhbHVhdGVOZXh0Rml0bmVzcyhwb3B1bGF0aW9uLCBjdXJyZW50ICsgMSwgcmVzb2x2ZSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8gUE9QVUxBVElPTiBGVU5DVElPTlMgLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuXHJcbiAgICBwdWJsaWMgZ2V0TWF4QXZnTWluRml0bmVzcygpOiBbbnVtYmVyLCBudW1iZXIsIG51bWJlcl0ge1xyXG4gICAgICAgIGxldCBtYXg6IG51bWJlcjtcclxuICAgICAgICBsZXQgc3VtOiBudW1iZXIgPSAwO1xyXG4gICAgICAgIGxldCBtaW46IG51bWJlcjtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnBvcHVsYXRpb24ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGYgPSB0aGlzLnBvcHVsYXRpb25baV0uZml0bmVzcztcclxuXHJcbiAgICAgICAgICAgIGlmIChtYXggPT09IHVuZGVmaW5lZCB8fCBtYXggPCBmKSB7XHJcbiAgICAgICAgICAgICAgICBtYXggPSBmO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobWluID09PSB1bmRlZmluZWQgfHwgbWluID4gZikge1xyXG4gICAgICAgICAgICAgICAgbWluID0gZjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc3VtICs9IGY7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gW21heCwgc3VtIC8gdGhpcy5wb3B1bGF0aW9uLmxlbmd0aCwgbWluXTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIHByaW50KGluZGl2aWR1YWxzOiBJbmRpdmlkdWFscykge1xyXG4gICAgICAgIHZhciBhcnIgPSBpbmRpdmlkdWFscy5tYXAoKGl0ZW06IEJhc2VOZWF0SW5kaXZpZHVhbCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gaXRlbS50b1N0cmluZygpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zb2xlLmRpcihhcnIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vIEdFVFRFUlMgYW5kIFNFVFRFUlMgLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuXHJcbiAgICAvLyB0b3VkbmFtZW50U2l6ZVxyXG4gICAgcHJpdmF0ZSBnZXQgdG91cm5hbWVudFNpemUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RvdXJuYW1lbnRTaXplO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2V0IHRvdXJuYW1lbnRTaXplKHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl90b3VybmFtZW50U2l6ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUb3VybmFtZW50U2l6ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50b3VybmFtZW50U2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0VG91cm5hbWVudFNpemUodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMudG91cm5hbWVudFNpemUgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBvZmZzaXByaW5nU2l6ZVxyXG4gICAgcHJpdmF0ZSBnZXQgb2Zmc3ByaW5nU2l6ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fb2Zmc3ByaW5nU2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNldCBvZmZzcHJpbmdTaXplKHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9vZmZzcHJpbmdTaXplID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldE9mZnNwcmluZ1NpemUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2Zmc3ByaW5nU2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0T2Zmc3ByaW5nU2l6ZSh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5vZmZzcHJpbmdTaXplID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIE15TWF0aCB7XHJcblxyXG4gICAgcHJpdmF0ZSBjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVzY3JpcHRpb24gcmV0dXJucyBhIGdhdXNzaWFuIHJhbmRvbSBmdW5jdGlvbiB3aXRoIHRoZSBnaXZlbiBtZWFuIGFuZCBzdGRldi5cclxuICAgICAqIEBhdXRob3IgaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8yNTU4Mjg4Mi9qYXZhc2NyaXB0LW1hdGgtcmFuZG9tLW5vcm1hbC1kaXN0cmlidXRpb24tZ2F1c3NpYW4tYmVsbC1jdXJ2ZSNhbnN3ZXItMzU1OTkxODFcclxuICAgICAqIEB2aXogaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvTWFyc2FnbGlhX3BvbGFyX21ldGhvZFxyXG4gICAgICogQHBhcmFtIG1lYW4gKG11KVxyXG4gICAgICogQHBhcmFtIHN0ZGV2IChzaWdtYSkgc3RhbmRhcmQgZGV2aWF0aW9uXHJcbiAgICAgKiBAcmV0dXJucyB7KCk9Pm51bWJlcn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXRpYyBnYXVzc2lhbihtZWFuOiBudW1iZXIsIHN0ZGV2OiBudW1iZXIpOiAoKSA9PiBudW1iZXIge1xyXG4gICAgICAgIHZhciB5MjogbnVtYmVyO1xyXG4gICAgICAgIHZhciB1c2VfbGFzdDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciB5MTogbnVtYmVyO1xyXG4gICAgICAgICAgICBpZiAodXNlX2xhc3QpIHtcclxuICAgICAgICAgICAgICAgIHkxID0geTI7XHJcbiAgICAgICAgICAgICAgICB1c2VfbGFzdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFyIHgxOiBudW1iZXIsIHgyOiBudW1iZXIsIHc6IG51bWJlcjtcclxuICAgICAgICAgICAgICAgIGRvIHtcclxuICAgICAgICAgICAgICAgICAgICB4MSA9IDIuMCAqIE1hdGgucmFuZG9tKCkgLSAxLjA7XHJcbiAgICAgICAgICAgICAgICAgICAgeDIgPSAyLjAgKiBNYXRoLnJhbmRvbSgpIC0gMS4wO1xyXG4gICAgICAgICAgICAgICAgICAgIHcgPSB4MSAqIHgxICsgeDIgKiB4MjtcclxuICAgICAgICAgICAgICAgIH0gd2hpbGUgKHcgPj0gMS4wKTtcclxuICAgICAgICAgICAgICAgIHcgPSBNYXRoLnNxcnQoKC0yLjAgKiBNYXRoLmxvZyh3KSkgLyB3KTtcclxuICAgICAgICAgICAgICAgIHkxID0geDEgKiB3O1xyXG4gICAgICAgICAgICAgICAgeTIgPSB4MiAqIHc7XHJcbiAgICAgICAgICAgICAgICB1c2VfbGFzdCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHZhciByZXR2YWwgPSBtZWFuICsgc3RkZXYgKiB5MTtcclxuICAgICAgICAgICAgLy8gaWYocmV0dmFsID4gMClcclxuICAgICAgICAgICAgLy8gICAgIHJldHVybiByZXR2YWw7XHJcbiAgICAgICAgICAgIC8vIHJldHVybiAtcmV0dmFsO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHJldHZhbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gcHVibGljIHN0YXRpYyByYW5kb21Ob3JtYWwgPSBNeU1hdGguZ2F1c3NpYW4oMCwgMSk7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyByYW5kb21Ob3JtYWwoc3RkZXY6bnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIE15TWF0aC5nYXVzc2lhbigwLCBzdGRldik7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IE5vZGVHZW5lIGZyb20gXCIuL05vZGVHZW5lXCI7XHJcbmltcG9ydCBDb25uZWN0R2VuZSBmcm9tIFwiLi9Db25uZWN0R2VuZVwiO1xyXG5pbXBvcnQge05vZGVHZW5lc30gZnJvbSBcIi4vTm9kZUdlbmVcIjtcclxuaW1wb3J0IHtOb2RlR2VuZVR5cGV9IGZyb20gXCIuL05vZGVHZW5lXCI7XHJcbmltcG9ydCB7Q29ubmVjdEdlbmVzfSBmcm9tIFwiLi9Db25uZWN0R2VuZVwiO1xyXG5pbXBvcnQgTXlNYXRoIGZyb20gXCIuLi9NeU1hdGhcIjtcclxuaW1wb3J0IENvbmZpZyBmcm9tIFwiLi4vQ29uZmlnXCI7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEJhc2VJbmRpdmlkdWFsSW50ZXJmYWNlIHtcclxuICAgIGV2YWx1YXRlRml0bmVzcygpOiBQcm9taXNlPG51bWJlcj47XHJcbiAgICBicmVlZChwYXJ0bmVyOiBCYXNlSW5kaXZpZHVhbEludGVyZmFjZSk6IEJhc2VJbmRpdmlkdWFsSW50ZXJmYWNlO1xyXG4gICAgbXV0YXRlKCk6IGJvb2xlYW47XHJcbn1cclxuXHJcbmV4cG9ydCB0eXBlIEluZGl2aWR1YWxzID0gQXJyYXk8QmFzZU5lYXRJbmRpdmlkdWFsPjtcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlTmVhdEluZGl2aWR1YWwgaW1wbGVtZW50cyBCYXNlSW5kaXZpZHVhbEludGVyZmFjZSB7XHJcblxyXG4gICAgcHJvdGVjdGVkIF9pZDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2lkQ291bnRlcjogbnVtYmVyID0gMDtcclxuXHJcbiAgICBwcm90ZWN0ZWQgX2dlbm9tZTogW05vZGVHZW5lcywgQ29ubmVjdEdlbmVzXTsgLy8gdHVwbGUgb2YgdHdvIFwibWFwcy9kaWN0aW9uYXJpZXNcIlxyXG4gICAgcHJvdGVjdGVkIF9pbnB1dEdlbmVzOiBBcnJheTxOb2RlR2VuZT47XHJcbiAgICBwcm90ZWN0ZWQgX291dHB1dEdlbmVzOiBBcnJheTxOb2RlR2VuZT47XHJcblxyXG4gICAgcHJvdGVjdGVkIF9maXRuZXNzOiBudW1iZXI7XHJcblxyXG4gICAgcHVibGljIGVsaW1pbmF0ZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3IoaW5kaXZpZHVhbDogQmFzZU5lYXRJbmRpdmlkdWFsKTtcclxuICAgIGNvbnN0cnVjdG9yKGlucHV0c0xlbmd0aDogbnVtYmVyLCBvdXRwdXRzTGVuZ3RoOiBudW1iZXIpO1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5faWQgPSArK0Jhc2VOZWF0SW5kaXZpZHVhbC5faWRDb3VudGVyO1xyXG4gICAgICAgIHRoaXMuX2dlbm9tZSA9IFt7fSwge31dO1xyXG4gICAgICAgIHRoaXMuZWxpbWluYXRlZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZihhcmd1bWVudHMubGVuZ3RoID09IDEgJiYgYXJndW1lbnRzWzBdIGluc3RhbmNlb2YgQmFzZU5lYXRJbmRpdmlkdWFsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29weUNvbnN0cnVjdG9yKGFyZ3VtZW50c1swXSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5ub3JtYWxDb25zdHJ1Y3Rvci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBkZXNjcmlwdGlvbiBjb3B5IGNvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgY29weUNvbnN0cnVjdG9yKHRoYXQ6IEJhc2VOZWF0SW5kaXZpZHVhbCkge1xyXG4gICAgICAgIHRoaXMuX2lucHV0R2VuZXMgPSBbXTtcclxuICAgICAgICB0aGlzLl9vdXRwdXRHZW5lcyA9IFtdO1xyXG5cclxuICAgICAgICAvLyBjb3B5IG5vZGVHZW5lc1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoYXQubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkTm9kZUdlbmUobmV3IE5vZGVHZW5lKHRoYXQubm9kZUdlbmVzW2tleV0pKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGFkZCBzYW1lIGNvbm5lY3Rpb25zXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhhdC5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IHRoYXRDb25uZWN0Tm9kZSA9IHRoYXQuY29ubmVjdEdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ29ubmVjdGlvbihcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZUdlbmVzW3RoYXRDb25uZWN0Tm9kZS5pbk5vZGUuaW5ub3ZdLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlR2VuZXNbdGhhdENvbm5lY3ROb2RlLm91dE5vZGUuaW5ub3ZdLFxyXG4gICAgICAgICAgICAgICAgdGhhdENvbm5lY3ROb2RlLndlaWdodCxcclxuICAgICAgICAgICAgICAgIHRoYXRDb25uZWN0Tm9kZS5lbmFibGVkLFxyXG4gICAgICAgICAgICAgICAgdGhhdENvbm5lY3ROb2RlLmlubm92XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlc2NyaXB0aW9uIGFyZ3MgY29uc3RydWN0b3JcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBub3JtYWxDb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBsZXQgaW5wdXRzTGVuZ3RoOiBudW1iZXI7XHJcbiAgICAgICAgbGV0IG91dHB1dHNMZW5ndGg6IG51bWJlcjtcclxuXHJcbiAgICAgICAgaWYodHlwZW9mIGFyZ3VtZW50c1swXSA9PT0gXCJudW1iZXJcIiAmJiB0eXBlb2YgYXJndW1lbnRzWzFdID09PSBcIm51bWJlclwiKSB7XHJcbiAgICAgICAgICAgIGlucHV0c0xlbmd0aCA9IGFyZ3VtZW50c1swXTtcclxuICAgICAgICAgICAgb3V0cHV0c0xlbmd0aCA9IGFyZ3VtZW50c1sxXTtcclxuICAgICAgICAgICAgdGhpcy5faW5wdXRHZW5lcyA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLl9vdXRwdXRHZW5lcyA9IFtdO1xyXG5cclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGlucHV0c0xlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFkZE5vZGVHZW5lKG5ldyBOb2RlR2VuZShOb2RlR2VuZVR5cGUuSW5wdXQpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZm9yKGxldCBvID0gMDsgbyA8IG91dHB1dHNMZW5ndGg7IG8rKykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZGROb2RlR2VuZShuZXcgTm9kZUdlbmUoTm9kZUdlbmVUeXBlLk91dHB1dCkpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0gZWxzZSBpZihBcnJheS5pc0FycmF5KGFyZ3VtZW50c1swXSkgJiYgQXJyYXkuaXNBcnJheShhcmd1bWVudHNbMV0pKSB7XHJcbiAgICAgICAgICAgIGlucHV0c0xlbmd0aCA9IGFyZ3VtZW50c1swXS5sZW5ndGg7XHJcbiAgICAgICAgICAgIG91dHB1dHNMZW5ndGggPSBhcmd1bWVudHNbMV0ubGVuZ3RoO1xyXG4gICAgICAgICAgICB0aGlzLl9pbnB1dEdlbmVzID0gYXJndW1lbnRzWzBdO1xyXG4gICAgICAgICAgICB0aGlzLl9vdXRwdXRHZW5lcyA9IGFyZ3VtZW50c1sxXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBcIlVuZXhwZWN0ZWQgcGFyYW1ldGVyc1wiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGlucHV0c0xlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGZvcihsZXQgbyA9IDA7IG8gPCBvdXRwdXRzTGVuZ3RoOyBvKyspIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkQ29ubmVjdGlvbih0aGlzLl9pbnB1dEdlbmVzW2ldLCB0aGlzLl9vdXRwdXRHZW5lc1tvXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVzY3JpcHRpb24gbWVyZ2VzIG9uIHRvcCBvZiB0aGlzIGluZGl2aWR1YWwgKGRvZXMgbm90IGNoYW5nZSBtYXRjaGluZyBub2RlcylcclxuICAgICAqIEBwYXJhbSB0aGF0XHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgbWVyZ2UodGhhdDogQmFzZU5lYXRJbmRpdmlkdWFsKSB7XHJcblxyXG4gICAgICAgIC8vIGNvcHkgbm9kZUdlbmVzIHdoaWNoIGRvIG5vdCBleGlzdHMgeWV0XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhhdC5ub2RlR2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IHRoaXNOb2RlOiBOb2RlR2VuZSA9IHRoaXMubm9kZUdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIGxldCB0aGF0Tm9kZTogTm9kZUdlbmUgPSB0aGF0Lm5vZGVHZW5lc1trZXldO1xyXG5cclxuICAgICAgICAgICAgaWYoIXRoaXNOb2RlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFkZE5vZGVHZW5lKG5ldyBOb2RlR2VuZSh0aGF0Tm9kZSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBhZGQgY29ubmVjdGlvbnMgd2hpY2ggZG8gbm90IGV4aXN0cyB5ZXRcclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0aGF0LmNvbm5lY3RHZW5lcykge1xyXG4gICAgICAgICAgICBsZXQgdGhpc0Nvbm4gPSB0aGlzLmNvbm5lY3RHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBsZXQgdGhhdENvbm4gPSB0aGF0LmNvbm5lY3RHZW5lc1trZXldO1xyXG5cclxuICAgICAgICAgICAgaWYoIXRoaXNDb25uKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFkZENvbm5lY3Rpb24oXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlR2VuZXNbdGhhdENvbm4uaW5Ob2RlLmlubm92XSxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vZGVHZW5lc1t0aGF0Q29ubi5vdXROb2RlLmlubm92XSxcclxuICAgICAgICAgICAgICAgICAgICB0aGF0Q29ubi53ZWlnaHQsXHJcbiAgICAgICAgICAgICAgICAgICAgdGhhdENvbm4uZW5hYmxlZCxcclxuICAgICAgICAgICAgICAgICAgICB0aGF0Q29ubi5pbm5vdlxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGNyZWF0ZUluaXRQb3B1bGF0aW9uKHBvcHVsYXRpb25TaXplOiBudW1iZXIsIEluZGl2aWR1YWxDbGFzczogbmV3ICguLi5hcmdzOiBhbnlbXSkgPT4gQmFzZU5lYXRJbmRpdmlkdWFsLCBmaXJzdEluZGl2aWR1YWxDb25zdHJ1Y3RvckFyZ3M/OiBBcnJheTxhbnk+KSA6IEJhc2VOZWF0SW5kaXZpZHVhbFtdIHtcclxuICAgICAgICBsZXQgcG9wdWxhdGlvbjogQmFzZU5lYXRJbmRpdmlkdWFsW10gPSBbXTtcclxuICAgICAgICBsZXQgZmlyc3Q6IEJhc2VOZWF0SW5kaXZpZHVhbDtcclxuICAgICAgICBpZih0eXBlb2YgZmlyc3RJbmRpdmlkdWFsQ29uc3RydWN0b3JBcmdzICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICAgIGZpcnN0ID0gbmV3IEluZGl2aWR1YWxDbGFzcyguLi5maXJzdEluZGl2aWR1YWxDb25zdHJ1Y3RvckFyZ3MpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGZpcnN0ID0gbmV3IEluZGl2aWR1YWxDbGFzcygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcG9wdWxhdGlvbi5wdXNoKGZpcnN0KTtcclxuICAgICAgICBmb3IobGV0IGkgPSAxOyBpIDwgcG9wdWxhdGlvblNpemU7IGkrKykge1xyXG4gICAgICAgICAgICAvLyByYW5kb21pemUgd2VpZ2h0c1xyXG4gICAgICAgICAgICBsZXQgbmV4dCA9IG5ldyBJbmRpdmlkdWFsQ2xhc3MoZmlyc3QpO1xyXG5cclxuICAgICAgICAgICAgZm9yKGxldCBrZXkgaW4gbmV4dC5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgICAgIGxldCBjb25uZWN0aW9uID0gbmV4dC5jb25uZWN0R2VuZXNba2V5XTtcclxuICAgICAgICAgICAgICAgIGNvbm5lY3Rpb24ud2VpZ2h0ID0gMipNYXRoLnJhbmRvbSgpIC0gMTsgLy8gVE9ETyBjb25maWc/IHJhbmRvbVdlaWdodEZ1bmNcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcG9wdWxhdGlvbi5wdXNoKG5leHQpOyAvLyBjb3BpZXMgb2YgZmlyc3Qgc28gc2FtZSBpbm5vdiBudW1iZXJzXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcG9wdWxhdGlvbiA7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhZGROb2RlR2VuZShub2RlR2VuZTogTm9kZUdlbmUpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLm5vZGVHZW5lc1tub2RlR2VuZS5pbm5vdl0gPSBub2RlR2VuZTtcclxuICAgICAgICBpZihub2RlR2VuZS50eXBlID09IE5vZGVHZW5lVHlwZS5JbnB1dCkge1xyXG4gICAgICAgICAgICB0aGlzLl9pbnB1dEdlbmVzLnB1c2gobm9kZUdlbmUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZihub2RlR2VuZS50eXBlID09IE5vZGVHZW5lVHlwZS5PdXRwdXQpIHtcclxuICAgICAgICAgICAgdGhpcy5fb3V0cHV0R2VuZXMucHVzaChub2RlR2VuZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkQ29ubmVjdEdlbmUoY29ubmVjdEdlbmU6IENvbm5lY3RHZW5lKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jb25uZWN0R2VuZXNbY29ubmVjdEdlbmUuaW5ub3ZdID0gY29ubmVjdEdlbmU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhZGRDb25uZWN0aW9uKGluTm9kZTpOb2RlR2VuZSwgb3V0Tm9kZTpOb2RlR2VuZSwgd2VpZ2h0PyA6IG51bWJlciwgZW5hYmxlZD86IGJvb2xlYW4sIGlubm92PzogbnVtYmVyKSB7XHJcbiAgICAgICAgd2VpZ2h0ID0gdHlwZW9mIHdlaWdodCAhPSBcInVuZGVmaW5lZFwiID8gd2VpZ2h0IDogMipNYXRoLnJhbmRvbSgpIC0gMTsgLy8gVE9ETyBjb25maWc/IHJhbmRvbVdlaWdodEZ1bmNcclxuICAgICAgICB0aGlzLmFkZENvbm5lY3RHZW5lKG5ldyBDb25uZWN0R2VuZShpbk5vZGUsIG91dE5vZGUsIHdlaWdodCwgZW5hYmxlZCwgaW5ub3YpKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYWJzdHJhY3QgZXZhbHVhdGVGaXRuZXNzKCk6IFByb21pc2U8bnVtYmVyPjtcclxuXHJcbiAgICBwdWJsaWMgYnJlZWQocGFydG5lcjogQmFzZU5lYXRJbmRpdmlkdWFsKTogQmFzZU5lYXRJbmRpdmlkdWFsIHtcclxuXHJcbiAgICAgICAgbGV0IGJldHRlcjogQmFzZU5lYXRJbmRpdmlkdWFsO1xyXG4gICAgICAgIGxldCB3b3JzZTogQmFzZU5lYXRJbmRpdmlkdWFsO1xyXG5cclxuICAgICAgICBpZih0aGlzLmZpdG5lc3MgPiBwYXJ0bmVyLmZpdG5lc3MpIHtcclxuICAgICAgICAgICAgYmV0dGVyID0gdGhpcztcclxuICAgICAgICAgICAgd29yc2UgPSBwYXJ0bmVyO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGJldHRlciA9IHBhcnRuZXI7XHJcbiAgICAgICAgICAgIHdvcnNlID0gdGhpcztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIG1ha2UgY29weSBvZiBiZXR0ZXJcclxuICAgICAgICBsZXQgb2Zmc3ByaW5nOiBCYXNlTmVhdEluZGl2aWR1YWwgPSBuZXcgKDxhbnk+YmV0dGVyLmNvbnN0cnVjdG9yKShiZXR0ZXIpOyAvLyBjb21wYWN0IHdheVxyXG5cclxuICAgICAgICAvLyBpbmhlcml0IHJhbmRvbWx5IHdlaWdodCBmb3Igc2hhcmVkIGdlbmVzXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gb2Zmc3ByaW5nLmNvbm5lY3RHZW5lcykge1xyXG4gICAgICAgICAgICBsZXQgY29ubk9mZnNwcmluZzogQ29ubmVjdEdlbmUgPSBvZmZzcHJpbmcuY29ubmVjdEdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIGxldCBjb25uV29yc2U6IENvbm5lY3RHZW5lID0gd29yc2UuY29ubmVjdEdlbmVzW2tleV07XHJcblxyXG4gICAgICAgICAgICBpZihjb25uV29yc2UpIHsgLy8gc2hhcmVkIGdlbmUgY3Jvc3NvdmVyXHJcbiAgICAgICAgICAgICAgICBsZXQgd2hpY2ggPSAwLjUgPiBNYXRoLnJhbmRvbSgpO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbm5PZmZzcHJpbmcud2VpZ2h0ID0gd2hpY2ggPyBjb25uT2Zmc3ByaW5nLndlaWdodCA6IGNvbm5Xb3JzZS53ZWlnaHQ7IC8vIFRPRE8gd2VpZ2h0Q3Jvc3NvdmVyRnVuY1xyXG4gICAgICAgICAgICAgICAgY29ubk9mZnNwcmluZy5lbmFibGVkID0gd2hpY2ggPyBjb25uT2Zmc3ByaW5nLmVuYWJsZWQgOiBjb25uV29yc2UuZW5hYmxlZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyoqIFRPRE9cclxuICAgICAgICAgKiB0aGlzIHBhcnQgaXMgbm90IHRvdGFsbHkgY2xlYXIgZm9yIG1lIC0gZnJvbSBodHRwOi8vbm4uY3MudXRleGFzLmVkdS9kb3dubG9hZHMvcGFwZXJzL3N0YW5sZXkuZWMwMi5wZGY6XHJcbiAgICAgICAgICogXCJJbiB0aGlzIGNhc2UsIGVxdWFsIGZpdG5lc3NlcyBhcmUgYXNzdW1lZCwgc28gdGhlIGRpc2pvaW50IGFuZCBleGNlc3MgZ2VuZXMgYXJlIGFsc28gaW5oZXJpdGVkIHJhbmRvbWx5LlwiXHJcbiAgICAgICAgICogSG93IGNhbiBiZSBkaXNqb2ludCBhbmQgZXhjZXNzIGdlbmVzIGluaGVycml0ZWQgcmFuZG9tbHkgd2hlbiB0aGV5IGFyZSBvbmx5IGluIG9uZSBvZiB0aGUgcGFyZW50P1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIC8vIGlmIHNhbWUgZml0bmVzc2VzLCBhbHNvIGluaGVyaXQgKGNvcHkpIGdlbmVzIGZyb20gb3RoZXIgcGFyZW50XHJcbiAgICAgICAgaWYgKHRoaXMuZml0bmVzcyA9PSBwYXJ0bmVyLmZpdG5lc3MpIHtcclxuICAgICAgICAgICAgb2Zmc3ByaW5nLm1lcmdlKHdvcnNlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGluaGVyaXQgcmFuZG9tbHkgdGhyZXNob2xkcyAtIGltYWdpbmUgYXMgYW5vdGhlciBpbiBjb25uZWN0aW9uXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gb2Zmc3ByaW5nLm5vZGVHZW5lcykge1xyXG4gICAgICAgICAgICBsZXQgbm9kZU9mZnNwcmluZzogTm9kZUdlbmUgPSBvZmZzcHJpbmcubm9kZUdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIGxldCBub2RlV29yc2U6IE5vZGVHZW5lID0gd29yc2Uubm9kZUdlbmVzW2tleV07XHJcblxyXG4gICAgICAgICAgICBpZihub2RlV29yc2UpIHtcclxuICAgICAgICAgICAgICAgIG5vZGVPZmZzcHJpbmcudGhyZXNob2xkID0gMC41ID4gTWF0aC5yYW5kb20oKSA/IG5vZGVPZmZzcHJpbmcudGhyZXNob2xkIDogbm9kZVdvcnNlLnRocmVzaG9sZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gY2hhbmNlIHRvIGVuYWJsZSBjb25uZWN0aW9uIGdlbmVzXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gb2Zmc3ByaW5nLmNvbm5lY3RHZW5lcykge1xyXG4gICAgICAgICAgICBsZXQgY29ubk9mZnNwcmluZzogQ29ubmVjdEdlbmUgPSBvZmZzcHJpbmcuY29ubmVjdEdlbmVzW2tleV07XHJcblxyXG4gICAgICAgICAgICBjb25uT2Zmc3ByaW5nLmVuYWJsZWQgPSAwLjI1ID4gTWF0aC5yYW5kb20oKSA/IHRydWUgOiBjb25uT2Zmc3ByaW5nLmVuYWJsZWQ7IC8vIFRPRE8gZWRpdGFibGUgY2hhbmNlXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gb2Zmc3ByaW5nO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBtdXRhdGUoKTogYm9vbGVhbiB7XHJcblxyXG4gICAgICAgIGxldCBpc011dGF0ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAgICAgbGV0IHRvcG9sb2d5TXV0T3B0T2YgPSBDb25maWcubXV0YXRpb25PcHRpb25zLmluZGl2aWR1YWxUb3BvbG9neTtcclxuICAgICAgICBsZXQgd2VpZ2h0c011dE9wdE9mID0gQ29uZmlnLm11dGF0aW9uT3B0aW9ucy5pbmRpdmlkdWFsV2VpZ2h0cztcclxuXHJcbiAgICAgICAgaWYodG9wb2xvZ3lNdXRPcHRPZi5jaGFuY2UgPiBNYXRoLnJhbmRvbSgpKSB7XHJcbiAgICAgICAgICAgIGxldCB4b3JDaGFuY2U6IG51bWJlcjsgLy8gMiBmb3IgZmFsc2UsIDwwOzEpIGZvciBhZGROb2RlLCA8MTsyKSBmb3IgYWRkQ29ubmVjdGlvblxyXG4gICAgICAgICAgICBpZiAodG9wb2xvZ3lNdXRPcHRPZi5hZGROb2RlWE9SYWRkQ29ubmVjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgeG9yQ2hhbmNlID0gTWF0aC5yYW5kb20oKSAqIDI7IC8vIE1hdGgucmFuZG9tKCkgbmV2ZXIgZXF1YWxzIDEgPT4geG9yQ2hhbmNlIG5ldmVyIGVxdWFscyAyIHdoaWNoIGlzIHJlc2VydmVkIGZvciB4b3JDaGFuY2UgZmFsc2VcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHhvckNoYW5jZSA9IDI7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh4b3JDaGFuY2UgPT09IDIgfHwgKHhvckNoYW5jZSA+PSAwICYmIHhvckNoYW5jZSA8IDEpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodG9wb2xvZ3lNdXRPcHRPZi5hZGROb2RlLmNoYW5jZSA+IE1hdGgucmFuZG9tKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm11dGF0ZUFkZE5vZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBpc011dGF0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoeG9yQ2hhbmNlID09PSAyIHx8ICh4b3JDaGFuY2UgPj0gMSAmJiB4b3JDaGFuY2UgPCAyKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRvcG9sb2d5TXV0T3B0T2YuYWRkQ29ubmVjdGlvbi5jaGFuY2UgPiBNYXRoLnJhbmRvbSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tdXRhdGVBZGRDb25uZWN0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNNdXRhdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYod2VpZ2h0c011dE9wdE9mLmNoYW5jZSA+IE1hdGgucmFuZG9tKCkpIHtcclxuXHJcbiAgICAgICAgICAgIGlmKHdlaWdodHNNdXRPcHRPZi53ZWlnaHRzLmNoYW5jZSA+IE1hdGgucmFuZG9tKCkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubXV0YXRlV2VpZ2h0cygpO1xyXG4gICAgICAgICAgICAgICAgaXNNdXRhdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYod2VpZ2h0c011dE9wdE9mLnRocmVzaG9sZHMuY2hhbmNlID4gTWF0aC5yYW5kb20oKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tdXRhdGVUaHJlc2hvbGRzKCk7XHJcbiAgICAgICAgICAgICAgICBpc011dGF0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9maXRuZXNzID0gbnVsbDtcclxuXHJcbiAgICAgICAgcmV0dXJuIGlzTXV0YXRlZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG11dGF0ZUFkZENvbm5lY3Rpb24oKTogdm9pZCB7XHJcblxyXG4gICAgICAgIGxldCBuMTogTm9kZUdlbmUgPSB0aGlzLmdldFJhbmRvbU5vZGVHZW5lKCk7XHJcbiAgICAgICAgbGV0IG4yOiBOb2RlR2VuZTtcclxuXHJcbiAgICAgICAgZG8geyAvLyBUT0RPIHBvc3NpYmxlIGluZmluaXRlIGxvb3BcclxuICAgICAgICAgICAgbjIgPSB0aGlzLmdldFJhbmRvbU5vZGVHZW5lKCk7XHJcbiAgICAgICAgfSB3aGlsZShuMS5pZCA9PSBuMi5pZCk7XHJcblxyXG4gICAgICAgIGlmKCF0aGlzLmFyZUNvbm5lY3RlZChuMSwgbjIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFkZENvbm5lY3Rpb24objEsbjIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm11dGF0ZUFkZENvbm5lY3Rpb24oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBtdXRhdGVBZGROb2RlKCk6IHZvaWQge1xyXG4gICAgICAgIGxldCBlZGdlOiBDb25uZWN0R2VuZTtcclxuXHJcbiAgICAgICAgZG8geyAvLyBUT0RPIHBvc3NpYmxlIGluZmluaXRlIGxvb3BcclxuICAgICAgICAgICAgZWRnZSA9IHRoaXMuZ2V0UmFuZG9tQ29ubmVjdEdlbmUoKVxyXG4gICAgICAgIH0gd2hpbGUoZWRnZS5lbmFibGVkID09IGZhbHNlKTtcclxuXHJcbiAgICAgICAgZWRnZS5lbmFibGVkID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IGluTm9kZSA9IGVkZ2UuaW5Ob2RlO1xyXG4gICAgICAgIGxldCBvdXROb2RlID0gZWRnZS5vdXROb2RlO1xyXG4gICAgICAgIGxldCBpbm5lck5vZGUgPSBuZXcgTm9kZUdlbmUoKTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGROb2RlR2VuZShpbm5lck5vZGUpO1xyXG4gICAgICAgIHRoaXMuYWRkQ29ubmVjdGlvbihpbk5vZGUsIGlubmVyTm9kZSwgMSk7XHJcbiAgICAgICAgdGhpcy5hZGRDb25uZWN0aW9uKGlubmVyTm9kZSwgb3V0Tm9kZSwgZWRnZS53ZWlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbXV0YXRlV2VpZ2h0cygpOiB2b2lkIHtcclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0aGlzLmNvbm5lY3RHZW5lcykge1xyXG4gICAgICAgICAgICBpZihDb25maWcubXV0YXRpb25PcHRpb25zLmluZGl2aWR1YWxXZWlnaHRzLndlaWdodHMubXV0YXRlU2luZ2xlLmNoYW5jZSA+IE1hdGgucmFuZG9tKCkpIHtcclxuICAgICAgICAgICAgICAgIGxldCBjb25uZWN0aW9uOiBDb25uZWN0R2VuZSA9IHRoaXMuY29ubmVjdEdlbmVzW2tleV07XHJcbiAgICAgICAgICAgICAgICBjb25uZWN0aW9uLndlaWdodCArPSBNeU1hdGgucmFuZG9tTm9ybWFsKENvbmZpZy5tdXRhdGlvbk9wdGlvbnMuaW5kaXZpZHVhbFdlaWdodHMud2VpZ2h0cy5tdXRhdGVTaW5nbGUuc3RkZXYpKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBtdXRhdGVUaHJlc2hvbGRzKCk6IHZvaWQge1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoaXMubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIGlmKENvbmZpZy5tdXRhdGlvbk9wdGlvbnMuaW5kaXZpZHVhbFdlaWdodHMudGhyZXNob2xkcy5tdXRhdGVTaW5nbGUuY2hhbmNlID4gTWF0aC5yYW5kb20oKSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IG5vZGU6IE5vZGVHZW5lID0gdGhpcy5ub2RlR2VuZXNba2V5XTtcclxuICAgICAgICAgICAgICAgIG5vZGUudGhyZXNob2xkICs9IE15TWF0aC5yYW5kb21Ob3JtYWwoQ29uZmlnLm11dGF0aW9uT3B0aW9ucy5pbmRpdmlkdWFsV2VpZ2h0cy50aHJlc2hvbGRzLm11dGF0ZVNpbmdsZS5zdGRldikoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZXZhbHVhdGVOZXR3b3JrKGlucHV0czogbnVtYmVyW10pOiBudW1iZXJbXSB7XHJcbiAgICAgICAgbGV0IG91dHB1dHM6IG51bWJlcltdID0gW107XHJcblxyXG4gICAgICAgIC8vIGV2YWx1YXRlIGlucHV0IG5vZGVzIChyZWN1cnNpb24gZW5kIGNvbmRpdGlvbilcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5pbnB1dEdlbmVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXRHZW5lc1tpXS5ldmFsdWF0ZU91dHB1dChpbnB1dHNbaV0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZXZhbHVhdGUgbmV0d29yayBmcm9tIG91dHB1dCBub2RlcyBieSByZWN1cnNpb25cclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0aGlzLm91dHB1dEdlbmVzKSB7XHJcbiAgICAgICAgICAgIG91dHB1dHMucHVzaChcclxuICAgICAgICAgICAgICAgIHRoaXMub3V0cHV0R2VuZXNba2V5XS5ldmFsdWF0ZU91dHB1dCgpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHJlc2V0IG5ldHdvcmsgZXZhbHVhdGlvbiBmb3IgbmV4dCBldmFsdWF0aW9uXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5ub2RlR2VuZXMpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlR2VuZXNba2V5XS5yZXNldE91dHB1dCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG91dHB1dHM7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRvU3RyaW5nKCk6IHN0cmluZyB7XHJcblxyXG4gICAgICAgIGxldCBub2RlR2VuZXNTdHJpbmc6IHN0cmluZyA9IFwiXCI7XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5ub2RlR2VuZXMpIHtcclxuICAgICAgICAgICAgbm9kZUdlbmVzU3RyaW5nICs9IFwiXFx0XCIgKyB0aGlzLm5vZGVHZW5lc1trZXldLnRvU3RyaW5nKCkgKyBcIjtcXG5cIjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBjb25uZWN0R2VuZXNTdHJpbmc6IHN0cmluZyA9IFwiXCI7XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgICAgIGNvbm5lY3RHZW5lc1N0cmluZyArPSBcIlxcdFwiICsgdGhpcy5jb25uZWN0R2VuZXNba2V5XS50b1N0cmluZygpICsgXCI7XFxuXCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gXCJCYXNlTmVhdEluZGl2aWR1YWwgPSB7XFxuXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJcXHRpZDogXCIgKyB0aGlzLmlkICsgXCI7XFxuXCIgK1xyXG4gICAgICAgICAgICAgICAgXCJcXHRmaXRuZXNzOiBcIiArIHRoaXMuZml0bmVzcyArIFwiO1xcblwiICtcclxuICAgICAgICAgICAgICAgIG5vZGVHZW5lc1N0cmluZyArXHJcbiAgICAgICAgICAgICAgICBjb25uZWN0R2VuZXNTdHJpbmdcclxuICAgICAgICAgICAgKyBcIn1cIjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHN0YXRpYyBub2RlR2VuZVRvVGV4dChub2RlR2VuZTogTm9kZUdlbmUpIHtcclxuICAgICAgICByZXR1cm4gYGlubm92OiAke25vZGVHZW5lLmlubm92fVxyXG4gICAgICAgICAgICAgICAgdGhyZXNob2xkOiAke25vZGVHZW5lLnRocmVzaG9sZH1gO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc3RhdGljIGNvbm5lY3RHZW5lVG9UZXh0KGNvbm5lY3RHZW5lR2VuZTogQ29ubmVjdEdlbmUpIHtcclxuICAgICAgICByZXR1cm4gYGlubm92OiAke2Nvbm5lY3RHZW5lR2VuZS5pbm5vdn1cclxuICAgICAgICAgICAgICAgIHdlaWdodDogJHtjb25uZWN0R2VuZUdlbmUud2VpZ2h0LnRvRml4ZWQoMil9XHJcbiAgICAgICAgICAgICAgICAke2Nvbm5lY3RHZW5lR2VuZS5lbmFibGVkID8gXCJcIiA6IFwiRElTQUJMRURcIn1gXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRvR09KUygpOiBPYmplY3Qge1xyXG4gICAgICAgIGxldCBqc29uIDogYW55ID0ge307XHJcbiAgICAgICAganNvbltcIm5vZGVLZXlQcm9wZXJ0eVwiXSA9IFwiaWRcIjtcclxuXHJcbiAgICAgICAganNvbltcIm5vZGVEYXRhQXJyYXlcIl0gPSBbXTtcclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0aGlzLm5vZGVHZW5lcykge1xyXG4gICAgICAgICAgICBsZXQgbm9kZUdlbmUgPXRoaXMubm9kZUdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIGpzb25bXCJub2RlRGF0YUFycmF5XCJdLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgaWQ6IG5vZGVHZW5lLmlkLFxyXG4gICAgICAgICAgICAgICAgdGV4dDogQmFzZU5lYXRJbmRpdmlkdWFsLm5vZGVHZW5lVG9UZXh0KG5vZGVHZW5lKVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGpzb25bXCJsaW5rRGF0YUFycmF5XCJdID0gW107XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IGNvbm5lY3RHZW5lID10aGlzLmNvbm5lY3RHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBqc29uW1wibGlua0RhdGFBcnJheVwiXS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGZyb206IGNvbm5lY3RHZW5lLmluTm9kZS5pZCxcclxuICAgICAgICAgICAgICAgIHRvOiBjb25uZWN0R2VuZS5vdXROb2RlLmlkLFxyXG4gICAgICAgICAgICAgICAgdGV4dDogQmFzZU5lYXRJbmRpdmlkdWFsLmNvbm5lY3RHZW5lVG9UZXh0KGNvbm5lY3RHZW5lKVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGpzb25bXCJub2RlRGF0YUFycmF5XCJdID0gW3sgXCJpZFwiOiAwLCBcImxvY1wiOiBcIjEyMCAxMjBcIiwgXCJ0ZXh0XCI6IFwiWFhYXCIgfV07XHJcbiAgICAgICAgLy8ganNvbltcImxpbmtEYXRhQXJyYXlcIl0gPSBbeyBcImZyb21cIjogMCwgXCJ0b1wiOiAwLCBcInRleHRcIjogXCJ1cCBvciB0aW1lclwiLCBcImN1cnZpbmVzc1wiOiAtMjAgfV07XHJcblxyXG4gICAgICAgIHJldHVybiBqc29uO1xyXG4gICAgfVxyXG5cclxuICAgIC8vXHJcbiAgICAvLyBHRVQgZnVuY3Rpb25zXHJcbiAgICAvL1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGNvbXBsZXhpdHkgTygjTkcpXHJcbiAgICAgKiBUT0RPIG9mdGVuIGZ1bmN0aW9uIGFuZCBPKG4pIGNvbXBsZXhpdHksIGNvdWxkIGJlIGVsaW1pbmF0ZWQgaWYgYXJyYXkgb2YgTm9kZUdlbmVzIGlzIHVzZWQsIGJ1dCBob3cgYWJvdXQgb3RoZXIgZnVuY3Rpb25zIChjcm9zc292ZXIpLCBwcm9iYWJseSBlYXN5IGJ5IGFycmF5IGZ1bmN0aW9uc1xyXG4gICAgICogcGljayByYW5kb20gZnJvbSBzdHJlYW0gLSBjb21wbGV4aXR5IE8obilcclxuICAgICAqIHByb2JsZW0gdGhhdCBvYmplY3QgaXMgdXNlZCBub3QgYXJyYXlcclxuICAgICAqIHNpemUgd2lsbCBub3QgaGVscFxyXG4gICAgICogQHNlZSBuZXh0IHNvbHV0aW9uXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZ2V0UmFuZG9tTm9kZUdlbmUoKTogTm9kZUdlbmUge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IE5vZGVHZW5lO1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIGlmIChNYXRoLnJhbmRvbSgpIDwgMSAvICsrY291bnQpIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMubm9kZUdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvLyAvKipcclxuICAgIC8vICAqIFRPRE8gcXVlc3Rpb24gaXMgaWYgT2JqZWN0LmtleXMoKSBpcyBPKDEpIG9yIE8obilcclxuICAgIC8vICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICAvLyAgKi9cclxuICAgIC8vIHByaXZhdGUgZ2V0UmFuZG9tTm9kZUdlbmVYWFgoKSB7XHJcbiAgICAvLyAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyh0aGlzLm5vZGVHZW5lcyk7IC8vIE8obikgc28gbWFrZXMgbm8gc2VuY2VcclxuICAgIC8vICAgICByZXR1cm4gdGhpcy5ub2RlR2VuZXNba2V5c1sga2V5cy5sZW5ndGggKiBNYXRoLnJhbmRvbSgpIDw8IDBdXTtcclxuICAgIC8vIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRPRE8gc2FtZSBwcm9ibGVtIGFzIGZvciBnZXRSYW5kb21OZGVHZW5lXHJcbiAgICAgKiBAY29tcGxleGl0eSBPKCNDRylcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBnZXRSYW5kb21Db25uZWN0R2VuZSgpOiBDb25uZWN0R2VuZSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogQ29ubmVjdEdlbmU7XHJcbiAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuICAgICAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgaWYgKE1hdGgucmFuZG9tKCkgPCAxIC8gKytjb3VudCkge1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5jb25uZWN0R2VuZXNba2V5XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGNvbXBsZXhpdHkgTygjQ0cpIGJ1dCBwcm9iYWJseSBjYW5ub3QgYmUgYmV0dGVyXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgYXJlQ29ubmVjdGVkKG4xOiBOb2RlR2VuZSwgbjI6IE5vZGVHZW5lKSB7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLmNvbm5lY3RHZW5lcykge1xyXG4gICAgICAgICAgICAvLyBjaGVja1xyXG4gICAgICAgICAgICBsZXQgZWRnZTogQ29ubmVjdEdlbmUgPSB0aGlzLmNvbm5lY3RHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBpZiAoZWRnZS5pbk5vZGUuaWQgPT0gbjEuaWQgJiYgZWRnZS5vdXROb2RlLmlkID09IG4yLmlkXHJcbiAgICAgICAgICAgICAgICB8fCBlZGdlLmluTm9kZS5pZCA9PSBuMi5pZCAmJiBlZGdlLm91dE5vZGUuaWQgPT0gbjEuaWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBjb21wYXJlKGZpcnN0OiBCYXNlTmVhdEluZGl2aWR1YWwsIHNlY29uZDogQmFzZU5lYXRJbmRpdmlkdWFsKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gc2Vjb25kLmZpdG5lc3MgLSBmaXJzdC5maXRuZXNzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBkaXN0YW5jZVRvKHRoYXQ6IEJhc2VOZWF0SW5kaXZpZHVhbCkge1xyXG4gICAgICAgIGxldCBjX2UgPSBDb25maWcuc3BlY2lhdGlvbi5leGNlc3NDb2VmO1xyXG4gICAgICAgIGxldCBjX2QgPSBDb25maWcuc3BlY2lhdGlvbi5kaXNqb2ludENvZWY7XHJcbiAgICAgICAgbGV0IGNfbSA9IENvbmZpZy5zcGVjaWF0aW9uLm1hdGNoaW5nQ29lZjtcclxuICAgICAgICBsZXQgTiA9IDA7IC8vICNnZW5lcyBpbiBsb25nZXJcclxuICAgICAgICBsZXQgRCA9IDA7IC8vICNnZW5lcyBkaXNqb2ludFxyXG4gICAgICAgIGxldCBFID0gMDsgLy8gI2dlbmVzIGV4Y2Vzc1xyXG4gICAgICAgIGxldCBXID0gMDsgLy8gYXZlcmFnZSB3ZWlnaHQgZGlmZmVyZW5jZXMgb2YgbWF0Y2hpbmcgZ2VuZXMgV1xyXG5cclxuICAgICAgICBsZXQgTSA9IDA7IC8vICNnZW5lcyBtYXRjaGluZ1xyXG5cclxuICAgICAgICBsZXQgdGhpc0FycmF5ID0gdGhpcy5nZXRDb25uZWN0R2VuZXNBc0FycmF5KCk7XHJcbiAgICAgICAgbGV0IHRoYXRBcnJheSA9IHRoYXQuZ2V0Q29ubmVjdEdlbmVzQXNBcnJheSgpO1xyXG5cclxuICAgICAgICBsZXQgaSA9IDA7XHJcbiAgICAgICAgbGV0IGogPSAwO1xyXG4gICAgICAgIHdoaWxlKGkgPCB0aGlzQXJyYXkubGVuZ3RoICYmIGogPCB0aGF0QXJyYXkubGVuZ3RoKSB7IC8vIGVuZCB3aGVuIGF0IHRoZSBlbmQgb2Ygb25lXHJcbiAgICAgICAgICAgIGxldCB0aGlzQ29ubiA9IHRoaXNBcnJheVtpXTtcclxuICAgICAgICAgICAgbGV0IHRoYXRDb25uID0gdGhhdEFycmF5W2pdO1xyXG5cclxuICAgICAgICAgICAgaWYodGhpc0Nvbm4uaW5ub3YgPT0gdGhhdENvbm4uaW5ub3YpIHsgLy8gc2FtZVxyXG4gICAgICAgICAgICAgICAgVyArPSBNYXRoLmFicyh0aGlzQ29ubi53ZWlnaHQgLSB0aGF0Q29ubi53ZWlnaHQpO1xyXG5cclxuICAgICAgICAgICAgICAgIE0rKztcclxuICAgICAgICAgICAgICAgIGkrKztcclxuICAgICAgICAgICAgICAgIGorKztcclxuICAgICAgICAgICAgfSBlbHNlIHsgLy8gZGlzam9pbnRcclxuXHJcbiAgICAgICAgICAgICAgICBEKys7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzQ29ubi5pbm5vdiA8IHRoYXRDb25uLmlubm92KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaisrO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlICB7XHJcbiAgICAgICAgICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICBFID0gaSA8IHRoaXNBcnJheS5sZW5ndGggPyB0aGlzQXJyYXkubGVuZ3RoIC0gaSA6IChqIDwgdGhhdEFycmF5Lmxlbmd0aCA/IHRoYXRBcnJheS5sZW5ndGggLSBqOiAwKTsgLy8gZXhjZXNzXHJcbiAgICAgICAgTiA9IHRoaXNBcnJheS5sZW5ndGggPiB0aGF0QXJyYXkubGVuZ3RoID8gdGhpc0FycmF5Lmxlbmd0aCA6IHRoYXRBcnJheS5sZW5ndGg7XHJcblxyXG4gICAgICAgIC8vIGNvdW50IHRocmVzaG9sZHMgYWxzbyBhcyBpbnB1dCBjb25uZWN0aW9uXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5ub2RlR2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IHRoaXNOb2RlID0gdGhpcy5ub2RlR2VuZXNba2V5XTtcclxuICAgICAgICAgICAgbGV0IHRoYXROb2RlID0gdGhhdC5ub2RlR2VuZXNba2V5XTtcclxuXHJcbiAgICAgICAgICAgIGlmKHRoYXROb2RlKSB7IC8vIHNhbWVcclxuICAgICAgICAgICAgICAgIFcgKz0gTWF0aC5hYnModGhpc05vZGUudGhyZXNob2xkIC0gdGhhdE5vZGUudGhyZXNob2xkKTtcclxuICAgICAgICAgICAgICAgIE0rKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgVyA9IFcgLyBNO1xyXG5cclxuICAgICAgICByZXR1cm4gKGNfZSpFKS9OICsgKGNfZCpEKS9OICsgY19tKlc7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8vXHJcbiAgICAvLyBHRVRURVJTIGFuZCBTRVRURVJTXHJcbiAgICAvL1xyXG5cclxuICAgIHB1YmxpYyBnZXQgaWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lkO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgZml0bmVzcygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9maXRuZXNzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgbm9kZUdlbmVzKCk6IE5vZGVHZW5lcyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dlbm9tZVswXTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IGNvbm5lY3RHZW5lcygpOiBDb25uZWN0R2VuZXMge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nZW5vbWVbMV07XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldENvbm5lY3RHZW5lc0FzQXJyYXkoKTogQXJyYXk8Q29ubmVjdEdlbmU+IHtcclxuICAgICAgICBsZXQgcmVzOiBBcnJheTxDb25uZWN0R2VuZT4gPSBbXTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMuY29ubmVjdEdlbmVzKSB7XHJcbiAgICAgICAgICAgIHJlcy5wdXNoKHRoaXMuY29ubmVjdEdlbmVzW2tleV0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlcy5zb3J0KChhLCBiKSA9PiB7cmV0dXJuIGIuaW5ub3YgLSBhLmlubm92fSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgaW5wdXRHZW5lcygpOiBBcnJheTxOb2RlR2VuZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pbnB1dEdlbmVzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IG91dHB1dEdlbmVzKCk6IEFycmF5PE5vZGVHZW5lPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX291dHB1dEdlbmVzO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBCYXNlTmVhdEluZGl2aWR1YWw7IiwiaW1wb3J0IE5vZGVHZW5lIGZyb20gXCIuL05vZGVHZW5lXCI7XHJcblxyXG5leHBvcnQgdHlwZSBDb25uZWN0R2VuZXMgPSB7IFtrZXk6IG51bWJlcl06IENvbm5lY3RHZW5lOyB9O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29ubmVjdEdlbmUge1xyXG5cclxuICAgIHByaXZhdGUgX2lkOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHN0YXRpYyBfaWRDb3VudGVyOiBudW1iZXIgPSAwO1xyXG4gICAgcHJpdmF0ZSBfaW5ub3Y6IG51bWJlcjtcclxuICAgIHByaXZhdGUgc3RhdGljIF9pbm5vdkNvdW50ZXI6IG51bWJlciA9IDA7XHJcbiAgICBwdWJsaWMgZW5hYmxlZDogYm9vbGVhbjtcclxuICAgIC8vIHB1YmxpYyBpbk5vZGU6IE5vZGVHZW5lO1xyXG4gICAgLy8gcHVibGljIG91dE5vZGU6IE5vZGVHZW5lO1xyXG4gICAgLy8gcHVibGljIHdlaWdodDogbnVtYmVyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBpbk5vZGU6IE5vZGVHZW5lLCBwdWJsaWMgb3V0Tm9kZTogTm9kZUdlbmUsIHB1YmxpYyB3ZWlnaHQ6IG51bWJlciwgZW5hYmxlZD86IGJvb2xlYW4sIGlubm92PzogbnVtYmVyLCApIHtcclxuICAgICAgICB0aGlzLl9pZCA9ICsrQ29ubmVjdEdlbmUuX2lkQ291bnRlcjtcclxuICAgICAgICB0aGlzLl9pbm5vdiA9IHR5cGVvZiBpbm5vdiAhPSBcInVuZGVmaW5lZFwiID8gaW5ub3YgOiArK0Nvbm5lY3RHZW5lLl9pbm5vdkNvdW50ZXI7XHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gdHlwZW9mIGVuYWJsZWQgIT0gXCJ1bmRlZmluZWRcIiA/IGVuYWJsZWQ6IHRydWU7XHJcbiAgICAgICAgdGhpcy5vdXROb2RlLmFkZEluQ29ubmVjdGlvbih0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdG9TdHJpbmcoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gXCJDb25uZWNHZW5lID0ge2lkOiBcIiArIHRoaXMuaWQgKyBcIjsgaW5ub3Y6IFwiICsgdGhpcy5pbm5vdiArIFwiOyBpbjogXCIgKyB0aGlzLmluTm9kZS5pZCArIFwiOyBvdXQ6IFwiICsgdGhpcy5vdXROb2RlLmlkICsgXCI7IHdlaWdodDogXCIgKyB0aGlzLndlaWdodC50b0ZpeGVkKDIpICsgXCI7IGVuYWJsZWQ6IFwiICsgdGhpcy5lbmFibGVkICsgXCJ9XCI7XHJcbiAgICB9XHJcblxyXG4gICAgLy9cclxuICAgIC8vIEdFVFRFUlMgYW5kIFNFVFRFUlNcclxuICAgIC8vXHJcblxyXG4gICAgcHVibGljIGdldCBpZCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IGlubm92KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lubm92O1xyXG4gICAgfVxyXG5cclxufSIsImltcG9ydCBDb25uZWN0R2VuZSBmcm9tIFwiLi9Db25uZWN0R2VuZVwiO1xyXG5cclxuZXhwb3J0IHR5cGUgTm9kZUdlbmVzID0geyBba2V5OiBudW1iZXJdOiBOb2RlR2VuZTsgfTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE5vZGVHZW5lIHtcclxuXHJcbiAgICBwcml2YXRlIF9pZDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2lkQ291bnRlcjogbnVtYmVyID0gMDtcclxuICAgIHByaXZhdGUgX2lubm92OiBudW1iZXI7IC8vIG5vdCBuZWNlc3NhcnksIGJldHRlciBmb3Igdml6dWFsaXphdGlvbiBhbmQgdHJhY2tpbmcgY29weSBvZiB0aGlzIG5vZGVcclxuICAgIHByaXZhdGUgc3RhdGljIF9pbm5vdkNvdW50ZXI6IG51bWJlciA9IDA7XHJcblxyXG4gICAgcHJvdGVjdGVkIF90eXBlOiBOb2RlR2VuZVR5cGU7XHJcbiAgICBwcm90ZWN0ZWQgX3N0YXRlOiBOb2RlR2VuZVN0YXRlO1xyXG5cclxuICAgIHByb3RlY3RlZCBfaW5Db25uZWN0aW9uczogQ29ubmVjdEdlbmVbXTtcclxuICAgIHByb3RlY3RlZCBfYWN0aXZhdGlvbkZ1bmM6IGFjdEZ1bmNUeXBlO1xyXG4gICAgcHVibGljIG91dHB1dDogbnVtYmVyO1xyXG4gICAgcHVibGljIGxhc3RPdXRwdXQ6IG51bWJlcjtcclxuICAgIHB1YmxpYyB0aHJlc2hvbGQ6IG51bWJlcjsgLy8gYmlhc1xyXG5cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihub2RlR2VuZTogTm9kZUdlbmUpO1xyXG4gICAgY29uc3RydWN0b3IodHlwZT86IE5vZGVHZW5lVHlwZSwgYWN0aXZhdGlvbkZ1bmM/OiBhY3RGdW5jVHlwZSk7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLl9pZCA9ICsrTm9kZUdlbmUuX2lkQ291bnRlcjtcclxuXHJcbiAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT0gMSAmJiBhcmd1bWVudHNbMF0gaW5zdGFuY2VvZiBOb2RlR2VuZSkge1xyXG4gICAgICAgICAgICB0aGlzLmNvcHlDb25zdHJ1Y3Rvcihhcmd1bWVudHNbMF0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9ybWFsQ29uc3RydWN0b3IoYXJndW1lbnRzWzBdLCBhcmd1bWVudHNbMV0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNvcHlDb25zdHJ1Y3Rvcih0aGF0OiBOb2RlR2VuZSkge1xyXG4gICAgICAgIC8vIGRvZXMgbm90IGNvcHkgY29ubmVjdEdlbmVzIC0gY29waWVkIGZyb20gY2FsbGVyXHJcbiAgICAgICAgdGhpcy5faW5ub3YgPSB0aGF0Ll9pbm5vdjtcclxuICAgICAgICB0aGlzLl9pbkNvbm5lY3Rpb25zID0gW107XHJcblxyXG4gICAgICAgIHRoaXMudGhyZXNob2xkID0gdGhhdC50aHJlc2hvbGQ7XHJcbiAgICAgICAgdGhpcy5vdXRwdXQgPSB0aGF0Lm91dHB1dDtcclxuICAgICAgICB0aGlzLmxhc3RPdXRwdXQgPSB0aGF0Lmxhc3RPdXRwdXQ7XHJcblxyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gdGhhdC5fc3RhdGU7XHJcbiAgICAgICAgdGhpcy5fdHlwZSA9IHRoYXQuX3R5cGU7XHJcblxyXG4gICAgICAgIHRoaXMuX2FjdGl2YXRpb25GdW5jID0gdGhhdC5fYWN0aXZhdGlvbkZ1bmM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBub3JtYWxDb25zdHJ1Y3Rvcih0eXBlPzogTm9kZUdlbmVUeXBlLCBhY3RpdmF0aW9uRnVuYz86IGFjdEZ1bmNUeXBlKSB7XHJcbiAgICAgICAgdGhpcy5faW5ub3YgPSArK05vZGVHZW5lLl9pbm5vdkNvdW50ZXI7XHJcbiAgICAgICAgdGhpcy5faW5Db25uZWN0aW9ucyA9IFtdO1xyXG5cclxuICAgICAgICB0aGlzLnRocmVzaG9sZCA9IDA7XHJcbiAgICAgICAgdGhpcy5vdXRwdXQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMubGFzdE91dHB1dCA9IDA7XHJcblxyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gTm9kZUdlbmVTdGF0ZS5OZXc7XHJcbiAgICAgICAgdGhpcy5fdHlwZSA9IHR5cGVvZiB0eXBlICE9PSBcInVuZGVmaW5lZFwiID8gYXJndW1lbnRzWzBdIDogTm9kZUdlbmVUeXBlLkhpZGRlbjtcclxuXHJcbiAgICAgICAgdGhpcy5fYWN0aXZhdGlvbkZ1bmMgPSB0eXBlb2YgYWN0aXZhdGlvbkZ1bmMgIT09IFwidW5kZWZpbmVkXCIgPyBhY3RpdmF0aW9uRnVuYyA6IHNvZnRTdGVwO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhZGRJbkNvbm5lY3Rpb24oaW5Db25uZWN0aW9uOiBDb25uZWN0R2VuZSkge1xyXG4gICAgICAgIHRoaXMuaW5Db25uZWN0aW9ucy5wdXNoKGluQ29ubmVjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGV2YWx1YXRlT3V0cHV0KGlucHV0PzogbnVtYmVyKTogbnVtYmVyIHtcclxuICAgICAgICBpZih0aGlzLnN0YXRlID09IE5vZGVHZW5lU3RhdGUuT3BlbikgeyAvLyByZWN1cnJlbmNlXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmxhc3RPdXRwdXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZSA9PSBOb2RlR2VuZVN0YXRlLkNsb3NlZCkgeyAvLyBhbHJlYWR5IGV2YWx1YXRlZFxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5vdXRwdXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBvdGhlciAtIGV2YWx1YXRlIHJlY3Vyc2l2ZWx5XHJcbiAgICAgICAgdGhpcy5fc3RhdGUgPSBOb2RlR2VuZVN0YXRlLk9wZW47XHJcblxyXG4gICAgICAgIGxldCBzdW06IG51bWJlciA9IDA7XHJcblxyXG4gICAgICAgIC8vIGlucHV0IG5vZGVzIC0gc3VtIG92ZXIgaW5wdXRzLCB0aGVuIG92ZXIgcmVjdXJyZW50IGNvbm5lY3Rpb25zXHJcbiAgICAgICAgaWYgKHRoaXMudHlwZSA9PSBOb2RlR2VuZVR5cGUuSW5wdXQgJiYgYXJndW1lbnRzLmxlbmd0aCA9PSAxKSB7XHJcbiAgICAgICAgICAgIHN1bSArPSBpbnB1dDtcclxuXHJcbiAgICAgICAgICAgIHN1bSArPSB0aGlzLmluQ29ubmVjdGlvbnMucmVkdWNlKFxyXG4gICAgICAgICAgICAgICAgKHN1bTogbnVtYmVyLCBjdXJyZW50OiBDb25uZWN0R2VuZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50LmVuYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1bSArIGN1cnJlbnQuaW5Ob2RlLmxhc3RPdXRwdXQgKiBjdXJyZW50LndlaWdodDsgLy8gTk8gcmVjdXJzaW9uIC0gcmVjdXJyZW50IGNvbm5lY3Rpb25cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VtO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIDApO1xyXG5cclxuICAgICAgICAgICAgc3VtICs9IHRoaXMudGhyZXNob2xkO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIGhpZGRlbiwgb3V0cHV0IG5vZGVzIC0gc3VtIG92ZXIgaW4gY29ubmVjdGlvbnNcclxuICAgICAgICAgICAgc3VtICs9IHRoaXMuaW5Db25uZWN0aW9ucy5yZWR1Y2UoXHJcbiAgICAgICAgICAgICAgICAoc3VtOiBudW1iZXIsIGN1cnJlbnQ6IENvbm5lY3RHZW5lKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGN1cnJlbnQuZW5hYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VtICsgY3VycmVudC5pbk5vZGUuZXZhbHVhdGVPdXRwdXQoKSAqIGN1cnJlbnQud2VpZ2h0OyAvLyByZWN1cnNpb25cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3VtO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgICAgICBzdW0gKz0gdGhpcy50aHJlc2hvbGQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IE5vZGVHZW5lU3RhdGUuQ2xvc2VkO1xyXG4gICAgICAgIHRoaXMub3V0cHV0ID0gdGhpcy5hY3RpdmF0aW9uRnVuYyhzdW0pO1xyXG4gICAgICAgIHJldHVybiB0aGlzLm91dHB1dDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVzZXRPdXRwdXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5sYXN0T3V0cHV0ID0gdGhpcy5vdXRwdXQ7XHJcbiAgICAgICAgdGhpcy5vdXRwdXQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gTm9kZUdlbmVTdGF0ZS5OZXc7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRvU3RyaW5nKCkge1xyXG4gICAgICAgIHJldHVybiBcIk5vZGVHZW5lID0ge2lkOiBcIiArIHRoaXMuaWQgKyBcIjsgaW5ub3Y6IFwiICsgdGhpcy5pbm5vdiArIFwiOyB0eXBlOlwiICsgdGhpcy50eXBlICsgXCI7IHRocmVzaG9sZDogXCIgKyB0aGlzLnRocmVzaG9sZC50b0ZpeGVkKDIpICsgXCJ9XCI7XHJcbiAgICB9XHJcblxyXG4gICAgLy9cclxuICAgIC8vIEdFVFRFUlMgYW5kIFNFVFRFUlNcclxuICAgIC8vXHJcblxyXG4gICAgcHVibGljIGdldCBpZCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IGlubm92KCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lubm92O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgdHlwZSgpOiBOb2RlR2VuZVR5cGUge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90eXBlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgYWN0aXZhdGlvbkZ1bmMoKTogYWN0RnVuY1R5cGUge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hY3RpdmF0aW9uRnVuYztcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgZ2V0IGluQ29ubmVjdGlvbnMoKTogQ29ubmVjdEdlbmVbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luQ29ubmVjdGlvbnM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldCBzdGF0ZSgpOiBOb2RlR2VuZVN0YXRlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdGU7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIE5vZGVHZW5lVHlwZSB7XHJcbiAgICBJbnB1dCxcclxuICAgIEhpZGRlbixcclxuICAgIE91dHB1dFxyXG59XHJcblxyXG5leHBvcnQgZW51bSBOb2RlR2VuZVN0YXRlIHtcclxuICAgIE5ldyxcclxuICAgIE9wZW4sXHJcbiAgICBDbG9zZWRcclxufVxyXG5cclxuLy8gVE9ETyBzb21laG93IGJldHRlciwgb3IgY2xhc3MgYW5kIHN0YXRpYyBmaWVsZHlcclxuZXhwb3J0IGVudW0gTm9kZUdlbmVBY3RGdW5jIHtcclxuXHJcbn1cclxuZXhwb3J0IHR5cGUgYWN0RnVuY1R5cGUgPSAoeDogbnVtYmVyKSA9PiBudW1iZXI7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc29mdFN0ZXAoeDogbnVtYmVyKTogbnVtYmVyIHtcclxuICAgIHJldHVybiAxIC8gKDEgKyBNYXRoLmV4cCgteCkpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gYmluYXJ5U3RlcCh4OiBudW1iZXIpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHggPj0gMCA/IDEgOiAwO1xyXG59IiwiaW1wb3J0IHtJbmRpdmlkdWFscywgQmFzZU5lYXRJbmRpdmlkdWFsfSBmcm9tIFwiLi9CYXNlTmVhdEluZGl2aWR1YWxcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNwZWNpZXMge1xyXG5cclxuICAgIHByaXZhdGUgX2lkOiBudW1iZXI7XHJcbiAgICBwcm90ZWN0ZWQgc3RhdGljIF9pZENvdW50ZXI6IG51bWJlciA9IDA7XHJcbiAgICBwdWJsaWMgaW5kaXZpZHVhbHM6IEluZGl2aWR1YWxzO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGluZGl2aWR1YWxzPzogSW5kaXZpZHVhbHMpIHtcclxuICAgICAgICB0aGlzLl9pZCA9ICsrU3BlY2llcy5faWRDb3VudGVyO1xyXG4gICAgICAgIHRoaXMuaW5kaXZpZHVhbHMgPSB0eXBlb2YgaW5kaXZpZHVhbHMgIT0gXCJ1bmRlZmluZWRcIiA/IGluZGl2aWR1YWxzIDogW107XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFkZChpbmRpdmlkdWFsOiBCYXNlTmVhdEluZGl2aWR1YWwpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmluZGl2aWR1YWxzLnB1c2goaW5kaXZpZHVhbCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBrZWVwcyBzaW5nbGUgcmFuZG9tIHJlcHJlc2VudGF0aXZlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBjbGVhcigpOiBib29sZWFuIHtcclxuXHJcbiAgICAgICAgbGV0IHJlcHJlc2VudGF0aXZlOiBCYXNlTmVhdEluZGl2aWR1YWwgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMuaW5kaXZpZHVhbHMpIHtcclxuICAgICAgICAgICAgaWYoIXRoaXMuaW5kaXZpZHVhbHNba2V5XS5lbGltaW5hdGVkKSB7IC8vIHNraXAgZWxpbWluYXRlZFxyXG4gICAgICAgICAgICAgICAgaWYgKE1hdGgucmFuZG9tKCkgPCAxIC8gKytjb3VudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlcHJlc2VudGF0aXZlID0gdGhpcy5pbmRpdmlkdWFsc1trZXldO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZihyZXByZXNlbnRhdGl2ZSA9PSBudWxsKSB7IC8vIG5vIGluZGl2aWR1YWxzIGluIHNwZWNpZXNcclxuICAgICAgICAgICAgdGhpcy5pbmRpdmlkdWFscyA9IFtdO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHsgLy8ga2VlcCByZXByZXNlbnRhdGl2ZVxyXG4gICAgICAgICAgICB0aGlzLmluZGl2aWR1YWxzID0gW3JlcHJlc2VudGF0aXZlXTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogYXZlcmFnZSBmaXRuZXNzIG92ZXIgc3BlY2llc1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0U2hhcmVkRml0bmVzcygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmluZGl2aWR1YWxzLnJlZHVjZSgoc3VtLCBjdXJyKSA9PiB7IHJldHVybiBzdW0gKyBjdXJyLmZpdG5lc3M7IH0sIDApIC8gdGhpcy5pbmRpdmlkdWFscy5sZW5ndGg7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGlzRW1wdHkoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5kaXZpZHVhbHMubGVuZ3RoID09IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCByZXByZXNlbnRhdGl2ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbmRpdmlkdWFsc1swXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaWQoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XHJcbiAgICB9XHJcbn1cclxuIl19
