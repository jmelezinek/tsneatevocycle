// ==UserScript==
// @name         lib.slitherio.js
// @namespace    http://neat.melezinek.cz/
// @version      0.1
// @description  tsneatevocycle distribution for slither.io
// @author       Jakub Melezinek (melezjak)
// @match        http://slither.io/
// @grant        none
// @run-at       document-end
// ==/UserScript==

// after DOM loaded
$(function(){

    /* jshint ignore:start */ // necessary fot Tampermonkey due its too strict "compiler"
    (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
class Config {
}
Config.log = {
    functions: false,
    cycle: false,
    generations: false,
    nextFitness: false,
};
Config.cycle = {
    continue: false,
    running: false
};
Config.general = {
    alwaysEvaluateFitness: false
};
Config.mutationOptions = {
    mutateOffsprings: true,
    mutateByCloning: true,
    individualTopology: {
        chance: 0.33,
        addNode: {
            chance: 1
        },
        addConnection: {
            chance: 1
        },
        addNodeXORaddConnection: true,
    },
    individualWeights: {
        chance: 0.33,
        weights: {
            chance: 1,
            mutateSingle: {
                chance: 0.8,
                stdev: 1
            }
        },
        thresholds: {
            chance: 1,
            mutateSingle: {
                chance: 0.8,
                stdev: 1
            }
        }
    }
};
Config.crossoverOptions = {
    offspringRatio: 0.33,
    tournamentRatio: 0.5,
    geneDisabled: {
        chance: 0.75
    }
};
/**
 * distance function coefficient
 * d = (c_e*E)/N + (c_d*D)/N + c_m*W;
 * 3.0 = 1.0 ...... 1.0 ...... 0.4 - values from NEAT paper capter 4.1
 */
Config.speciation = {
    excessCoef: 1,
    disjointCoef: 1,
    matchingCoef: 0.4,
    distanceThreshold: 0.8
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Config;

},{}],2:[function(require,module,exports){
"use strict";
const Config_1 = require("./Config");
const Species_1 = require("./model/Species");
const Individual_1 = require("./model/Individual");
/**
 * @class
 * @description Main library class
 */
class EvoCycle {
    constructor(individuals) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.constructor");
        }
        this.config = Config_1.default;
        this._generationCounter = 0;
        this._observers = [];
        this._population = individuals;
        this._species = [new Species_1.default(individuals)];
        this._parents = [];
        this._offsprings = [];
        this._mutants = [];
    }
    //////////////////// EVOLUTION FUNCTIONS ////////////////////
    continue(singleGeneration = false) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.continue - " + "running: " + Config_1.default.cycle.running + "; continue: " + Config_1.default.cycle.continue + "; singleGeneration: " + singleGeneration);
        }
        if (!this._population) {
            throw "EvoCycle has to be inicialized first with init population! See EvoCycle.init(individuals: Individuals)";
        }
        if (Config_1.default.cycle.running) {
            console.warn("EvoCycle.continue is already running");
            return; // prevent multiple call
        }
        if (!singleGeneration && !Config_1.default.cycle.continue) {
            console.warn("EvoCycle.continue will not continue EvoCycle.config.cycle.continue flag is false");
            return; // do not continue
        }
        Config_1.default.cycle.running = true;
        this.doReduction().then((population) => {
            if (Config_1.default.log.functions) {
                console.log("EvoCycle.continue doReduction-then");
            }
            if (Config_1.default.log.generations) {
                console.log("GENERATION");
                EvoCycle.print(this._population);
            }
            this.step(); // continue with cycle
        });
    }
    // step whole new generation
    step() {
        if (Config_1.default.log.cycle) {
            console.log("population - NEXT STEP:");
            console.dir(this._population);
            EvoCycle.print(this._population);
        }
        // classify individuals
        this.doSpeciation();
        if (Config_1.default.log.cycle) {
            console.log("species - doSpeciation:");
            console.dir(this._species);
        }
        // select individuals to be parents for breeding
        this.doSelection();
        if (Config_1.default.log.cycle) {
            console.log("parents - doSelection:");
            console.dir(this._parents); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this._parents.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // generate new individuals by crossover
        this.doCrossover();
        if (Config_1.default.log.cycle) {
            console.log("offsprings - doCrossover:");
            console.dir(this._offsprings); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this._offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // change individuals by mutations
        this.doMutation();
        if (Config_1.default.log.cycle) {
            console.log("population and offsprings - doMutation:");
            console.dir(this._population);
            console.dir(this._offsprings);
            EvoCycle.print(this._population);
            EvoCycle.print(this._offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        Config_1.default.cycle.running = false;
        this.continue();
    }
    filterDuplicates(individials) {
        return individials.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        });
    }
    doSpeciation() {
        // random species representatives
        let i = this._species.length;
        while (i--) {
            if (this._species[i].clear() == false) {
                this._species.splice(i, 1); // remove this species
            }
        }
        // remove species which can be merged
        // this is not in specification but sounds like a good idea
        // i = this.species.length;
        // while(i-- > 1) { // WARNING iterating in reverse because of splice (removing items)
        //     if(this.species[i].representative.distanceTo(this.species[i-1].representative) < Config.speciation.distanceThreshold) {
        //         this.species[i].individuals = []; // empty the species // still in population so it will be classify again
        //         this.species.splice(i, 1); // remove this species
        //     }
        // }
        // classify individuals
        for (let key in this._population) {
            let indiv = this._population[key];
            let placed = false;
            for (let i = 0; i < this._species.length; i++) {
                if (indiv == this._species[i].representative) {
                    placed = true;
                    break;
                }
            }
            if (placed) {
                continue;
            }
            for (let i = 0; i < this._species.length; i++) {
                // if (indiv != this.species[i].representative)
                // place in existing class
                if (indiv.constructor.distance(indiv, this._species[i].representative) < Config_1.default.speciation.distanceThreshold) {
                    this._species[i].add(indiv);
                    placed = true;
                    break;
                }
            }
            if (placed) {
                continue;
            }
            else {
                this._species.push(new Species_1.default([indiv]));
            }
        }
        this._observers.forEach((observer) => {
            observer.notifyDoneSpeciation(this, this._species);
        });
    }
    /**
     * Simple tournament selection implementation.
     * Randomly chooses (with repetition) k individuals and picks the best one of the tournament. Repeats until whole population is filled.
     */
    doSelection() {
        // how many offsprings to generate
        let offspringSize = Math.round(this._population.length * Config_1.default.crossoverOptions.offspringRatio);
        // to know how many from each species
        let sumSharedFittness = this._species.reduce((sum, curr) => {
            return sum + curr.getSharedFitness();
        }, 0);
        this._parents = [];
        for (let key in this._species) {
            let spec = this._species[key];
            let specSize = spec.individuals.length;
            let specOffspringSize = Math.round(spec.getSharedFitness() / sumSharedFittness * offspringSize);
            specOffspringSize = specOffspringSize > 0 ? specOffspringSize : 1; // atleast one offspring
            let tournamentSize = Math.round(specSize * Config_1.default.crossoverOptions.tournamentRatio);
            tournamentSize = tournamentSize < 1 ? 1 : tournamentSize;
            tournamentSize = tournamentSize > specSize ? specSize : tournamentSize;
            // by tournament selection select as many individuals
            // as it is needed for crossover (2 times as much as offspring size)
            let winners = this._parents[key] = [];
            while (winners.length < specOffspringSize * 2) {
                // select k random individuals (with repetition)
                let contestants = [];
                while (contestants.length < tournamentSize) {
                    let r = Math.floor(Math.random() * specSize);
                    contestants.push(spec.individuals[r]);
                }
                // sort them and select first as winner
                contestants.sort(Individual_1.default.compare);
                winners.push(contestants[0]);
            }
        }
        this._observers.forEach((observer) => {
            observer.notifyDoneSelection(this, this.parents);
        });
    }
    /**
     * Crossover
     */
    doCrossover() {
        this._offsprings = [];
        for (let key in this._parents) {
            let parents = this._parents[key];
            let offsprings = this._offsprings[key] = [];
            for (let i = 0; i < this._parents[key].length; i += 2) {
                offsprings.push(parents[i].breed(parents[i + 1]));
            }
        }
        this._observers.forEach((observer) => {
            observer.notifyDoneCrossover(this, this.offsprings);
        });
    }
    doMutation() {
        this._mutants = [];
        for (let key in this._population) {
            let indiv = this._population[key];
            if (Config_1.default.mutationOptions.mutateByCloning) {
                let clone = new indiv.constructor(indiv);
                let wasMutated = clone.mutate();
                if (wasMutated) {
                    this._mutants.push(clone);
                } // else will be forgotten
            }
            else {
                indiv.mutate();
            }
        }
        if (Config_1.default.mutationOptions.mutateOffsprings) {
            let allOffsprings = this._offsprings.reduce((a, b) => {
                return a.concat(b);
            });
            for (let key in allOffsprings) {
                let indiv = allOffsprings[key];
                indiv.mutate();
            }
        }
        this._observers.forEach((observer) => {
            observer.notifyDoneMutation(this, this.mutants);
        });
    }
    ;
    doReduction() {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.doReduction");
        }
        let allIndividials = [].concat.apply(this._population, this._offsprings).concat(this._mutants);
        // let allIndividials = this.population.concat(this.offsprings).concat(this.mutants); // why not?
        // evaluateFitness if needed
        return this.evaluateAllFitness(allIndividials).then((population) => {
            if (Config_1.default.log.functions) {
                console.log("EvoCycle.doReduction evaluateAllFitness-then");
            }
            // sort all individuals and returns the fittest
            allIndividials.sort(Individual_1.default.compare); // in-place
            let eliminated = allIndividials.splice(this._population.length); // in-place and return value
            this._population = allIndividials;
            this._generationCounter++;
            for (let i = 0; i < eliminated.length; i++) {
                eliminated[i].eliminated = true;
            }
            this._observers.forEach((observer) => {
                observer.notifyDoneReduction(this, this.population);
            });
            return this._population; // chained promise
        });
    }
    evaluateAllFitness(population) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.EvaluateAllFitness");
        }
        // CANNOT use Promise.All, needs to be evaluated sequentially
        var allDonePromise = new Promise((resolve, reject) => {
            this.evaluateNextFitness(population, 0, resolve); // init promise recursion
        });
        return allDonePromise;
    }
    evaluateNextFitness(population, current, resolve) {
        if ((Config_1.default.log.functions && Config_1.default.log.nextFitness)
            || Config_1.default.log.nextFitness) {
            console.log("EvoCycle.evaluateNextFitness");
        }
        // end condition
        if (current >= population.length) {
            resolve(population);
        }
        else {
            this.current = population[current];
            // recursion
            if (population[current].fitness && !Config_1.default.general.alwaysEvaluateFitness) {
                // do not evaluate if fitness is known, call next immediately
                this.evaluateNextFitness(population, current + 1, resolve);
            }
            else {
                // evaluate unknown fitness, then call next
                population[current].evaluateFitness().then((res) => {
                    this._observers.forEach((observer) => {
                        observer.notifyEvaluatedNextFitness(this, population[current]);
                    });
                    this.evaluateNextFitness(population, current + 1, resolve);
                });
            }
        }
    }
    static print(individuals) {
        var arr = individuals.map((item) => {
            return item.toString();
        });
        console.dir(arr);
    }
    //////////////////// GETTERS and SETTERS ////////////////////
    addObserver(observer) {
        this._observers.push(observer);
    }
    get generationCounter() {
        return this._generationCounter;
    }
    get population() {
        return this._population;
    }
    get species() {
        return this._species;
    }
    get parents() {
        return this._parents;
    }
    get offsprings() {
        return this._offsprings;
    }
    get mutants() {
        return this._mutants;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = EvoCycle;

},{"./Config":1,"./model/Individual":7,"./model/Species":9}],3:[function(require,module,exports){
"use strict";
class MyMath {
    constructor() { }
    /**
     * @description returns a gaussian random function with the given mean and stdev.
     * @author http://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve#answer-35599181
     * @viz https://en.wikipedia.org/wiki/Marsaglia_polar_method
     * @param mean (mu)
     * @param stdev (sigma) standard deviation
     * @returns {()=>number}
     */
    static gaussian(mean, stdev) {
        var y2;
        var use_last = false;
        return function () {
            var y1;
            if (use_last) {
                y1 = y2;
                use_last = false;
            }
            else {
                var x1, x2, w;
                do {
                    x1 = 2.0 * Math.random() - 1.0;
                    x2 = 2.0 * Math.random() - 1.0;
                    w = x1 * x1 + x2 * x2;
                } while (w >= 1.0);
                w = Math.sqrt((-2.0 * Math.log(w)) / w);
                y1 = x1 * w;
                y2 = x2 * w;
                use_last = true;
            }
            var retval = mean + stdev * y1;
            // if(retval > 0)
            //     return retval;
            // return -retval;
            return retval;
        };
    }
    // public static randomNormal = MyMath.gaussian(0, 1);
    static randomNormal(stdev) {
        return MyMath.gaussian(0, stdev);
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MyMath;

},{}],4:[function(require,module,exports){
"use strict";
class EvoCycleControls {
    constructor($scope, $http, $interval, $timeout) {
        this.$scope = $scope;
        this.$http = $http;
        this.$interval = $interval;
        this.$timeout = $timeout;
        this.state = {
            current: {
                viewLong: false
            },
            population: {
                viewLong: [false] // viewLong[0] toggle all
            },
            offsprings: {
                viewLong: [false] // viewLong[0] toggle all
            },
            mutants: {
                viewLong: [false] // viewLong[0] toggle all
            }
        };
        this.fitnessChartObject = {};
        this.fitnessChartObject.type = "LineChart";
        this.fitnessChartObject.options = {
            title: "Fitness",
            legend: { position: 'bottom' }
        };
        this.fitnessChartObject.data = {
            cols: [{
                    id: "generation",
                    label: "Generation",
                    type: "string"
                }, {
                    id: "min",
                    label: "min",
                    type: "number"
                }, {
                    id: "max",
                    label: "max",
                    type: "number"
                }, {
                    id: "avg",
                    label: "avg",
                    type: "number"
                }],
            rows: [] // filled by EvoCycleControls::notifyDoneReduction
        };
        this.speciesChartObject = {};
        this.speciesChartObject.type = "AreaChart";
        this.speciesChartObject.options = {
            title: "Species",
            legend: { position: 'none' },
            isStacked: "true",
            fill: 20,
            displayExactValues: true
        };
        this.speciesChartObject.data = {
            cols: [
                {
                    id: "generation",
                    label: "Generation",
                    type: "string"
                }, {
                    id: "specie_0",
                    label: "Species #0",
                    type: "number"
                }
            ],
            rows: [] // set by setModel
        }; // rest filled by EvoCycleControls::notifyDoneSpeciation
    }
    /**
     * creates and starts ng app for EvoCycleControls
     */
    static init(model) {
        let tsneatEvoCycleControlsDiv = document.getElementById('tsneatevocycle-controller');
        let tsneatEvoCycleControlsApp = angular.module('tsneatevocycleControlsApp', ['googlechart'])
            .controller('tsNeatEvoCycleCtrl', EvoCycleControls);
        //.directive('individualGraph', IndividualGraph.Factory())
        // load angular app by hand not by ng-app attribute in case multiple angular app is running
        // WARNING there will be still problem if ng-app attr is on html or body because nested ng-app are not allowed // for possible solution see http://stackoverflow.com/questions/22548610/can-i-use-one-ng-app-inside-another-one-in-angularjs#answer-28030105
        angular.bootstrap(tsneatEvoCycleControlsDiv, ['tsneatevocycleControlsApp']);
        angular.element(tsneatEvoCycleControlsDiv).controller().setModel(model);
    }
    setModel(evoCycle) {
        this.model = evoCycle;
        this.speciesChartObject.data.rows = [{
                c: [{
                        v: '#0'
                    }, {
                        v: this.model.population.length
                    }]
            }];
        this.model.addObserver(this);
        this.refresh();
    }
    saveModel() {
        // TODO serialize or save to local storage or ??
        // JSON.stringify(this.model.population); // circullar error
        throw "Not Implemented!";
    }
    loadModel() {
        // TODO deserialize
        throw "Not Implemented!";
    }
    refresh() {
        // this.$scope.$apply();
        this.$scope.$evalAsync();
    }
    // start or continue with next generation
    continue(singleGeneration = false) {
        this.model.config.cycle.continue = !singleGeneration;
        this.model.continue(singleGeneration); // prevents multiple call
    }
    notifyDoneReduction(evocycle, population) {
        this.log("population:", population);
        let min = Number.MAX_VALUE;
        let max = Number.MIN_VALUE;
        let avg = this.model.population.reduce((sum, curr) => {
            min = curr.fitness < min ? curr.fitness : min;
            max = curr.fitness > max ? curr.fitness : max;
            return sum + curr.fitness;
        }, 0) / this.model.population.length;
        this.updateFitnessGraph(min, max, avg);
        // this.refresh();
    }
    updateFitnessGraph(min, max, avg) {
        this.fitnessChartObject.data.rows.push({
            c: [{
                    v: "#" + this.model.generationCounter
                }, {
                    v: min
                }, {
                    v: max
                }, {
                    v: avg
                }]
        });
    }
    notifyDoneSpeciation(evocycle, species) {
        this.log("species:", species);
        let maxId = -1;
        let speciesQuantity = [];
        species.forEach((specie) => {
            // [specId, specSize]
            maxId = specie.id > maxId ? specie.id : maxId;
            speciesQuantity[specie.id] = specie.individuals.reduce((sum, curr) => {
                return sum + 1;
            }, 0);
        });
        speciesQuantity.length = maxId + 1;
        speciesQuantity.forEach((item, index, array) => {
            array[index] = typeof item == "undefined" ? 0 : item;
        });
        speciesQuantity[0] = null;
        this.updateSpeciesGraph(speciesQuantity);
    }
    updateSpeciesGraph(speciesQuantity) {
        let cols = this.speciesChartObject.data.cols;
        let rows = this.speciesChartObject.data.rows;
        // rewrite column (titles) to contain all species
        cols = [
            {
                id: "generation",
                label: "Generation",
                type: "string"
            }
        ];
        for (let i = 1; i < speciesQuantity.length; i++) {
            cols.push({
                id: "specie_" + i,
                label: "Species #" + i,
                type: "number"
            });
        }
        this.speciesChartObject.data.cols = cols;
        // update rows (values) with last values
        rows.push({
            c: [{
                    v: "#" + this.model.generationCounter
                }]
        });
        for (let i = 1; i < speciesQuantity.length; i++) {
            // for new also update one before current
            if (typeof rows[rows.length - 2] != "undefined"
                && typeof rows[rows.length - 2].c[i] == "undefined") {
                rows[rows.length - 2].c[i] =
                    {
                        v: 0
                    };
            }
            // current generation
            rows[rows.length - 1].c.push({
                v: speciesQuantity[i]
            });
        }
    }
    notifyEvaluatedNextFitness(evocycle, individual) {
        this.log("individual:", individual);
        this.refresh();
    }
    notifyDoneSelection(evocycle, parents) {
        this.log("parents:", parents);
    }
    notifyDoneCrossover(evocycle, offsprings) {
        this.log("offsprings:", offsprings);
    }
    notifyDoneMutation(evocycle, mutants) {
        this.log("mutants:", mutants);
    }
    log(msg, obj) {
        let doLogs = false;
        if (doLogs) {
            console.log(msg);
            console.dir(obj);
        }
    }
}
EvoCycleControls.$inject = ["$scope", "$http", "$interval", "$timeout"];
exports.EvoCycleControls = EvoCycleControls;
/**
 * @deprecated
 */
class IndividualGraph {
    constructor() {
        this.restrict = 'E';
        this.template = '<div></div>';
        this.replace = true;
        this.scope = { individual: "<individual" };
        this.link = (scope, element, attrs) => {
            let makeGO = go.GraphObject.make;
            let myDiagram = makeGO(go.Diagram, element[0], {
                isReadOnly: true,
                isEnabled: false,
                "initialContentAlignment": go.Spot.Center,
                "toolManager.mouseWheelBehavior": go.ToolManager.WheelNone,
                "animationManager.isEnabled": false,
                "layout": new go.ForceDirectedLayout(),
                "InitialLayoutCompleted": function (e) {
                    // dynamic height for diagram
                    var dia = e.diagram;
                    dia.div.style.height = (dia.documentBounds.height + 8) + "px";
                }
            });
            myDiagram.model = go.Model.fromJson(scope.individual.toGOJS());
            // define the Node template
            myDiagram.nodeTemplate =
                makeGO(go.Node, "Auto", new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify), 
                // define the node's outer shape, which will surround the TextBlock
                makeGO(go.Shape, "RoundedRectangle", {
                    parameter1: 20,
                    fill: makeGO(go.Brush, "Linear", { 0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)" }),
                    stroke: null,
                    portId: "",
                    fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
                    toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
                    cursor: "pointer"
                }), makeGO(go.TextBlock, {
                    font: "9pt helvetica, arial, sans-serif",
                    editable: true // editing the text automatically updates the model data
                }, new go.Binding("text").makeTwoWay()));
            // replace the default Link template in the linkTemplateMap
            myDiagram.linkTemplate =
                makeGO(go.Link, // the whole link panel
                {
                    curve: go.Link.Bezier, adjusting: go.Link.Stretch,
                    reshapable: true, relinkableFrom: true, relinkableTo: true,
                    toShortLength: 3
                }, new go.Binding("points").makeTwoWay(), new go.Binding("curviness"), makeGO(go.Shape, // the link shape
                { strokeWidth: 1.5 }), makeGO(go.Shape, // the arrowhead
                { toArrow: "standard", stroke: null }), makeGO(go.Panel, "Auto", makeGO(go.Shape, // the label background, which becomes transparent around the edges
                {
                    fill: makeGO(go.Brush, "Radial", { 0: "rgb(240, 240, 240)", 0.3: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)" }),
                    stroke: null
                }), makeGO(go.TextBlock, "transition", // the label text
                {
                    textAlign: "center",
                    font: "9pt helvetica, arial, sans-serif",
                    margin: 4,
                    editable: true // enable in-place editing
                }, 
                // editing the text automatically updates the model data
                new go.Binding("text").makeTwoWay())));
        };
    }
    static Factory() {
        let directive = () => {
            return new IndividualGraph();
        };
        directive['$inject'] = [];
        return directive;
    }
}
exports.IndividualGraph = IndividualGraph;

},{}],5:[function(require,module,exports){
"use strict";
const NodeGene_1 = require("./NodeGene");
const ConnectGene_1 = require("./ConnectGene");
const NodeGene_2 = require("./NodeGene");
const MyMath_1 = require("../MyMath");
const Config_1 = require("../Config");
class BaseNeatIndividual {
    constructor() {
        this._id = ++BaseNeatIndividual._idCounter;
        this._genome = [{}, {}];
        this.eliminated = false;
        if (arguments.length == 1 && arguments[0] instanceof BaseNeatIndividual) {
            this.copyConstructor(arguments[0]);
        }
        else {
            this.normalConstructor.apply(this, arguments);
        }
    }
    /**
     * @description copy constructor
     */
    copyConstructor(that) {
        this._inputGenes = [];
        this._outputGenes = [];
        // copy nodeGenes
        for (let key in that.nodeGenes) {
            this.addNodeGene(new NodeGene_1.default(that.nodeGenes[key]));
        }
        // add same connections
        for (let key in that.connectGenes) {
            let thatConnectNode = that.connectGenes[key];
            this.addConnection(this.nodeGenes[thatConnectNode.inNode.innov], this.nodeGenes[thatConnectNode.outNode.innov], thatConnectNode.weight, thatConnectNode.enabled, thatConnectNode.innov);
        }
    }
    /**
     * @description args constructor
     */
    normalConstructor() {
        let inputsLength;
        let outputsLength;
        if (typeof arguments[0] === "number" && typeof arguments[1] === "number") {
            inputsLength = arguments[0];
            outputsLength = arguments[1];
            this._inputGenes = [];
            this._outputGenes = [];
            for (let i = 0; i < inputsLength; i++) {
                this.addNodeGene(new NodeGene_1.default(NodeGene_2.NodeGeneType.Input));
            }
            for (let o = 0; o < outputsLength; o++) {
                this.addNodeGene(new NodeGene_1.default(NodeGene_2.NodeGeneType.Output));
            }
        }
        else if (Array.isArray(arguments[0]) && Array.isArray(arguments[1])) {
            inputsLength = arguments[0].length;
            outputsLength = arguments[1].length;
            this._inputGenes = arguments[0];
            this._outputGenes = arguments[1];
        }
        else {
            throw "Unexpected parameters";
        }
        for (let i = 0; i < inputsLength; i++) {
            for (let o = 0; o < outputsLength; o++) {
                this.addConnection(this._inputGenes[i], this._outputGenes[o]);
            }
        }
    }
    /**
     * @description merges on top of this individual (does not change matching nodes)
     * @param that
     */
    merge(that) {
        // copy nodeGenes which do not exists yet
        for (let key in that.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];
            if (!thisNode) {
                this.addNodeGene(new NodeGene_1.default(thatNode));
            }
        }
        // add connections which do not exists yet
        for (let key in that.connectGenes) {
            let thisConn = this.connectGenes[key];
            let thatConn = that.connectGenes[key];
            if (!thisConn) {
                this.addConnection(this.nodeGenes[thatConn.inNode.innov], this.nodeGenes[thatConn.outNode.innov], thatConn.weight, thatConn.enabled, thatConn.innov);
            }
        }
    }
    /**
     * @desc DO NOT call on BaseNeatIndividual but on non-abstract subclass!
     * @returns {BaseNeatIndividual[]}
     */
    static createInitPopulation(populationSize, firstIndividualConstructorArgs) {
        if (this === BaseNeatIndividual) {
            throw "Called on abstract BaseNeatIndividual class. Implement non-abstract subclass and call on that instead.";
        }
        let population = [];
        let first;
        if (typeof firstIndividualConstructorArgs !== "undefined") {
            first = new this(...firstIndividualConstructorArgs);
        }
        else {
            first = new this();
        }
        population.push(first);
        for (let i = 1; i < populationSize; i++) {
            // randomize weights and thresholds
            let next = new this(first);
            for (let key in next.connectGenes) {
                let connection = next.connectGenes[key];
                connection.weight = 2 * Math.random() - 1;
            }
            for (let key in next.nodeGenes) {
                let node = next.nodeGenes[key];
                node.threshold = 2 * Math.random() - 1;
            }
            population.push(next); // copies of first so same innov numbers
        }
        return population;
    }
    addNodeGene(nodeGene) {
        this.nodeGenes[nodeGene.innov] = nodeGene;
        if (nodeGene.type == NodeGene_2.NodeGeneType.Input) {
            this._inputGenes.push(nodeGene);
        }
        if (nodeGene.type == NodeGene_2.NodeGeneType.Output) {
            this._outputGenes.push(nodeGene);
        }
    }
    addConnectGene(connectGene) {
        this.connectGenes[connectGene.innov] = connectGene;
    }
    addConnection(inNode, outNode, weight, enabled, innov) {
        this.addConnectGene(new ConnectGene_1.default(inNode, outNode, weight, enabled, innov));
    }
    breed(partner) {
        let better;
        let worse;
        if (this.fitness > partner.fitness) {
            better = this;
            worse = partner;
        }
        else {
            better = partner;
            worse = this;
        }
        // make copy of better
        // Stanley: non-matching genes are inherited from the more fit parent.
        let offspring = new better.constructor(better);
        // inherit randomly weight for shared genes
        // Stanley: matching genes are inherrited randomly
        for (let key in offspring.connectGenes) {
            let connOffspring = offspring.connectGenes[key];
            let connWorse = worse.connectGenes[key];
            if (connWorse) {
                let which = 0.5 > Math.random();
                connOffspring.weight = which ? connOffspring.weight : connWorse.weight;
                connOffspring.enabled = which ? connOffspring.enabled : connWorse.enabled;
                // Stanley: There was a 75% chance that an inherited gene was disabled if it was disabled in either parent
                if (!connOffspring.enabled || !connWorse.enabled) {
                    connOffspring.enabled = (0.75 > Math.random() ? false : true);
                }
                else {
                    connOffspring.enabled = true;
                }
            }
        }
        /**
         * this part is not totally clear for me - from http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf:
         * Stanley: "In this case, equal fitnesses are assumed, so the disjoint and excess genes are also inherited randomly."
         * How can be disjoint and excess genes inherrited randomly when they are only in one of the parent?
         */
        // if same fitnesses, also inherit (copy) genes from other parent
        if (this.fitness == partner.fitness) {
            offspring.merge(worse);
        }
        // inherit randomly thresholds - imagine as another in connection
        for (let key in offspring.nodeGenes) {
            let nodeOffspring = offspring.nodeGenes[key];
            let nodeWorse = worse.nodeGenes[key];
            if (nodeWorse) {
                nodeOffspring.threshold = 0.5 > Math.random() ? nodeOffspring.threshold : nodeWorse.threshold;
            }
        }
        return offspring;
    }
    mutate() {
        let isMutated = false;
        let topologyMutOptOf = Config_1.default.mutationOptions.individualTopology;
        let weightsMutOptOf = Config_1.default.mutationOptions.individualWeights;
        if (topologyMutOptOf.chance > Math.random()) {
            let xorChance; // 2 for false, <0;1) for addNode, <1;2) for addConnection
            if (topologyMutOptOf.addNodeXORaddConnection) {
                xorChance = Math.random() * 2; // Math.random() never equals 1 => xorChance never equals 2 which is reserved for xorChance false
            }
            else {
                xorChance = 2;
            }
            if (xorChance === 2 || (xorChance >= 0 && xorChance < 1)) {
                if (topologyMutOptOf.addNode.chance > Math.random()) {
                    this.mutateAddNode();
                    isMutated = true;
                }
            }
            if (xorChance === 2 || (xorChance >= 1 && xorChance < 2)) {
                if (topologyMutOptOf.addConnection.chance > Math.random()) {
                    this.mutateAddConnection();
                    isMutated = true;
                }
            }
        }
        if (weightsMutOptOf.chance > Math.random()) {
            if (weightsMutOptOf.weights.chance > Math.random()) {
                this.mutateWeights();
                isMutated = true;
            }
            if (weightsMutOptOf.thresholds.chance > Math.random()) {
                this.mutateThresholds();
                isMutated = true;
            }
        }
        this._fitness = null;
        return isMutated;
    }
    mutateAddConnection() {
        let n1 = this.getRandomNodeGene();
        let n2;
        let maxAttempts = 100;
        do {
            n2 = this.getRandomNodeGene();
            maxAttempts--;
        } while (n1.id == n2.id && maxAttempts > 0);
        if (maxAttempts == 0) {
            console.warn("Cannot find nodes to mutateAddConnection.");
            console.dir(this);
            return;
        }
        if (!this.areConnected(n1, n2)) {
            return this.addConnection(n1, n2);
        }
        else {
            return this.mutateAddConnection();
        }
    }
    mutateAddNode() {
        let edge;
        let maxAttempts = 100;
        do {
            edge = this.getRandomConnectGene();
            maxAttempts--;
        } while (edge.enabled == false && maxAttempts > 0);
        if (maxAttempts == 0) {
            console.warn("Cannot find edge to mutateAddNode.");
            console.dir(this);
            return;
        }
        edge.enabled = false;
        let inNode = edge.inNode;
        let outNode = edge.outNode;
        let innerNode = new NodeGene_1.default();
        this.addNodeGene(innerNode);
        this.addConnection(inNode, innerNode, 1);
        this.addConnection(innerNode, outNode, edge.weight);
    }
    mutateWeights() {
        for (let key in this.connectGenes) {
            if (Config_1.default.mutationOptions.individualWeights.weights.mutateSingle.chance > Math.random()) {
                let connection = this.connectGenes[key];
                connection.weight += MyMath_1.default.randomNormal(Config_1.default.mutationOptions.individualWeights.weights.mutateSingle.stdev)();
            }
        }
    }
    mutateThresholds() {
        for (let key in this.nodeGenes) {
            if (Config_1.default.mutationOptions.individualWeights.thresholds.mutateSingle.chance > Math.random()) {
                let node = this.nodeGenes[key];
                node.threshold += MyMath_1.default.randomNormal(Config_1.default.mutationOptions.individualWeights.thresholds.mutateSingle.stdev)();
            }
        }
    }
    evaluateNetwork(inputs) {
        let outputs = [];
        // evaluate input nodes (recursion end condition)
        for (let i = 0; i < this.inputGenes.length; i++) {
            this.inputGenes[i].evaluateOutput(inputs[i]);
        }
        // evaluate network from output nodes by recursion
        for (let key in this.outputGenes) {
            outputs.push(this.outputGenes[key].evaluateOutput());
        }
        // reset network evaluation for next evaluation
        for (let key in this.nodeGenes) {
            this.nodeGenes[key].resetOutput();
        }
        return outputs;
    }
    toString() {
        let nodeGenesString = "";
        for (let key in this.nodeGenes) {
            nodeGenesString += "\t" + this.nodeGenes[key].toString() + ";\n";
        }
        let connectGenesString = "";
        for (let key in this.connectGenes) {
            connectGenesString += "\t" + this.connectGenes[key].toString() + ";\n";
        }
        return "BaseNeatIndividual = {\n" +
            "\tid: " + this.id + ";\n" +
            "\tfitness: " + this.fitness + ";\n" +
            nodeGenesString +
            connectGenesString
            + "}";
    }
    static nodeGeneToText(nodeGene) {
        return 'innov: ' + nodeGene.innov + '\n' +
            'threshold: ' + nodeGene.threshold;
    }
    static connectGeneToText(connectGeneGene) {
        return 'innov: ' + connectGeneGene.innov + '\n' +
            'weight: ' + connectGeneGene.weight.toFixed(2) + '\n' +
            (connectGeneGene.enabled ? "" : "DISABLED");
    }
    /**
     * @deprecated
     */
    toGOJS() {
        let json = {};
        json["nodeKeyProperty"] = "id";
        json["nodeDataArray"] = [];
        for (let key in this.nodeGenes) {
            let nodeGene = this.nodeGenes[key];
            json["nodeDataArray"].push({
                id: nodeGene.id,
                text: BaseNeatIndividual.nodeGeneToText(nodeGene)
            });
        }
        json["linkDataArray"] = [];
        for (let key in this.connectGenes) {
            let connectGene = this.connectGenes[key];
            json["linkDataArray"].push({
                from: connectGene.inNode.id,
                to: connectGene.outNode.id,
                text: BaseNeatIndividual.connectGeneToText(connectGene)
            });
        }
        // json["nodeDataArray"] = [{ "id": 0, "loc": "120 120", "text": "XXX" }];
        // json["linkDataArray"] = [{ "from": 0, "to": 0, "text": "up or timer", "curviness": -20 }];
        return json;
    }
    //
    // GET functions
    //
    /**
     * @complexity O(#NG); O(1) can be achieved if NodeGenes stored in array but harder to implement breed and other functions; #NG is relatively low
     */
    getRandomNodeGene() {
        let result;
        let count = 0;
        for (let key in this.nodeGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.nodeGenes[key];
            }
        }
        return result;
    }
    /**
     * @complexity O(#CG); O(1) can be achieved if NodeGenes stored in array but harder to implement breed and other functions; #CG is relatively low
     */
    getRandomConnectGene() {
        let result;
        let count = 0;
        for (let key in this.connectGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.connectGenes[key];
            }
        }
        return result;
    }
    /**
     * @complexity O(#CG) but probably cannot be better
     */
    areConnected(n1, n2) {
        for (let key in this.connectGenes) {
            // check
            let edge = this.connectGenes[key];
            if (edge.inNode.id == n1.id && edge.outNode.id == n2.id
                || edge.inNode.id == n2.id && edge.outNode.id == n1.id) {
                return true;
            }
        }
        return false;
    }
    static compare(first, second) {
        return second.fitness - first.fitness;
    }
    static distance(first, second) {
        return first.distanceTo(second);
    }
    distanceTo(that) {
        let c_e = Config_1.default.speciation.excessCoef;
        let c_d = Config_1.default.speciation.disjointCoef;
        let c_m = Config_1.default.speciation.matchingCoef;
        let N = 0; // #genes in longer
        let D = 0; // #genes disjoint
        let E = 0; // #genes excess
        let W = 0; // average weight differences of matching genes W
        let M = 0; // #genes matching
        let thisArray = this.getConnectGenesAsArray();
        let thatArray = that.getConnectGenesAsArray();
        let i = 0;
        let j = 0;
        while (i < thisArray.length && j < thatArray.length) {
            let thisConn = thisArray[i];
            let thatConn = thatArray[j];
            if (thisConn.innov == thatConn.innov) {
                W += Math.abs(thisConn.weight - thatConn.weight);
                M++;
                i++;
                j++;
            }
            else {
                D++;
                if (thisConn.innov < thatConn.innov) {
                    j++;
                }
                else {
                    i++;
                }
            }
        }
        E = i < thisArray.length ? thisArray.length - i : (j < thatArray.length ? thatArray.length - j : 0); // excess
        N = thisArray.length > thatArray.length ? thisArray.length : thatArray.length;
        // count thresholds also as input connection
        for (let key in this.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];
            if (thatNode) {
                W += Math.abs(thisNode.threshold - thatNode.threshold);
                M++;
            }
        }
        W = W / M;
        return (c_e * E) / N + (c_d * D) / N + c_m * W;
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get fitness() {
        return this._fitness;
    }
    get nodeGenes() {
        return this._genome[0];
    }
    get connectGenes() {
        return this._genome[1];
    }
    getConnectGenesAsArray() {
        let res = [];
        for (let key in this.connectGenes) {
            res.push(this.connectGenes[key]);
        }
        return res.sort((a, b) => { return b.innov - a.innov; });
    }
    get inputGenes() {
        return this._inputGenes;
    }
    get outputGenes() {
        return this._outputGenes;
    }
}
BaseNeatIndividual._idCounter = 0;
exports.BaseNeatIndividual = BaseNeatIndividual;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BaseNeatIndividual;

},{"../Config":1,"../MyMath":3,"./ConnectGene":6,"./NodeGene":8}],6:[function(require,module,exports){
"use strict";
class ConnectGene {
    // public inNode: NodeGene;
    // public outNode: NodeGene;
    // public weight: number;
    constructor(inNode, outNode, weight, enabled, innov) {
        this.inNode = inNode;
        this.outNode = outNode;
        this.weight = weight;
        this._id = ++ConnectGene._idCounter;
        this._innov = typeof innov != "undefined" ? innov : ++ConnectGene._innovCounter;
        this.enabled = typeof enabled != "undefined" ? enabled : true;
        this.weight = typeof weight != "undefined" ? weight : 2 * Math.random() - 1;
        this.outNode.addInConnection(this);
    }
    toString() {
        return "ConnecGene = {id: " + this.id + "; innov: " + this.innov + "; in: " + this.inNode.id + "; out: " + this.outNode.id + "; weight: " + this.weight.toFixed(2) + "; enabled: " + this.enabled + "}";
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get innov() {
        return this._innov;
    }
}
ConnectGene._idCounter = 0;
ConnectGene._innovCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ConnectGene;

},{}],7:[function(require,module,exports){
"use strict";
/**
 * @interface
 * @desc Intended to be used as interface
 */
class Individual {
    constructor(individual) { throw "Not Implemented!"; }
    static distance(first, second) { throw "Not Implemented!"; }
    static compare(first, second) { return second.fitness - first.fitness; }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Individual;

},{}],8:[function(require,module,exports){
"use strict";
class NodeGene {
    constructor() {
        this._id = ++NodeGene._idCounter;
        if (arguments.length == 1 && arguments[0] instanceof NodeGene) {
            this.copyConstructor(arguments[0]);
        }
        else {
            this.normalConstructor(arguments[0], arguments[1]);
        }
    }
    copyConstructor(that) {
        // does not copy connectGenes - copied from caller
        this._innov = that._innov;
        this._inConnections = [];
        this.threshold = that.threshold;
        this.output = that.output;
        this.lastOutput = that.lastOutput;
        this._state = that._state;
        this._type = that._type;
        this._activationFunc = that._activationFunc;
    }
    normalConstructor(type, activationFunc) {
        this._innov = ++NodeGene._innovCounter;
        this._inConnections = [];
        this.threshold = 2 * Math.random() - 1;
        this.output = null;
        this.lastOutput = 0;
        this._state = NodeGeneState.New;
        this._type = typeof type !== "undefined" ? arguments[0] : NodeGeneType.Hidden;
        this._activationFunc = typeof activationFunc !== "undefined" ? activationFunc : softStep;
    }
    addInConnection(inConnection) {
        this.inConnections.push(inConnection);
    }
    evaluateOutput(input) {
        if (this.state == NodeGeneState.Open) {
            return this.lastOutput;
        }
        if (this.state == NodeGeneState.Closed) {
            return this.output;
        }
        // other - evaluate recursively
        this._state = NodeGeneState.Open;
        let sum = 0;
        // input nodes - sum over inputs, then over recurrent connections
        if (this.type == NodeGeneType.Input && arguments.length == 1) {
            sum += input;
            sum += this.inConnections.reduce((sum, current) => {
                if (current.enabled) {
                    return sum + current.inNode.lastOutput * current.weight; // NO recursion - recurrent connection
                }
                else {
                    return sum;
                }
            }, 0);
            sum += this.threshold;
        }
        else {
            // hidden, output nodes - sum over in connections
            sum += this.inConnections.reduce((sum, current) => {
                if (current.enabled) {
                    return sum + current.inNode.evaluateOutput() * current.weight; // recursion
                }
                else {
                    return sum;
                }
            }, 0);
            sum += this.threshold;
        }
        this._state = NodeGeneState.Closed;
        this.output = this.activationFunc(sum);
        return this.output;
    }
    resetOutput() {
        this.lastOutput = this.output;
        this.output = null;
        this._state = NodeGeneState.New;
    }
    toString() {
        return "NodeGene = {id: " + this.id + "; innov: " + this.innov + "; type:" + this.type + "; threshold: " + this.threshold.toFixed(2) + "}";
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get innov() {
        return this._innov;
    }
    get type() {
        return this._type;
    }
    get activationFunc() {
        return this._activationFunc;
    }
    get inConnections() {
        return this._inConnections;
    }
    get state() {
        return this._state;
    }
}
NodeGene._idCounter = 0;
NodeGene._innovCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = NodeGene;
var NodeGeneType;
(function (NodeGeneType) {
    NodeGeneType[NodeGeneType["Input"] = 0] = "Input";
    NodeGeneType[NodeGeneType["Hidden"] = 1] = "Hidden";
    NodeGeneType[NodeGeneType["Output"] = 2] = "Output";
})(NodeGeneType = exports.NodeGeneType || (exports.NodeGeneType = {}));
var NodeGeneState;
(function (NodeGeneState) {
    NodeGeneState[NodeGeneState["New"] = 0] = "New";
    NodeGeneState[NodeGeneState["Open"] = 1] = "Open";
    NodeGeneState[NodeGeneState["Closed"] = 2] = "Closed";
})(NodeGeneState = exports.NodeGeneState || (exports.NodeGeneState = {}));
function softStep(x) {
    return 1 / (1 + Math.exp(-x));
}
exports.softStep = softStep;
function binaryStep(x) {
    return x >= 0 ? 1 : 0;
}
exports.binaryStep = binaryStep;

},{}],9:[function(require,module,exports){
"use strict";
class Species {
    constructor(individuals) {
        this._id = ++Species._idCounter;
        this.individuals = typeof individuals != "undefined" ? individuals : [];
    }
    add(individual) {
        this.individuals.push(individual);
    }
    /**
     * keeps single random representative
     */
    clear() {
        let representative = null;
        let count = 0;
        for (let key in this.individuals) {
            if (!this.individuals[key].eliminated) {
                if (Math.random() < 1 / ++count) {
                    representative = this.individuals[key];
                }
            }
        }
        if (representative == null) {
            this.individuals = [];
            return false;
        }
        else {
            this.individuals = [representative];
            return true;
        }
    }
    /**
     * average fitness over species
     */
    getSharedFitness() {
        return this.individuals.reduce((sum, curr) => { return sum + curr.fitness; }, 0) / this.individuals.length;
    }
    isEmpty() {
        return this.individuals.length == 0;
    }
    get representative() {
        return this.individuals[0];
    }
    get id() {
        return this._id;
    }
}
Species._idCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Species;

},{}],10:[function(require,module,exports){
"use strict";
const BaseNeatIndividual_1 = require("../lib/model/BaseNeatIndividual");
const SlitherioController_1 = require("./SlitherioController");
class SlitherioBot extends BaseNeatIndividual_1.BaseNeatIndividual {
    constructor() {
        super(...arguments);
        this._gameCtrl = SlitherioController_1.default;
        this._ticks = 0;
        this._precalculated = {
            sensorWidth: null,
            sensorsWidth: null,
            sensorsRadius: null,
            sensors: null
        };
    }
    evaluateFitness() {
        return this.start()
            .then((res) => {
            this._fitness = res;
            return res;
        });
    }
    start() {
        let fitnessPromise = new Promise((resolve, reject) => {
            this._timer = window.setInterval(this.update.bind(this, resolve), 100);
        });
        this.gameCtrl.startNewGame();
        return fitnessPromise;
    }
    update(resolve) {
        if (!this.gameCtrl.getSnake()) {
            console.warn("Cannot update: snake not available.");
            return;
        }
        if (this.gameCtrl.isGameOver()) {
            window.clearInterval(this._timer);
            this.gameCtrl.getGameScore().then((score) => resolve(score));
            return;
        }
        this.precalculate();
        this.makeDecision();
        this._ticks++;
    }
    /**
     * @desc Precalculates values repeatedly used in decision.
     */
    precalculate() {
        this._precalculated.sensorWidth = SlitherioBot.sensorWidth * this.gameCtrl.getZoom(); // length of patch in pixels
        this._precalculated.sensorsWidth = Math.pow(2, SlitherioBot.noSensorLayers - 1) * this._precalculated.sensorWidth;
        this._precalculated.sensorsRadius = Math.sqrt(this._precalculated.sensorsWidth * this._precalculated.sensorsWidth + this._precalculated.sensorsWidth * this._precalculated.sensorsWidth);
        this._precalculated.sensors = [];
        for (let s = 0; s < SlitherioBot.noSensors; s++) {
            this._precalculated.sensors[s] = this.getSensor(s);
        }
    }
    makeDecision() {
        // transform game values to inputs
        let inputs = this.environmentToInputs();
        // brain
        let outputs = this.evaluateNetwork(inputs);
        // transform outputs to game moves
        this.outputsToDecision(outputs);
        this.drawSensors(inputs);
        // this.drawSnakeParts();
    }
    environmentToInputs() {
        let sensorValues = [];
        sensorValues.length = SlitherioBot.noSensors;
        sensorValues.fill(0, 0, sensorValues.length); // array full of 0
        // set sensor to amount of food and pray (by size) which is in it
        let foods = this.gameCtrl.getAllFoodsAndPreys();
        for (let i = 0; i < foods.length; i++) {
            let food = foods[i];
            if (this.isInRange(food)) {
                for (let s = 0; s < SlitherioBot.noSensors; s++) {
                    if (this.isInSensor(s, food)) {
                        sensorValues[s] += food.sz;
                        continue; // can't be in multiple, only on edges // don't care
                    }
                }
            }
        }
        // set sensor to -1 if there is other snake part obstacle
        let snakes = this.gameCtrl.getAllOtherSnakes(); // cannot use squareDistance easily because snake is consisted of multiple parts
        for (let i = 0; i < snakes.length; i++) {
            let snake = snakes[i];
            let radius = Math.round(snake.sc * 29.0) / 2;
            if (this.isInRange(snake, radius)) {
                for (let s = 0; s < SlitherioBot.noSensors; s++) {
                    if (this.isInSensor(s, snake, radius)) {
                        sensorValues[s] = -1;
                    }
                }
            }
            for (let j = 0; j < snake.pts.length; j++) {
                let part = snake.pts[j];
                if (part != null && this.isInRange(part, radius)) {
                    for (let s = 0; s < SlitherioBot.noSensors; s++) {
                        if (!part.dying && this.isInSensor(s, part, radius)) {
                            sensorValues[s] = -1;
                        }
                    }
                }
            }
        }
        // set sensor to -1 if end of word
        if (SlitherioController_1.default.distance(this.gameCtrl.getSnake(), this.gameCtrl.word.center) + this._precalculated.sensorsRadius >= this.gameCtrl.word.radius) {
            for (let s = 0; s < SlitherioBot.noSensors; s++) {
                if (this.isSensorOutOfMap(s)) {
                    sensorValues[s] = -1;
                }
            }
        }
        return sensorValues;
    }
    drawSensors(sensorValues) {
        this.gameCtrl.setCustomRedraw(() => {
            let snk = this.gameCtrl.getSnake();
            if (!snk) {
                return;
            }
            // heading
            let headingCircle = { center: { x: snk.xx + this.gameCtrl.getMouseCoor().x, y: snk.yy + this.gameCtrl.getMouseCoor().y }, radius: 5 };
            this.gameCtrl.drawCircle(headingCircle, '#ffffff', false, true, 0.66);
            // sensor grid
            for (let s = 0; s < SlitherioBot.noSensors; s++) {
                this.gameCtrl.drawRect(this.getSensor(s), '#000000', true, false, 1);
            }
            // sensor filling
            for (let s = 0; s < SlitherioBot.noSensors; s++) {
                let color;
                let alpha;
                let maxAlpha = 0.66;
                if (sensorValues[s] == -1) {
                    color = '#b20000';
                    alpha = maxAlpha;
                }
                else {
                    color = '#00b200';
                    alpha = Math.min(sensorValues[s] / 100, maxAlpha);
                }
                this.gameCtrl.drawRect(this.getSensor(s), color, false, true, alpha);
            }
        });
    }
    drawSnakeParts() {
        this.gameCtrl.setCustomRedraw(() => {
            let snakes = this.gameCtrl.getAllSnakes();
            for (let i = 0; i < snakes.length; i++) {
                let snk = snakes[i];
                this.gameCtrl.drawCircle({ center: { x: snk.xx, y: snk.yy }, radius: 5 }, '#000000', false, true, 1);
                for (let j = 0; j < snk.pts.length; j++) {
                    let prt = snk.pts[j];
                    if (!(prt.dying)) {
                        this.gameCtrl.drawCircle({ center: { x: prt.xx, y: prt.yy }, radius: 5 }, '#000000', false, true, 1);
                    }
                }
            }
        });
    }
    isInSensor(sensorNumber, point, radius = 0) {
        let sensor = this._precalculated.sensors[sensorNumber];
        return SlitherioBot.isInRect(sensor.cornerA, sensor.cornerB, point, radius);
    }
    isSensorOutOfMap(sensorNumber) {
        let word = this.gameCtrl.word;
        let sensor = this._precalculated.sensors[sensorNumber];
        // Find the furthermost point to the circle within the rectangle
        let furthestX = SlitherioBot.clampRev(word.center.x, sensor.cornerA.x, sensor.cornerB.x);
        let furthestY = SlitherioBot.clampRev(word.center.y, sensor.cornerA.y, sensor.cornerB.y);
        // Calculate the distance between the circle's center and this closest point
        let distanceX = word.center.x - furthestX;
        let distanceY = word.center.y - furthestY;
        // make sure it is *not* in the circle
        return !((distanceX * distanceX) + (distanceY * distanceY) <= (word.radius * word.radius));
    }
    static clamp(value, minOrMax, maxorMin) {
        let min = Math.min(minOrMax, maxorMin);
        let max = Math.max(minOrMax, maxorMin);
        let res = value;
        if (res < min) {
            res = min;
        }
        else if (res > max) {
            res = max;
        }
        return res;
    }
    ;
    static clampRev(value, minOrMax, maxorMin) {
        let min = Math.min(minOrMax, maxorMin);
        let max = Math.max(minOrMax, maxorMin);
        let res = value;
        if (res < max) {
            res = max;
        }
        else if (res > min) {
            res = min;
        }
        return res;
    }
    ;
    getSensor(sensorNumber) {
        let width = this._precalculated.sensorWidth;
        let xx = this.gameCtrl.getSnake().xx;
        let yy = this.gameCtrl.getSnake().yy;
        /**
         * First layer of 4 sensors
         */
        if (sensorNumber < 4) {
            switch (sensorNumber) {
                case 0: return { cornerA: { x: xx, y: yy }, cornerB: { x: xx + width, y: yy + width } };
                case 1: return { cornerA: { x: xx, y: yy }, cornerB: { x: xx - width, y: yy + width } };
                case 2: return { cornerA: { x: xx, y: yy }, cornerB: { x: xx - width, y: yy - width } };
                case 3: return { cornerA: { x: xx, y: yy }, cornerB: { x: xx + width, y: yy - width } };
            }
        }
        /**
         * Second and further layers of 12 sensors
         *
         * Imagine two rect (small, 2 times bigger) with same center
         * originRectPoint - corners of smaller rect - changes every 4
         * secondRectPoint - corners and halfs - changes like 012 234 456 678
         */
        for (let l = 0; l < SlitherioBot.noSensorLayers - 1; l++) {
            let lenInLayer = width * Math.pow(2, l);
            let sensorNumberInLayer = sensorNumber - 4 - 12 * l; // in a layer
            if (sensorNumberInLayer >= 12) {
                continue; // next layer
            }
            // changes every 4
            let originRectPoint;
            switch (Math.floor(sensorNumberInLayer / 3)) {
                case 0:
                    originRectPoint = { x: xx + lenInLayer, y: yy + lenInLayer };
                    break;
                case 1:
                    originRectPoint = { x: xx - lenInLayer, y: yy + lenInLayer };
                    break;
                case 2:
                    originRectPoint = { x: xx - lenInLayer, y: yy - lenInLayer };
                    break;
                case 3:
                    originRectPoint = { x: xx + lenInLayer, y: yy - lenInLayer };
                    break;
            }
            // changes like 012 234 456 678
            let secondRectPoint = {
                x: xx + (SlitherioBot.secondPointFn(sensorNumberInLayer) * 2 * lenInLayer),
                y: yy + (SlitherioBot.secondPointFn((sensorNumberInLayer + 12 - 3) % 12) * 2 * lenInLayer)
            };
            return { cornerA: originRectPoint, cornerB: secondRectPoint };
        }
    }
    /**
     * @returns 0-11: 1,1,0,0,-1,-1, -1,-1,0,0,1,1
     */
    static secondPointFn(n) {
        if (n > 5) {
            n = 11 - n;
        }
        return -(Math.floor(n / 2) - 1);
    }
    /**
     * Do not forget that canvas starts top left, x goes right, y goes down
     */
    static isInRect(cornerA, cornerB, point, radius = 0) {
        // sort by x axis
        if (cornerA.x > cornerB.x) {
            let tmpA = { x: cornerA.x, y: cornerA.y };
            cornerA = cornerB;
            cornerB = tmpA;
        }
        // one option in x axe
        if (point.xx >= cornerA.x - radius && point.xx <= cornerB.x + radius) {
            // two option in y axe
            if ((point.yy >= cornerA.y - radius && point.yy <= cornerB.y + radius)
                || (point.yy >= cornerB.y - radius && point.yy <= cornerA.y + radius)) {
                return true;
            }
        }
        return false;
    }
    isInRange(point, radius = 0) {
        return SlitherioController_1.default.distance(this.gameCtrl.getSnake(), point) <= this._precalculated.sensorsRadius + radius;
    }
    //
    // GETTERST and SETTERS
    //
    get gameCtrl() {
        return this._gameCtrl;
    }
    get ticks() {
        return this._ticks;
    }
    get precalculated() {
        return this._precalculated;
    }
}
SlitherioBot.sensorWidth = 50;
SlitherioBot.noSensorLayers = 4;
SlitherioBot.noSensors = 4 + (SlitherioBot.noSensorLayers - 1) * 12; // 4 in first layer and 12 fore each other layyer
SlitherioBot.noInputs = SlitherioBot.noSensors;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SlitherioBot;

},{"../lib/model/BaseNeatIndividual":5,"./SlitherioController":12}],11:[function(require,module,exports){
"use strict";
const SlitherioBot_1 = require("./SlitherioBot");
const SlitherioController_1 = require("./SlitherioController");
class SlitherioBotSensorMove extends SlitherioBot_1.default {
    outputsToDecision(outputs) {
        // acceleration
        let acc = outputs.pop() > 0 ? true : false; // remove and use last which is acceleration
        this.gameCtrl.setAcceleration(acc);
        // move to one of 12 possible direction
        let s = outputs.indexOf(Math.max(...outputs)); // first 12 values are sensors to move
        this.gameCtrl.setMouseCoor(this.sensorCenter(4 + s));
    }
    sensorCenter(sensorNumber) {
        let A = SlitherioController_1.default.wordToCanvasCoor(this.precalculated.sensors[sensorNumber].cornerA);
        let B = SlitherioController_1.default.wordToCanvasCoor(this.precalculated.sensors[sensorNumber].cornerB);
        let snkObj = this.gameCtrl.getSnake();
        let snkPt = this.gameCtrl.wordToCanvasCoor({ x: snkObj.xx, y: snkObj.yy });
        let res = {
            x: ((A.x + B.x) / 2 - snkPt.x) / this.gameCtrl.getZoom(),
            y: ((A.y + B.y) / 2 - snkPt.y) / this.gameCtrl.getZoom()
        };
        return res;
    }
}
SlitherioBotSensorMove.noOutputs = 13; // 12 move directions + acceleration
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SlitherioBotSensorMove;

},{"./SlitherioBot":10,"./SlitherioController":12}],12:[function(require,module,exports){
"use strict";
/// <reference path="../../node_modules/@types/angular/index.d.ts" />
function isPositionable(entity) {
    return entity.xx !== undefined && entity.yy !== undefined;
}
function isPoint(entity) {
    return entity.x !== undefined && entity.y !== undefined;
}
var game = window;
var ctrl = (_a = class SlitherioController {
        static init() {
            game.slitherioController = { gsc: game.gsc };
            game.origController = {};
            // low graphics for better performance
            game.render_mode = 1;
            game.want_quality = 0;
            game.high_quality = false;
            // additional custom redraw function
            game.origController.redraw = game.redraw;
            game.redraw = function () {
                game.origController.redraw();
                ctrl._customRedraw();
            };
            // additional eof function
            game.origController.oef = game.oef;
            game.oef = function () {
                game.origController.oef();
                // keep zoom
                game.gsc = game.slitherioController.gsc;
            };
            // additional onmousemove function
            game.origController.onmousemove = game.onmousemove;
            game.onmousemove = function (e) {
                if (ctrl._isMouseMoveEnabled) {
                    game.origController.onmousemove(e);
                }
            };
            // additional onmousedown function
            game.origController.onmousedown = game.onmousedown;
            game.onmousedown = function (e) {
                if (ctrl._isMouseMoveEnabled) {
                    game.origController.onmousedown(e);
                }
            };
            // additional custom key bindings
            game.origController.onkeydown = document.onkeydown;
            document.onkeydown = function (e) {
                ctrl._customOnkeydown(e); // custom first to disable space
                game.origController.onkeydown(e);
            };
            console.log("Custom shortcuts:");
            console.log("\t+/- zoom");
            console.log("\t D  debug [d]ump");
            console.log("\t M  toggle [m]ouse moving (default off)");
            console.log("\t H  toggle [h]idden/visible controls div (default visible)");
            ctrl._isInitialized = true;
        }
        static startNewGame() {
            if (!ctrl._isInitialized) {
                ctrl.init();
            }
            document.getElementById("nick").value = "playing...";
            document.getElementById("playh").children[0].click();
            ctrl.setMouseCoor({ x: 0, y: 0 });
            ctrl._botPlaying = true;
        }
        static isGameOver() {
            if (ctrl.isBotPlaying() && game.snake != null && game.snake.dead_amt >= 1 && game.snake.alive_amt >= 1) {
                ctrl.resetCustomRedraw();
                game.snake.dead_amt = 0; // IMPORTANT slither.io itself doesn't set these variables for new game immediately, takes old instead which mess up with this function
                game.snake.alive_amt = 0; // IMPORTANT slither.io itself doesn't set these variables for new game immediately, takes old instead which mess up with this function
                ctrl._botPlaying = false;
                ctrl._gameScoreChecked = false;
                return true;
            }
            return false;
        }
        static isBotPlaying() {
            return ctrl._botPlaying;
        }
        static getGameScore() {
            let scorePromise = new Promise((resolve, reject) => {
                ctrl._checkScoreTimer = window.setInterval(ctrl.checkGameScore.bind(this, resolve), 100);
            });
            return scorePromise;
        }
        static checkGameScore(resolveScore) {
            // wait until score is there
            if (game.lastscore
                && game.lastscore.childNodes[1]
                && game.lastscore.childNodes[1].innerHTML != ""
                && ctrl._gameScoreChecked == false) {
                window.clearInterval(ctrl._checkScoreTimer);
                let score = parseInt(game.lastscore.childNodes[1].innerHTML);
                ctrl._gameScoreChecked = true; // otherwise next bot will read wrong score
                // game.lastscore.childNodes[1].innerHTML = ""; // otherwise next bot will read wrong score, also with waiting it solves problem of next bot thinking it is already isGameover == true
                // resolveScore(score);
                window.setTimeout(() => { resolveScore(score); }, 3000); // problems with playing again so wait a little bit
            }
        }
        static getSnake() {
            return game.snake;
        }
        static getAllFoodsAndPreys() {
            return [].concat(game.foods.concat(), game.preys.concat()).filter(ctrl.nullFilter);
        }
        static getClosestSnakes(n) {
            n = (typeof n == "undefined") ? game.snakes.length : n;
            let res = game.snakes.concat()
                .filter(ctrl.nullFilter)
                .sort(ctrl.distanceCoparator);
            res.splice(n);
            res.shift(); // do not include game.snake
            return res;
        }
        static getAllSnakes() {
            return game.snakes.concat();
        }
        static getAllOtherSnakes() {
            return game.snakes.concat()
                .filter(ctrl.nullFilter)
                .filter((snk) => { return !(snk.id == game.snake.id); });
        }
        static positionableToPoint(entity) {
            if (isPoint(entity)) {
                return entity;
            }
            else {
                return { x: entity.xx, y: entity.yy };
            }
        }
        static distance(entityA, entityB) {
            let s1 = ctrl.positionableToPoint(entityA).x - ctrl.positionableToPoint(entityB).x;
            let s2 = ctrl.positionableToPoint(entityA).y - ctrl.positionableToPoint(entityB).y;
            return Math.sqrt(s1 * s1 + s2 * s2);
        }
        static distanceCoparator(entityA, entityB) {
            return ctrl.distance(game.snake, entityA) - ctrl.distance(game.snake, entityB);
        }
        static squareDistance(entityA, entityB) {
            let s1 = Math.abs(entityA.xx - entityB.xx);
            let s2 = Math.abs(entityA.yy - entityB.yy);
            return Math.sqrt(s1 + s2);
        }
        static squareDistanceCoparator(entityA, entityB) {
            return ctrl.squareDistance(game.snake, entityA) - ctrl.distance(game.snake, entityB);
        }
        static sizeComparator() {
            let entityA = arguments[0];
            let entityB = arguments[1];
            return entityB.sz - entityA.sz || entityB.pts.length - entityA.pts.length;
        }
        static nullFilter(entity) {
            return !(entity == null);
        }
        static drawRect(rect, color, stroke = true, fill = true, alpha = 0.5) {
            let drawRect = ctrl.rectToDrawRect(ctrl.rectToRectWH(rect));
            let ctx = ctrl.getCanvasContext();
            ctx.save();
            ctx.beginPath();
            ctx.globalAlpha = alpha;
            ctx.lineWidth = 0.5;
            ctx.rect(drawRect.topLeft.x, drawRect.topLeft.y, drawRect.width, drawRect.height);
            if (stroke) {
                ctx.strokeStyle = color;
                ctx.stroke();
            }
            if (fill) {
                ctx.fillStyle = color;
                ctx.fill();
            }
            ctx.restore();
        }
        static rectToRectWH(rect) {
            let w = Math.abs(rect.cornerA.x - rect.cornerB.x);
            let h = Math.abs(rect.cornerA.y - rect.cornerB.y);
            let tl = {
                x: rect.cornerA.x < rect.cornerB.x ? rect.cornerA.x : rect.cornerB.x,
                y: rect.cornerA.y < rect.cornerB.y ? rect.cornerA.y : rect.cornerB.y,
            };
            return {
                topLeft: tl,
                width: w,
                height: h
            };
        }
        static rectToDrawRect(rect) {
            return {
                topLeft: ctrl.wordToCanvasCoor(rect.topLeft),
                width: rect.width * game.gsc,
                height: rect.height * game.gsc,
            };
        }
        // Draw a circle on the canvas.
        static drawCircle(circle, color, stroke = true, fill = true, alpha = 0.5) {
            let drawCircle = ctrl.mapCircleToCanvas(circle);
            let ctx = ctrl.getCanvasContext();
            ctx.save();
            ctx.beginPath();
            ctx.globalAlpha = alpha;
            ctx.lineWidth = 0.5;
            ctx.arc(drawCircle.center.x, drawCircle.center.y, drawCircle.radius, 0, Math.PI * 2);
            if (stroke) {
                ctx.strokeStyle = color;
                ctx.stroke();
            }
            if (fill) {
                ctx.fillStyle = color;
                ctx.fill();
            }
            ctx.restore();
        }
        /**
         * Maps and scales point.
         */
        static wordToCanvasCoor(point) {
            return {
                x: game.mww2 + (point.x - game.view_xx) * game.gsc,
                y: game.mhh2 + (point.y - game.view_yy) * game.gsc
            };
        }
        /**
         * Maps and scales circle.
         */
        static mapCircleToCanvas(circle) {
            return {
                center: ctrl.wordToCanvasCoor(circle.center),
                radius: circle.radius * game.gsc
            };
        }
        static getCanvasContext() {
            return game.mc.getContext('2d');
        }
        static getMouseCoor() {
            return { x: game.xm, y: game.ym };
        }
        /**
         * @desc [0,0] is s snake head and axes are like for canvas => x increasing right, y increasing down
         */
        static setMouseCoor(point) {
            game.xm = point.x;
            game.ym = point.y;
        }
        static setAccelerationXX(value) {
            // prevents blocking going fast if hold even if not enouhgt mass
            // TODO check jestli je dost dlouhje a jenom v tu chvili opravdu boostovat
            if (value == 1) {
                ctrl.accTrueInRow++;
            }
            else {
                ctrl.accTrueInRow = 0;
            }
            if (ctrl.accTrueInRow % 10) {
                game.setAcceleration(0);
            }
            else {
                game.setAcceleration(value);
            }
        }
        static setAcceleration(value) {
            // prevents blocking going fast if hold whole time and not big enought to accelerate
            if (ctrl.getSnake().pts.filter((item) => { return !item.dying; }).length < 3) {
                game.setAcceleration(0);
                return; // not long enough
            }
            if (value) {
                game.setAcceleration(1);
            }
            else {
                game.setAcceleration(0);
            }
        }
        static getZoom() {
            return game.gsc;
        }
        static setCustomRedraw(func) {
            ctrl._customRedraw = func;
        }
        static resetCustomRedraw() {
            ctrl._customRedraw = function () { };
        }
        static getGame() {
            return game;
        }
    },
    _a._botPlaying = false,
    _a._isInitialized = false,
    _a._gameScoreChecked = false,
    _a._isMouseMoveEnabled = false,
    _a._isControlsVisible = true,
    _a.word = { center: { x: 21600, y: 21600 }, radius: 21160 },
    _a.accTrueInRow = 0,
    _a._customRedraw = function () { },
    _a._customOnkeydown = function (e) {
        switch (e.keyCode) {
            // space for acceleration disabled
            case 32:
                break;
            // + increase zoom
            case 107:
                game.slitherioController.gsc += 0.1;
                console.log("zoom increased to " + game.slitherioController.gsc.toFixed(2));
                break;
            // - decrease zoom
            case 109:
                game.slitherioController.gsc -= 0.1;
                console.log("zoom decreased to " + game.slitherioController.gsc.toFixed(2));
                break;
            // D debug dump
            case 68:
                game.origController.dump = angular.element(document.getElementById('tsneatevocycle-controller')).controller().model;
                console.dir(game.origController.dump);
                console.log("debug dump saved to window.origController.dump:");
                break;
            // M toggle mouse control
            case 77:
                ctrl._isMouseMoveEnabled = !ctrl._isMouseMoveEnabled;
                console.log("mouse move " + (ctrl._isMouseMoveEnabled ? "enabled" : "disabled"));
                break;
            // H toggle hide
            case 72:
                ctrl._isControlsVisible = !ctrl._isControlsVisible;
                document.getElementById('tsneatevocycle-controller').style.display = ctrl._isControlsVisible ? 'block' : 'none';
                console.log("controls div " + (ctrl._isControlsVisible ? "visible" : "hidden"));
                break;
            default:
                console.warn("no key binding");
                break;
        }
    },
    _a);
var SlitherioController = ctrl;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SlitherioController;
var _a;

},{}],13:[function(require,module,exports){
"use strict";
const EvoCycle_1 = require("../lib/EvoCycle");
const EvoCycleControls_1 = require("../lib/controls/EvoCycleControls");
const SlitherioController_1 = require("./SlitherioController");
const SlitherioBotSensorMove_1 = require("./SlitherioBotSensorMove");
class SlitherioLoader {
    constructor() { }
    /**
     * Make sure that lib.controls.js is loaded first.
     * Then (this code) lib.slitherio.js can be loaded.
     */
    static init() {
        // create evocycle with init population
        // let evocycle = new EvoCycle(SlitherioBotXYMove.createInitPopulation(2, [SlitherioBotXYMove.noInputs, SlitherioBotXYMove.noOutputs]));
        let evocycle = new EvoCycle_1.default(SlitherioBotSensorMove_1.default.createInitPopulation(50, [SlitherioBotSensorMove_1.default.noInputs, SlitherioBotSensorMove_1.default.noOutputs]));
        // initialize SlitherioController
        SlitherioController_1.default.init();
        // creates and starts angular app
        EvoCycleControls_1.EvoCycleControls.init(evocycle);
    }
}
exports.SlitherioLoader = SlitherioLoader;
/**
 * @description This is the starting point for Slitherio EvoCycle
 */
SlitherioLoader.init();

},{"../lib/EvoCycle":2,"../lib/controls/EvoCycleControls":4,"./SlitherioBotSensorMove":11,"./SlitherioController":12}]},{},[13]);

    /* jshint ignore:end */

});