(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
class Config {
}
Config.log = {
    functions: false,
    cycle: false,
    generations: false,
    nextFitness: false
};
Config.cycle = {
    continue: false,
    running: false
};
Config.general = {
    alwaysEvaluateFitness: false
};
Config.mutationOptions = {
    mutateOffsprings: true,
    mutateByCloning: true,
    individualTopology: {
        chance: 0.25,
        addNode: {
            chance: 0.03
        },
        addConnection: {
            chance: 0.05
        },
        addNodeXORaddConnection: true,
    },
    individualWeights: {
        chance: 0.8,
        weights: {
            chance: 1,
            mutateSingle: {
                chance: 1,
                stdev: 1
            }
        },
        thresholds: {
            chance: 1,
            mutateSingle: {
                chance: 1,
                stdev: 1
            }
        }
    }
};
Config.crossoverOptions = {
    offspringRatio: 0.8,
    tournamentRatio: 0.5
};
/**
 * distance function coefficient
 * d = (c_e*E)/N + (c_d*D)/N + c_m*W;
 * 3.0 = 1.0 ...... 1.0 ...... 0.4 - values from NEAT paper capter 4.1
 */
Config.speciation = {
    excessCoef: 1,
    disjointCoef: 1,
    matchingCoef: 0.4,
    distanceThreshold: 3.0
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Config;
},{}],2:[function(require,module,exports){
"use strict";
const Config_1 = require("./Config");
const BaseNeatIndividual_1 = require("./model/BaseNeatIndividual");
const Species_1 = require("./model/Species");
/**
 * @class
 * @description Main library class
 */
class EvoCycle {
    constructor(individuals) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.constructor");
        }
        this.config = Config_1.default;
        this.generationCounter = 0;
        this._observers = [];
        this.population = individuals;
        this.species = [new Species_1.default(individuals)];
        this.offsprings = [];
        this.mutants = [];
    }
    addObserver(observer) {
        this._observers.push(observer);
    }
    //////////////////// EVOLUTION FUNCTIONS ////////////////////
    continue(singleGeneration = false) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.continue - " + "running: " + Config_1.default.cycle.running + "; continue: " + Config_1.default.cycle.continue + "; singleGeneration: " + singleGeneration);
        }
        if (!this.population) {
            throw "EvoCycle has to be inicialized first with init population! See EvoCycle.init(individuals: Individuals)";
        }
        if (Config_1.default.cycle.running) {
            console.warn("EvoCycle.continue is already running");
            return; // prevent multiple call
        }
        if (!singleGeneration && !Config_1.default.cycle.continue) {
            console.warn("EvoCycle.continue cannot continue EvoCycle.config.cycle.continue flag is false");
            return; // do not continue
        }
        Config_1.default.cycle.running = true;
        this.doReduction().then((population) => {
            if (Config_1.default.log.functions) {
                console.log("EvoCycle.continue doReduction-then");
            }
            this.population = population;
            this.generationCounter++;
            if (Config_1.default.log.generations) {
                console.log("GENERATION");
                EvoCycle.print(this.population);
            }
            this._observers.forEach((observer) => {
                observer.notifyDoneReduction(this);
            });
            this.step(); // continue with cycle
        });
    }
    // step whole new generation
    step() {
        if (Config_1.default.log.cycle) {
            console.log("population - NEXT STEP:");
            console.dir(this.population);
            EvoCycle.print(this.population);
        }
        // classify individuals
        this.doSpeciation();
        if (Config_1.default.log.cycle) {
            console.log("species - doSpeciation:");
            console.dir(this.species);
        }
        // select individuals to be parents for breeding
        this.doSelection();
        if (Config_1.default.log.cycle) {
            console.log("parents - doSelection:");
            console.dir(this.parents); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this.parents.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // generate new individuals by crossover
        this.doCrossover();
        if (Config_1.default.log.cycle) {
            console.log("offsprings - doCrossover:");
            console.dir(this.offsprings); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this.offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // change individuals by mutations
        this.doMutation();
        if (Config_1.default.log.cycle) {
            console.log("population and offsprings - doMutation:");
            console.dir(this.population);
            console.dir(this.offsprings);
            EvoCycle.print(this.population);
            EvoCycle.print(this.offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }
        // this.population = this.population.concat(offsprings); // TODO az v redukci
        Config_1.default.cycle.running = false;
        this.continue();
    }
    filterDuplicates(individials) {
        return individials.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        });
    }
    doSpeciation() {
        // random species representatives
        let i = this.species.length;
        while (i--) {
            if (this.species[i].clear() == false) {
                this.species.splice(i, 1); // remove this species
            }
        }
        // remove species which can be merged
        // this is not in specification but sounds like a good idea
        // i = this.species.length;
        // while(i-- > 1) { // WARNING iterating in reverse because of splice (removing items)
        //     if(this.species[i].representative.distanceTo(this.species[i-1].representative) < Config.speciation.distanceThreshold) {
        //         this.species[i].individuals = []; // empty the species // still in population so it will be classify again
        //         this.species.splice(i, 1); // remove this species
        //     }
        // }
        // classify individuals
        for (let key in this.population) {
            let indiv = this.population[key];
            let placed = false;
            for (let i = 0; i < this.species.length; i++) {
                if (indiv == this.species[i].representative) {
                    placed = true;
                    break;
                }
            }
            if (placed) {
                continue;
            }
            for (let i = 0; i < this.species.length; i++) {
                // if (indiv != this.species[i].representative)
                // place in existing class
                if (indiv.distanceTo(this.species[i].representative) < Config_1.default.speciation.distanceThreshold) {
                    this.species[i].add(indiv);
                    placed = true;
                    break;
                }
            }
            if (placed) {
                continue;
            }
            else {
                this.species.push(new Species_1.default([indiv]));
            }
        }
        this._observers.forEach((observer) => {
            observer.notifyDoneSpeciation(this, this.species);
        });
    }
    /**
     * Simple tournament selection implementation.
     * Randomly chooses (with repetition) k individuals and picks the best one of the tournament. Repeats until whole population is filled.
     */
    doSelection() {
        // TODO tournament by species
        // TODO how much to crossover vs how much to mutate
        // TODO taky mutovat asi nejen rodice ale vsechno co projde
        // protoze kdyz vypnu crossover uplne tak by nikdy nemutovali
        // nejde mutovat jenom potomky nebo jenom rodice protoze kdyz neni zapnutej crossover tak jsou tyhle skupiny prazdny
        // jak velkou cast populace vygenerovat pres crossover
        let offspringSize = Math.round(this.population.length * Config_1.default.crossoverOptions.offspringRatio);
        // zjistit kolik z jaky specie podle shared fitness
        let sumSharedFittness = this.species.reduce((sum, curr) => {
            return sum + curr.getSharedFitness();
        }, 0);
        this.parents = [];
        for (let key in this.species) {
            let spec = this.species[key];
            let specSize = spec.individuals.length;
            let speciOffspringSize = Math.round(spec.getSharedFitness() / sumSharedFittness * offspringSize);
            speciOffspringSize = speciOffspringSize > 0 ? speciOffspringSize : 1; // atleast one offspring
            // this.parents[key].push(null);
            let tournamentSize = Math.round(specSize * Config_1.default.crossoverOptions.tournamentRatio);
            tournamentSize = tournamentSize < 1 ? 1 : tournamentSize;
            tournamentSize = tournamentSize > specSize ? specSize : tournamentSize;
            // by tournament selection select as many individuals
            // as it is needed for crossover (2 times as much as offspring size)
            let winners = this.parents[key] = [];
            while (winners.length < speciOffspringSize * 2) {
                // select k random individuals (with repetition)
                let contestants = [];
                while (contestants.length < tournamentSize) {
                    let r = Math.floor(Math.random() * specSize);
                    contestants.push(spec.individuals[r]);
                }
                // sort them and select first as winner
                contestants.sort(BaseNeatIndividual_1.BaseNeatIndividual.compare);
                winners.push(contestants[0]);
            }
        }
        // let tournSize = this.tournamentSize;
        // let popSize = choices.length;
        // if (tournSize < 1 || tournSize > popSize) {
        //     throw new RangeError("Tournament size must be greater than 1 and less than or equal to population size: " +
        //         "tournamentSize = " + tournSize + ", populationSize = " + popSize);
        // }
        //
        // // by tournament selection select as many individuals
        // // as it is needed for crossover (2 times as much as offspring size)
        // let winners: Individuals = [];
        // while (winners.length < this.offspringSize * 2) {
        //     // select k random individuals (with repetition)
        //     let contestants: Individuals = [];
        //     while (contestants.length < tournSize) {
        //         let r = Math.floor(Math.random() * popSize);
        //         contestants.push(choices[r]);
        //     }
        //
        //     // sort them and select first as winner
        //     contestants.sort(BaseNeatIndividual.compare);
        //     winners.push(contestants[0]);
        // }
        //
        // return winners;
    }
    /**
     * Crossover
     */
    doCrossover() {
        this.offsprings = [];
        for (let key in this.parents) {
            let parents = this.parents[key];
            let offsprings = this.offsprings[key] = [];
            for (let i = 0; i < this.parents[key].length; i += 2) {
                offsprings.push(parents[i].breed(parents[i + 1]));
            }
        }
    }
    doMutation() {
        for (let key in this.population) {
            let indiv = this.population[key];
            this.mutants = [];
            if (Config_1.default.mutationOptions.mutateByCloning) {
                let clone = new indiv.constructor(indiv);
                let wasMutated = clone.mutate();
                if (wasMutated) {
                    this.mutants.push(clone);
                } // else will be forgotten
            }
            else {
                indiv.mutate();
            }
        }
        if (Config_1.default.mutationOptions.mutateOffsprings) {
            let allOffsprings = this.offsprings.reduce((a, b) => {
                return a.concat(b);
            });
            for (let key in allOffsprings) {
                let indiv = allOffsprings[key];
                indiv.mutate();
            }
        }
    }
    ;
    doReduction() {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.doReduction");
        }
        let allIndividials = [].concat.apply(this.population, this.offsprings).concat(this.mutants);
        // evaluateFitness if needed
        return this.evaluateAllFitness(allIndividials).then((population) => {
            if (Config_1.default.log.functions) {
                console.log("EvoCycle.doReduction evaluateAllFitness-then");
            }
            // sort all individuals and returns the fittest
            allIndividials.sort(BaseNeatIndividual_1.BaseNeatIndividual.compare); // in-place
            let eliminated = allIndividials.splice(this.population.length); // in-place
            this.population = allIndividials;
            for (let i = 0; i < eliminated.length; i++) {
                eliminated[i].eliminated = true;
            }
            return this.population; // chained promise
        });
    }
    evaluateAllFitness(population) {
        if (Config_1.default.log.functions) {
            console.log("EvoCycle.EvaluateAllFitness");
        }
        // CANNOT use Promise.All, needs to be evaluated sequentially
        var allDonePromise = new Promise((resolve, reject) => {
            this.evaluateNextFitness(population, 0, resolve); // init promise recursion
        });
        return allDonePromise;
    }
    evaluateNextFitness(population, current, resolve) {
        if ((Config_1.default.log.functions && Config_1.default.log.nextFitness)
            || Config_1.default.log.nextFitness) {
            console.log("EvoCycle.evaluateNextFitness");
        }
        // end condition
        if (current >= population.length) {
            resolve(population);
        }
        else {
            // recursion
            if (population[current].fitness && !Config_1.default.general.alwaysEvaluateFitness) {
                // do not evaluate if fitness is known, call next immediately
                this.evaluateNextFitness(population, current + 1, resolve);
            }
            else {
                // evaluate unknown fitness, then call next
                population[current].evaluateFitness().then((res) => {
                    this._observers.forEach((observer) => {
                        observer.notifyDoneEvaluateNextFitness(this, population[current]);
                    });
                    this.evaluateNextFitness(population, current + 1, resolve);
                });
            }
        }
    }
    //////////////////// POPULATION FUNCTIONS ////////////////////
    getMaxAvgMinFitness() {
        let max;
        let sum = 0;
        let min;
        for (let i = 0; i < this.population.length; i++) {
            let f = this.population[i].fitness;
            if (max === undefined || max < f) {
                max = f;
            }
            if (min === undefined || min > f) {
                min = f;
            }
            sum += f;
        }
        return [max, sum / this.population.length, min];
    }
    static print(individuals) {
        var arr = individuals.map((item) => {
            return item.toString();
        });
        console.dir(arr);
    }
    //////////////////// GETTERS and SETTERS ////////////////////
    // toudnamentSize
    get tournamentSize() {
        return this._tournamentSize;
    }
    set tournamentSize(value) {
        this._tournamentSize = value;
    }
    getTournamentSize() {
        return this.tournamentSize;
    }
    setTournamentSize(value) {
        this.tournamentSize = value;
    }
    // offsipringSize
    get offspringSize() {
        return this._offspringSize;
    }
    set offspringSize(value) {
        this._offspringSize = value;
    }
    getOffspringSize() {
        return this.offspringSize;
    }
    setOffspringSize(value) {
        this.offspringSize = value;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = EvoCycle;
},{"./Config":1,"./model/BaseNeatIndividual":4,"./model/Species":7}],3:[function(require,module,exports){
"use strict";
class MyMath {
    constructor() { }
    /**
     * @description returns a gaussian random function with the given mean and stdev.
     * @author http://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve#answer-35599181
     * @viz https://en.wikipedia.org/wiki/Marsaglia_polar_method
     * @param mean (mu)
     * @param stdev (sigma) standard deviation
     * @returns {()=>number}
     */
    static gaussian(mean, stdev) {
        var y2;
        var use_last = false;
        return function () {
            var y1;
            if (use_last) {
                y1 = y2;
                use_last = false;
            }
            else {
                var x1, x2, w;
                do {
                    x1 = 2.0 * Math.random() - 1.0;
                    x2 = 2.0 * Math.random() - 1.0;
                    w = x1 * x1 + x2 * x2;
                } while (w >= 1.0);
                w = Math.sqrt((-2.0 * Math.log(w)) / w);
                y1 = x1 * w;
                y2 = x2 * w;
                use_last = true;
            }
            var retval = mean + stdev * y1;
            // if(retval > 0)
            //     return retval;
            // return -retval;
            return retval;
        };
    }
    // public static randomNormal = MyMath.gaussian(0, 1);
    static randomNormal(stdev) {
        return MyMath.gaussian(0, stdev);
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MyMath;
},{}],4:[function(require,module,exports){
"use strict";
const NodeGene_1 = require("./NodeGene");
const ConnectGene_1 = require("./ConnectGene");
const NodeGene_2 = require("./NodeGene");
const MyMath_1 = require("../MyMath");
const Config_1 = require("../Config");
class BaseNeatIndividual {
    constructor() {
        this._id = ++BaseNeatIndividual._idCounter;
        this._genome = [{}, {}];
        this.eliminated = false;
        if (arguments.length == 1 && arguments[0] instanceof BaseNeatIndividual) {
            this.copyConstructor(arguments[0]);
        }
        else {
            this.normalConstructor.apply(this, arguments);
        }
    }
    /**
     * @description copy constructor
     */
    copyConstructor(that) {
        this._inputGenes = [];
        this._outputGenes = [];
        // copy nodeGenes
        for (let key in that.nodeGenes) {
            this.addNodeGene(new NodeGene_1.default(that.nodeGenes[key]));
        }
        // add same connections
        for (let key in that.connectGenes) {
            let thatConnectNode = that.connectGenes[key];
            this.addConnection(this.nodeGenes[thatConnectNode.inNode.innov], this.nodeGenes[thatConnectNode.outNode.innov], thatConnectNode.weight, thatConnectNode.enabled, thatConnectNode.innov);
        }
    }
    /**
     * @description args constructor
     */
    normalConstructor() {
        let inputsLength;
        let outputsLength;
        if (typeof arguments[0] === "number" && typeof arguments[1] === "number") {
            inputsLength = arguments[0];
            outputsLength = arguments[1];
            this._inputGenes = [];
            this._outputGenes = [];
            for (let i = 0; i < inputsLength; i++) {
                this.addNodeGene(new NodeGene_1.default(NodeGene_2.NodeGeneType.Input));
            }
            for (let o = 0; o < outputsLength; o++) {
                this.addNodeGene(new NodeGene_1.default(NodeGene_2.NodeGeneType.Output));
            }
        }
        else if (Array.isArray(arguments[0]) && Array.isArray(arguments[1])) {
            inputsLength = arguments[0].length;
            outputsLength = arguments[1].length;
            this._inputGenes = arguments[0];
            this._outputGenes = arguments[1];
        }
        else {
            throw "Unexpected parameters";
        }
        for (let i = 0; i < inputsLength; i++) {
            for (let o = 0; o < outputsLength; o++) {
                this.addConnection(this._inputGenes[i], this._outputGenes[o]);
            }
        }
    }
    /**
     * @description merges on top of this individual (does not change matching nodes)
     * @param that
     */
    merge(that) {
        // copy nodeGenes which do not exists yet
        for (let key in that.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];
            if (!thisNode) {
                this.addNodeGene(new NodeGene_1.default(thatNode));
            }
        }
        // add connections which do not exists yet
        for (let key in that.connectGenes) {
            let thisConn = this.connectGenes[key];
            let thatConn = that.connectGenes[key];
            if (!thisConn) {
                this.addConnection(this.nodeGenes[thatConn.inNode.innov], this.nodeGenes[thatConn.outNode.innov], thatConn.weight, thatConn.enabled, thatConn.innov);
            }
        }
    }
    static createInitPopulation(populationSize, IndividualClass, firstIndividualConstructorArgs) {
        let population = [];
        let first;
        if (typeof firstIndividualConstructorArgs !== "undefined") {
            first = new IndividualClass(...firstIndividualConstructorArgs);
        }
        else {
            first = new IndividualClass();
        }
        population.push(first);
        for (let i = 1; i < populationSize; i++) {
            // randomize weights
            let next = new IndividualClass(first);
            for (let key in next.connectGenes) {
                let connection = next.connectGenes[key];
                connection.weight = 2 * Math.random() - 1; // TODO config? randomWeightFunc
            }
            population.push(next); // copies of first so same innov numbers
        }
        return population;
    }
    addNodeGene(nodeGene) {
        this.nodeGenes[nodeGene.innov] = nodeGene;
        if (nodeGene.type == NodeGene_2.NodeGeneType.Input) {
            this._inputGenes.push(nodeGene);
        }
        if (nodeGene.type == NodeGene_2.NodeGeneType.Output) {
            this._outputGenes.push(nodeGene);
        }
    }
    addConnectGene(connectGene) {
        this.connectGenes[connectGene.innov] = connectGene;
    }
    addConnection(inNode, outNode, weight, enabled, innov) {
        weight = typeof weight != "undefined" ? weight : 2 * Math.random() - 1; // TODO config? randomWeightFunc
        this.addConnectGene(new ConnectGene_1.default(inNode, outNode, weight, enabled, innov));
    }
    breed(partner) {
        let better;
        let worse;
        if (this.fitness > partner.fitness) {
            better = this;
            worse = partner;
        }
        else {
            better = partner;
            worse = this;
        }
        // make copy of better
        let offspring = new better.constructor(better); // compact way
        // inherit randomly weight for shared genes
        for (let key in offspring.connectGenes) {
            let connOffspring = offspring.connectGenes[key];
            let connWorse = worse.connectGenes[key];
            if (connWorse) {
                let which = 0.5 > Math.random();
                connOffspring.weight = which ? connOffspring.weight : connWorse.weight; // TODO weightCrossoverFunc
                connOffspring.enabled = which ? connOffspring.enabled : connWorse.enabled;
            }
        }
        /** TODO
         * this part is not totally clear for me - from http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf:
         * "In this case, equal fitnesses are assumed, so the disjoint and excess genes are also inherited randomly."
         * How can be disjoint and excess genes inherrited randomly when they are only in one of the parent?
         */
        // if same fitnesses, also inherit (copy) genes from other parent
        if (this.fitness == partner.fitness) {
            offspring.merge(worse);
        }
        // inherit randomly thresholds - imagine as another in connection
        for (let key in offspring.nodeGenes) {
            let nodeOffspring = offspring.nodeGenes[key];
            let nodeWorse = worse.nodeGenes[key];
            if (nodeWorse) {
                nodeOffspring.threshold = 0.5 > Math.random() ? nodeOffspring.threshold : nodeWorse.threshold;
            }
        }
        // chance to enable connection genes
        for (let key in offspring.connectGenes) {
            let connOffspring = offspring.connectGenes[key];
            connOffspring.enabled = 0.25 > Math.random() ? true : connOffspring.enabled; // TODO editable chance
        }
        return offspring;
    }
    mutate() {
        let isMutated = false;
        let topologyMutOptOf = Config_1.default.mutationOptions.individualTopology;
        let weightsMutOptOf = Config_1.default.mutationOptions.individualWeights;
        if (topologyMutOptOf.chance > Math.random()) {
            let xorChance; // 2 for false, <0;1) for addNode, <1;2) for addConnection
            if (topologyMutOptOf.addNodeXORaddConnection) {
                xorChance = Math.random() * 2; // Math.random() never equals 1 => xorChance never equals 2 which is reserved for xorChance false
            }
            else {
                xorChance = 2;
            }
            if (xorChance === 2 || (xorChance >= 0 && xorChance < 1)) {
                if (topologyMutOptOf.addNode.chance > Math.random()) {
                    this.mutateAddNode();
                    isMutated = true;
                }
            }
            if (xorChance === 2 || (xorChance >= 1 && xorChance < 2)) {
                if (topologyMutOptOf.addConnection.chance > Math.random()) {
                    this.mutateAddConnection();
                    isMutated = true;
                }
            }
        }
        if (weightsMutOptOf.chance > Math.random()) {
            if (weightsMutOptOf.weights.chance > Math.random()) {
                this.mutateWeights();
                isMutated = true;
            }
            if (weightsMutOptOf.thresholds.chance > Math.random()) {
                this.mutateThresholds();
                isMutated = true;
            }
        }
        this._fitness = null;
        return isMutated;
    }
    mutateAddConnection() {
        let n1 = this.getRandomNodeGene();
        let n2;
        do {
            n2 = this.getRandomNodeGene();
        } while (n1.id == n2.id);
        if (!this.areConnected(n1, n2)) {
            return this.addConnection(n1, n2);
        }
        else {
            return this.mutateAddConnection();
        }
    }
    mutateAddNode() {
        let edge;
        do {
            edge = this.getRandomConnectGene();
        } while (edge.enabled == false);
        edge.enabled = false;
        let inNode = edge.inNode;
        let outNode = edge.outNode;
        let innerNode = new NodeGene_1.default();
        this.addNodeGene(innerNode);
        this.addConnection(inNode, innerNode, 1);
        this.addConnection(innerNode, outNode, edge.weight);
    }
    mutateWeights() {
        for (let key in this.connectGenes) {
            if (Config_1.default.mutationOptions.individualWeights.weights.mutateSingle.chance > Math.random()) {
                let connection = this.connectGenes[key];
                connection.weight += MyMath_1.default.randomNormal(Config_1.default.mutationOptions.individualWeights.weights.mutateSingle.stdev)();
            }
        }
    }
    mutateThresholds() {
        for (let key in this.nodeGenes) {
            if (Config_1.default.mutationOptions.individualWeights.thresholds.mutateSingle.chance > Math.random()) {
                let node = this.nodeGenes[key];
                node.threshold += MyMath_1.default.randomNormal(Config_1.default.mutationOptions.individualWeights.thresholds.mutateSingle.stdev)();
            }
        }
    }
    evaluateNetwork(inputs) {
        let outputs = [];
        // evaluate input nodes (recursion end condition)
        for (let i = 0; i < this.inputGenes.length; i++) {
            this.inputGenes[i].evaluateOutput(inputs[i]);
        }
        // evaluate network from output nodes by recursion
        for (let key in this.outputGenes) {
            outputs.push(this.outputGenes[key].evaluateOutput());
        }
        // reset network evaluation for next evaluation
        for (let key in this.nodeGenes) {
            this.nodeGenes[key].resetOutput();
        }
        return outputs;
    }
    toString() {
        let nodeGenesString = "";
        for (let key in this.nodeGenes) {
            nodeGenesString += "\t" + this.nodeGenes[key].toString() + ";\n";
        }
        let connectGenesString = "";
        for (let key in this.connectGenes) {
            connectGenesString += "\t" + this.connectGenes[key].toString() + ";\n";
        }
        return "BaseNeatIndividual = {\n" +
            "\tid: " + this.id + ";\n" +
            "\tfitness: " + this.fitness + ";\n" +
            nodeGenesString +
            connectGenesString
            + "}";
    }
    static nodeGeneToText(nodeGene) {
        return `innov: ${nodeGene.innov}
                threshold: ${nodeGene.threshold}`;
    }
    static connectGeneToText(connectGeneGene) {
        return `innov: ${connectGeneGene.innov}
                weight: ${connectGeneGene.weight.toFixed(2)}
                ${connectGeneGene.enabled ? "" : "DISABLED"}`;
    }
    toGOJS() {
        let json = {};
        json["nodeKeyProperty"] = "id";
        json["nodeDataArray"] = [];
        for (let key in this.nodeGenes) {
            let nodeGene = this.nodeGenes[key];
            json["nodeDataArray"].push({
                id: nodeGene.id,
                text: BaseNeatIndividual.nodeGeneToText(nodeGene)
            });
        }
        json["linkDataArray"] = [];
        for (let key in this.connectGenes) {
            let connectGene = this.connectGenes[key];
            json["linkDataArray"].push({
                from: connectGene.inNode.id,
                to: connectGene.outNode.id,
                text: BaseNeatIndividual.connectGeneToText(connectGene)
            });
        }
        // json["nodeDataArray"] = [{ "id": 0, "loc": "120 120", "text": "XXX" }];
        // json["linkDataArray"] = [{ "from": 0, "to": 0, "text": "up or timer", "curviness": -20 }];
        return json;
    }
    //
    // GET functions
    //
    /**
     * @complexity O(#NG)
     * TODO often function and O(n) complexity, could be eliminated if array of NodeGenes is used, but how about other functions (crossover), probably easy by array functions
     * pick random from stream - complexity O(n)
     * problem that object is used not array
     * size will not help
     * @see next solution
     */
    getRandomNodeGene() {
        let result;
        let count = 0;
        for (let key in this.nodeGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.nodeGenes[key];
            }
        }
        return result;
    }
    // /**
    //  * TODO question is if Object.keys() is O(1) or O(n)
    //  * @returns {any}
    //  */
    // private getRandomNodeGeneXXX() {
    //     var keys = Object.keys(this.nodeGenes); // O(n) so makes no sence
    //     return this.nodeGenes[keys[ keys.length * Math.random() << 0]];
    // }
    /**
     * TODO same problem as for getRandomNdeGene
     * @complexity O(#CG)
     */
    getRandomConnectGene() {
        let result;
        let count = 0;
        for (let key in this.connectGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.connectGenes[key];
            }
        }
        return result;
    }
    /**
     * @complexity O(#CG) but probably cannot be better
     */
    areConnected(n1, n2) {
        for (let key in this.connectGenes) {
            // check
            let edge = this.connectGenes[key];
            if (edge.inNode.id == n1.id && edge.outNode.id == n2.id
                || edge.inNode.id == n2.id && edge.outNode.id == n1.id) {
                return true;
            }
        }
        return false;
    }
    static compare(first, second) {
        return second.fitness - first.fitness;
    }
    distanceTo(that) {
        let c_e = Config_1.default.speciation.excessCoef;
        let c_d = Config_1.default.speciation.disjointCoef;
        let c_m = Config_1.default.speciation.matchingCoef;
        let N = 0; // #genes in longer
        let D = 0; // #genes disjoint
        let E = 0; // #genes excess
        let W = 0; // average weight differences of matching genes W
        let M = 0; // #genes matching
        let thisArray = this.getConnectGenesAsArray();
        let thatArray = that.getConnectGenesAsArray();
        let i = 0;
        let j = 0;
        while (i < thisArray.length && j < thatArray.length) {
            let thisConn = thisArray[i];
            let thatConn = thatArray[j];
            if (thisConn.innov == thatConn.innov) {
                W += Math.abs(thisConn.weight - thatConn.weight);
                M++;
                i++;
                j++;
            }
            else {
                D++;
                if (thisConn.innov < thatConn.innov) {
                    j++;
                }
                else {
                    i++;
                }
            }
        }
        E = i < thisArray.length ? thisArray.length - i : (j < thatArray.length ? thatArray.length - j : 0); // excess
        N = thisArray.length > thatArray.length ? thisArray.length : thatArray.length;
        // count thresholds also as input connection
        for (let key in this.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];
            if (thatNode) {
                W += Math.abs(thisNode.threshold - thatNode.threshold);
                M++;
            }
        }
        W = W / M;
        return (c_e * E) / N + (c_d * D) / N + c_m * W;
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get fitness() {
        return this._fitness;
    }
    get nodeGenes() {
        return this._genome[0];
    }
    get connectGenes() {
        return this._genome[1];
    }
    getConnectGenesAsArray() {
        let res = [];
        for (let key in this.connectGenes) {
            res.push(this.connectGenes[key]);
        }
        return res.sort((a, b) => { return b.innov - a.innov; });
    }
    get inputGenes() {
        return this._inputGenes;
    }
    get outputGenes() {
        return this._outputGenes;
    }
}
BaseNeatIndividual._idCounter = 0;
exports.BaseNeatIndividual = BaseNeatIndividual;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BaseNeatIndividual;
},{"../Config":1,"../MyMath":3,"./ConnectGene":5,"./NodeGene":6}],5:[function(require,module,exports){
"use strict";
class ConnectGene {
    // public inNode: NodeGene;
    // public outNode: NodeGene;
    // public weight: number;
    constructor(inNode, outNode, weight, enabled, innov) {
        this.inNode = inNode;
        this.outNode = outNode;
        this.weight = weight;
        this._id = ++ConnectGene._idCounter;
        this._innov = typeof innov != "undefined" ? innov : ++ConnectGene._innovCounter;
        this.enabled = typeof enabled != "undefined" ? enabled : true;
        this.outNode.addInConnection(this);
    }
    toString() {
        return "ConnecGene = {id: " + this.id + "; innov: " + this.innov + "; in: " + this.inNode.id + "; out: " + this.outNode.id + "; weight: " + this.weight.toFixed(2) + "; enabled: " + this.enabled + "}";
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get innov() {
        return this._innov;
    }
}
ConnectGene._idCounter = 0;
ConnectGene._innovCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ConnectGene;
},{}],6:[function(require,module,exports){
"use strict";
class NodeGene {
    constructor() {
        this._id = ++NodeGene._idCounter;
        if (arguments.length == 1 && arguments[0] instanceof NodeGene) {
            this.copyConstructor(arguments[0]);
        }
        else {
            this.normalConstructor(arguments[0], arguments[1]);
        }
    }
    copyConstructor(that) {
        // does not copy connectGenes - copied from caller
        this._innov = that._innov;
        this._inConnections = [];
        this.threshold = that.threshold;
        this.output = that.output;
        this.lastOutput = that.lastOutput;
        this._state = that._state;
        this._type = that._type;
        this._activationFunc = that._activationFunc;
    }
    normalConstructor(type, activationFunc) {
        this._innov = ++NodeGene._innovCounter;
        this._inConnections = [];
        this.threshold = 0;
        this.output = null;
        this.lastOutput = 0;
        this._state = NodeGeneState.New;
        this._type = typeof type !== "undefined" ? arguments[0] : NodeGeneType.Hidden;
        this._activationFunc = typeof activationFunc !== "undefined" ? activationFunc : softStep;
    }
    addInConnection(inConnection) {
        this.inConnections.push(inConnection);
    }
    evaluateOutput(input) {
        if (this.state == NodeGeneState.Open) {
            return this.lastOutput;
        }
        if (this.state == NodeGeneState.Closed) {
            return this.output;
        }
        // other - evaluate recursively
        this._state = NodeGeneState.Open;
        let sum = 0;
        // input nodes - sum over inputs, then over recurrent connections
        if (this.type == NodeGeneType.Input && arguments.length == 1) {
            sum += input;
            sum += this.inConnections.reduce((sum, current) => {
                if (current.enabled) {
                    return sum + current.inNode.lastOutput * current.weight; // NO recursion - recurrent connection
                }
                else {
                    return sum;
                }
            }, 0);
            sum += this.threshold;
        }
        else {
            // hidden, output nodes - sum over in connections
            sum += this.inConnections.reduce((sum, current) => {
                if (current.enabled) {
                    return sum + current.inNode.evaluateOutput() * current.weight; // recursion
                }
                else {
                    return sum;
                }
            }, 0);
            sum += this.threshold;
        }
        this._state = NodeGeneState.Closed;
        this.output = this.activationFunc(sum);
        return this.output;
    }
    resetOutput() {
        this.lastOutput = this.output;
        this.output = null;
        this._state = NodeGeneState.New;
    }
    toString() {
        return "NodeGene = {id: " + this.id + "; innov: " + this.innov + "; type:" + this.type + "; threshold: " + this.threshold.toFixed(2) + "}";
    }
    //
    // GETTERS and SETTERS
    //
    get id() {
        return this._id;
    }
    get innov() {
        return this._innov;
    }
    get type() {
        return this._type;
    }
    get activationFunc() {
        return this._activationFunc;
    }
    get inConnections() {
        return this._inConnections;
    }
    get state() {
        return this._state;
    }
}
NodeGene._idCounter = 0;
NodeGene._innovCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = NodeGene;
var NodeGeneType;
(function (NodeGeneType) {
    NodeGeneType[NodeGeneType["Input"] = 0] = "Input";
    NodeGeneType[NodeGeneType["Hidden"] = 1] = "Hidden";
    NodeGeneType[NodeGeneType["Output"] = 2] = "Output";
})(NodeGeneType = exports.NodeGeneType || (exports.NodeGeneType = {}));
var NodeGeneState;
(function (NodeGeneState) {
    NodeGeneState[NodeGeneState["New"] = 0] = "New";
    NodeGeneState[NodeGeneState["Open"] = 1] = "Open";
    NodeGeneState[NodeGeneState["Closed"] = 2] = "Closed";
})(NodeGeneState = exports.NodeGeneState || (exports.NodeGeneState = {}));
// TODO somehow better, or class and static fieldy
var NodeGeneActFunc;
(function (NodeGeneActFunc) {
})(NodeGeneActFunc = exports.NodeGeneActFunc || (exports.NodeGeneActFunc = {}));
function softStep(x) {
    return 1 / (1 + Math.exp(-x));
}
exports.softStep = softStep;
function binaryStep(x) {
    return x >= 0 ? 1 : 0;
}
exports.binaryStep = binaryStep;
},{}],7:[function(require,module,exports){
"use strict";
class Species {
    constructor(individuals) {
        this._id = ++Species._idCounter;
        this.individuals = typeof individuals != "undefined" ? individuals : [];
    }
    add(individual) {
        this.individuals.push(individual);
    }
    /**
     * keeps single random representative
     */
    clear() {
        let representative = null;
        let count = 0;
        for (let key in this.individuals) {
            if (!this.individuals[key].eliminated) {
                if (Math.random() < 1 / ++count) {
                    representative = this.individuals[key];
                }
            }
        }
        if (representative == null) {
            this.individuals = [];
            return false;
        }
        else {
            this.individuals = [representative];
            return true;
        }
    }
    /**
     * average fitness over species
     */
    getSharedFitness() {
        return this.individuals.reduce((sum, curr) => { return sum + curr.fitness; }, 0) / this.individuals.length;
    }
    isEmpty() {
        return this.individuals.length == 0;
    }
    get representative() {
        return this.individuals[0];
    }
    get id() {
        return this._id;
    }
}
Species._idCounter = 0;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Species;
},{}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJDb25maWcudHMiLCJFdm9DeWNsZS50cyIsIk15TWF0aC50cyIsIm1vZGVsL0Jhc2VOZWF0SW5kaXZpZHVhbC50cyIsIm1vZGVsL0Nvbm5lY3RHZW5lLnRzIiwibW9kZWwvTm9kZUdlbmUudHMiLCJtb2RlbC9TcGVjaWVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQ0FBOztBQUNrQixVQUFHLEdBQUc7SUFDaEIsU0FBUyxFQUFFLEtBQUs7SUFDaEIsS0FBSyxFQUFFLEtBQUs7SUFDWixXQUFXLEVBQUUsS0FBSztJQUNsQixXQUFXLEVBQUUsS0FBSztDQUNyQixDQUFDO0FBRVksWUFBSyxHQUFHO0lBQ2xCLFFBQVEsRUFBRSxLQUFLO0lBQ2YsT0FBTyxFQUFFLEtBQUs7Q0FDakIsQ0FBQztBQUVZLGNBQU8sR0FBRztJQUNwQixxQkFBcUIsRUFBRSxLQUFLO0NBQy9CLENBQUM7QUFFWSxzQkFBZSxHQUFHO0lBQzVCLGdCQUFnQixFQUFFLElBQUk7SUFDdEIsZUFBZSxFQUFFLElBQUk7SUFDckIsa0JBQWtCLEVBQUU7UUFDaEIsTUFBTSxFQUFFLElBQUk7UUFDWixPQUFPLEVBQUU7WUFDTCxNQUFNLEVBQUUsSUFBSTtTQUNmO1FBQ0QsYUFBYSxFQUFFO1lBQ1gsTUFBTSxFQUFFLElBQUk7U0FDZjtRQUNELHVCQUF1QixFQUFFLElBQUk7S0FDaEM7SUFDRCxpQkFBaUIsRUFBRTtRQUNmLE1BQU0sRUFBRSxHQUFHO1FBQ1gsT0FBTyxFQUFFO1lBQ0wsTUFBTSxFQUFFLENBQUM7WUFDVCxZQUFZLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsS0FBSyxFQUFFLENBQUM7YUFDWDtTQUNKO1FBQ0QsVUFBVSxFQUFFO1lBQ1IsTUFBTSxFQUFFLENBQUM7WUFDVCxZQUFZLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLENBQUM7Z0JBQ1QsS0FBSyxFQUFFLENBQUM7YUFDWDtTQUNKO0tBQ0o7Q0FDSixDQUFDO0FBRVksdUJBQWdCLEdBQUc7SUFDN0IsY0FBYyxFQUFFLEdBQUc7SUFDbkIsZUFBZSxFQUFFLEdBQUc7Q0FDdkIsQ0FBQztBQUVGOzs7O0dBSUc7QUFDVyxpQkFBVSxHQUFHO0lBQ3ZCLFVBQVUsRUFBRSxDQUFDO0lBQ2IsWUFBWSxFQUFHLENBQUM7SUFDaEIsWUFBWSxFQUFFLEdBQUc7SUFDakIsaUJBQWlCLEVBQUUsR0FBRztDQUN6QixDQUFDOztBQWhFTix5QkFrRUM7OztBQ2xFRCxxQ0FBOEI7QUFDOUIsbUVBQTJFO0FBQzNFLDZDQUFzQztBQWF0Qzs7O0dBR0c7QUFDSDtJQWdCSSxZQUFZLFdBQXdCO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUVyQixJQUFJLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQztRQUM5QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxpQkFBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVNLFdBQVcsQ0FBQyxRQUEwQjtRQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsNkRBQTZEO0lBRXRELFFBQVEsQ0FBQyxtQkFBNEIsS0FBSztRQUM3QyxFQUFFLENBQUMsQ0FBQyxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEdBQUcsV0FBVyxHQUFHLGdCQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxjQUFjLEdBQUcsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLHNCQUFzQixHQUFHLGdCQUFnQixDQUFDLENBQUM7UUFDbEssQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSx3R0FBd0csQ0FBQztRQUNuSCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFDckQsTUFBTSxDQUFDLENBQUMsd0JBQXdCO1FBQ3BDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLENBQUMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM5QyxPQUFPLENBQUMsSUFBSSxDQUFDLGdGQUFnRixDQUFDLENBQUM7WUFDL0YsTUFBTSxDQUFDLENBQUMsa0JBQWtCO1FBQzlCLENBQUM7UUFFRCxnQkFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBRTVCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0NBQW9DLENBQUMsQ0FBQztZQUN0RCxDQUFDO1lBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7WUFDN0IsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFFekIsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEMsQ0FBQztZQUVELElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBMEI7Z0JBQy9DLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUN0QyxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLHNCQUFzQjtRQUN2QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0QkFBNEI7SUFDckIsSUFBSTtRQUVQLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFFRCx1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlCLENBQUM7UUFFRCxnREFBZ0Q7UUFDaEQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsMEVBQTBFO1lBQ3JHLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDcEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDUixDQUFDO1FBRUQsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixFQUFFLENBQUMsQ0FBQyxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLDBFQUEwRTtZQUN4RyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3ZDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ1IsQ0FBQztRQUVELGtDQUFrQztRQUNsQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEIsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7WUFDdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDN0IsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDaEMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN2QyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNSLENBQUM7UUFFRCw2RUFBNkU7UUFDN0UsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVPLGdCQUFnQixDQUFDLFdBQXdCO1FBQzdDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJO1lBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTyxZQUFZO1FBQ2hCLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztRQUM1QixPQUFPLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDVCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLHNCQUFzQjtZQUNyRCxDQUFDO1FBQ0wsQ0FBQztRQUVELHFDQUFxQztRQUNyQywyREFBMkQ7UUFDM0QsMkJBQTJCO1FBQzNCLHNGQUFzRjtRQUN0Riw4SEFBOEg7UUFDOUgscUhBQXFIO1FBQ3JILDREQUE0RDtRQUM1RCxRQUFRO1FBQ1IsSUFBSTtRQUVKLHVCQUF1QjtRQUN2QixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUM5QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRWpDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNuQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzNDLEVBQUUsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQzFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ2QsS0FBSyxDQUFDO2dCQUNWLENBQUM7WUFDTCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDVCxRQUFRLENBQUM7WUFDYixDQUFDO1lBRUQsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUMzQywrQ0FBK0M7Z0JBQy9DLDBCQUEwQjtnQkFDMUIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDekYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzNCLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ2QsS0FBSyxDQUFDO2dCQUNWLENBQUM7WUFDTCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDVCxRQUFRLENBQUM7WUFDYixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxpQkFBTyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUEwQjtZQUMvQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7O09BR0c7SUFDSyxXQUFXO1FBR2YsNkJBQTZCO1FBQzdCLG1EQUFtRDtRQUVuRCwyREFBMkQ7UUFDM0QsNkRBQTZEO1FBQzdELG9IQUFvSDtRQUVwSCxzREFBc0Q7UUFDdEQsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxnQkFBTSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBRWhHLG1EQUFtRDtRQUNuRCxJQUFJLGlCQUFpQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUk7WUFDbEQsTUFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN6QyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFTixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNsQixHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTdCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBRXZDLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxpQkFBaUIsR0FBRyxhQUFhLENBQUMsQ0FBQztZQUNqRyxrQkFBa0IsR0FBRyxrQkFBa0IsR0FBRyxDQUFDLEdBQUcsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLENBQUMsd0JBQXdCO1lBRTlGLGdDQUFnQztZQUVoQyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxnQkFBTSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3BGLGNBQWMsR0FBRyxjQUFjLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxjQUFjLENBQUM7WUFDekQsY0FBYyxHQUFHLGNBQWMsR0FBRyxRQUFRLEdBQUcsUUFBUSxHQUFHLGNBQWMsQ0FBQztZQUV2RSxxREFBcUQ7WUFDckQsb0VBQW9FO1lBQ3BFLElBQUksT0FBTyxHQUFnQixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNsRCxPQUFPLE9BQU8sQ0FBQyxNQUFNLEdBQUcsa0JBQWtCLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQzdDLGdEQUFnRDtnQkFDaEQsSUFBSSxXQUFXLEdBQWdCLEVBQUUsQ0FBQztnQkFDbEMsT0FBTyxXQUFXLENBQUMsTUFBTSxHQUFHLGNBQWMsRUFBRSxDQUFDO29CQUN6QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsQ0FBQztvQkFDN0MsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLENBQUM7Z0JBRUQsdUNBQXVDO2dCQUN2QyxXQUFXLENBQUMsSUFBSSxDQUFDLHVDQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUM3QyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLENBQUM7UUFFTCxDQUFDO1FBRUQsdUNBQXVDO1FBQ3ZDLGdDQUFnQztRQUNoQyw4Q0FBOEM7UUFDOUMsa0hBQWtIO1FBQ2xILDhFQUE4RTtRQUM5RSxJQUFJO1FBQ0osRUFBRTtRQUNGLHdEQUF3RDtRQUN4RCx1RUFBdUU7UUFDdkUsaUNBQWlDO1FBQ2pDLG9EQUFvRDtRQUNwRCx1REFBdUQ7UUFDdkQseUNBQXlDO1FBQ3pDLCtDQUErQztRQUMvQyx1REFBdUQ7UUFDdkQsd0NBQXdDO1FBQ3hDLFFBQVE7UUFDUixFQUFFO1FBQ0YsOENBQThDO1FBQzlDLG9EQUFvRDtRQUNwRCxvQ0FBb0M7UUFDcEMsSUFBSTtRQUNKLEVBQUU7UUFDRixrQkFBa0I7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssV0FBVztRQUVmLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzNCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFaEMsSUFBSSxVQUFVLEdBQWdCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3hELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNuRCxVQUFVLENBQUMsSUFBSSxDQUNYLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUNuQyxDQUFDO1lBQ04sQ0FBQztRQUNMLENBQUM7SUFFTCxDQUFDO0lBRU8sVUFBVTtRQUVkLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsRUFBRSxDQUFBLENBQUMsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxLQUFLLEdBQXVCLElBQVUsS0FBSyxDQUFDLFdBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFcEUsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUVoQyxFQUFFLENBQUEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNaLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixDQUFDLENBQUMseUJBQXlCO1lBQy9CLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDbkIsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDNUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7WUFDSCxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixJQUFJLEtBQUssR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRS9CLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNuQixDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFBQSxDQUFDO0lBRU0sV0FBVztRQUNmLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFFRCxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRTVGLDRCQUE0QjtRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVU7WUFDM0QsRUFBRSxDQUFDLENBQUMsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQ2hFLENBQUM7WUFFRCwrQ0FBK0M7WUFDL0MsY0FBYyxDQUFDLElBQUksQ0FBQyx1Q0FBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFdBQVc7WUFDNUQsSUFBSSxVQUFVLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsV0FBVztZQUMzRSxJQUFJLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQztZQUVqQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDekMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDcEMsQ0FBQztZQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsa0JBQWtCO1FBQzlDLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUVPLGtCQUFrQixDQUFDLFVBQXVCO1FBQzlDLEVBQUUsQ0FBQyxDQUFDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1FBQy9DLENBQUM7UUFFRCw2REFBNkQ7UUFFN0QsSUFBSSxjQUFjLEdBQUcsSUFBSSxPQUFPLENBQWMsQ0FBQyxPQUFPLEVBQUUsTUFBTTtZQUMxRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLHlCQUF5QjtRQUMvRSxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxjQUFjLENBQUM7SUFDMUIsQ0FBQztJQUVPLG1CQUFtQixDQUFDLFVBQXVCLEVBQUUsT0FBZSxFQUFFLE9BQVk7UUFDOUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLElBQUksZ0JBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO2VBQzdDLGdCQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1FBQ2hELENBQUM7UUFFRCxnQkFBZ0I7UUFDaEIsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFFSixZQUFZO1lBQ1osRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGdCQUFNLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztnQkFDdkUsNkRBQTZEO2dCQUM3RCxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLE9BQU8sR0FBRyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDL0QsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLDJDQUEyQztnQkFDM0MsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUc7b0JBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBeUI7d0JBQzlDLFFBQVEsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ3RFLENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsT0FBTyxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDL0QsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBQ0wsQ0FBQztJQUVMLENBQUM7SUFFRCw4REFBOEQ7SUFFdkQsbUJBQW1CO1FBQ3RCLElBQUksR0FBVyxDQUFDO1FBQ2hCLElBQUksR0FBRyxHQUFXLENBQUMsQ0FBQztRQUNwQixJQUFJLEdBQVcsQ0FBQztRQUVoQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDOUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFFbkMsRUFBRSxDQUFDLENBQUMsR0FBRyxLQUFLLFNBQVMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0IsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNaLENBQUM7WUFFRCxFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQztZQUVELEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDYixDQUFDO1FBRUQsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRU0sTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUF3QjtRQUN4QyxJQUFJLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBd0I7WUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELDZEQUE2RDtJQUU3RCxpQkFBaUI7SUFDakIsSUFBWSxjQUFjO1FBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxJQUFZLGNBQWMsQ0FBQyxLQUFhO1FBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxpQkFBaUI7UUFDcEIsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVNLGlCQUFpQixDQUFDLEtBQWE7UUFDbEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVELGlCQUFpQjtJQUNqQixJQUFZLGFBQWE7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELElBQVksYUFBYSxDQUFDLEtBQWE7UUFDbkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUVNLGdCQUFnQjtRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRU0sZ0JBQWdCLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0NBRUo7O0FBL2NELDJCQStjQzs7O0FDbGVEO0lBRUksZ0JBQXVCLENBQUM7SUFFeEI7Ozs7Ozs7T0FPRztJQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBWSxFQUFFLEtBQWE7UUFDOUMsSUFBSSxFQUFVLENBQUM7UUFDZixJQUFJLFFBQVEsR0FBWSxLQUFLLENBQUM7UUFDOUIsTUFBTSxDQUFDO1lBQ0gsSUFBSSxFQUFVLENBQUM7WUFDZixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNYLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBQ1IsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNyQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxFQUFVLEVBQUUsRUFBVSxFQUFFLENBQVMsQ0FBQztnQkFDdEMsR0FBRyxDQUFDO29CQUNBLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQztvQkFDL0IsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDO29CQUMvQixDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUMxQixDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsRUFBRTtnQkFDbkIsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDcEIsQ0FBQztZQUVELElBQUksTUFBTSxHQUFHLElBQUksR0FBRyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQy9CLGlCQUFpQjtZQUNqQixxQkFBcUI7WUFDckIsa0JBQWtCO1lBRWxCLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDbEIsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQUVELHNEQUFzRDtJQUUvQyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQVk7UUFDbkMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Q0FDSjs7QUEvQ0QseUJBK0NDOzs7QUMvQ0QseUNBQWtDO0FBQ2xDLCtDQUF3QztBQUV4Qyx5Q0FBd0M7QUFFeEMsc0NBQStCO0FBQy9CLHNDQUErQjtBQVUvQjtJQWVJO1FBQ0ksSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLGtCQUFrQixDQUFDLFVBQVUsQ0FBQztRQUMzQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBRXhCLEVBQUUsQ0FBQSxDQUFDLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsWUFBWSxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNsRCxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ssZUFBZSxDQUFDLElBQXdCO1FBQzVDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBRXZCLGlCQUFpQjtRQUNqQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksa0JBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCxDQUFDO1FBRUQsdUJBQXVCO1FBQ3ZCLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FDZCxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFDN0MsZUFBZSxDQUFDLE1BQU0sRUFDdEIsZUFBZSxDQUFDLE9BQU8sRUFDdkIsZUFBZSxDQUFDLEtBQUssQ0FDeEIsQ0FBQztRQUNOLENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSyxpQkFBaUI7UUFDckIsSUFBSSxZQUFvQixDQUFDO1FBQ3pCLElBQUksYUFBcUIsQ0FBQztRQUUxQixFQUFFLENBQUEsQ0FBQyxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLElBQUksT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN0RSxZQUFZLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLGFBQWEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFFdkIsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLGtCQUFRLENBQUMsdUJBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELENBQUM7WUFFRCxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksa0JBQVEsQ0FBQyx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDeEQsQ0FBQztRQUVMLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSxZQUFZLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNuQyxhQUFhLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLHVCQUF1QixDQUFDO1FBQ2xDLENBQUM7UUFFRCxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ25DLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEUsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssS0FBSyxDQUFDLElBQXdCO1FBRWxDLHlDQUF5QztRQUN6QyxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLFFBQVEsR0FBYSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdDLElBQUksUUFBUSxHQUFhLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFN0MsRUFBRSxDQUFBLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNYLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxrQkFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDN0MsQ0FBQztRQUNMLENBQUM7UUFFRCwwQ0FBMEM7UUFDMUMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXRDLEVBQUUsQ0FBQSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsYUFBYSxDQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUN0QyxRQUFRLENBQUMsTUFBTSxFQUNmLFFBQVEsQ0FBQyxPQUFPLEVBQ2hCLFFBQVEsQ0FBQyxLQUFLLENBQ2pCLENBQUM7WUFDTixDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsb0JBQW9CLENBQUMsY0FBc0IsRUFBRSxlQUEyRCxFQUFFLDhCQUEyQztRQUMvSixJQUFJLFVBQVUsR0FBeUIsRUFBRSxDQUFDO1FBQzFDLElBQUksS0FBeUIsQ0FBQztRQUM5QixFQUFFLENBQUEsQ0FBQyxPQUFPLDhCQUE4QixLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDdkQsS0FBSyxHQUFHLElBQUksZUFBZSxDQUFDLEdBQUcsOEJBQThCLENBQUMsQ0FBQztRQUNuRSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixLQUFLLEdBQUcsSUFBSSxlQUFlLEVBQUUsQ0FBQztRQUNsQyxDQUFDO1FBRUQsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQWMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3JDLG9CQUFvQjtZQUNwQixJQUFJLElBQUksR0FBRyxJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUV0QyxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLGdDQUFnQztZQUM3RSxDQUFDO1lBRUQsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHdDQUF3QztRQUNuRSxDQUFDO1FBRUQsTUFBTSxDQUFDLFVBQVUsQ0FBRTtJQUN2QixDQUFDO0lBRU8sV0FBVyxDQUFDLFFBQWtCO1FBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQztRQUMxQyxFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLHVCQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBQ0QsRUFBRSxDQUFBLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckMsQ0FBQztJQUNMLENBQUM7SUFFTyxjQUFjLENBQUMsV0FBd0I7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsV0FBVyxDQUFDO0lBQ3ZELENBQUM7SUFFTyxhQUFhLENBQUMsTUFBZSxFQUFFLE9BQWdCLEVBQUUsTUFBZ0IsRUFBRSxPQUFpQixFQUFFLEtBQWM7UUFDeEcsTUFBTSxHQUFHLE9BQU8sTUFBTSxJQUFJLFdBQVcsR0FBRyxNQUFNLEdBQUcsQ0FBQyxHQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxnQ0FBZ0M7UUFDdEcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLHFCQUFXLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUlNLEtBQUssQ0FBQyxPQUEyQjtRQUVwQyxJQUFJLE1BQTBCLENBQUM7UUFDL0IsSUFBSSxLQUF5QixDQUFDO1FBRTlCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDaEMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNkLEtBQUssR0FBRyxPQUFPLENBQUM7UUFDcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNqQixLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLENBQUM7UUFFRCxzQkFBc0I7UUFDdEIsSUFBSSxTQUFTLEdBQXVCLElBQVUsTUFBTSxDQUFDLFdBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLGNBQWM7UUFFekYsMkNBQTJDO1FBQzNDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksYUFBYSxHQUFnQixTQUFTLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdELElBQUksU0FBUyxHQUFnQixLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXJELEVBQUUsQ0FBQSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsSUFBSSxLQUFLLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFFaEMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLEdBQUcsYUFBYSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsMkJBQTJCO2dCQUNuRyxhQUFhLENBQUMsT0FBTyxHQUFHLEtBQUssR0FBRyxhQUFhLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUM7WUFDOUUsQ0FBQztRQUNMLENBQUM7UUFFRDs7OztXQUlHO1FBQ0gsaUVBQWlFO1FBQ2pFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDbEMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixDQUFDO1FBRUQsaUVBQWlFO1FBQ2pFLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLElBQUksYUFBYSxHQUFhLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkQsSUFBSSxTQUFTLEdBQWEsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUUvQyxFQUFFLENBQUEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNYLGFBQWEsQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxhQUFhLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7WUFDbEcsQ0FBQztRQUNMLENBQUM7UUFFRCxvQ0FBb0M7UUFDcEMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDcEMsSUFBSSxhQUFhLEdBQWdCLFNBQVMsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFN0QsYUFBYSxDQUFDLE9BQU8sR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsdUJBQXVCO1FBQ3hHLENBQUM7UUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxNQUFNO1FBRVQsSUFBSSxTQUFTLEdBQVksS0FBSyxDQUFDO1FBRS9CLElBQUksZ0JBQWdCLEdBQUcsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUM7UUFDakUsSUFBSSxlQUFlLEdBQUcsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7UUFFL0QsRUFBRSxDQUFBLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxTQUFpQixDQUFDLENBQUMsMERBQTBEO1lBQ2pGLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztnQkFDM0MsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxpR0FBaUc7WUFDcEksQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDbEIsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDbEQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUNyQixTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixDQUFDO1lBQ0wsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLFNBQVMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7b0JBQzNCLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3JCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztZQUV4QyxFQUFFLENBQUEsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3JCLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQztZQUVELEVBQUUsQ0FBQSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN4QixTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFFckIsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRU8sbUJBQW1CO1FBRXZCLElBQUksRUFBRSxHQUFhLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzVDLElBQUksRUFBWSxDQUFDO1FBRWpCLEdBQUcsQ0FBQztZQUNBLEVBQUUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUNsQyxDQUFDLFFBQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFO1FBRXhCLEVBQUUsQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFTyxhQUFhO1FBQ2pCLElBQUksSUFBaUIsQ0FBQztRQUV0QixHQUFHLENBQUM7WUFDQSxJQUFJLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUE7UUFDdEMsQ0FBQyxRQUFPLElBQUksQ0FBQyxPQUFPLElBQUksS0FBSyxFQUFFO1FBRS9CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDekIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUMzQixJQUFJLFNBQVMsR0FBRyxJQUFJLGtCQUFRLEVBQUUsQ0FBQztRQUUvQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTyxhQUFhO1FBQ2pCLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQy9CLEVBQUUsQ0FBQSxDQUFDLGdCQUFNLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RGLElBQUksVUFBVSxHQUFnQixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyRCxVQUFVLENBQUMsTUFBTSxJQUFJLGdCQUFNLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztZQUNwSCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTyxnQkFBZ0I7UUFDcEIsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsRUFBRSxDQUFBLENBQUMsZ0JBQU0sQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDekYsSUFBSSxJQUFJLEdBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLFNBQVMsSUFBSSxnQkFBTSxDQUFDLFlBQVksQ0FBQyxnQkFBTSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDcEgsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sZUFBZSxDQUFDLE1BQWdCO1FBQ25DLElBQUksT0FBTyxHQUFhLEVBQUUsQ0FBQztRQUUzQixpREFBaUQ7UUFDakQsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFFRCxrREFBa0Q7UUFDbEQsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDOUIsT0FBTyxDQUFDLElBQUksQ0FDUixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7UUFDaEQsQ0FBQztRQUVELCtDQUErQztRQUMvQyxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RDLENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFTSxRQUFRO1FBRVgsSUFBSSxlQUFlLEdBQVcsRUFBRSxDQUFDO1FBQ2pDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzVCLGVBQWUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxLQUFLLENBQUM7UUFDckUsQ0FBQztRQUVELElBQUksa0JBQWtCLEdBQVcsRUFBRSxDQUFDO1FBQ3BDLEdBQUcsQ0FBQSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQzNCLGtCQUFrQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxHQUFHLEtBQUssQ0FBQztRQUMvRSxDQUFDO1FBRUQsTUFBTSxDQUFDLDBCQUEwQjtZQUN6QixRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFLO1lBQzFCLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUs7WUFDcEMsZUFBZTtZQUNmLGtCQUFrQjtjQUNwQixHQUFHLENBQUM7SUFDZCxDQUFDO0lBRU8sTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFrQjtRQUM1QyxNQUFNLENBQUMsVUFBVSxRQUFRLENBQUMsS0FBSzs2QkFDVixRQUFRLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVPLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxlQUE0QjtRQUN6RCxNQUFNLENBQUMsVUFBVSxlQUFlLENBQUMsS0FBSzswQkFDcEIsZUFBZSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2tCQUN6QyxlQUFlLENBQUMsT0FBTyxHQUFHLEVBQUUsR0FBRyxVQUFVLEVBQUUsQ0FBQTtJQUN6RCxDQUFDO0lBRU0sTUFBTTtRQUNULElBQUksSUFBSSxHQUFTLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxJQUFJLENBQUM7UUFFL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMzQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLFFBQVEsR0FBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDZixJQUFJLEVBQUUsa0JBQWtCLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQzthQUNwRCxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMzQixHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLFdBQVcsR0FBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzNCLEVBQUUsRUFBRSxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzFCLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUM7YUFDMUQsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELDBFQUEwRTtRQUMxRSw2RkFBNkY7UUFFN0YsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsRUFBRTtJQUNGLGdCQUFnQjtJQUNoQixFQUFFO0lBRUY7Ozs7Ozs7T0FPRztJQUNLLGlCQUFpQjtRQUNyQixJQUFJLE1BQWdCLENBQUM7UUFDckIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsTUFBTTtJQUNOLHVEQUF1RDtJQUN2RCxvQkFBb0I7SUFDcEIsTUFBTTtJQUNOLG1DQUFtQztJQUNuQyx3RUFBd0U7SUFDeEUsc0VBQXNFO0lBQ3RFLElBQUk7SUFFSjs7O09BR0c7SUFDSyxvQkFBb0I7UUFDeEIsSUFBSSxNQUFtQixDQUFDO1FBQ3hCLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNwQyxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ssWUFBWSxDQUFDLEVBQVksRUFBRSxFQUFZO1FBRTNDLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLFFBQVE7WUFDUixJQUFJLElBQUksR0FBZ0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFO21CQUNoRCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2hCLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRU0sTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUF5QixFQUFFLE1BQTBCO1FBQ3ZFLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUM7SUFDMUMsQ0FBQztJQUVNLFVBQVUsQ0FBQyxJQUF3QjtRQUN0QyxJQUFJLEdBQUcsR0FBRyxnQkFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7UUFDdkMsSUFBSSxHQUFHLEdBQUcsZ0JBQU0sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO1FBQ3pDLElBQUksR0FBRyxHQUFHLGdCQUFNLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUN6QyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxtQkFBbUI7UUFDOUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsa0JBQWtCO1FBQzdCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLGdCQUFnQjtRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxpREFBaUQ7UUFFNUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsa0JBQWtCO1FBRTdCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBRTlDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNWLE9BQU0sQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNqRCxJQUFJLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxRQUFRLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTVCLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUVqRCxDQUFDLEVBQUUsQ0FBQztnQkFDSixDQUFDLEVBQUUsQ0FBQztnQkFDSixDQUFDLEVBQUUsQ0FBQztZQUNSLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFFSixDQUFDLEVBQUUsQ0FBQztnQkFDSixFQUFFLENBQUEsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxDQUFDLEVBQUUsQ0FBQztnQkFDUixDQUFDO2dCQUFDLElBQUksQ0FBRSxDQUFDO29CQUNMLENBQUMsRUFBRSxDQUFDO2dCQUNSLENBQUM7WUFDTCxDQUFDO1FBRUwsQ0FBQztRQUNELENBQUMsR0FBRyxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUztRQUM3RyxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQztRQUU5RSw0Q0FBNEM7UUFDNUMsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRW5DLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3ZELENBQUMsRUFBRSxDQUFDO1lBQ1IsQ0FBQztRQUNMLENBQUM7UUFFRCxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVWLE1BQU0sQ0FBQyxDQUFDLEdBQUcsR0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBQyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUdELEVBQUU7SUFDRixzQkFBc0I7SUFDdEIsRUFBRTtJQUVGLElBQVcsRUFBRTtRQUNULE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRUQsSUFBVyxTQUFTO1FBQ2hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxJQUFXLFlBQVk7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVNLHNCQUFzQjtRQUN6QixJQUFJLEdBQUcsR0FBdUIsRUFBRSxDQUFDO1FBRWpDLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3JDLENBQUM7UUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU0sTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxJQUFZLFVBQVU7UUFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQUVELElBQVksV0FBVztRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDOztBQXRqQmMsNkJBQVUsR0FBVyxDQUFDLENBQUM7QUFIMUMsZ0RBMGpCQzs7QUFFRCxrQkFBZSxrQkFBa0IsQ0FBQzs7O0FDeGtCbEM7SUFPSSwyQkFBMkI7SUFDM0IsNEJBQTRCO0lBQzVCLHlCQUF5QjtJQUV6QixZQUFtQixNQUFnQixFQUFTLE9BQWlCLEVBQVMsTUFBYyxFQUFFLE9BQWlCLEVBQUUsS0FBYztRQUFwRyxXQUFNLEdBQU4sTUFBTSxDQUFVO1FBQVMsWUFBTyxHQUFQLE9BQU8sQ0FBVTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDaEYsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLEtBQUssSUFBSSxXQUFXLEdBQUcsS0FBSyxHQUFHLEVBQUUsV0FBVyxDQUFDLGFBQWEsQ0FBQztRQUNoRixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sT0FBTyxJQUFJLFdBQVcsR0FBRyxPQUFPLEdBQUUsSUFBSSxDQUFDO1FBQzdELElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFTSxRQUFRO1FBQ1gsTUFBTSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsYUFBYSxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO0lBQzVNLENBQUM7SUFFRCxFQUFFO0lBQ0Ysc0JBQXNCO0lBQ3RCLEVBQUU7SUFFRixJQUFXLEVBQUU7UUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNwQixDQUFDO0lBRUQsSUFBVyxLQUFLO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7QUE3QmMsc0JBQVUsR0FBVyxDQUFDLENBQUM7QUFFdkIseUJBQWEsR0FBVyxDQUFDLENBQUM7O0FBTDdDLDhCQWtDQzs7O0FDbENEO0lBbUJJO1FBQ0ksSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLFFBQVEsQ0FBQyxVQUFVLENBQUM7UUFFakMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELENBQUM7SUFDTCxDQUFDO0lBRU8sZUFBZSxDQUFDLElBQWM7UUFDbEMsa0RBQWtEO1FBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUV6QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUVsQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBRXhCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoRCxDQUFDO0lBRU8saUJBQWlCLENBQUMsSUFBbUIsRUFBRSxjQUE0QjtRQUN2RSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQztRQUN2QyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUV6QixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUVwQixJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLElBQUksS0FBSyxXQUFXLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7UUFFOUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLGNBQWMsS0FBSyxXQUFXLEdBQUcsY0FBYyxHQUFHLFFBQVEsQ0FBQztJQUM3RixDQUFDO0lBRU0sZUFBZSxDQUFDLFlBQXlCO1FBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFTSxjQUFjLENBQUMsS0FBYztRQUNoQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzNCLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7UUFFRCwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO1FBRWpDLElBQUksR0FBRyxHQUFXLENBQUMsQ0FBQztRQUVwQixpRUFBaUU7UUFDakUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzRCxHQUFHLElBQUksS0FBSyxDQUFDO1lBRWIsR0FBRyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUM1QixDQUFDLEdBQVcsRUFBRSxPQUFvQjtnQkFDOUIsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLHNDQUFzQztnQkFDbkcsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNmLENBQUM7WUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFVixHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixpREFBaUQ7WUFDakQsR0FBRyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUM1QixDQUFDLEdBQVcsRUFBRSxPQUFvQjtnQkFDOUIsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLE1BQU0sQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsWUFBWTtnQkFDL0UsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNmLENBQUM7WUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDVixHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO1FBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRU0sV0FBVztRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUM7SUFDcEMsQ0FBQztJQUVNLFFBQVE7UUFDWCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxlQUFlLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQy9JLENBQUM7SUFFRCxFQUFFO0lBQ0Ysc0JBQXNCO0lBQ3RCLEVBQUU7SUFFRixJQUFXLEVBQUU7UUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNwQixDQUFDO0lBRUQsSUFBVyxLQUFLO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELElBQVcsSUFBSTtRQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFXLGNBQWM7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDaEMsQ0FBQztJQUVELElBQWMsYUFBYTtRQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQsSUFBYyxLQUFLO1FBQ2YsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7QUE3SWMsbUJBQVUsR0FBVyxDQUFDLENBQUM7QUFFdkIsc0JBQWEsR0FBVyxDQUFDLENBQUM7O0FBTDdDLDJCQWlKQztBQUVELElBQVksWUFJWDtBQUpELFdBQVksWUFBWTtJQUNwQixpREFBSyxDQUFBO0lBQ0wsbURBQU0sQ0FBQTtJQUNOLG1EQUFNLENBQUE7QUFDVixDQUFDLEVBSlcsWUFBWSxHQUFaLG9CQUFZLEtBQVosb0JBQVksUUFJdkI7QUFFRCxJQUFZLGFBSVg7QUFKRCxXQUFZLGFBQWE7SUFDckIsK0NBQUcsQ0FBQTtJQUNILGlEQUFJLENBQUE7SUFDSixxREFBTSxDQUFBO0FBQ1YsQ0FBQyxFQUpXLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBSXhCO0FBRUQsa0RBQWtEO0FBQ2xELElBQVksZUFFWDtBQUZELFdBQVksZUFBZTtBQUUzQixDQUFDLEVBRlcsZUFBZSxHQUFmLHVCQUFlLEtBQWYsdUJBQWUsUUFFMUI7QUFHRCxrQkFBeUIsQ0FBUztJQUM5QixNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2xDLENBQUM7QUFGRCw0QkFFQztBQUVELG9CQUEyQixDQUFTO0lBQ2hDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDMUIsQ0FBQztBQUZELGdDQUVDOzs7QUM3S0Q7SUFNSSxZQUFZLFdBQXlCO1FBQ2pDLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxXQUFXLElBQUksV0FBVyxHQUFHLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDNUUsQ0FBQztJQUVNLEdBQUcsQ0FBQyxVQUE4QjtRQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSxLQUFLO1FBRVIsSUFBSSxjQUFjLEdBQXVCLElBQUksQ0FBQztRQUM5QyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUMvQixFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDbkMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQzlCLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMzQyxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUEsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxnQkFBZ0I7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksT0FBTyxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7SUFDL0csQ0FBQztJQUVNLE9BQU87UUFDVixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFRCxJQUFXLGNBQWM7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELElBQUksRUFBRTtRQUNGLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ3BCLENBQUM7O0FBckRnQixrQkFBVSxHQUFXLENBQUMsQ0FBQzs7QUFINUMsMEJBeURDIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbmZpZyB7XHJcbiAgICBwdWJsaWMgc3RhdGljIGxvZyA9IHtcclxuICAgICAgICBmdW5jdGlvbnM6IGZhbHNlLFxyXG4gICAgICAgIGN5Y2xlOiBmYWxzZSxcclxuICAgICAgICBnZW5lcmF0aW9uczogZmFsc2UsXHJcbiAgICAgICAgbmV4dEZpdG5lc3M6IGZhbHNlXHJcbiAgICB9O1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgY3ljbGUgPSB7XHJcbiAgICAgICAgY29udGludWU6IGZhbHNlLFxyXG4gICAgICAgIHJ1bm5pbmc6IGZhbHNlXHJcbiAgICB9O1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2VuZXJhbCA9IHtcclxuICAgICAgICBhbHdheXNFdmFsdWF0ZUZpdG5lc3M6IGZhbHNlXHJcbiAgICB9O1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgbXV0YXRpb25PcHRpb25zID0ge1xyXG4gICAgICAgIG11dGF0ZU9mZnNwcmluZ3M6IHRydWUsXHJcbiAgICAgICAgbXV0YXRlQnlDbG9uaW5nOiB0cnVlLFxyXG4gICAgICAgIGluZGl2aWR1YWxUb3BvbG9neToge1xyXG4gICAgICAgICAgICBjaGFuY2U6IDAuMjUsXHJcbiAgICAgICAgICAgIGFkZE5vZGU6IHtcclxuICAgICAgICAgICAgICAgIGNoYW5jZTogMC4wM1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBhZGRDb25uZWN0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICBjaGFuY2U6IDAuMDVcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYWRkTm9kZVhPUmFkZENvbm5lY3Rpb246IHRydWUsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBpbmRpdmlkdWFsV2VpZ2h0czoge1xyXG4gICAgICAgICAgICBjaGFuY2U6IDAuOCxcclxuICAgICAgICAgICAgd2VpZ2h0czoge1xyXG4gICAgICAgICAgICAgICAgY2hhbmNlOiAxLFxyXG4gICAgICAgICAgICAgICAgbXV0YXRlU2luZ2xlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2hhbmNlOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0ZGV2OiAxXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHRocmVzaG9sZHM6IHtcclxuICAgICAgICAgICAgICAgIGNoYW5jZTogMSxcclxuICAgICAgICAgICAgICAgIG11dGF0ZVNpbmdsZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNoYW5jZTogMSxcclxuICAgICAgICAgICAgICAgICAgICBzdGRldjogMVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGNyb3Nzb3Zlck9wdGlvbnMgPSB7XHJcbiAgICAgICAgb2Zmc3ByaW5nUmF0aW86IDAuOCxcclxuICAgICAgICB0b3VybmFtZW50UmF0aW86IDAuNVxyXG4gICAgfTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIGRpc3RhbmNlIGZ1bmN0aW9uIGNvZWZmaWNpZW50XHJcbiAgICAgKiBkID0gKGNfZSpFKS9OICsgKGNfZCpEKS9OICsgY19tKlc7XHJcbiAgICAgKiAzLjAgPSAxLjAgLi4uLi4uIDEuMCAuLi4uLi4gMC40IC0gdmFsdWVzIGZyb20gTkVBVCBwYXBlciBjYXB0ZXIgNC4xXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgc3BlY2lhdGlvbiA9IHtcclxuICAgICAgICBleGNlc3NDb2VmOiAxLFxyXG4gICAgICAgIGRpc2pvaW50Q29lZjogIDEsXHJcbiAgICAgICAgbWF0Y2hpbmdDb2VmOiAwLjQsXHJcbiAgICAgICAgZGlzdGFuY2VUaHJlc2hvbGQ6IDMuMFxyXG4gICAgfTtcclxuXHJcbn1cclxuIiwiaW1wb3J0IENvbmZpZyBmcm9tIFwiLi9Db25maWdcIjtcclxuaW1wb3J0IHtCYXNlTmVhdEluZGl2aWR1YWwsIEluZGl2aWR1YWxzfSBmcm9tIFwiLi9tb2RlbC9CYXNlTmVhdEluZGl2aWR1YWxcIjtcclxuaW1wb3J0IFNwZWNpZXMgZnJvbSBcIi4vbW9kZWwvU3BlY2llc1wiO1xyXG5cclxuLyoqXHJcbiAqIEBpbnRlcmZhY2VcclxuICogQGRlc2NyaXB0aW9uIEV2b0N5Y2xlIGlzIG9ic2VydmFibGUgZnJvbSBvdXRzaWRlLiBUaGlzIGlzIGl0cyBvYnNlcnZlciBpbnRlcmZhY2UuXHJcbiAqIERvbid0IGZvcmdldCB0aGF0IGV2YWx1YXRpbmcgZml0bmVzcyByZXR1cm5zIFByb21pc2UgLSBhc3luY2hyb25vdXMuXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIEV2b0N5Y2xlT2JzZXJ2ZXIge1xyXG4gICAgbm90aWZ5RG9uZVJlZHVjdGlvbihldm9jeWNsZTogRXZvQ3ljbGUpOiB2b2lkXHJcbiAgICBub3RpZnlEb25lU3BlY2lhdGlvbihldm9jeWNsZTogRXZvQ3ljbGUsIHNwZWNpZXM6IFNwZWNpZXNbXSk6IHZvaWRcclxuICAgIG5vdGlmeURvbmVFdmFsdWF0ZU5leHRGaXRuZXNzKGV2b2N5Y2xlOiBFdm9DeWNsZSwgaW5kaXZpZHVhbDogQmFzZU5lYXRJbmRpdmlkdWFsKTogdm9pZFxyXG59XHJcblxyXG4vKipcclxuICogQGNsYXNzXHJcbiAqIEBkZXNjcmlwdGlvbiBNYWluIGxpYnJhcnkgY2xhc3NcclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEV2b0N5Y2xlIHtcclxuXHJcbiAgICBwdWJsaWMgY29uZmlnOiBDb25maWcgfCBhbnk7XHJcbiAgICBwcml2YXRlIF90b3VybmFtZW50U2l6ZTogbnVtYmVyOyAvLyBnZXRTZWxlY3Rpb25PcHRpb25zXHJcbiAgICBwcml2YXRlIF9vZmZzcHJpbmdTaXplOiBudW1iZXI7XHJcblxyXG4gICAgcHVibGljIHBvcHVsYXRpb246IEluZGl2aWR1YWxzO1xyXG4gICAgcHVibGljIGdlbmVyYXRpb25Db3VudGVyOiBudW1iZXI7XHJcblxyXG4gICAgcHJpdmF0ZSBzcGVjaWVzOiBTcGVjaWVzW107XHJcbiAgICBwcml2YXRlIHBhcmVudHM6IEluZGl2aWR1YWxzW107XHJcbiAgICBwcml2YXRlIG9mZnNwcmluZ3M6IEluZGl2aWR1YWxzW107XHJcbiAgICBwcml2YXRlIG11dGFudHM6IEluZGl2aWR1YWxzO1xyXG5cclxuICAgIHByaXZhdGUgX29ic2VydmVyczogRXZvQ3ljbGVPYnNlcnZlcltdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGluZGl2aWR1YWxzOiBJbmRpdmlkdWFscykge1xyXG4gICAgICAgIGlmIChDb25maWcubG9nLmZ1bmN0aW9ucykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2b0N5Y2xlLmNvbnN0cnVjdG9yXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5jb25maWcgPSBDb25maWc7XHJcbiAgICAgICAgdGhpcy5nZW5lcmF0aW9uQ291bnRlciA9IDA7XHJcbiAgICAgICAgdGhpcy5fb2JzZXJ2ZXJzID0gW107XHJcblxyXG4gICAgICAgIHRoaXMucG9wdWxhdGlvbiA9IGluZGl2aWR1YWxzO1xyXG4gICAgICAgIHRoaXMuc3BlY2llcyA9IFtuZXcgU3BlY2llcyhpbmRpdmlkdWFscyldO1xyXG4gICAgICAgIHRoaXMub2Zmc3ByaW5ncyA9IFtdO1xyXG4gICAgICAgIHRoaXMubXV0YW50cyA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhZGRPYnNlcnZlcihvYnNlcnZlcjogRXZvQ3ljbGVPYnNlcnZlcikge1xyXG4gICAgICAgIHRoaXMuX29ic2VydmVycy5wdXNoKG9ic2VydmVyKTtcclxuICAgIH1cclxuXHJcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLyBFVk9MVVRJT04gRlVOQ1RJT05TIC8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcblxyXG4gICAgcHVibGljIGNvbnRpbnVlKHNpbmdsZUdlbmVyYXRpb246IGJvb2xlYW4gPSBmYWxzZSkge1xyXG4gICAgICAgIGlmIChDb25maWcubG9nLmZ1bmN0aW9ucykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2b0N5Y2xlLmNvbnRpbnVlIC0gXCIgKyBcInJ1bm5pbmc6IFwiICsgQ29uZmlnLmN5Y2xlLnJ1bm5pbmcgKyBcIjsgY29udGludWU6IFwiICsgQ29uZmlnLmN5Y2xlLmNvbnRpbnVlICsgXCI7IHNpbmdsZUdlbmVyYXRpb246IFwiICsgc2luZ2xlR2VuZXJhdGlvbik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMucG9wdWxhdGlvbikge1xyXG4gICAgICAgICAgICB0aHJvdyBcIkV2b0N5Y2xlIGhhcyB0byBiZSBpbmljaWFsaXplZCBmaXJzdCB3aXRoIGluaXQgcG9wdWxhdGlvbiEgU2VlIEV2b0N5Y2xlLmluaXQoaW5kaXZpZHVhbHM6IEluZGl2aWR1YWxzKVwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKENvbmZpZy5jeWNsZS5ydW5uaW5nKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkV2b0N5Y2xlLmNvbnRpbnVlIGlzIGFscmVhZHkgcnVubmluZ1wiKTtcclxuICAgICAgICAgICAgcmV0dXJuOyAvLyBwcmV2ZW50IG11bHRpcGxlIGNhbGxcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghc2luZ2xlR2VuZXJhdGlvbiAmJiAhQ29uZmlnLmN5Y2xlLmNvbnRpbnVlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkV2b0N5Y2xlLmNvbnRpbnVlIGNhbm5vdCBjb250aW51ZSBFdm9DeWNsZS5jb25maWcuY3ljbGUuY29udGludWUgZmxhZyBpcyBmYWxzZVwiKTtcclxuICAgICAgICAgICAgcmV0dXJuOyAvLyBkbyBub3QgY29udGludWVcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIENvbmZpZy5jeWNsZS5ydW5uaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5kb1JlZHVjdGlvbigpLnRoZW4oKHBvcHVsYXRpb24pID0+IHtcclxuICAgICAgICAgICAgaWYgKENvbmZpZy5sb2cuZnVuY3Rpb25zKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2b0N5Y2xlLmNvbnRpbnVlIGRvUmVkdWN0aW9uLXRoZW5cIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMucG9wdWxhdGlvbiA9IHBvcHVsYXRpb247XHJcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGlvbkNvdW50ZXIrKztcclxuXHJcbiAgICAgICAgICAgIGlmIChDb25maWcubG9nLmdlbmVyYXRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkdFTkVSQVRJT05cIik7XHJcbiAgICAgICAgICAgICAgICBFdm9DeWNsZS5wcmludCh0aGlzLnBvcHVsYXRpb24pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9vYnNlcnZlcnMuZm9yRWFjaCgob2JzZXJ2ZXI6IEV2b0N5Y2xlT2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5vdGlmeURvbmVSZWR1Y3Rpb24odGhpcylcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnN0ZXAoKTsgLy8gY29udGludWUgd2l0aCBjeWNsZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHN0ZXAgd2hvbGUgbmV3IGdlbmVyYXRpb25cclxuICAgIHB1YmxpYyBzdGVwKCkge1xyXG5cclxuICAgICAgICBpZiAoQ29uZmlnLmxvZy5jeWNsZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInBvcHVsYXRpb24gLSBORVhUIFNURVA6XCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmRpcih0aGlzLnBvcHVsYXRpb24pO1xyXG4gICAgICAgICAgICBFdm9DeWNsZS5wcmludCh0aGlzLnBvcHVsYXRpb24pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gY2xhc3NpZnkgaW5kaXZpZHVhbHNcclxuICAgICAgICB0aGlzLmRvU3BlY2lhdGlvbigpO1xyXG4gICAgICAgIGlmIChDb25maWcubG9nLmN5Y2xlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwic3BlY2llcyAtIGRvU3BlY2lhdGlvbjpcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKHRoaXMuc3BlY2llcyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBzZWxlY3QgaW5kaXZpZHVhbHMgdG8gYmUgcGFyZW50cyBmb3IgYnJlZWRpbmdcclxuICAgICAgICB0aGlzLmRvU2VsZWN0aW9uKCk7XHJcbiAgICAgICAgaWYgKENvbmZpZy5sb2cuY3ljbGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJwYXJlbnRzIC0gZG9TZWxlY3Rpb246XCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmRpcih0aGlzLnBhcmVudHMpOyAvLyBpZiB0aGlzIGNoYW5nZWQgbGF0ZXIgKG11dGF0aW9uKSwgbmV3IHZhbHVlIG1heSBiZSBkaXNwbGF5ZWQgaW4gY29uc29sZVxyXG4gICAgICAgICAgICBFdm9DeWNsZS5wcmludCh0aGlzLnBhcmVudHMucmVkdWNlKChhLCBiKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYS5jb25jYXQoW10pLmNvbmNhdChiKTtcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZ2VuZXJhdGUgbmV3IGluZGl2aWR1YWxzIGJ5IGNyb3Nzb3ZlclxyXG4gICAgICAgIHRoaXMuZG9Dcm9zc292ZXIoKTtcclxuICAgICAgICBpZiAoQ29uZmlnLmxvZy5jeWNsZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIm9mZnNwcmluZ3MgLSBkb0Nyb3Nzb3ZlcjpcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKHRoaXMub2Zmc3ByaW5ncyk7IC8vIGlmIHRoaXMgY2hhbmdlZCBsYXRlciAobXV0YXRpb24pLCBuZXcgdmFsdWUgbWF5IGJlIGRpc3BsYXllZCBpbiBjb25zb2xlXHJcbiAgICAgICAgICAgIEV2b0N5Y2xlLnByaW50KHRoaXMub2Zmc3ByaW5ncy5yZWR1Y2UoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhLmNvbmNhdChbXSkuY29uY2F0KGIpO1xyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjaGFuZ2UgaW5kaXZpZHVhbHMgYnkgbXV0YXRpb25zXHJcbiAgICAgICAgdGhpcy5kb011dGF0aW9uKCk7XHJcbiAgICAgICAgaWYgKENvbmZpZy5sb2cuY3ljbGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJwb3B1bGF0aW9uIGFuZCBvZmZzcHJpbmdzIC0gZG9NdXRhdGlvbjpcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKHRoaXMucG9wdWxhdGlvbik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGlyKHRoaXMub2Zmc3ByaW5ncyk7XHJcbiAgICAgICAgICAgIEV2b0N5Y2xlLnByaW50KHRoaXMucG9wdWxhdGlvbik7XHJcbiAgICAgICAgICAgIEV2b0N5Y2xlLnByaW50KHRoaXMub2Zmc3ByaW5ncy5yZWR1Y2UoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhLmNvbmNhdChbXSkuY29uY2F0KGIpO1xyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyB0aGlzLnBvcHVsYXRpb24gPSB0aGlzLnBvcHVsYXRpb24uY29uY2F0KG9mZnNwcmluZ3MpOyAvLyBUT0RPIGF6IHYgcmVkdWtjaVxyXG4gICAgICAgIENvbmZpZy5jeWNsZS5ydW5uaW5nID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5jb250aW51ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZmlsdGVyRHVwbGljYXRlcyhpbmRpdmlkaWFsczogSW5kaXZpZHVhbHMpIHtcclxuICAgICAgICByZXR1cm4gaW5kaXZpZGlhbHMuZmlsdGVyKGZ1bmN0aW9uIChpdGVtLCBwb3MsIHNlbGYpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHNlbGYuaW5kZXhPZihpdGVtKSA9PSBwb3M7XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRvU3BlY2lhdGlvbigpIHtcclxuICAgICAgICAvLyByYW5kb20gc3BlY2llcyByZXByZXNlbnRhdGl2ZXNcclxuICAgICAgICBsZXQgaSA9IHRoaXMuc3BlY2llcy5sZW5ndGg7XHJcbiAgICAgICAgd2hpbGUgKGktLSkgeyAvLyBXQVJOSU5HIGl0ZXJhdGluZyBpbiByZXZlcnNlIGJlY2F1c2Ugb2Ygc3BsaWNlIChyZW1vdmluZyBpdGVtcylcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3BlY2llc1tpXS5jbGVhcigpID09IGZhbHNlKSB7IC8vIGtlZXBzIHNpbmdsZSByZXByZXNlbnRhdGl2ZSBhbmQgcmV0dXJucyBmYWxzZSBpZiBlbXB0eVxyXG4gICAgICAgICAgICAgICAgdGhpcy5zcGVjaWVzLnNwbGljZShpLCAxKTsgLy8gcmVtb3ZlIHRoaXMgc3BlY2llc1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyByZW1vdmUgc3BlY2llcyB3aGljaCBjYW4gYmUgbWVyZ2VkXHJcbiAgICAgICAgLy8gdGhpcyBpcyBub3QgaW4gc3BlY2lmaWNhdGlvbiBidXQgc291bmRzIGxpa2UgYSBnb29kIGlkZWFcclxuICAgICAgICAvLyBpID0gdGhpcy5zcGVjaWVzLmxlbmd0aDtcclxuICAgICAgICAvLyB3aGlsZShpLS0gPiAxKSB7IC8vIFdBUk5JTkcgaXRlcmF0aW5nIGluIHJldmVyc2UgYmVjYXVzZSBvZiBzcGxpY2UgKHJlbW92aW5nIGl0ZW1zKVxyXG4gICAgICAgIC8vICAgICBpZih0aGlzLnNwZWNpZXNbaV0ucmVwcmVzZW50YXRpdmUuZGlzdGFuY2VUbyh0aGlzLnNwZWNpZXNbaS0xXS5yZXByZXNlbnRhdGl2ZSkgPCBDb25maWcuc3BlY2lhdGlvbi5kaXN0YW5jZVRocmVzaG9sZCkge1xyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5zcGVjaWVzW2ldLmluZGl2aWR1YWxzID0gW107IC8vIGVtcHR5IHRoZSBzcGVjaWVzIC8vIHN0aWxsIGluIHBvcHVsYXRpb24gc28gaXQgd2lsbCBiZSBjbGFzc2lmeSBhZ2FpblxyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5zcGVjaWVzLnNwbGljZShpLCAxKTsgLy8gcmVtb3ZlIHRoaXMgc3BlY2llc1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICAvLyBjbGFzc2lmeSBpbmRpdmlkdWFsc1xyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLnBvcHVsYXRpb24pIHtcclxuICAgICAgICAgICAgbGV0IGluZGl2ID0gdGhpcy5wb3B1bGF0aW9uW2tleV07XHJcblxyXG4gICAgICAgICAgICBsZXQgcGxhY2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zcGVjaWVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaW5kaXYgPT0gdGhpcy5zcGVjaWVzW2ldLnJlcHJlc2VudGF0aXZlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHBsYWNlZCkgeyAvLyBza2lwcGluZyByZXByZXNlbnRhdGl2ZXNcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3BlY2llcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgKGluZGl2ICE9IHRoaXMuc3BlY2llc1tpXS5yZXByZXNlbnRhdGl2ZSlcclxuICAgICAgICAgICAgICAgIC8vIHBsYWNlIGluIGV4aXN0aW5nIGNsYXNzXHJcbiAgICAgICAgICAgICAgICBpZiAoaW5kaXYuZGlzdGFuY2VUbyh0aGlzLnNwZWNpZXNbaV0ucmVwcmVzZW50YXRpdmUpIDwgQ29uZmlnLnNwZWNpYXRpb24uZGlzdGFuY2VUaHJlc2hvbGQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNwZWNpZXNbaV0uYWRkKGluZGl2KTtcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAocGxhY2VkKSB7IC8vIGNvbnRpbnVlIHdpdGggbmV4dFxyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7IC8vIG5ldyBjbGFzc1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zcGVjaWVzLnB1c2gobmV3IFNwZWNpZXMoW2luZGl2XSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9vYnNlcnZlcnMuZm9yRWFjaCgob2JzZXJ2ZXI6IEV2b0N5Y2xlT2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIubm90aWZ5RG9uZVNwZWNpYXRpb24odGhpcywgdGhpcy5zcGVjaWVzKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNpbXBsZSB0b3VybmFtZW50IHNlbGVjdGlvbiBpbXBsZW1lbnRhdGlvbi5cclxuICAgICAqIFJhbmRvbWx5IGNob29zZXMgKHdpdGggcmVwZXRpdGlvbikgayBpbmRpdmlkdWFscyBhbmQgcGlja3MgdGhlIGJlc3Qgb25lIG9mIHRoZSB0b3VybmFtZW50LiBSZXBlYXRzIHVudGlsIHdob2xlIHBvcHVsYXRpb24gaXMgZmlsbGVkLlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGRvU2VsZWN0aW9uKCkge1xyXG5cclxuXHJcbiAgICAgICAgLy8gVE9ETyB0b3VybmFtZW50IGJ5IHNwZWNpZXNcclxuICAgICAgICAvLyBUT0RPIGhvdyBtdWNoIHRvIGNyb3Nzb3ZlciB2cyBob3cgbXVjaCB0byBtdXRhdGVcclxuXHJcbiAgICAgICAgLy8gVE9ETyB0YWt5IG11dG92YXQgYXNpIG5lamVuIHJvZGljZSBhbGUgdnNlY2hubyBjbyBwcm9qZGVcclxuICAgICAgICAvLyBwcm90b3plIGtkeXogdnlwbnUgY3Jvc3NvdmVyIHVwbG5lIHRhayBieSBuaWtkeSBuZW11dG92YWxpXHJcbiAgICAgICAgLy8gbmVqZGUgbXV0b3ZhdCBqZW5vbSBwb3RvbWt5IG5lYm8gamVub20gcm9kaWNlIHByb3RvemUga2R5eiBuZW5pIHphcG51dGVqIGNyb3Nzb3ZlciB0YWsganNvdSB0eWhsZSBza3VwaW55IHByYXpkbnlcclxuXHJcbiAgICAgICAgLy8gamFrIHZlbGtvdSBjYXN0IHBvcHVsYWNlIHZ5Z2VuZXJvdmF0IHByZXMgY3Jvc3NvdmVyXHJcbiAgICAgICAgbGV0IG9mZnNwcmluZ1NpemUgPSBNYXRoLnJvdW5kKHRoaXMucG9wdWxhdGlvbi5sZW5ndGggKiBDb25maWcuY3Jvc3NvdmVyT3B0aW9ucy5vZmZzcHJpbmdSYXRpbyk7XHJcblxyXG4gICAgICAgIC8vIHpqaXN0aXQga29saWsgeiBqYWt5IHNwZWNpZSBwb2RsZSBzaGFyZWQgZml0bmVzc1xyXG4gICAgICAgIGxldCBzdW1TaGFyZWRGaXR0bmVzcyA9IHRoaXMuc3BlY2llcy5yZWR1Y2UoKHN1bSwgY3VycikgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gc3VtICsgY3Vyci5nZXRTaGFyZWRGaXRuZXNzKCk7XHJcbiAgICAgICAgfSwgMCk7XHJcblxyXG4gICAgICAgIHRoaXMucGFyZW50cyA9IFtdO1xyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLnNwZWNpZXMpIHtcclxuICAgICAgICAgICAgbGV0IHNwZWMgPSB0aGlzLnNwZWNpZXNba2V5XTtcclxuXHJcbiAgICAgICAgICAgIGxldCBzcGVjU2l6ZSA9IHNwZWMuaW5kaXZpZHVhbHMubGVuZ3RoO1xyXG5cclxuICAgICAgICAgICAgbGV0IHNwZWNpT2Zmc3ByaW5nU2l6ZSA9IE1hdGgucm91bmQoc3BlYy5nZXRTaGFyZWRGaXRuZXNzKCkgLyBzdW1TaGFyZWRGaXR0bmVzcyAqIG9mZnNwcmluZ1NpemUpO1xyXG4gICAgICAgICAgICBzcGVjaU9mZnNwcmluZ1NpemUgPSBzcGVjaU9mZnNwcmluZ1NpemUgPiAwID8gc3BlY2lPZmZzcHJpbmdTaXplIDogMTsgLy8gYXRsZWFzdCBvbmUgb2Zmc3ByaW5nXHJcblxyXG4gICAgICAgICAgICAvLyB0aGlzLnBhcmVudHNba2V5XS5wdXNoKG51bGwpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHRvdXJuYW1lbnRTaXplID0gTWF0aC5yb3VuZChzcGVjU2l6ZSAqIENvbmZpZy5jcm9zc292ZXJPcHRpb25zLnRvdXJuYW1lbnRSYXRpbyk7XHJcbiAgICAgICAgICAgIHRvdXJuYW1lbnRTaXplID0gdG91cm5hbWVudFNpemUgPCAxID8gMSA6IHRvdXJuYW1lbnRTaXplO1xyXG4gICAgICAgICAgICB0b3VybmFtZW50U2l6ZSA9IHRvdXJuYW1lbnRTaXplID4gc3BlY1NpemUgPyBzcGVjU2l6ZSA6IHRvdXJuYW1lbnRTaXplO1xyXG5cclxuICAgICAgICAgICAgLy8gYnkgdG91cm5hbWVudCBzZWxlY3Rpb24gc2VsZWN0IGFzIG1hbnkgaW5kaXZpZHVhbHNcclxuICAgICAgICAgICAgLy8gYXMgaXQgaXMgbmVlZGVkIGZvciBjcm9zc292ZXIgKDIgdGltZXMgYXMgbXVjaCBhcyBvZmZzcHJpbmcgc2l6ZSlcclxuICAgICAgICAgICAgbGV0IHdpbm5lcnM6IEluZGl2aWR1YWxzID0gdGhpcy5wYXJlbnRzW2tleV0gPSBbXTtcclxuICAgICAgICAgICAgd2hpbGUgKHdpbm5lcnMubGVuZ3RoIDwgc3BlY2lPZmZzcHJpbmdTaXplICogMikge1xyXG4gICAgICAgICAgICAgICAgLy8gc2VsZWN0IGsgcmFuZG9tIGluZGl2aWR1YWxzICh3aXRoIHJlcGV0aXRpb24pXHJcbiAgICAgICAgICAgICAgICBsZXQgY29udGVzdGFudHM6IEluZGl2aWR1YWxzID0gW107XHJcbiAgICAgICAgICAgICAgICB3aGlsZSAoY29udGVzdGFudHMubGVuZ3RoIDwgdG91cm5hbWVudFNpemUpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgciA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIHNwZWNTaXplKTtcclxuICAgICAgICAgICAgICAgICAgICBjb250ZXN0YW50cy5wdXNoKHNwZWMuaW5kaXZpZHVhbHNbcl0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIHNvcnQgdGhlbSBhbmQgc2VsZWN0IGZpcnN0IGFzIHdpbm5lclxyXG4gICAgICAgICAgICAgICAgY29udGVzdGFudHMuc29ydChCYXNlTmVhdEluZGl2aWR1YWwuY29tcGFyZSk7XHJcbiAgICAgICAgICAgICAgICB3aW5uZXJzLnB1c2goY29udGVzdGFudHNbMF0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gbGV0IHRvdXJuU2l6ZSA9IHRoaXMudG91cm5hbWVudFNpemU7XHJcbiAgICAgICAgLy8gbGV0IHBvcFNpemUgPSBjaG9pY2VzLmxlbmd0aDtcclxuICAgICAgICAvLyBpZiAodG91cm5TaXplIDwgMSB8fCB0b3VyblNpemUgPiBwb3BTaXplKSB7XHJcbiAgICAgICAgLy8gICAgIHRocm93IG5ldyBSYW5nZUVycm9yKFwiVG91cm5hbWVudCBzaXplIG11c3QgYmUgZ3JlYXRlciB0aGFuIDEgYW5kIGxlc3MgdGhhbiBvciBlcXVhbCB0byBwb3B1bGF0aW9uIHNpemU6IFwiICtcclxuICAgICAgICAvLyAgICAgICAgIFwidG91cm5hbWVudFNpemUgPSBcIiArIHRvdXJuU2l6ZSArIFwiLCBwb3B1bGF0aW9uU2l6ZSA9IFwiICsgcG9wU2l6ZSk7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gLy8gYnkgdG91cm5hbWVudCBzZWxlY3Rpb24gc2VsZWN0IGFzIG1hbnkgaW5kaXZpZHVhbHNcclxuICAgICAgICAvLyAvLyBhcyBpdCBpcyBuZWVkZWQgZm9yIGNyb3Nzb3ZlciAoMiB0aW1lcyBhcyBtdWNoIGFzIG9mZnNwcmluZyBzaXplKVxyXG4gICAgICAgIC8vIGxldCB3aW5uZXJzOiBJbmRpdmlkdWFscyA9IFtdO1xyXG4gICAgICAgIC8vIHdoaWxlICh3aW5uZXJzLmxlbmd0aCA8IHRoaXMub2Zmc3ByaW5nU2l6ZSAqIDIpIHtcclxuICAgICAgICAvLyAgICAgLy8gc2VsZWN0IGsgcmFuZG9tIGluZGl2aWR1YWxzICh3aXRoIHJlcGV0aXRpb24pXHJcbiAgICAgICAgLy8gICAgIGxldCBjb250ZXN0YW50czogSW5kaXZpZHVhbHMgPSBbXTtcclxuICAgICAgICAvLyAgICAgd2hpbGUgKGNvbnRlc3RhbnRzLmxlbmd0aCA8IHRvdXJuU2l6ZSkge1xyXG4gICAgICAgIC8vICAgICAgICAgbGV0IHIgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBwb3BTaXplKTtcclxuICAgICAgICAvLyAgICAgICAgIGNvbnRlc3RhbnRzLnB1c2goY2hvaWNlc1tyXSk7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvL1xyXG4gICAgICAgIC8vICAgICAvLyBzb3J0IHRoZW0gYW5kIHNlbGVjdCBmaXJzdCBhcyB3aW5uZXJcclxuICAgICAgICAvLyAgICAgY29udGVzdGFudHMuc29ydChCYXNlTmVhdEluZGl2aWR1YWwuY29tcGFyZSk7XHJcbiAgICAgICAgLy8gICAgIHdpbm5lcnMucHVzaChjb250ZXN0YW50c1swXSk7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLy8gcmV0dXJuIHdpbm5lcnM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcm9zc292ZXJcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBkb0Nyb3Nzb3ZlcigpIHtcclxuXHJcbiAgICAgICAgdGhpcy5vZmZzcHJpbmdzID0gW107XHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMucGFyZW50cykge1xyXG4gICAgICAgICAgICBsZXQgcGFyZW50cyA9IHRoaXMucGFyZW50c1trZXldO1xyXG5cclxuICAgICAgICAgICAgbGV0IG9mZnNwcmluZ3M6IEluZGl2aWR1YWxzID0gdGhpcy5vZmZzcHJpbmdzW2tleV0gPSBbXTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnBhcmVudHNba2V5XS5sZW5ndGg7IGkgKz0gMikge1xyXG4gICAgICAgICAgICAgICAgb2Zmc3ByaW5ncy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgIHBhcmVudHNbaV0uYnJlZWQocGFyZW50c1tpICsgMV0pXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRvTXV0YXRpb24oKSB7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLnBvcHVsYXRpb24pIHtcclxuICAgICAgICAgICAgbGV0IGluZGl2ID0gdGhpcy5wb3B1bGF0aW9uW2tleV07XHJcblxyXG4gICAgICAgICAgICB0aGlzLm11dGFudHMgPSBbXTtcclxuICAgICAgICAgICAgaWYoQ29uZmlnLm11dGF0aW9uT3B0aW9ucy5tdXRhdGVCeUNsb25pbmcpIHtcclxuICAgICAgICAgICAgICAgIGxldCBjbG9uZTogQmFzZU5lYXRJbmRpdmlkdWFsID0gbmV3ICg8YW55PmluZGl2LmNvbnN0cnVjdG9yKShpbmRpdik7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHdhc011dGF0ZWQgPSBjbG9uZS5tdXRhdGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZih3YXNNdXRhdGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tdXRhbnRzLnB1c2goY2xvbmUpO1xyXG4gICAgICAgICAgICAgICAgfSAvLyBlbHNlIHdpbGwgYmUgZm9yZ290dGVuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpbmRpdi5tdXRhdGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKENvbmZpZy5tdXRhdGlvbk9wdGlvbnMubXV0YXRlT2Zmc3ByaW5ncykge1xyXG4gICAgICAgICAgICBsZXQgYWxsT2Zmc3ByaW5ncyA9IHRoaXMub2Zmc3ByaW5ncy5yZWR1Y2UoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhLmNvbmNhdChiKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGtleSBpbiBhbGxPZmZzcHJpbmdzKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgaW5kaXYgPSBhbGxPZmZzcHJpbmdzW2tleV07XHJcblxyXG4gICAgICAgICAgICAgICAgaW5kaXYubXV0YXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHByaXZhdGUgZG9SZWR1Y3Rpb24oKTogUHJvbWlzZTxJbmRpdmlkdWFscz4ge1xyXG4gICAgICAgIGlmIChDb25maWcubG9nLmZ1bmN0aW9ucykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2b0N5Y2xlLmRvUmVkdWN0aW9uXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGFsbEluZGl2aWRpYWxzID0gW10uY29uY2F0LmFwcGx5KHRoaXMucG9wdWxhdGlvbiwgdGhpcy5vZmZzcHJpbmdzKS5jb25jYXQodGhpcy5tdXRhbnRzKTtcclxuXHJcbiAgICAgICAgLy8gZXZhbHVhdGVGaXRuZXNzIGlmIG5lZWRlZFxyXG4gICAgICAgIHJldHVybiB0aGlzLmV2YWx1YXRlQWxsRml0bmVzcyhhbGxJbmRpdmlkaWFscykudGhlbigocG9wdWxhdGlvbikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoQ29uZmlnLmxvZy5mdW5jdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXZvQ3ljbGUuZG9SZWR1Y3Rpb24gZXZhbHVhdGVBbGxGaXRuZXNzLXRoZW5cIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNvcnQgYWxsIGluZGl2aWR1YWxzIGFuZCByZXR1cm5zIHRoZSBmaXR0ZXN0XHJcbiAgICAgICAgICAgIGFsbEluZGl2aWRpYWxzLnNvcnQoQmFzZU5lYXRJbmRpdmlkdWFsLmNvbXBhcmUpOyAvLyBpbi1wbGFjZVxyXG4gICAgICAgICAgICBsZXQgZWxpbWluYXRlZCA9IGFsbEluZGl2aWRpYWxzLnNwbGljZSh0aGlzLnBvcHVsYXRpb24ubGVuZ3RoKTsgLy8gaW4tcGxhY2VcclxuICAgICAgICAgICAgdGhpcy5wb3B1bGF0aW9uID0gYWxsSW5kaXZpZGlhbHM7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGVsaW1pbmF0ZWQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGVsaW1pbmF0ZWRbaV0uZWxpbWluYXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnBvcHVsYXRpb247IC8vIGNoYWluZWQgcHJvbWlzZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGV2YWx1YXRlQWxsRml0bmVzcyhwb3B1bGF0aW9uOiBJbmRpdmlkdWFscyk6IFByb21pc2U8SW5kaXZpZHVhbHM+IHtcclxuICAgICAgICBpZiAoQ29uZmlnLmxvZy5mdW5jdGlvbnMpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFdm9DeWNsZS5FdmFsdWF0ZUFsbEZpdG5lc3NcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBDQU5OT1QgdXNlIFByb21pc2UuQWxsLCBuZWVkcyB0byBiZSBldmFsdWF0ZWQgc2VxdWVudGlhbGx5XHJcblxyXG4gICAgICAgIHZhciBhbGxEb25lUHJvbWlzZSA9IG5ldyBQcm9taXNlPEluZGl2aWR1YWxzPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZXZhbHVhdGVOZXh0Rml0bmVzcyhwb3B1bGF0aW9uLCAwLCByZXNvbHZlKTsgLy8gaW5pdCBwcm9taXNlIHJlY3Vyc2lvblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gYWxsRG9uZVByb21pc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBldmFsdWF0ZU5leHRGaXRuZXNzKHBvcHVsYXRpb246IEluZGl2aWR1YWxzLCBjdXJyZW50OiBudW1iZXIsIHJlc29sdmU6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGlmICgoQ29uZmlnLmxvZy5mdW5jdGlvbnMgJiYgQ29uZmlnLmxvZy5uZXh0Rml0bmVzcylcclxuICAgICAgICAgICAgfHwgQ29uZmlnLmxvZy5uZXh0Rml0bmVzcykge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2b0N5Y2xlLmV2YWx1YXRlTmV4dEZpdG5lc3NcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBlbmQgY29uZGl0aW9uXHJcbiAgICAgICAgaWYgKGN1cnJlbnQgPj0gcG9wdWxhdGlvbi5sZW5ndGgpIHtcclxuICAgICAgICAgICAgcmVzb2x2ZShwb3B1bGF0aW9uKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgLy8gcmVjdXJzaW9uXHJcbiAgICAgICAgICAgIGlmIChwb3B1bGF0aW9uW2N1cnJlbnRdLmZpdG5lc3MgJiYgIUNvbmZpZy5nZW5lcmFsLmFsd2F5c0V2YWx1YXRlRml0bmVzcykge1xyXG4gICAgICAgICAgICAgICAgLy8gZG8gbm90IGV2YWx1YXRlIGlmIGZpdG5lc3MgaXMga25vd24sIGNhbGwgbmV4dCBpbW1lZGlhdGVseVxyXG4gICAgICAgICAgICAgICAgdGhpcy5ldmFsdWF0ZU5leHRGaXRuZXNzKHBvcHVsYXRpb24sIGN1cnJlbnQgKyAxLCByZXNvbHZlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIGV2YWx1YXRlIHVua25vd24gZml0bmVzcywgdGhlbiBjYWxsIG5leHRcclxuICAgICAgICAgICAgICAgIHBvcHVsYXRpb25bY3VycmVudF0uZXZhbHVhdGVGaXRuZXNzKCkudGhlbigocmVzKT0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9vYnNlcnZlcnMuZm9yRWFjaCgob2JzZXJ2ZXI6RXZvQ3ljbGVPYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5ub3RpZnlEb25lRXZhbHVhdGVOZXh0Rml0bmVzcyh0aGlzLCBwb3B1bGF0aW9uW2N1cnJlbnRdKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmV2YWx1YXRlTmV4dEZpdG5lc3MocG9wdWxhdGlvbiwgY3VycmVudCArIDEsIHJlc29sdmUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vIFBPUFVMQVRJT04gRlVOQ1RJT05TIC8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcblxyXG4gICAgcHVibGljIGdldE1heEF2Z01pbkZpdG5lc3MoKTogW251bWJlciwgbnVtYmVyLCBudW1iZXJdIHtcclxuICAgICAgICBsZXQgbWF4OiBudW1iZXI7XHJcbiAgICAgICAgbGV0IHN1bTogbnVtYmVyID0gMDtcclxuICAgICAgICBsZXQgbWluOiBudW1iZXI7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5wb3B1bGF0aW9uLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBmID0gdGhpcy5wb3B1bGF0aW9uW2ldLmZpdG5lc3M7XHJcblxyXG4gICAgICAgICAgICBpZiAobWF4ID09PSB1bmRlZmluZWQgfHwgbWF4IDwgZikge1xyXG4gICAgICAgICAgICAgICAgbWF4ID0gZjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKG1pbiA9PT0gdW5kZWZpbmVkIHx8IG1pbiA+IGYpIHtcclxuICAgICAgICAgICAgICAgIG1pbiA9IGY7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHN1bSArPSBmO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIFttYXgsIHN1bSAvIHRoaXMucG9wdWxhdGlvbi5sZW5ndGgsIG1pbl07XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBwcmludChpbmRpdmlkdWFsczogSW5kaXZpZHVhbHMpIHtcclxuICAgICAgICB2YXIgYXJyID0gaW5kaXZpZHVhbHMubWFwKChpdGVtOiBCYXNlTmVhdEluZGl2aWR1YWwpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGl0ZW0udG9TdHJpbmcoKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY29uc29sZS5kaXIoYXJyKTtcclxuICAgIH1cclxuXHJcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLyBHRVRURVJTIGFuZCBTRVRURVJTIC8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcblxyXG4gICAgLy8gdG91ZG5hbWVudFNpemVcclxuICAgIHByaXZhdGUgZ2V0IHRvdXJuYW1lbnRTaXplKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90b3VybmFtZW50U2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNldCB0b3VybmFtZW50U2l6ZSh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fdG91cm5hbWVudFNpemUgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VG91cm5hbWVudFNpemUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudG91cm5hbWVudFNpemU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldFRvdXJuYW1lbnRTaXplKHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnRvdXJuYW1lbnRTaXplID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gb2Zmc2lwcmluZ1NpemVcclxuICAgIHByaXZhdGUgZ2V0IG9mZnNwcmluZ1NpemUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX29mZnNwcmluZ1NpemU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzZXQgb2Zmc3ByaW5nU2l6ZSh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fb2Zmc3ByaW5nU2l6ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRPZmZzcHJpbmdTaXplKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9mZnNwcmluZ1NpemU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldE9mZnNwcmluZ1NpemUodmFsdWU6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMub2Zmc3ByaW5nU2l6ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxufVxyXG4iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBNeU1hdGgge1xyXG5cclxuICAgIHByaXZhdGUgY29uc3RydWN0b3IoKSB7fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlc2NyaXB0aW9uIHJldHVybnMgYSBnYXVzc2lhbiByYW5kb20gZnVuY3Rpb24gd2l0aCB0aGUgZ2l2ZW4gbWVhbiBhbmQgc3RkZXYuXHJcbiAgICAgKiBAYXV0aG9yIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMjU1ODI4ODIvamF2YXNjcmlwdC1tYXRoLXJhbmRvbS1ub3JtYWwtZGlzdHJpYnV0aW9uLWdhdXNzaWFuLWJlbGwtY3VydmUjYW5zd2VyLTM1NTk5MTgxXHJcbiAgICAgKiBAdml6IGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL01hcnNhZ2xpYV9wb2xhcl9tZXRob2RcclxuICAgICAqIEBwYXJhbSBtZWFuIChtdSlcclxuICAgICAqIEBwYXJhbSBzdGRldiAoc2lnbWEpIHN0YW5kYXJkIGRldmlhdGlvblxyXG4gICAgICogQHJldHVybnMgeygpPT5udW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2F1c3NpYW4obWVhbjogbnVtYmVyLCBzdGRldjogbnVtYmVyKTogKCkgPT4gbnVtYmVyIHtcclxuICAgICAgICB2YXIgeTI6IG51bWJlcjtcclxuICAgICAgICB2YXIgdXNlX2xhc3Q6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgeTE6IG51bWJlcjtcclxuICAgICAgICAgICAgaWYgKHVzZV9sYXN0KSB7XHJcbiAgICAgICAgICAgICAgICB5MSA9IHkyO1xyXG4gICAgICAgICAgICAgICAgdXNlX2xhc3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhciB4MTogbnVtYmVyLCB4MjogbnVtYmVyLCB3OiBudW1iZXI7XHJcbiAgICAgICAgICAgICAgICBkbyB7XHJcbiAgICAgICAgICAgICAgICAgICAgeDEgPSAyLjAgKiBNYXRoLnJhbmRvbSgpIC0gMS4wO1xyXG4gICAgICAgICAgICAgICAgICAgIHgyID0gMi4wICogTWF0aC5yYW5kb20oKSAtIDEuMDtcclxuICAgICAgICAgICAgICAgICAgICB3ID0geDEgKiB4MSArIHgyICogeDI7XHJcbiAgICAgICAgICAgICAgICB9IHdoaWxlICh3ID49IDEuMCk7XHJcbiAgICAgICAgICAgICAgICB3ID0gTWF0aC5zcXJ0KCgtMi4wICogTWF0aC5sb2codykpIC8gdyk7XHJcbiAgICAgICAgICAgICAgICB5MSA9IHgxICogdztcclxuICAgICAgICAgICAgICAgIHkyID0geDIgKiB3O1xyXG4gICAgICAgICAgICAgICAgdXNlX2xhc3QgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB2YXIgcmV0dmFsID0gbWVhbiArIHN0ZGV2ICogeTE7XHJcbiAgICAgICAgICAgIC8vIGlmKHJldHZhbCA+IDApXHJcbiAgICAgICAgICAgIC8vICAgICByZXR1cm4gcmV0dmFsO1xyXG4gICAgICAgICAgICAvLyByZXR1cm4gLXJldHZhbDtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiByZXR2YWw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIHB1YmxpYyBzdGF0aWMgcmFuZG9tTm9ybWFsID0gTXlNYXRoLmdhdXNzaWFuKDAsIDEpO1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgcmFuZG9tTm9ybWFsKHN0ZGV2Om51bWJlcikge1xyXG4gICAgICAgIHJldHVybiBNeU1hdGguZ2F1c3NpYW4oMCwgc3RkZXYpO1xyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCBOb2RlR2VuZSBmcm9tIFwiLi9Ob2RlR2VuZVwiO1xyXG5pbXBvcnQgQ29ubmVjdEdlbmUgZnJvbSBcIi4vQ29ubmVjdEdlbmVcIjtcclxuaW1wb3J0IHtOb2RlR2VuZXN9IGZyb20gXCIuL05vZGVHZW5lXCI7XHJcbmltcG9ydCB7Tm9kZUdlbmVUeXBlfSBmcm9tIFwiLi9Ob2RlR2VuZVwiO1xyXG5pbXBvcnQge0Nvbm5lY3RHZW5lc30gZnJvbSBcIi4vQ29ubmVjdEdlbmVcIjtcclxuaW1wb3J0IE15TWF0aCBmcm9tIFwiLi4vTXlNYXRoXCI7XHJcbmltcG9ydCBDb25maWcgZnJvbSBcIi4uL0NvbmZpZ1wiO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBCYXNlSW5kaXZpZHVhbEludGVyZmFjZSB7XHJcbiAgICBldmFsdWF0ZUZpdG5lc3MoKTogUHJvbWlzZTxudW1iZXI+O1xyXG4gICAgYnJlZWQocGFydG5lcjogQmFzZUluZGl2aWR1YWxJbnRlcmZhY2UpOiBCYXNlSW5kaXZpZHVhbEludGVyZmFjZTtcclxuICAgIG11dGF0ZSgpOiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgdHlwZSBJbmRpdmlkdWFscyA9IEFycmF5PEJhc2VOZWF0SW5kaXZpZHVhbD47XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZU5lYXRJbmRpdmlkdWFsIGltcGxlbWVudHMgQmFzZUluZGl2aWR1YWxJbnRlcmZhY2Uge1xyXG5cclxuICAgIHByb3RlY3RlZCBfaWQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgc3RhdGljIF9pZENvdW50ZXI6IG51bWJlciA9IDA7XHJcblxyXG4gICAgcHJvdGVjdGVkIF9nZW5vbWU6IFtOb2RlR2VuZXMsIENvbm5lY3RHZW5lc107IC8vIHR1cGxlIG9mIHR3byBcIm1hcHMvZGljdGlvbmFyaWVzXCJcclxuICAgIHByb3RlY3RlZCBfaW5wdXRHZW5lczogQXJyYXk8Tm9kZUdlbmU+O1xyXG4gICAgcHJvdGVjdGVkIF9vdXRwdXRHZW5lczogQXJyYXk8Tm9kZUdlbmU+O1xyXG5cclxuICAgIHByb3RlY3RlZCBfZml0bmVzczogbnVtYmVyO1xyXG5cclxuICAgIHB1YmxpYyBlbGltaW5hdGVkOiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGluZGl2aWR1YWw6IEJhc2VOZWF0SW5kaXZpZHVhbCk7XHJcbiAgICBjb25zdHJ1Y3RvcihpbnB1dHNMZW5ndGg6IG51bWJlciwgb3V0cHV0c0xlbmd0aDogbnVtYmVyKTtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuX2lkID0gKytCYXNlTmVhdEluZGl2aWR1YWwuX2lkQ291bnRlcjtcclxuICAgICAgICB0aGlzLl9nZW5vbWUgPSBbe30sIHt9XTtcclxuICAgICAgICB0aGlzLmVsaW1pbmF0ZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYoYXJndW1lbnRzLmxlbmd0aCA9PSAxICYmIGFyZ3VtZW50c1swXSBpbnN0YW5jZW9mIEJhc2VOZWF0SW5kaXZpZHVhbCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvcHlDb25zdHJ1Y3Rvcihhcmd1bWVudHNbMF0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9ybWFsQ29uc3RydWN0b3IuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVzY3JpcHRpb24gY29weSBjb25zdHJ1Y3RvclxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGNvcHlDb25zdHJ1Y3Rvcih0aGF0OiBCYXNlTmVhdEluZGl2aWR1YWwpIHtcclxuICAgICAgICB0aGlzLl9pbnB1dEdlbmVzID0gW107XHJcbiAgICAgICAgdGhpcy5fb3V0cHV0R2VuZXMgPSBbXTtcclxuXHJcbiAgICAgICAgLy8gY29weSBub2RlR2VuZXNcclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0aGF0Lm5vZGVHZW5lcykge1xyXG4gICAgICAgICAgICB0aGlzLmFkZE5vZGVHZW5lKG5ldyBOb2RlR2VuZSh0aGF0Lm5vZGVHZW5lc1trZXldKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBhZGQgc2FtZSBjb25uZWN0aW9uc1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoYXQuY29ubmVjdEdlbmVzKSB7XHJcbiAgICAgICAgICAgIGxldCB0aGF0Q29ubmVjdE5vZGUgPSB0aGF0LmNvbm5lY3RHZW5lc1trZXldO1xyXG4gICAgICAgICAgICB0aGlzLmFkZENvbm5lY3Rpb24oXHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGVHZW5lc1t0aGF0Q29ubmVjdE5vZGUuaW5Ob2RlLmlubm92XSxcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZUdlbmVzW3RoYXRDb25uZWN0Tm9kZS5vdXROb2RlLmlubm92XSxcclxuICAgICAgICAgICAgICAgIHRoYXRDb25uZWN0Tm9kZS53ZWlnaHQsXHJcbiAgICAgICAgICAgICAgICB0aGF0Q29ubmVjdE5vZGUuZW5hYmxlZCxcclxuICAgICAgICAgICAgICAgIHRoYXRDb25uZWN0Tm9kZS5pbm5vdlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBkZXNjcmlwdGlvbiBhcmdzIGNvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgbm9ybWFsQ29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgbGV0IGlucHV0c0xlbmd0aDogbnVtYmVyO1xyXG4gICAgICAgIGxldCBvdXRwdXRzTGVuZ3RoOiBudW1iZXI7XHJcblxyXG4gICAgICAgIGlmKHR5cGVvZiBhcmd1bWVudHNbMF0gPT09IFwibnVtYmVyXCIgJiYgdHlwZW9mIGFyZ3VtZW50c1sxXSA9PT0gXCJudW1iZXJcIikge1xyXG4gICAgICAgICAgICBpbnB1dHNMZW5ndGggPSBhcmd1bWVudHNbMF07XHJcbiAgICAgICAgICAgIG91dHB1dHNMZW5ndGggPSBhcmd1bWVudHNbMV07XHJcbiAgICAgICAgICAgIHRoaXMuX2lucHV0R2VuZXMgPSBbXTtcclxuICAgICAgICAgICAgdGhpcy5fb3V0cHV0R2VuZXMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBpbnB1dHNMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZGROb2RlR2VuZShuZXcgTm9kZUdlbmUoTm9kZUdlbmVUeXBlLklucHV0KSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvcihsZXQgbyA9IDA7IG8gPCBvdXRwdXRzTGVuZ3RoOyBvKyspIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkTm9kZUdlbmUobmV3IE5vZGVHZW5lKE5vZGVHZW5lVHlwZS5PdXRwdXQpKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9IGVsc2UgaWYoQXJyYXkuaXNBcnJheShhcmd1bWVudHNbMF0pICYmIEFycmF5LmlzQXJyYXkoYXJndW1lbnRzWzFdKSkge1xyXG4gICAgICAgICAgICBpbnB1dHNMZW5ndGggPSBhcmd1bWVudHNbMF0ubGVuZ3RoO1xyXG4gICAgICAgICAgICBvdXRwdXRzTGVuZ3RoID0gYXJndW1lbnRzWzFdLmxlbmd0aDtcclxuICAgICAgICAgICAgdGhpcy5faW5wdXRHZW5lcyA9IGFyZ3VtZW50c1swXTtcclxuICAgICAgICAgICAgdGhpcy5fb3V0cHV0R2VuZXMgPSBhcmd1bWVudHNbMV07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgXCJVbmV4cGVjdGVkIHBhcmFtZXRlcnNcIjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBpbnB1dHNMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBmb3IobGV0IG8gPSAwOyBvIDwgb3V0cHV0c0xlbmd0aDsgbysrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFkZENvbm5lY3Rpb24odGhpcy5faW5wdXRHZW5lc1tpXSwgdGhpcy5fb3V0cHV0R2VuZXNbb10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlc2NyaXB0aW9uIG1lcmdlcyBvbiB0b3Agb2YgdGhpcyBpbmRpdmlkdWFsIChkb2VzIG5vdCBjaGFuZ2UgbWF0Y2hpbmcgbm9kZXMpXHJcbiAgICAgKiBAcGFyYW0gdGhhdFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG1lcmdlKHRoYXQ6IEJhc2VOZWF0SW5kaXZpZHVhbCkge1xyXG5cclxuICAgICAgICAvLyBjb3B5IG5vZGVHZW5lcyB3aGljaCBkbyBub3QgZXhpc3RzIHlldFxyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoYXQubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIGxldCB0aGlzTm9kZTogTm9kZUdlbmUgPSB0aGlzLm5vZGVHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBsZXQgdGhhdE5vZGU6IE5vZGVHZW5lID0gdGhhdC5ub2RlR2VuZXNba2V5XTtcclxuXHJcbiAgICAgICAgICAgIGlmKCF0aGlzTm9kZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZGROb2RlR2VuZShuZXcgTm9kZUdlbmUodGhhdE5vZGUpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gYWRkIGNvbm5lY3Rpb25zIHdoaWNoIGRvIG5vdCBleGlzdHMgeWV0XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhhdC5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IHRoaXNDb25uID0gdGhpcy5jb25uZWN0R2VuZXNba2V5XTtcclxuICAgICAgICAgICAgbGV0IHRoYXRDb25uID0gdGhhdC5jb25uZWN0R2VuZXNba2V5XTtcclxuXHJcbiAgICAgICAgICAgIGlmKCF0aGlzQ29ubikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZGRDb25uZWN0aW9uKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZUdlbmVzW3RoYXRDb25uLmluTm9kZS5pbm5vdl0sXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlR2VuZXNbdGhhdENvbm4ub3V0Tm9kZS5pbm5vdl0sXHJcbiAgICAgICAgICAgICAgICAgICAgdGhhdENvbm4ud2VpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgIHRoYXRDb25uLmVuYWJsZWQsXHJcbiAgICAgICAgICAgICAgICAgICAgdGhhdENvbm4uaW5ub3ZcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBjcmVhdGVJbml0UG9wdWxhdGlvbihwb3B1bGF0aW9uU2l6ZTogbnVtYmVyLCBJbmRpdmlkdWFsQ2xhc3M6IG5ldyAoLi4uYXJnczogYW55W10pID0+IEJhc2VOZWF0SW5kaXZpZHVhbCwgZmlyc3RJbmRpdmlkdWFsQ29uc3RydWN0b3JBcmdzPzogQXJyYXk8YW55PikgOiBCYXNlTmVhdEluZGl2aWR1YWxbXSB7XHJcbiAgICAgICAgbGV0IHBvcHVsYXRpb246IEJhc2VOZWF0SW5kaXZpZHVhbFtdID0gW107XHJcbiAgICAgICAgbGV0IGZpcnN0OiBCYXNlTmVhdEluZGl2aWR1YWw7XHJcbiAgICAgICAgaWYodHlwZW9mIGZpcnN0SW5kaXZpZHVhbENvbnN0cnVjdG9yQXJncyAhPT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgICAgICBmaXJzdCA9IG5ldyBJbmRpdmlkdWFsQ2xhc3MoLi4uZmlyc3RJbmRpdmlkdWFsQ29uc3RydWN0b3JBcmdzKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBmaXJzdCA9IG5ldyBJbmRpdmlkdWFsQ2xhc3MoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHBvcHVsYXRpb24ucHVzaChmaXJzdCk7XHJcbiAgICAgICAgZm9yKGxldCBpID0gMTsgaSA8IHBvcHVsYXRpb25TaXplOyBpKyspIHtcclxuICAgICAgICAgICAgLy8gcmFuZG9taXplIHdlaWdodHNcclxuICAgICAgICAgICAgbGV0IG5leHQgPSBuZXcgSW5kaXZpZHVhbENsYXNzKGZpcnN0KTtcclxuXHJcbiAgICAgICAgICAgIGZvcihsZXQga2V5IGluIG5leHQuY29ubmVjdEdlbmVzKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgY29ubmVjdGlvbiA9IG5leHQuY29ubmVjdEdlbmVzW2tleV07XHJcbiAgICAgICAgICAgICAgICBjb25uZWN0aW9uLndlaWdodCA9IDIqTWF0aC5yYW5kb20oKSAtIDE7IC8vIFRPRE8gY29uZmlnPyByYW5kb21XZWlnaHRGdW5jXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHBvcHVsYXRpb24ucHVzaChuZXh0KTsgLy8gY29waWVzIG9mIGZpcnN0IHNvIHNhbWUgaW5ub3YgbnVtYmVyc1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHBvcHVsYXRpb24gO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkTm9kZUdlbmUobm9kZUdlbmU6IE5vZGVHZW5lKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5ub2RlR2VuZXNbbm9kZUdlbmUuaW5ub3ZdID0gbm9kZUdlbmU7XHJcbiAgICAgICAgaWYobm9kZUdlbmUudHlwZSA9PSBOb2RlR2VuZVR5cGUuSW5wdXQpIHtcclxuICAgICAgICAgICAgdGhpcy5faW5wdXRHZW5lcy5wdXNoKG5vZGVHZW5lKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYobm9kZUdlbmUudHlwZSA9PSBOb2RlR2VuZVR5cGUuT3V0cHV0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX291dHB1dEdlbmVzLnB1c2gobm9kZUdlbmUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGFkZENvbm5lY3RHZW5lKGNvbm5lY3RHZW5lOiBDb25uZWN0R2VuZSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29ubmVjdEdlbmVzW2Nvbm5lY3RHZW5lLmlubm92XSA9IGNvbm5lY3RHZW5lO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkQ29ubmVjdGlvbihpbk5vZGU6Tm9kZUdlbmUsIG91dE5vZGU6Tm9kZUdlbmUsIHdlaWdodD8gOiBudW1iZXIsIGVuYWJsZWQ/OiBib29sZWFuLCBpbm5vdj86IG51bWJlcikge1xyXG4gICAgICAgIHdlaWdodCA9IHR5cGVvZiB3ZWlnaHQgIT0gXCJ1bmRlZmluZWRcIiA/IHdlaWdodCA6IDIqTWF0aC5yYW5kb20oKSAtIDE7IC8vIFRPRE8gY29uZmlnPyByYW5kb21XZWlnaHRGdW5jXHJcbiAgICAgICAgdGhpcy5hZGRDb25uZWN0R2VuZShuZXcgQ29ubmVjdEdlbmUoaW5Ob2RlLCBvdXROb2RlLCB3ZWlnaHQsIGVuYWJsZWQsIGlubm92KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFic3RyYWN0IGV2YWx1YXRlRml0bmVzcygpOiBQcm9taXNlPG51bWJlcj47XHJcblxyXG4gICAgcHVibGljIGJyZWVkKHBhcnRuZXI6IEJhc2VOZWF0SW5kaXZpZHVhbCk6IEJhc2VOZWF0SW5kaXZpZHVhbCB7XHJcblxyXG4gICAgICAgIGxldCBiZXR0ZXI6IEJhc2VOZWF0SW5kaXZpZHVhbDtcclxuICAgICAgICBsZXQgd29yc2U6IEJhc2VOZWF0SW5kaXZpZHVhbDtcclxuXHJcbiAgICAgICAgaWYodGhpcy5maXRuZXNzID4gcGFydG5lci5maXRuZXNzKSB7XHJcbiAgICAgICAgICAgIGJldHRlciA9IHRoaXM7XHJcbiAgICAgICAgICAgIHdvcnNlID0gcGFydG5lcjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBiZXR0ZXIgPSBwYXJ0bmVyO1xyXG4gICAgICAgICAgICB3b3JzZSA9IHRoaXM7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBtYWtlIGNvcHkgb2YgYmV0dGVyXHJcbiAgICAgICAgbGV0IG9mZnNwcmluZzogQmFzZU5lYXRJbmRpdmlkdWFsID0gbmV3ICg8YW55PmJldHRlci5jb25zdHJ1Y3RvcikoYmV0dGVyKTsgLy8gY29tcGFjdCB3YXlcclxuXHJcbiAgICAgICAgLy8gaW5oZXJpdCByYW5kb21seSB3ZWlnaHQgZm9yIHNoYXJlZCBnZW5lc1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIG9mZnNwcmluZy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IGNvbm5PZmZzcHJpbmc6IENvbm5lY3RHZW5lID0gb2Zmc3ByaW5nLmNvbm5lY3RHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBsZXQgY29ubldvcnNlOiBDb25uZWN0R2VuZSA9IHdvcnNlLmNvbm5lY3RHZW5lc1trZXldO1xyXG5cclxuICAgICAgICAgICAgaWYoY29ubldvcnNlKSB7IC8vIHNoYXJlZCBnZW5lIGNyb3Nzb3ZlclxyXG4gICAgICAgICAgICAgICAgbGV0IHdoaWNoID0gMC41ID4gTWF0aC5yYW5kb20oKTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25uT2Zmc3ByaW5nLndlaWdodCA9IHdoaWNoID8gY29ubk9mZnNwcmluZy53ZWlnaHQgOiBjb25uV29yc2Uud2VpZ2h0OyAvLyBUT0RPIHdlaWdodENyb3Nzb3ZlckZ1bmNcclxuICAgICAgICAgICAgICAgIGNvbm5PZmZzcHJpbmcuZW5hYmxlZCA9IHdoaWNoID8gY29ubk9mZnNwcmluZy5lbmFibGVkIDogY29ubldvcnNlLmVuYWJsZWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qKiBUT0RPXHJcbiAgICAgICAgICogdGhpcyBwYXJ0IGlzIG5vdCB0b3RhbGx5IGNsZWFyIGZvciBtZSAtIGZyb20gaHR0cDovL25uLmNzLnV0ZXhhcy5lZHUvZG93bmxvYWRzL3BhcGVycy9zdGFubGV5LmVjMDIucGRmOlxyXG4gICAgICAgICAqIFwiSW4gdGhpcyBjYXNlLCBlcXVhbCBmaXRuZXNzZXMgYXJlIGFzc3VtZWQsIHNvIHRoZSBkaXNqb2ludCBhbmQgZXhjZXNzIGdlbmVzIGFyZSBhbHNvIGluaGVyaXRlZCByYW5kb21seS5cIlxyXG4gICAgICAgICAqIEhvdyBjYW4gYmUgZGlzam9pbnQgYW5kIGV4Y2VzcyBnZW5lcyBpbmhlcnJpdGVkIHJhbmRvbWx5IHdoZW4gdGhleSBhcmUgb25seSBpbiBvbmUgb2YgdGhlIHBhcmVudD9cclxuICAgICAgICAgKi9cclxuICAgICAgICAvLyBpZiBzYW1lIGZpdG5lc3NlcywgYWxzbyBpbmhlcml0IChjb3B5KSBnZW5lcyBmcm9tIG90aGVyIHBhcmVudFxyXG4gICAgICAgIGlmICh0aGlzLmZpdG5lc3MgPT0gcGFydG5lci5maXRuZXNzKSB7XHJcbiAgICAgICAgICAgIG9mZnNwcmluZy5tZXJnZSh3b3JzZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpbmhlcml0IHJhbmRvbWx5IHRocmVzaG9sZHMgLSBpbWFnaW5lIGFzIGFub3RoZXIgaW4gY29ubmVjdGlvblxyXG4gICAgICAgIGZvcihsZXQga2V5IGluIG9mZnNwcmluZy5ub2RlR2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IG5vZGVPZmZzcHJpbmc6IE5vZGVHZW5lID0gb2Zmc3ByaW5nLm5vZGVHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBsZXQgbm9kZVdvcnNlOiBOb2RlR2VuZSA9IHdvcnNlLm5vZGVHZW5lc1trZXldO1xyXG5cclxuICAgICAgICAgICAgaWYobm9kZVdvcnNlKSB7XHJcbiAgICAgICAgICAgICAgICBub2RlT2Zmc3ByaW5nLnRocmVzaG9sZCA9IDAuNSA+IE1hdGgucmFuZG9tKCkgPyBub2RlT2Zmc3ByaW5nLnRocmVzaG9sZCA6IG5vZGVXb3JzZS50aHJlc2hvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGNoYW5jZSB0byBlbmFibGUgY29ubmVjdGlvbiBnZW5lc1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIG9mZnNwcmluZy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IGNvbm5PZmZzcHJpbmc6IENvbm5lY3RHZW5lID0gb2Zmc3ByaW5nLmNvbm5lY3RHZW5lc1trZXldO1xyXG5cclxuICAgICAgICAgICAgY29ubk9mZnNwcmluZy5lbmFibGVkID0gMC4yNSA+IE1hdGgucmFuZG9tKCkgPyB0cnVlIDogY29ubk9mZnNwcmluZy5lbmFibGVkOyAvLyBUT0RPIGVkaXRhYmxlIGNoYW5jZVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG9mZnNwcmluZztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgbXV0YXRlKCk6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICBsZXQgaXNNdXRhdGVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGxldCB0b3BvbG9neU11dE9wdE9mID0gQ29uZmlnLm11dGF0aW9uT3B0aW9ucy5pbmRpdmlkdWFsVG9wb2xvZ3k7XHJcbiAgICAgICAgbGV0IHdlaWdodHNNdXRPcHRPZiA9IENvbmZpZy5tdXRhdGlvbk9wdGlvbnMuaW5kaXZpZHVhbFdlaWdodHM7XHJcblxyXG4gICAgICAgIGlmKHRvcG9sb2d5TXV0T3B0T2YuY2hhbmNlID4gTWF0aC5yYW5kb20oKSkge1xyXG4gICAgICAgICAgICBsZXQgeG9yQ2hhbmNlOiBudW1iZXI7IC8vIDIgZm9yIGZhbHNlLCA8MDsxKSBmb3IgYWRkTm9kZSwgPDE7MikgZm9yIGFkZENvbm5lY3Rpb25cclxuICAgICAgICAgICAgaWYgKHRvcG9sb2d5TXV0T3B0T2YuYWRkTm9kZVhPUmFkZENvbm5lY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgIHhvckNoYW5jZSA9IE1hdGgucmFuZG9tKCkgKiAyOyAvLyBNYXRoLnJhbmRvbSgpIG5ldmVyIGVxdWFscyAxID0+IHhvckNoYW5jZSBuZXZlciBlcXVhbHMgMiB3aGljaCBpcyByZXNlcnZlZCBmb3IgeG9yQ2hhbmNlIGZhbHNlXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB4b3JDaGFuY2UgPSAyO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoeG9yQ2hhbmNlID09PSAyIHx8ICh4b3JDaGFuY2UgPj0gMCAmJiB4b3JDaGFuY2UgPCAxKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRvcG9sb2d5TXV0T3B0T2YuYWRkTm9kZS5jaGFuY2UgPiBNYXRoLnJhbmRvbSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tdXRhdGVBZGROb2RlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNNdXRhdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHhvckNoYW5jZSA9PT0gMiB8fCAoeG9yQ2hhbmNlID49IDEgJiYgeG9yQ2hhbmNlIDwgMikpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0b3BvbG9neU11dE9wdE9mLmFkZENvbm5lY3Rpb24uY2hhbmNlID4gTWF0aC5yYW5kb20oKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXV0YXRlQWRkQ29ubmVjdGlvbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlzTXV0YXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKHdlaWdodHNNdXRPcHRPZi5jaGFuY2UgPiBNYXRoLnJhbmRvbSgpKSB7XHJcblxyXG4gICAgICAgICAgICBpZih3ZWlnaHRzTXV0T3B0T2Yud2VpZ2h0cy5jaGFuY2UgPiBNYXRoLnJhbmRvbSgpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm11dGF0ZVdlaWdodHMoKTtcclxuICAgICAgICAgICAgICAgIGlzTXV0YXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmKHdlaWdodHNNdXRPcHRPZi50aHJlc2hvbGRzLmNoYW5jZSA+IE1hdGgucmFuZG9tKCkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubXV0YXRlVGhyZXNob2xkcygpO1xyXG4gICAgICAgICAgICAgICAgaXNNdXRhdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fZml0bmVzcyA9IG51bGw7XHJcblxyXG4gICAgICAgIHJldHVybiBpc011dGF0ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBtdXRhdGVBZGRDb25uZWN0aW9uKCk6IHZvaWQge1xyXG5cclxuICAgICAgICBsZXQgbjE6IE5vZGVHZW5lID0gdGhpcy5nZXRSYW5kb21Ob2RlR2VuZSgpO1xyXG4gICAgICAgIGxldCBuMjogTm9kZUdlbmU7XHJcblxyXG4gICAgICAgIGRvIHsgLy8gVE9ETyBwb3NzaWJsZSBpbmZpbml0ZSBsb29wXHJcbiAgICAgICAgICAgIG4yID0gdGhpcy5nZXRSYW5kb21Ob2RlR2VuZSgpO1xyXG4gICAgICAgIH0gd2hpbGUobjEuaWQgPT0gbjIuaWQpO1xyXG5cclxuICAgICAgICBpZighdGhpcy5hcmVDb25uZWN0ZWQobjEsIG4yKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hZGRDb25uZWN0aW9uKG4xLG4yKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tdXRhdGVBZGRDb25uZWN0aW9uKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbXV0YXRlQWRkTm9kZSgpOiB2b2lkIHtcclxuICAgICAgICBsZXQgZWRnZTogQ29ubmVjdEdlbmU7XHJcblxyXG4gICAgICAgIGRvIHsgLy8gVE9ETyBwb3NzaWJsZSBpbmZpbml0ZSBsb29wXHJcbiAgICAgICAgICAgIGVkZ2UgPSB0aGlzLmdldFJhbmRvbUNvbm5lY3RHZW5lKClcclxuICAgICAgICB9IHdoaWxlKGVkZ2UuZW5hYmxlZCA9PSBmYWxzZSk7XHJcblxyXG4gICAgICAgIGVkZ2UuZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBpbk5vZGUgPSBlZGdlLmluTm9kZTtcclxuICAgICAgICBsZXQgb3V0Tm9kZSA9IGVkZ2Uub3V0Tm9kZTtcclxuICAgICAgICBsZXQgaW5uZXJOb2RlID0gbmV3IE5vZGVHZW5lKCk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkTm9kZUdlbmUoaW5uZXJOb2RlKTtcclxuICAgICAgICB0aGlzLmFkZENvbm5lY3Rpb24oaW5Ob2RlLCBpbm5lck5vZGUsIDEpO1xyXG4gICAgICAgIHRoaXMuYWRkQ29ubmVjdGlvbihpbm5lck5vZGUsIG91dE5vZGUsIGVkZ2Uud2VpZ2h0KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG11dGF0ZVdlaWdodHMoKTogdm9pZCB7XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgaWYoQ29uZmlnLm11dGF0aW9uT3B0aW9ucy5pbmRpdmlkdWFsV2VpZ2h0cy53ZWlnaHRzLm11dGF0ZVNpbmdsZS5jaGFuY2UgPiBNYXRoLnJhbmRvbSgpKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgY29ubmVjdGlvbjogQ29ubmVjdEdlbmUgPSB0aGlzLmNvbm5lY3RHZW5lc1trZXldO1xyXG4gICAgICAgICAgICAgICAgY29ubmVjdGlvbi53ZWlnaHQgKz0gTXlNYXRoLnJhbmRvbU5vcm1hbChDb25maWcubXV0YXRpb25PcHRpb25zLmluZGl2aWR1YWxXZWlnaHRzLndlaWdodHMubXV0YXRlU2luZ2xlLnN0ZGV2KSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbXV0YXRlVGhyZXNob2xkcygpOiB2b2lkIHtcclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0aGlzLm5vZGVHZW5lcykge1xyXG4gICAgICAgICAgICBpZihDb25maWcubXV0YXRpb25PcHRpb25zLmluZGl2aWR1YWxXZWlnaHRzLnRocmVzaG9sZHMubXV0YXRlU2luZ2xlLmNoYW5jZSA+IE1hdGgucmFuZG9tKCkpIHtcclxuICAgICAgICAgICAgICAgIGxldCBub2RlOiBOb2RlR2VuZSA9IHRoaXMubm9kZUdlbmVzW2tleV07XHJcbiAgICAgICAgICAgICAgICBub2RlLnRocmVzaG9sZCArPSBNeU1hdGgucmFuZG9tTm9ybWFsKENvbmZpZy5tdXRhdGlvbk9wdGlvbnMuaW5kaXZpZHVhbFdlaWdodHMudGhyZXNob2xkcy5tdXRhdGVTaW5nbGUuc3RkZXYpKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGV2YWx1YXRlTmV0d29yayhpbnB1dHM6IG51bWJlcltdKTogbnVtYmVyW10ge1xyXG4gICAgICAgIGxldCBvdXRwdXRzOiBudW1iZXJbXSA9IFtdO1xyXG5cclxuICAgICAgICAvLyBldmFsdWF0ZSBpbnB1dCBub2RlcyAocmVjdXJzaW9uIGVuZCBjb25kaXRpb24pXHJcbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuaW5wdXRHZW5lcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmlucHV0R2VuZXNbaV0uZXZhbHVhdGVPdXRwdXQoaW5wdXRzW2ldKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGV2YWx1YXRlIG5ldHdvcmsgZnJvbSBvdXRwdXQgbm9kZXMgYnkgcmVjdXJzaW9uXHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5vdXRwdXRHZW5lcykge1xyXG4gICAgICAgICAgICBvdXRwdXRzLnB1c2goXHJcbiAgICAgICAgICAgICAgICB0aGlzLm91dHB1dEdlbmVzW2tleV0uZXZhbHVhdGVPdXRwdXQoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyByZXNldCBuZXR3b3JrIGV2YWx1YXRpb24gZm9yIG5leHQgZXZhbHVhdGlvblxyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoaXMubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZUdlbmVzW2tleV0ucmVzZXRPdXRwdXQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBvdXRwdXRzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b1N0cmluZygpOiBzdHJpbmcge1xyXG5cclxuICAgICAgICBsZXQgbm9kZUdlbmVzU3RyaW5nOiBzdHJpbmcgPSBcIlwiO1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoaXMubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIG5vZGVHZW5lc1N0cmluZyArPSBcIlxcdFwiICsgdGhpcy5ub2RlR2VuZXNba2V5XS50b1N0cmluZygpICsgXCI7XFxuXCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY29ubmVjdEdlbmVzU3RyaW5nOiBzdHJpbmcgPSBcIlwiO1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoaXMuY29ubmVjdEdlbmVzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25uZWN0R2VuZXNTdHJpbmcgKz0gXCJcXHRcIiArIHRoaXMuY29ubmVjdEdlbmVzW2tleV0udG9TdHJpbmcoKSArIFwiO1xcblwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIFwiQmFzZU5lYXRJbmRpdmlkdWFsID0ge1xcblwiICtcclxuICAgICAgICAgICAgICAgIFwiXFx0aWQ6IFwiICsgdGhpcy5pZCArIFwiO1xcblwiICtcclxuICAgICAgICAgICAgICAgIFwiXFx0Zml0bmVzczogXCIgKyB0aGlzLmZpdG5lc3MgKyBcIjtcXG5cIiArXHJcbiAgICAgICAgICAgICAgICBub2RlR2VuZXNTdHJpbmcgK1xyXG4gICAgICAgICAgICAgICAgY29ubmVjdEdlbmVzU3RyaW5nXHJcbiAgICAgICAgICAgICsgXCJ9XCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzdGF0aWMgbm9kZUdlbmVUb1RleHQobm9kZUdlbmU6IE5vZGVHZW5lKSB7XHJcbiAgICAgICAgcmV0dXJuIGBpbm5vdjogJHtub2RlR2VuZS5pbm5vdn1cclxuICAgICAgICAgICAgICAgIHRocmVzaG9sZDogJHtub2RlR2VuZS50aHJlc2hvbGR9YDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHN0YXRpYyBjb25uZWN0R2VuZVRvVGV4dChjb25uZWN0R2VuZUdlbmU6IENvbm5lY3RHZW5lKSB7XHJcbiAgICAgICAgcmV0dXJuIGBpbm5vdjogJHtjb25uZWN0R2VuZUdlbmUuaW5ub3Z9XHJcbiAgICAgICAgICAgICAgICB3ZWlnaHQ6ICR7Y29ubmVjdEdlbmVHZW5lLndlaWdodC50b0ZpeGVkKDIpfVxyXG4gICAgICAgICAgICAgICAgJHtjb25uZWN0R2VuZUdlbmUuZW5hYmxlZCA/IFwiXCIgOiBcIkRJU0FCTEVEXCJ9YFxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b0dPSlMoKTogT2JqZWN0IHtcclxuICAgICAgICBsZXQganNvbiA6IGFueSA9IHt9O1xyXG4gICAgICAgIGpzb25bXCJub2RlS2V5UHJvcGVydHlcIl0gPSBcImlkXCI7XHJcblxyXG4gICAgICAgIGpzb25bXCJub2RlRGF0YUFycmF5XCJdID0gW107XHJcbiAgICAgICAgZm9yKGxldCBrZXkgaW4gdGhpcy5ub2RlR2VuZXMpIHtcclxuICAgICAgICAgICAgbGV0IG5vZGVHZW5lID10aGlzLm5vZGVHZW5lc1trZXldO1xyXG4gICAgICAgICAgICBqc29uW1wibm9kZURhdGFBcnJheVwiXS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGlkOiBub2RlR2VuZS5pZCxcclxuICAgICAgICAgICAgICAgIHRleHQ6IEJhc2VOZWF0SW5kaXZpZHVhbC5ub2RlR2VuZVRvVGV4dChub2RlR2VuZSlcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBqc29uW1wibGlua0RhdGFBcnJheVwiXSA9IFtdO1xyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoaXMuY29ubmVjdEdlbmVzKSB7XHJcbiAgICAgICAgICAgIGxldCBjb25uZWN0R2VuZSA9dGhpcy5jb25uZWN0R2VuZXNba2V5XTtcclxuICAgICAgICAgICAganNvbltcImxpbmtEYXRhQXJyYXlcIl0ucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBmcm9tOiBjb25uZWN0R2VuZS5pbk5vZGUuaWQsXHJcbiAgICAgICAgICAgICAgICB0bzogY29ubmVjdEdlbmUub3V0Tm9kZS5pZCxcclxuICAgICAgICAgICAgICAgIHRleHQ6IEJhc2VOZWF0SW5kaXZpZHVhbC5jb25uZWN0R2VuZVRvVGV4dChjb25uZWN0R2VuZSlcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBqc29uW1wibm9kZURhdGFBcnJheVwiXSA9IFt7IFwiaWRcIjogMCwgXCJsb2NcIjogXCIxMjAgMTIwXCIsIFwidGV4dFwiOiBcIlhYWFwiIH1dO1xyXG4gICAgICAgIC8vIGpzb25bXCJsaW5rRGF0YUFycmF5XCJdID0gW3sgXCJmcm9tXCI6IDAsIFwidG9cIjogMCwgXCJ0ZXh0XCI6IFwidXAgb3IgdGltZXJcIiwgXCJjdXJ2aW5lc3NcIjogLTIwIH1dO1xyXG5cclxuICAgICAgICByZXR1cm4ganNvbjtcclxuICAgIH1cclxuXHJcbiAgICAvL1xyXG4gICAgLy8gR0VUIGZ1bmN0aW9uc1xyXG4gICAgLy9cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBjb21wbGV4aXR5IE8oI05HKVxyXG4gICAgICogVE9ETyBvZnRlbiBmdW5jdGlvbiBhbmQgTyhuKSBjb21wbGV4aXR5LCBjb3VsZCBiZSBlbGltaW5hdGVkIGlmIGFycmF5IG9mIE5vZGVHZW5lcyBpcyB1c2VkLCBidXQgaG93IGFib3V0IG90aGVyIGZ1bmN0aW9ucyAoY3Jvc3NvdmVyKSwgcHJvYmFibHkgZWFzeSBieSBhcnJheSBmdW5jdGlvbnNcclxuICAgICAqIHBpY2sgcmFuZG9tIGZyb20gc3RyZWFtIC0gY29tcGxleGl0eSBPKG4pXHJcbiAgICAgKiBwcm9ibGVtIHRoYXQgb2JqZWN0IGlzIHVzZWQgbm90IGFycmF5XHJcbiAgICAgKiBzaXplIHdpbGwgbm90IGhlbHBcclxuICAgICAqIEBzZWUgbmV4dCBzb2x1dGlvblxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGdldFJhbmRvbU5vZGVHZW5lKCk6IE5vZGVHZW5lIHtcclxuICAgICAgICBsZXQgcmVzdWx0OiBOb2RlR2VuZTtcclxuICAgICAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLm5vZGVHZW5lcykge1xyXG4gICAgICAgICAgICBpZiAoTWF0aC5yYW5kb20oKSA8IDEgLyArK2NvdW50KSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgPSB0aGlzLm5vZGVHZW5lc1trZXldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLyoqXHJcbiAgICAvLyAgKiBUT0RPIHF1ZXN0aW9uIGlzIGlmIE9iamVjdC5rZXlzKCkgaXMgTygxKSBvciBPKG4pXHJcbiAgICAvLyAgKiBAcmV0dXJucyB7YW55fVxyXG4gICAgLy8gICovXHJcbiAgICAvLyBwcml2YXRlIGdldFJhbmRvbU5vZGVHZW5lWFhYKCkge1xyXG4gICAgLy8gICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXModGhpcy5ub2RlR2VuZXMpOyAvLyBPKG4pIHNvIG1ha2VzIG5vIHNlbmNlXHJcbiAgICAvLyAgICAgcmV0dXJuIHRoaXMubm9kZUdlbmVzW2tleXNbIGtleXMubGVuZ3RoICogTWF0aC5yYW5kb20oKSA8PCAwXV07XHJcbiAgICAvLyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUT0RPIHNhbWUgcHJvYmxlbSBhcyBmb3IgZ2V0UmFuZG9tTmRlR2VuZVxyXG4gICAgICogQGNvbXBsZXhpdHkgTygjQ0cpXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZ2V0UmFuZG9tQ29ubmVjdEdlbmUoKTogQ29ubmVjdEdlbmUge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IENvbm5lY3RHZW5lO1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMuY29ubmVjdEdlbmVzKSB7XHJcbiAgICAgICAgICAgIGlmIChNYXRoLnJhbmRvbSgpIDwgMSAvICsrY291bnQpIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuY29ubmVjdEdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBjb21wbGV4aXR5IE8oI0NHKSBidXQgcHJvYmFibHkgY2Fubm90IGJlIGJldHRlclxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGFyZUNvbm5lY3RlZChuMTogTm9kZUdlbmUsIG4yOiBOb2RlR2VuZSkge1xyXG5cclxuICAgICAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5jb25uZWN0R2VuZXMpIHtcclxuICAgICAgICAgICAgLy8gY2hlY2tcclxuICAgICAgICAgICAgbGV0IGVkZ2U6IENvbm5lY3RHZW5lID0gdGhpcy5jb25uZWN0R2VuZXNba2V5XTtcclxuICAgICAgICAgICAgaWYgKGVkZ2UuaW5Ob2RlLmlkID09IG4xLmlkICYmIGVkZ2Uub3V0Tm9kZS5pZCA9PSBuMi5pZFxyXG4gICAgICAgICAgICAgICAgfHwgZWRnZS5pbk5vZGUuaWQgPT0gbjIuaWQgJiYgZWRnZS5vdXROb2RlLmlkID09IG4xLmlkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgY29tcGFyZShmaXJzdDogQmFzZU5lYXRJbmRpdmlkdWFsLCBzZWNvbmQ6IEJhc2VOZWF0SW5kaXZpZHVhbCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHNlY29uZC5maXRuZXNzIC0gZmlyc3QuZml0bmVzcztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZGlzdGFuY2VUbyh0aGF0OiBCYXNlTmVhdEluZGl2aWR1YWwpIHtcclxuICAgICAgICBsZXQgY19lID0gQ29uZmlnLnNwZWNpYXRpb24uZXhjZXNzQ29lZjtcclxuICAgICAgICBsZXQgY19kID0gQ29uZmlnLnNwZWNpYXRpb24uZGlzam9pbnRDb2VmO1xyXG4gICAgICAgIGxldCBjX20gPSBDb25maWcuc3BlY2lhdGlvbi5tYXRjaGluZ0NvZWY7XHJcbiAgICAgICAgbGV0IE4gPSAwOyAvLyAjZ2VuZXMgaW4gbG9uZ2VyXHJcbiAgICAgICAgbGV0IEQgPSAwOyAvLyAjZ2VuZXMgZGlzam9pbnRcclxuICAgICAgICBsZXQgRSA9IDA7IC8vICNnZW5lcyBleGNlc3NcclxuICAgICAgICBsZXQgVyA9IDA7IC8vIGF2ZXJhZ2Ugd2VpZ2h0IGRpZmZlcmVuY2VzIG9mIG1hdGNoaW5nIGdlbmVzIFdcclxuXHJcbiAgICAgICAgbGV0IE0gPSAwOyAvLyAjZ2VuZXMgbWF0Y2hpbmdcclxuXHJcbiAgICAgICAgbGV0IHRoaXNBcnJheSA9IHRoaXMuZ2V0Q29ubmVjdEdlbmVzQXNBcnJheSgpO1xyXG4gICAgICAgIGxldCB0aGF0QXJyYXkgPSB0aGF0LmdldENvbm5lY3RHZW5lc0FzQXJyYXkoKTtcclxuXHJcbiAgICAgICAgbGV0IGkgPSAwO1xyXG4gICAgICAgIGxldCBqID0gMDtcclxuICAgICAgICB3aGlsZShpIDwgdGhpc0FycmF5Lmxlbmd0aCAmJiBqIDwgdGhhdEFycmF5Lmxlbmd0aCkgeyAvLyBlbmQgd2hlbiBhdCB0aGUgZW5kIG9mIG9uZVxyXG4gICAgICAgICAgICBsZXQgdGhpc0Nvbm4gPSB0aGlzQXJyYXlbaV07XHJcbiAgICAgICAgICAgIGxldCB0aGF0Q29ubiA9IHRoYXRBcnJheVtqXTtcclxuXHJcbiAgICAgICAgICAgIGlmKHRoaXNDb25uLmlubm92ID09IHRoYXRDb25uLmlubm92KSB7IC8vIHNhbWVcclxuICAgICAgICAgICAgICAgIFcgKz0gTWF0aC5hYnModGhpc0Nvbm4ud2VpZ2h0IC0gdGhhdENvbm4ud2VpZ2h0KTtcclxuXHJcbiAgICAgICAgICAgICAgICBNKys7XHJcbiAgICAgICAgICAgICAgICBpKys7XHJcbiAgICAgICAgICAgICAgICBqKys7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7IC8vIGRpc2pvaW50XHJcblxyXG4gICAgICAgICAgICAgICAgRCsrO1xyXG4gICAgICAgICAgICAgICAgaWYodGhpc0Nvbm4uaW5ub3YgPCB0aGF0Q29ubi5pbm5vdikge1xyXG4gICAgICAgICAgICAgICAgICAgIGorKztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSAge1xyXG4gICAgICAgICAgICAgICAgICAgIGkrKztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgRSA9IGkgPCB0aGlzQXJyYXkubGVuZ3RoID8gdGhpc0FycmF5Lmxlbmd0aCAtIGkgOiAoaiA8IHRoYXRBcnJheS5sZW5ndGggPyB0aGF0QXJyYXkubGVuZ3RoIC0gajogMCk7IC8vIGV4Y2Vzc1xyXG4gICAgICAgIE4gPSB0aGlzQXJyYXkubGVuZ3RoID4gdGhhdEFycmF5Lmxlbmd0aCA/IHRoaXNBcnJheS5sZW5ndGggOiB0aGF0QXJyYXkubGVuZ3RoO1xyXG5cclxuICAgICAgICAvLyBjb3VudCB0aHJlc2hvbGRzIGFsc28gYXMgaW5wdXQgY29ubmVjdGlvblxyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRoaXMubm9kZUdlbmVzKSB7XHJcbiAgICAgICAgICAgIGxldCB0aGlzTm9kZSA9IHRoaXMubm9kZUdlbmVzW2tleV07XHJcbiAgICAgICAgICAgIGxldCB0aGF0Tm9kZSA9IHRoYXQubm9kZUdlbmVzW2tleV07XHJcblxyXG4gICAgICAgICAgICBpZih0aGF0Tm9kZSkgeyAvLyBzYW1lXHJcbiAgICAgICAgICAgICAgICBXICs9IE1hdGguYWJzKHRoaXNOb2RlLnRocmVzaG9sZCAtIHRoYXROb2RlLnRocmVzaG9sZCk7XHJcbiAgICAgICAgICAgICAgICBNKys7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFcgPSBXIC8gTTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChjX2UqRSkvTiArIChjX2QqRCkvTiArIGNfbSpXO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvL1xyXG4gICAgLy8gR0VUVEVSUyBhbmQgU0VUVEVSU1xyXG4gICAgLy9cclxuXHJcbiAgICBwdWJsaWMgZ2V0IGlkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IGZpdG5lc3MoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZml0bmVzcztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IG5vZGVHZW5lcygpOiBOb2RlR2VuZXMge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nZW5vbWVbMF07XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCBjb25uZWN0R2VuZXMoKTogQ29ubmVjdEdlbmVzIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZ2Vub21lWzFdO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRDb25uZWN0R2VuZXNBc0FycmF5KCk6IEFycmF5PENvbm5lY3RHZW5lPiB7XHJcbiAgICAgICAgbGV0IHJlczogQXJyYXk8Q29ubmVjdEdlbmU+ID0gW107XHJcblxyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLmNvbm5lY3RHZW5lcykge1xyXG4gICAgICAgICAgICByZXMucHVzaCh0aGlzLmNvbm5lY3RHZW5lc1trZXldKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXMuc29ydCgoYSwgYikgPT4ge3JldHVybiBiLmlubm92IC0gYS5pbm5vdn0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IGlucHV0R2VuZXMoKTogQXJyYXk8Tm9kZUdlbmU+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faW5wdXRHZW5lcztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBvdXRwdXRHZW5lcygpOiBBcnJheTxOb2RlR2VuZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9vdXRwdXRHZW5lcztcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQmFzZU5lYXRJbmRpdmlkdWFsOyIsImltcG9ydCBOb2RlR2VuZSBmcm9tIFwiLi9Ob2RlR2VuZVwiO1xyXG5cclxuZXhwb3J0IHR5cGUgQ29ubmVjdEdlbmVzID0geyBba2V5OiBudW1iZXJdOiBDb25uZWN0R2VuZTsgfTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbm5lY3RHZW5lIHtcclxuXHJcbiAgICBwcml2YXRlIF9pZDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2lkQ291bnRlcjogbnVtYmVyID0gMDtcclxuICAgIHByaXZhdGUgX2lubm92OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHN0YXRpYyBfaW5ub3ZDb3VudGVyOiBudW1iZXIgPSAwO1xyXG4gICAgcHVibGljIGVuYWJsZWQ6IGJvb2xlYW47XHJcbiAgICAvLyBwdWJsaWMgaW5Ob2RlOiBOb2RlR2VuZTtcclxuICAgIC8vIHB1YmxpYyBvdXROb2RlOiBOb2RlR2VuZTtcclxuICAgIC8vIHB1YmxpYyB3ZWlnaHQ6IG51bWJlcjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgaW5Ob2RlOiBOb2RlR2VuZSwgcHVibGljIG91dE5vZGU6IE5vZGVHZW5lLCBwdWJsaWMgd2VpZ2h0OiBudW1iZXIsIGVuYWJsZWQ/OiBib29sZWFuLCBpbm5vdj86IG51bWJlciwgKSB7XHJcbiAgICAgICAgdGhpcy5faWQgPSArK0Nvbm5lY3RHZW5lLl9pZENvdW50ZXI7XHJcbiAgICAgICAgdGhpcy5faW5ub3YgPSB0eXBlb2YgaW5ub3YgIT0gXCJ1bmRlZmluZWRcIiA/IGlubm92IDogKytDb25uZWN0R2VuZS5faW5ub3ZDb3VudGVyO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlZCA9IHR5cGVvZiBlbmFibGVkICE9IFwidW5kZWZpbmVkXCIgPyBlbmFibGVkOiB0cnVlO1xyXG4gICAgICAgIHRoaXMub3V0Tm9kZS5hZGRJbkNvbm5lY3Rpb24odGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRvU3RyaW5nKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIFwiQ29ubmVjR2VuZSA9IHtpZDogXCIgKyB0aGlzLmlkICsgXCI7IGlubm92OiBcIiArIHRoaXMuaW5ub3YgKyBcIjsgaW46IFwiICsgdGhpcy5pbk5vZGUuaWQgKyBcIjsgb3V0OiBcIiArIHRoaXMub3V0Tm9kZS5pZCArIFwiOyB3ZWlnaHQ6IFwiICsgdGhpcy53ZWlnaHQudG9GaXhlZCgyKSArIFwiOyBlbmFibGVkOiBcIiArIHRoaXMuZW5hYmxlZCArIFwifVwiO1xyXG4gICAgfVxyXG5cclxuICAgIC8vXHJcbiAgICAvLyBHRVRURVJTIGFuZCBTRVRURVJTXHJcbiAgICAvL1xyXG5cclxuICAgIHB1YmxpYyBnZXQgaWQoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCBpbm5vdigpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pbm5vdjtcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQ29ubmVjdEdlbmUgZnJvbSBcIi4vQ29ubmVjdEdlbmVcIjtcclxuXHJcbmV4cG9ydCB0eXBlIE5vZGVHZW5lcyA9IHsgW2tleTogbnVtYmVyXTogTm9kZUdlbmU7IH07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBOb2RlR2VuZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBfaWQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgc3RhdGljIF9pZENvdW50ZXI6IG51bWJlciA9IDA7XHJcbiAgICBwcml2YXRlIF9pbm5vdjogbnVtYmVyOyAvLyBub3QgbmVjZXNzYXJ5LCBiZXR0ZXIgZm9yIHZpenVhbGl6YXRpb24gYW5kIHRyYWNraW5nIGNvcHkgb2YgdGhpcyBub2RlXHJcbiAgICBwcml2YXRlIHN0YXRpYyBfaW5ub3ZDb3VudGVyOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIHByb3RlY3RlZCBfdHlwZTogTm9kZUdlbmVUeXBlO1xyXG4gICAgcHJvdGVjdGVkIF9zdGF0ZTogTm9kZUdlbmVTdGF0ZTtcclxuXHJcbiAgICBwcm90ZWN0ZWQgX2luQ29ubmVjdGlvbnM6IENvbm5lY3RHZW5lW107XHJcbiAgICBwcm90ZWN0ZWQgX2FjdGl2YXRpb25GdW5jOiBhY3RGdW5jVHlwZTtcclxuICAgIHB1YmxpYyBvdXRwdXQ6IG51bWJlcjtcclxuICAgIHB1YmxpYyBsYXN0T3V0cHV0OiBudW1iZXI7XHJcbiAgICBwdWJsaWMgdGhyZXNob2xkOiBudW1iZXI7IC8vIGJpYXNcclxuXHJcblxyXG4gICAgY29uc3RydWN0b3Iobm9kZUdlbmU6IE5vZGVHZW5lKTtcclxuICAgIGNvbnN0cnVjdG9yKHR5cGU/OiBOb2RlR2VuZVR5cGUsIGFjdGl2YXRpb25GdW5jPzogYWN0RnVuY1R5cGUpO1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5faWQgPSArK05vZGVHZW5lLl9pZENvdW50ZXI7XHJcblxyXG4gICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09IDEgJiYgYXJndW1lbnRzWzBdIGluc3RhbmNlb2YgTm9kZUdlbmUpIHtcclxuICAgICAgICAgICAgdGhpcy5jb3B5Q29uc3RydWN0b3IoYXJndW1lbnRzWzBdKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vcm1hbENvbnN0cnVjdG9yKGFyZ3VtZW50c1swXSwgYXJndW1lbnRzWzFdKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjb3B5Q29uc3RydWN0b3IodGhhdDogTm9kZUdlbmUpIHtcclxuICAgICAgICAvLyBkb2VzIG5vdCBjb3B5IGNvbm5lY3RHZW5lcyAtIGNvcGllZCBmcm9tIGNhbGxlclxyXG4gICAgICAgIHRoaXMuX2lubm92ID0gdGhhdC5faW5ub3Y7XHJcbiAgICAgICAgdGhpcy5faW5Db25uZWN0aW9ucyA9IFtdO1xyXG5cclxuICAgICAgICB0aGlzLnRocmVzaG9sZCA9IHRoYXQudGhyZXNob2xkO1xyXG4gICAgICAgIHRoaXMub3V0cHV0ID0gdGhhdC5vdXRwdXQ7XHJcbiAgICAgICAgdGhpcy5sYXN0T3V0cHV0ID0gdGhhdC5sYXN0T3V0cHV0O1xyXG5cclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IHRoYXQuX3N0YXRlO1xyXG4gICAgICAgIHRoaXMuX3R5cGUgPSB0aGF0Ll90eXBlO1xyXG5cclxuICAgICAgICB0aGlzLl9hY3RpdmF0aW9uRnVuYyA9IHRoYXQuX2FjdGl2YXRpb25GdW5jO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbm9ybWFsQ29uc3RydWN0b3IodHlwZT86IE5vZGVHZW5lVHlwZSwgYWN0aXZhdGlvbkZ1bmM/OiBhY3RGdW5jVHlwZSkge1xyXG4gICAgICAgIHRoaXMuX2lubm92ID0gKytOb2RlR2VuZS5faW5ub3ZDb3VudGVyO1xyXG4gICAgICAgIHRoaXMuX2luQ29ubmVjdGlvbnMgPSBbXTtcclxuXHJcbiAgICAgICAgdGhpcy50aHJlc2hvbGQgPSAwO1xyXG4gICAgICAgIHRoaXMub3V0cHV0ID0gbnVsbDtcclxuICAgICAgICB0aGlzLmxhc3RPdXRwdXQgPSAwO1xyXG5cclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IE5vZGVHZW5lU3RhdGUuTmV3O1xyXG4gICAgICAgIHRoaXMuX3R5cGUgPSB0eXBlb2YgdHlwZSAhPT0gXCJ1bmRlZmluZWRcIiA/IGFyZ3VtZW50c1swXSA6IE5vZGVHZW5lVHlwZS5IaWRkZW47XHJcblxyXG4gICAgICAgIHRoaXMuX2FjdGl2YXRpb25GdW5jID0gdHlwZW9mIGFjdGl2YXRpb25GdW5jICE9PSBcInVuZGVmaW5lZFwiID8gYWN0aXZhdGlvbkZ1bmMgOiBzb2Z0U3RlcDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYWRkSW5Db25uZWN0aW9uKGluQ29ubmVjdGlvbjogQ29ubmVjdEdlbmUpIHtcclxuICAgICAgICB0aGlzLmluQ29ubmVjdGlvbnMucHVzaChpbkNvbm5lY3Rpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBldmFsdWF0ZU91dHB1dChpbnB1dD86IG51bWJlcik6IG51bWJlciB7XHJcbiAgICAgICAgaWYodGhpcy5zdGF0ZSA9PSBOb2RlR2VuZVN0YXRlLk9wZW4pIHsgLy8gcmVjdXJyZW5jZVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sYXN0T3V0cHV0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUgPT0gTm9kZUdlbmVTdGF0ZS5DbG9zZWQpIHsgLy8gYWxyZWFkeSBldmFsdWF0ZWRcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMub3V0cHV0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gb3RoZXIgLSBldmFsdWF0ZSByZWN1cnNpdmVseVxyXG4gICAgICAgIHRoaXMuX3N0YXRlID0gTm9kZUdlbmVTdGF0ZS5PcGVuO1xyXG5cclxuICAgICAgICBsZXQgc3VtOiBudW1iZXIgPSAwO1xyXG5cclxuICAgICAgICAvLyBpbnB1dCBub2RlcyAtIHN1bSBvdmVyIGlucHV0cywgdGhlbiBvdmVyIHJlY3VycmVudCBjb25uZWN0aW9uc1xyXG4gICAgICAgIGlmICh0aGlzLnR5cGUgPT0gTm9kZUdlbmVUeXBlLklucHV0ICYmIGFyZ3VtZW50cy5sZW5ndGggPT0gMSkge1xyXG4gICAgICAgICAgICBzdW0gKz0gaW5wdXQ7XHJcblxyXG4gICAgICAgICAgICBzdW0gKz0gdGhpcy5pbkNvbm5lY3Rpb25zLnJlZHVjZShcclxuICAgICAgICAgICAgICAgIChzdW06IG51bWJlciwgY3VycmVudDogQ29ubmVjdEdlbmUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudC5lbmFibGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzdW0gKyBjdXJyZW50LmluTm9kZS5sYXN0T3V0cHV0ICogY3VycmVudC53ZWlnaHQ7IC8vIE5PIHJlY3Vyc2lvbiAtIHJlY3VycmVudCBjb25uZWN0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1bTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCAwKTtcclxuXHJcbiAgICAgICAgICAgIHN1bSArPSB0aGlzLnRocmVzaG9sZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBoaWRkZW4sIG91dHB1dCBub2RlcyAtIHN1bSBvdmVyIGluIGNvbm5lY3Rpb25zXHJcbiAgICAgICAgICAgIHN1bSArPSB0aGlzLmluQ29ubmVjdGlvbnMucmVkdWNlKFxyXG4gICAgICAgICAgICAgICAgKHN1bTogbnVtYmVyLCBjdXJyZW50OiBDb25uZWN0R2VuZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50LmVuYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1bSArIGN1cnJlbnQuaW5Ob2RlLmV2YWx1YXRlT3V0cHV0KCkgKiBjdXJyZW50LndlaWdodDsgLy8gcmVjdXJzaW9uXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1bTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCAwKTtcclxuICAgICAgICAgICAgc3VtICs9IHRoaXMudGhyZXNob2xkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fc3RhdGUgPSBOb2RlR2VuZVN0YXRlLkNsb3NlZDtcclxuICAgICAgICB0aGlzLm91dHB1dCA9IHRoaXMuYWN0aXZhdGlvbkZ1bmMoc3VtKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5vdXRwdXQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlc2V0T3V0cHV0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubGFzdE91dHB1dCA9IHRoaXMub3V0cHV0O1xyXG4gICAgICAgIHRoaXMub3V0cHV0ID0gbnVsbDtcclxuICAgICAgICB0aGlzLl9zdGF0ZSA9IE5vZGVHZW5lU3RhdGUuTmV3O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b1N0cmluZygpIHtcclxuICAgICAgICByZXR1cm4gXCJOb2RlR2VuZSA9IHtpZDogXCIgKyB0aGlzLmlkICsgXCI7IGlubm92OiBcIiArIHRoaXMuaW5ub3YgKyBcIjsgdHlwZTpcIiArIHRoaXMudHlwZSArIFwiOyB0aHJlc2hvbGQ6IFwiICsgdGhpcy50aHJlc2hvbGQudG9GaXhlZCgyKSArIFwifVwiO1xyXG4gICAgfVxyXG5cclxuICAgIC8vXHJcbiAgICAvLyBHRVRURVJTIGFuZCBTRVRURVJTXHJcbiAgICAvL1xyXG5cclxuICAgIHB1YmxpYyBnZXQgaWQoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCBpbm5vdigpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pbm5vdjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IHR5cGUoKTogTm9kZUdlbmVUeXBlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdHlwZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0IGFjdGl2YXRpb25GdW5jKCk6IGFjdEZ1bmNUeXBlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYWN0aXZhdGlvbkZ1bmM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldCBpbkNvbm5lY3Rpb25zKCk6IENvbm5lY3RHZW5lW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pbkNvbm5lY3Rpb25zO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBnZXQgc3RhdGUoKTogTm9kZUdlbmVTdGF0ZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXRlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZW51bSBOb2RlR2VuZVR5cGUge1xyXG4gICAgSW5wdXQsXHJcbiAgICBIaWRkZW4sXHJcbiAgICBPdXRwdXRcclxufVxyXG5cclxuZXhwb3J0IGVudW0gTm9kZUdlbmVTdGF0ZSB7XHJcbiAgICBOZXcsXHJcbiAgICBPcGVuLFxyXG4gICAgQ2xvc2VkXHJcbn1cclxuXHJcbi8vIFRPRE8gc29tZWhvdyBiZXR0ZXIsIG9yIGNsYXNzIGFuZCBzdGF0aWMgZmllbGR5XHJcbmV4cG9ydCBlbnVtIE5vZGVHZW5lQWN0RnVuYyB7XHJcblxyXG59XHJcbmV4cG9ydCB0eXBlIGFjdEZ1bmNUeXBlID0gKHg6IG51bWJlcikgPT4gbnVtYmVyO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHNvZnRTdGVwKHg6IG51bWJlcik6IG51bWJlciB7XHJcbiAgICByZXR1cm4gMSAvICgxICsgTWF0aC5leHAoLXgpKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGJpbmFyeVN0ZXAoeDogbnVtYmVyKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB4ID49IDAgPyAxIDogMDtcclxufSIsImltcG9ydCB7SW5kaXZpZHVhbHMsIEJhc2VOZWF0SW5kaXZpZHVhbH0gZnJvbSBcIi4vQmFzZU5lYXRJbmRpdmlkdWFsXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTcGVjaWVzIHtcclxuXHJcbiAgICBwcml2YXRlIF9pZDogbnVtYmVyO1xyXG4gICAgcHJvdGVjdGVkIHN0YXRpYyBfaWRDb3VudGVyOiBudW1iZXIgPSAwO1xyXG4gICAgcHVibGljIGluZGl2aWR1YWxzOiBJbmRpdmlkdWFscztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpbmRpdmlkdWFscz86IEluZGl2aWR1YWxzKSB7XHJcbiAgICAgICAgdGhpcy5faWQgPSArK1NwZWNpZXMuX2lkQ291bnRlcjtcclxuICAgICAgICB0aGlzLmluZGl2aWR1YWxzID0gdHlwZW9mIGluZGl2aWR1YWxzICE9IFwidW5kZWZpbmVkXCIgPyBpbmRpdmlkdWFscyA6IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhZGQoaW5kaXZpZHVhbDogQmFzZU5lYXRJbmRpdmlkdWFsKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5pbmRpdmlkdWFscy5wdXNoKGluZGl2aWR1YWwpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICoga2VlcHMgc2luZ2xlIHJhbmRvbSByZXByZXNlbnRhdGl2ZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgY2xlYXIoKTogYm9vbGVhbiB7XHJcblxyXG4gICAgICAgIGxldCByZXByZXNlbnRhdGl2ZTogQmFzZU5lYXRJbmRpdmlkdWFsID0gbnVsbDtcclxuICAgICAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLmluZGl2aWR1YWxzKSB7XHJcbiAgICAgICAgICAgIGlmKCF0aGlzLmluZGl2aWR1YWxzW2tleV0uZWxpbWluYXRlZCkgeyAvLyBza2lwIGVsaW1pbmF0ZWRcclxuICAgICAgICAgICAgICAgIGlmIChNYXRoLnJhbmRvbSgpIDwgMSAvICsrY291bnQpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXByZXNlbnRhdGl2ZSA9IHRoaXMuaW5kaXZpZHVhbHNba2V5XTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYocmVwcmVzZW50YXRpdmUgPT0gbnVsbCkgeyAvLyBubyBpbmRpdmlkdWFscyBpbiBzcGVjaWVzXHJcbiAgICAgICAgICAgIHRoaXMuaW5kaXZpZHVhbHMgPSBbXTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSB7IC8vIGtlZXAgcmVwcmVzZW50YXRpdmVcclxuICAgICAgICAgICAgdGhpcy5pbmRpdmlkdWFscyA9IFtyZXByZXNlbnRhdGl2ZV07XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGF2ZXJhZ2UgZml0bmVzcyBvdmVyIHNwZWNpZXNcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFNoYXJlZEZpdG5lc3MoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbmRpdmlkdWFscy5yZWR1Y2UoKHN1bSwgY3VycikgPT4geyByZXR1cm4gc3VtICsgY3Vyci5maXRuZXNzOyB9LCAwKSAvIHRoaXMuaW5kaXZpZHVhbHMubGVuZ3RoO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBpc0VtcHR5KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmluZGl2aWR1YWxzLmxlbmd0aCA9PSAwO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXQgcmVwcmVzZW50YXRpdmUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5kaXZpZHVhbHNbMF07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlkKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lkO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
