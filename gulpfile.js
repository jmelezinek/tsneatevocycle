var gulp = require("gulp");
var rename = require('gulp-rename');
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var clean = require('gulp-clean');
var tsify = require("tsify");
var fs = require("fs");
var replace = require('gulp-replace');
var concat = require('gulp-concat');

gulp.task('clean', function() {
    return gulp.src('dist/*', {read: false})
        .pipe(clean());
});

gulp.task('default', ['game', 'controls', 'balancing', 'slitherio'], function(){
    return true;
});

//////////////
// CONTROLS //
//////////////
gulp.task('controls', ['tm:controls'], function () {
    return true;
});

gulp.task('tm:controls', function () {
    return gulp.src('src/lib/controls/controls.tm.js')
        .pipe(replace('{{controls.css}}', fs.readFileSync('src/lib/controls/controls.css', 'utf8')))
        .pipe(replace('{{controls.html}}', fs.readFileSync('src/lib/controls/controls.html', 'utf8')))
        .pipe(rename('lib.controls.js'))
        .pipe(gulp.dest("dist/"));
});

///////////////
// SLITHERIO //
///////////////
gulp.task('slitherio', ['tm:slitherio'], function () {
    return true;
});

gulp.task('tm:slitherio', ['bundle:slitherio'], function () {
    gulp.src('src/slitheriobot/Slitherio.tm.js')
        .pipe(replace('{{lib.slitherio.bundle.js}}', fs.readFileSync('dist/slitheriobot/lib.slitherio.bundle.js', 'utf8')))
        .pipe(rename('lib.slitherio.js'))
        .pipe(gulp.dest("dist/slitheriobot/"));

    return gulp.src('dist/slitheriobot/lib.slitherio.bundle.js', {read: false})
        .pipe(clean());
});

gulp.task('bundle:slitherio', function () {

    return browserify({
        basedir: 'src/',
        // debug: true,
        entries: ['slitheriobot/SlitherioLoader.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .pipe(source('lib.slitherio.bundle.js'))
        .pipe(gulp.dest("dist/slitheriobot"));
});

///////////////
// BALANCING //
///////////////
gulp.task('balancing', ['tm:balancing'], function () {
    return true;
});

gulp.task('tm:balancing', ['bundle:balancing'], function () {
    gulp.src('src/balancingbot/Balancing.tm.js')
        .pipe(replace('{{lib.balancing.bundle.js}}', fs.readFileSync('dist/balancingbot/lib.balancing.bundle.js', 'utf8')))
        .pipe(rename('lib.balancing.js'))
        .pipe(gulp.dest("dist/balancingbot/"));

    return gulp.src('dist/balancingbot/lib.balancing.bundle.js', {read: false})
        .pipe(clean());
});


gulp.task('bundle:balancing', function () {

    return browserify({
        basedir: 'src/',
        // debug: true,
        entries: ['balancingbot/BalancingLoader.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .pipe(source('lib.balancing.bundle.js'))
        .pipe(gulp.dest("dist/balancingbot"));
});

//////////
// GAME //
//////////
gulp.task("game", ['game:bundle', 'game:vendors'], function() {
    return true;
});

gulp.task("game:bundle", function () {
    return gulp.src([
        'src/balancinggame/*.html',
        'src/balancinggame/*.css',
        'src/balancinggame/*.js'
    ])
        .pipe(gulp.dest("dist/balancinggame"));
});

gulp.task("game:vendors", function() {
    gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/angular/angular.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/angular-bootstrap-toggle/dist/angular-bootstrap-toggle.min.js',
        './src/balancinggame/vendor/jsgl.min.js'
    ])
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest('dist/balancinggame'));

    gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/angular-bootstrap-toggle/dist/angular-bootstrap-toggle.min.css'
    ])
        .pipe(concat('vendors.css'))
        .pipe(gulp.dest('dist/balancinggame'));

    return true;
});