import Config from "./Config";
import Species from "./model/Species";
import {Individuals, default as Individual} from "./model/Individual";

/**
 * @interface
 * @description EvoCycle is observable from outside. This is its observer interface.
 * Don't forget that evaluating fitness returns Promise - asynchronous.
 */
export interface EvoCycleObserver {
    notifyDoneReduction(evocycle: EvoCycle, population: Individuals): void
    notifyEvaluatedNextFitness(evocycle: EvoCycle, individual: Individual): void
    notifyDoneSpeciation(evocycle: EvoCycle, species: Species[]): void
    notifyDoneSelection(evocycle: EvoCycle, parents: Individuals[]): void
    notifyDoneCrossover(evocycle: EvoCycle, offsprings: Individuals[]): void
    notifyDoneMutation(evocycle: EvoCycle, mutants: Individuals): void
}

/**
 * @class
 * @description Main library class
 */
export default class EvoCycle {

    public config: Config | any;

    protected _generationCounter: number;
    public current: Individual;
    protected _population: Individuals;
    protected _species: Species[];
    protected _parents: Individuals[];
    protected _offsprings: Individuals[];
    protected _mutants: Individuals;

    protected _observers: EvoCycleObserver[];

    constructor(individuals: Individuals) {
        if (Config.log.functions) {
            console.log("EvoCycle.constructor");
        }

        this.config = Config;
        this._generationCounter = 0;
        this._observers = [];

        this._population = individuals;
        this._species = [new Species(individuals)];
        this._parents = [];
        this._offsprings = [];
        this._mutants = [];
    }

    //////////////////// EVOLUTION FUNCTIONS ////////////////////

    public continue(singleGeneration: boolean = false) {
        if (Config.log.functions) {
            console.log("EvoCycle.continue - " + "running: " + Config.cycle.running + "; continue: " + Config.cycle.continue + "; singleGeneration: " + singleGeneration);
        }

        if (!this._population) {
            throw "EvoCycle has to be inicialized first with init population! See EvoCycle.init(individuals: Individuals)";
        }

        if (Config.cycle.running) {
            console.warn("EvoCycle.continue is already running");
            return; // prevent multiple call
        }

        if (!singleGeneration && !Config.cycle.continue) {
            console.warn("EvoCycle.continue will not continue EvoCycle.config.cycle.continue flag is false");
            return; // do not continue
        }

        Config.cycle.running = true;

        this.doReduction().then((population) => {
            if (Config.log.functions) {
                console.log("EvoCycle.continue doReduction-then");
            }

            if (Config.log.generations) {
                console.log("GENERATION");
                EvoCycle.print(this._population);
            }

            this.step(); // continue with cycle
        });
    }

    // step whole new generation
    private step() {

        if (Config.log.cycle) {
            console.log("population - NEXT STEP:");
            console.dir(this._population);
            EvoCycle.print(this._population);
        }

        // classify individuals
        this.doSpeciation();
        if (Config.log.cycle) {
            console.log("species - doSpeciation:");
            console.dir(this._species);
        }

        // select individuals to be parents for breeding
        this.doSelection();
        if (Config.log.cycle) {
            console.log("parents - doSelection:");
            console.dir(this._parents); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this._parents.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }

        // generate new individuals by crossover
        this.doCrossover();
        if (Config.log.cycle) {
            console.log("offsprings - doCrossover:");
            console.dir(this._offsprings); // if this changed later (mutation), new value may be displayed in console
            EvoCycle.print(this._offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }

        // change individuals by mutations
        this.doMutation();
        if (Config.log.cycle) {
            console.log("population and offsprings - doMutation:");
            console.dir(this._population);
            console.dir(this._offsprings);
            EvoCycle.print(this._population);
            EvoCycle.print(this._offsprings.reduce((a, b) => {
                return a.concat([]).concat(b);
            }));
        }

        Config.cycle.running = false;
        this.continue();
    }

    private filterDuplicates(individials: Individuals) {
        return individials.filter(function (item, pos, self) {
            return self.indexOf(item) == pos;
        })
    }

    private doSpeciation() {
        // random species representatives
        let i = this._species.length;
        while (i--) { // WARNING iterating in reverse because of splice (removing items)
            if (this._species[i].clear() == false) { // keeps single representative and returns false if empty
                this._species.splice(i, 1); // remove this species
            }
        }

        // remove species which can be merged
        // this is not in specification but sounds like a good idea
        // i = this.species.length;
        // while(i-- > 1) { // WARNING iterating in reverse because of splice (removing items)
        //     if(this.species[i].representative.distanceTo(this.species[i-1].representative) < Config.speciation.distanceThreshold) {
        //         this.species[i].individuals = []; // empty the species // still in population so it will be classify again
        //         this.species.splice(i, 1); // remove this species
        //     }
        // }

        // classify individuals
        for (let key in this._population) {
            let indiv = this._population[key];

            let placed = false;
            for (let i = 0; i < this._species.length; i++) {
                if (indiv == this._species[i].representative) {
                    placed = true;
                    break;
                }
            }

            if (placed) { // skipping representatives
                continue;
            }

            for (let i = 0; i < this._species.length; i++) {
                // if (indiv != this.species[i].representative)
                // place in existing class
                if ((<any>indiv.constructor).distance(indiv, this._species[i].representative) < Config.speciation.distanceThreshold) {
                    this._species[i].add(indiv);
                    placed = true;
                    break;
                }
            }

            if (placed) { // continue with next
                continue;
            } else { // new class
                this._species.push(new Species([indiv]));
            }
        }

        this._observers.forEach((observer: EvoCycleObserver) => {
            observer.notifyDoneSpeciation(this, this._species);
        });
    }

    /**
     * Simple tournament selection implementation.
     * Randomly chooses (with repetition) k individuals and picks the best one of the tournament. Repeats until whole population is filled.
     */
    private doSelection() {

        // how many offsprings to generate
        let offspringSize = Math.round(this._population.length * Config.crossoverOptions.offspringRatio);

        // to know how many from each species
        let sumSharedFittness = this._species.reduce((sum, curr) => {
            return sum + curr.getSharedFitness();
        }, 0);

        this._parents = [];
        for (let key in this._species) {
            let spec = this._species[key];

            let specSize = spec.individuals.length;

            let specOffspringSize = Math.round(spec.getSharedFitness() / sumSharedFittness * offspringSize);
            specOffspringSize = specOffspringSize > 0 ? specOffspringSize : 1; // atleast one offspring

            let tournamentSize = Math.round(specSize * Config.crossoverOptions.tournamentRatio);
            tournamentSize = tournamentSize < 1 ? 1 : tournamentSize;
            tournamentSize = tournamentSize > specSize ? specSize : tournamentSize;

            // by tournament selection select as many individuals
            // as it is needed for crossover (2 times as much as offspring size)
            let winners: Individuals = this._parents[key] = [];
            while (winners.length < specOffspringSize * 2) {
                // select k random individuals (with repetition)
                let contestants: Individuals = [];
                while (contestants.length < tournamentSize) {
                    let r = Math.floor(Math.random() * specSize);
                    contestants.push(spec.individuals[r]);
                }

                // sort them and select first as winner
                contestants.sort(Individual.compare);
                winners.push(contestants[0]);
            }

        }

        this._observers.forEach((observer: EvoCycleObserver) => {
            observer.notifyDoneSelection(this, this.parents);
        });
    }

    /**
     * Crossover
     */
    private doCrossover() {

        this._offsprings = [];
        for (let key in this._parents) {
            let parents = this._parents[key];

            let offsprings: Individuals = this._offsprings[key] = [];
            for (let i = 0; i < this._parents[key].length; i += 2) {
                offsprings.push(parents[i].breed(parents[i + 1]));
            }
        }

        this._observers.forEach((observer: EvoCycleObserver) => {
            observer.notifyDoneCrossover(this, this.offsprings);
        });

    }

    private doMutation() {

        this._mutants = [];
        for (let key in this._population) {
            let indiv = this._population[key];

            if(Config.mutationOptions.mutateByCloning) {
                let clone: Individual = new (<any>indiv.constructor)(indiv);

                let wasMutated = clone.mutate();

                if(wasMutated) {
                    this._mutants.push(clone);
                } // else will be forgotten
            } else {
                indiv.mutate();
            }
        }

        if (Config.mutationOptions.mutateOffsprings) {
            let allOffsprings = this._offsprings.reduce((a, b) => {
                return a.concat(b);
            });
            for (let key in allOffsprings) {
                let indiv = allOffsprings[key];

                indiv.mutate();
            }
        }

        this._observers.forEach((observer: EvoCycleObserver) => {
            observer.notifyDoneMutation(this, this.mutants);
        });
    };

    private doReduction(): Promise<Individuals> {
        if (Config.log.functions) {
            console.log("EvoCycle.doReduction");
        }

        let allIndividials = [].concat.apply(this._population, this._offsprings).concat(this._mutants);
        // let allIndividials = this.population.concat(this.offsprings).concat(this.mutants); // why not?

        // evaluateFitness if needed
        return this.evaluateAllFitness(allIndividials).then((population) => {
            if (Config.log.functions) {
                console.log("EvoCycle.doReduction evaluateAllFitness-then");
            }

            // sort all individuals and returns the fittest
            allIndividials.sort(Individual.compare); // in-place
            let eliminated = allIndividials.splice(this._population.length); // in-place and return value
            this._population = allIndividials;
            this._generationCounter++;

            for (let i = 0; i < eliminated.length; i++) {
                eliminated[i].eliminated = true;
            }

            this._observers.forEach((observer: EvoCycleObserver) => {
                observer.notifyDoneReduction(this, this.population);
            });

            return this._population; // chained promise
        });

    }

    private evaluateAllFitness(population: Individuals): Promise<Individuals> {
        if (Config.log.functions) {
            console.log("EvoCycle.EvaluateAllFitness");
        }

        // CANNOT use Promise.All, needs to be evaluated sequentially

        var allDonePromise = new Promise<Individuals>((resolve, reject) => {
            this.evaluateNextFitness(population, 0, resolve); // init promise recursion
        });

        return allDonePromise;
    }



    private evaluateNextFitness(population: Individuals, current: number, resolve: (population: Individuals) => void): void {
        if ((Config.log.functions && Config.log.nextFitness)
            || Config.log.nextFitness) {
            console.log("EvoCycle.evaluateNextFitness");
        }

        // end condition
        if (current >= population.length) {
            resolve(population);
        } else {

            this.current = population[current];

            // recursion
            if (population[current].fitness && !Config.general.alwaysEvaluateFitness) {
                // do not evaluate if fitness is known, call next immediately
                this.evaluateNextFitness(population, current + 1, resolve);
            } else {
                // evaluate unknown fitness, then call next
                population[current].evaluateFitness().then((res)=> {
                    this._observers.forEach((observer:EvoCycleObserver) => {
                        observer.notifyEvaluatedNextFitness(this, population[current]);
                    });
                    this.evaluateNextFitness(population, current + 1, resolve);
                });
            }
        }

    }

    public static print(individuals: Individuals) {
        var arr = individuals.map((item: Individual) => {
            return item.toString();
        });

        console.dir(arr);
    }

    //////////////////// GETTERS and SETTERS ////////////////////

    public addObserver(observer: EvoCycleObserver) {
        this._observers.push(observer);
    }

    public get generationCounter(): number {
        return this._generationCounter;
    }

    public get population(): Individuals {
        return this._population;
    }

    public get species(): Species[] {
        return this._species;
    }

    public get parents(): Individuals[] {
        return this._parents;
    }

    public get offsprings(): Individuals[] {
        return this._offsprings;
    }

    public get mutants(): Individuals {
        return this._mutants;
    }
}
