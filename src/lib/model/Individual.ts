/**
 * @interface
 * @desc Intended to be used as interface
 */
abstract class Individual {

    public abstract get fitness(): number

    public constructor(individual: Individual) { throw "Not Implemented!" }
    public abstract evaluateFitness(): Promise<number>
    public abstract breed(partner: Individual): Individual
    public abstract mutate(): boolean

    public static distance(first: Individual, second: Individual): number { throw "Not Implemented!"; }
    public static compare(first: Individual, second: Individual): number { return second.fitness - first.fitness;}

    public eliminated: boolean;
}

export default Individual;
export type Individuals = Array<Individual>;