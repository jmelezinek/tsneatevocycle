import Individual from "./Individual";
import {Individuals} from "./Individual";

export default class Species {

    private _id: number;
    protected static _idCounter: number = 0;
    public individuals: Individuals;

    constructor(individuals?: Individuals) {
        this._id = ++Species._idCounter;
        this.individuals = typeof individuals != "undefined" ? individuals : [];
    }

    public add(individual: Individual): void {
        this.individuals.push(individual);
    }

    /**
     * keeps single random representative
     */
    public clear(): boolean {

        let representative: Individual = null;
        let count = 0;
        for (let key in this.individuals) {
            if(!this.individuals[key].eliminated) { // skip eliminated
                if (Math.random() < 1 / ++count) {
                    representative = this.individuals[key];
                }
            }
        }

        if(representative == null) { // no individuals in species
            this.individuals = [];
            return false;
        } else { // keep representative
            this.individuals = [representative];
            return true;
        }
    }

    /**
     * average fitness over species
     */
    public getSharedFitness(): number {
        return this.individuals.reduce((sum, curr) => { return sum + curr.fitness; }, 0) / this.individuals.length;
    }

    public isEmpty() {
        return this.individuals.length == 0;
    }

    public get representative() {
        return this.individuals[0];
    }

    get id(): number {
        return this._id;
    }
}
