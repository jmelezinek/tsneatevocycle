import NodeGene from "./NodeGene";
import ConnectGene from "./ConnectGene";
import {NodeGenes} from "./NodeGene";
import {NodeGeneType} from "./NodeGene";
import {ConnectGenes} from "./ConnectGene";
import MyMath from "../MyMath";
import Config from "../Config";
import Individual from "./Individual";
import {Individuals} from "./Individual";

export abstract class BaseNeatIndividual implements Individual {

    protected _id: number;
    private static _idCounter: number = 0;

    protected _genome: [NodeGenes, ConnectGenes]; // tuple of two "maps/dictionaries"
    protected _inputGenes: Array<NodeGene>;
    protected _outputGenes: Array<NodeGene>;

    protected _fitness: number;

    public eliminated: boolean;

    constructor(individual: BaseNeatIndividual);
    constructor(inputsLength: number, outputsLength: number);
    constructor() {
        this._id = ++BaseNeatIndividual._idCounter;
        this._genome = [{}, {}];
        this.eliminated = false;

        if(arguments.length == 1 && arguments[0] instanceof BaseNeatIndividual) {
            this.copyConstructor(arguments[0]);
        } else {
            this.normalConstructor.apply(this, arguments);
        }
    }

    /**
     * @description copy constructor
     */
    private copyConstructor(that: BaseNeatIndividual) {
        this._inputGenes = [];
        this._outputGenes = [];

        // copy nodeGenes
        for(let key in that.nodeGenes) {
            this.addNodeGene(new NodeGene(that.nodeGenes[key]));
        }

        // add same connections
        for(let key in that.connectGenes) {
            let thatConnectNode = that.connectGenes[key];
            this.addConnection(
                this.nodeGenes[thatConnectNode.inNode.innov],
                this.nodeGenes[thatConnectNode.outNode.innov],
                thatConnectNode.weight,
                thatConnectNode.enabled,
                thatConnectNode.innov
            );
        }
    }

    /**
     * @description args constructor
     */
    private normalConstructor() {
        let inputsLength: number;
        let outputsLength: number;

        if(typeof arguments[0] === "number" && typeof arguments[1] === "number") {
            inputsLength = arguments[0];
            outputsLength = arguments[1];
            this._inputGenes = [];
            this._outputGenes = [];

            for(let i = 0; i < inputsLength; i++) {
                this.addNodeGene(new NodeGene(NodeGeneType.Input));
            }

            for(let o = 0; o < outputsLength; o++) {
                this.addNodeGene(new NodeGene(NodeGeneType.Output));
            }

        } else if(Array.isArray(arguments[0]) && Array.isArray(arguments[1])) {
            inputsLength = arguments[0].length;
            outputsLength = arguments[1].length;
            this._inputGenes = arguments[0];
            this._outputGenes = arguments[1];
        } else {
            throw "Unexpected parameters";
        }

        for(let i = 0; i < inputsLength; i++) {
            for(let o = 0; o < outputsLength; o++) {
                this.addConnection(this._inputGenes[i], this._outputGenes[o]);
            }
        }
    }

    /**
     * @description merges on top of this individual (does not change matching nodes)
     * @param that
     */
    private merge(that: BaseNeatIndividual) {

        // copy nodeGenes which do not exists yet
        for(let key in that.nodeGenes) {
            let thisNode: NodeGene = this.nodeGenes[key];
            let thatNode: NodeGene = that.nodeGenes[key];

            if(!thisNode) {
                this.addNodeGene(new NodeGene(thatNode));
            }
        }

        // add connections which do not exists yet
        for(let key in that.connectGenes) {
            let thisConn = this.connectGenes[key];
            let thatConn = that.connectGenes[key];

            if(!thisConn) {
                this.addConnection(
                    this.nodeGenes[thatConn.inNode.innov],
                    this.nodeGenes[thatConn.outNode.innov],
                    thatConn.weight,
                    thatConn.enabled,
                    thatConn.innov
                );
            }
        }
    }

    /**
     * @desc DO NOT call on BaseNeatIndividual but on non-abstract subclass!
     * @returns {BaseNeatIndividual[]}
     */
    public static createInitPopulation(populationSize: number, firstIndividualConstructorArgs?: Array<any>) : BaseNeatIndividual[] {

        if(this === BaseNeatIndividual) {
            throw "Called on abstract BaseNeatIndividual class. Implement non-abstract subclass and call on that instead.";
        }

        let population: BaseNeatIndividual[] = [];
        let first: BaseNeatIndividual;
        if(typeof firstIndividualConstructorArgs !== "undefined") {
            first = new (<any>this)(...firstIndividualConstructorArgs);
        } else {
            first = new (<any>this)();
        }

        population.push(first);
        for(let i = 1; i < populationSize; i++) {
            // randomize weights and thresholds
            let next = new (<any>this)(first);

            for(let key in next.connectGenes) {
                let connection = next.connectGenes[key];
                connection.weight = 2*Math.random() - 1;
            }

            for(let key in next.nodeGenes) {
                let node = next.nodeGenes[key];
                node.threshold = 2*Math.random() - 1;
            }

            population.push(next); // copies of first so same innov numbers
        }

        return population ;
    }

    private addNodeGene(nodeGene: NodeGene): void {
        this.nodeGenes[nodeGene.innov] = nodeGene;
        if(nodeGene.type == NodeGeneType.Input) {
            this._inputGenes.push(nodeGene);
        }
        if(nodeGene.type == NodeGeneType.Output) {
            this._outputGenes.push(nodeGene);
        }
    }

    private addConnectGene(connectGene: ConnectGene): void {
        this.connectGenes[connectGene.innov] = connectGene;
    }

    private addConnection(inNode:NodeGene, outNode:NodeGene, weight? : number, enabled?: boolean, innov?: number) {
        this.addConnectGene(new ConnectGene(inNode, outNode, weight, enabled, innov));
    }

    public abstract evaluateFitness(): Promise<number>;

    public breed(partner: BaseNeatIndividual): BaseNeatIndividual {

        let better: BaseNeatIndividual;
        let worse: BaseNeatIndividual;

        if(this.fitness > partner.fitness) {
            better = this;
            worse = partner;
        } else {
            better = partner;
            worse = this;
        }

        // make copy of better
        // Stanley: non-matching genes are inherited from the more fit parent.
        let offspring: BaseNeatIndividual = new (<any>better.constructor)(better);

        // inherit randomly weight for shared genes
        // Stanley: matching genes are inherrited randomly
        for(let key in offspring.connectGenes) {
            let connOffspring: ConnectGene = offspring.connectGenes[key];
            let connWorse: ConnectGene = worse.connectGenes[key];

            if(connWorse) { // shared gene crossover
                let which = 0.5 > Math.random();

                connOffspring.weight = which ? connOffspring.weight : connWorse.weight;
                connOffspring.enabled = which ? connOffspring.enabled : connWorse.enabled;

                // Stanley: There was a 75% chance that an inherited gene was disabled if it was disabled in either parent
                if(!connOffspring.enabled || !connWorse.enabled) {
                    connOffspring.enabled = (0.75 > Math.random() ? false : true);
                } else {
                    connOffspring.enabled = true;
                }

            }
        }

        /**
         * this part is not totally clear for me - from http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf:
         * Stanley: "In this case, equal fitnesses are assumed, so the disjoint and excess genes are also inherited randomly."
         * How can be disjoint and excess genes inherrited randomly when they are only in one of the parent?
         */
        // if same fitnesses, also inherit (copy) genes from other parent
        if (this.fitness == partner.fitness) {
            offspring.merge(worse);
        }

        // inherit randomly thresholds - imagine as another in connection
        for(let key in offspring.nodeGenes) {
            let nodeOffspring: NodeGene = offspring.nodeGenes[key];
            let nodeWorse: NodeGene = worse.nodeGenes[key];

            if(nodeWorse) {
                nodeOffspring.threshold = 0.5 > Math.random() ? nodeOffspring.threshold : nodeWorse.threshold;
            }
        }

        return offspring;
    }

    public mutate(): boolean {

        let isMutated: boolean = false;

        let topologyMutOptOf = Config.mutationOptions.individualTopology;
        let weightsMutOptOf = Config.mutationOptions.individualWeights;

        if(topologyMutOptOf.chance > Math.random()) {
            let xorChance: number; // 2 for false, <0;1) for addNode, <1;2) for addConnection
            if (topologyMutOptOf.addNodeXORaddConnection) {
                xorChance = Math.random() * 2; // Math.random() never equals 1 => xorChance never equals 2 which is reserved for xorChance false
            } else {
                xorChance = 2;
            }

            if (xorChance === 2 || (xorChance >= 0 && xorChance < 1)) {
                if (topologyMutOptOf.addNode.chance > Math.random()) {
                    this.mutateAddNode();
                    isMutated = true;
                }
            }

            if (xorChance === 2 || (xorChance >= 1 && xorChance < 2)) {
                if (topologyMutOptOf.addConnection.chance > Math.random()) {
                    this.mutateAddConnection();
                    isMutated = true;
                }
            }
        }

        if(weightsMutOptOf.chance > Math.random()) {

            if(weightsMutOptOf.weights.chance > Math.random()) {
                this.mutateWeights();
                isMutated = true;
            }

            if(weightsMutOptOf.thresholds.chance > Math.random()) {
                this.mutateThresholds();
                isMutated = true;
            }
        }

        this._fitness = null;

        return isMutated;
    }

    private mutateAddConnection(): void {

        let n1: NodeGene = this.getRandomNodeGene();
        let n2: NodeGene;

        let maxAttempts = 100;
        do {
            n2 = this.getRandomNodeGene();
            maxAttempts--;
        } while(n1.id == n2.id && maxAttempts > 0);

        if(maxAttempts == 0) {
            console.warn("Cannot find nodes to mutateAddConnection.");
            console.dir(this);
            return;
        }

        if(!this.areConnected(n1, n2)) {
            return this.addConnection(n1,n2);
        } else {
            return this.mutateAddConnection();
        }
    }

    private mutateAddNode(): void {
        let edge: ConnectGene;

        let maxAttempts = 100;
        do {
            edge = this.getRandomConnectGene();
            maxAttempts--;
        } while(edge.enabled == false && maxAttempts > 0);

        if(maxAttempts == 0) {
            console.warn("Cannot find edge to mutateAddNode.");
            console.dir(this);
            return;
        }

        edge.enabled = false;
        let inNode = edge.inNode;
        let outNode = edge.outNode;
        let innerNode = new NodeGene();

        this.addNodeGene(innerNode);
        this.addConnection(inNode, innerNode, 1);
        this.addConnection(innerNode, outNode, edge.weight);
    }

    private mutateWeights(): void {
        for(let key in this.connectGenes) {
            if(Config.mutationOptions.individualWeights.weights.mutateSingle.chance > Math.random()) {
                let connection: ConnectGene = this.connectGenes[key];
                connection.weight += MyMath.randomNormal(Config.mutationOptions.individualWeights.weights.mutateSingle.stdev)();
            }
        }
    }

    private mutateThresholds(): void {
        for(let key in this.nodeGenes) {
            if(Config.mutationOptions.individualWeights.thresholds.mutateSingle.chance > Math.random()) {
                let node: NodeGene = this.nodeGenes[key];
                node.threshold += MyMath.randomNormal(Config.mutationOptions.individualWeights.thresholds.mutateSingle.stdev)();
            }
        }
    }

    public evaluateNetwork(inputs: number[]): number[] {
        let outputs: number[] = [];

        // evaluate input nodes (recursion end condition)
        for(let i = 0; i < this.inputGenes.length; i++) {
            this.inputGenes[i].evaluateOutput(inputs[i]);
        }

        // evaluate network from output nodes by recursion
        for(let key in this.outputGenes) {
            outputs.push(
                this.outputGenes[key].evaluateOutput());
        }

        // reset network evaluation for next evaluation
        for(let key in this.nodeGenes) {
            this.nodeGenes[key].resetOutput();
        }

        return outputs;
    }

    public toString(): string {

        let nodeGenesString: string = "";
        for(let key in this.nodeGenes) {
            nodeGenesString += "\t" + this.nodeGenes[key].toString() + ";\n";
        }

        let connectGenesString: string = "";
        for(let key in this.connectGenes) {
                connectGenesString += "\t" + this.connectGenes[key].toString() + ";\n";
        }

        return "BaseNeatIndividual = {\n" +
                "\tid: " + this.id + ";\n" +
                "\tfitness: " + this.fitness + ";\n" +
                nodeGenesString +
                connectGenesString
            + "}";
    }

    private static nodeGeneToText(nodeGene: NodeGene) {
        return 'innov: ' + nodeGene.innov + '\n' +
                'threshold: ' + nodeGene.threshold;
    }

    private static connectGeneToText(connectGeneGene: ConnectGene) {
        return 'innov: ' + connectGeneGene.innov + '\n' +
            'weight: ' + connectGeneGene.weight.toFixed(2) + '\n' +
            (connectGeneGene.enabled ? "" : "DISABLED");
    }

    /**
     * @deprecated
     */
    public toGOJS(): Object {
        let json : any = {};
        json["nodeKeyProperty"] = "id";

        json["nodeDataArray"] = [];
        for(let key in this.nodeGenes) {
            let nodeGene =this.nodeGenes[key];
            json["nodeDataArray"].push({
                id: nodeGene.id,
                text: BaseNeatIndividual.nodeGeneToText(nodeGene)
            });
        }

        json["linkDataArray"] = [];
        for(let key in this.connectGenes) {
            let connectGene =this.connectGenes[key];
            json["linkDataArray"].push({
                from: connectGene.inNode.id,
                to: connectGene.outNode.id,
                text: BaseNeatIndividual.connectGeneToText(connectGene)
            });
        }

        // json["nodeDataArray"] = [{ "id": 0, "loc": "120 120", "text": "XXX" }];
        // json["linkDataArray"] = [{ "from": 0, "to": 0, "text": "up or timer", "curviness": -20 }];

        return json;
    }

    //
    // GET functions
    //

    /**
     * @complexity O(#NG); O(1) can be achieved if NodeGenes stored in array but harder to implement breed and other functions; #NG is relatively low
     */
    private getRandomNodeGene(): NodeGene {
        let result: NodeGene;
        let count = 0;
        for (let key in this.nodeGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.nodeGenes[key];
            }
        }
        return result;
    }

    /**
     * @complexity O(#CG); O(1) can be achieved if NodeGenes stored in array but harder to implement breed and other functions; #CG is relatively low
     */
    private getRandomConnectGene(): ConnectGene {
        let result: ConnectGene;
        let count = 0;
        for (let key in this.connectGenes) {
            if (Math.random() < 1 / ++count) {
                result = this.connectGenes[key];
            }
        }
        return result;
    }

    /**
     * @complexity O(#CG) but probably cannot be better
     */
    private areConnected(n1: NodeGene, n2: NodeGene) {

        for (let key in this.connectGenes) {
            // check
            let edge: ConnectGene = this.connectGenes[key];
            if (edge.inNode.id == n1.id && edge.outNode.id == n2.id
                || edge.inNode.id == n2.id && edge.outNode.id == n1.id) {
                return true;
            }
        }

        return false;
    }

    public static compare(first: BaseNeatIndividual, second: BaseNeatIndividual): number {
        return second.fitness - first.fitness;
    }

    public static distance(first: BaseNeatIndividual, second: BaseNeatIndividual): number {
        return first.distanceTo(second);
    }

    public distanceTo(that: BaseNeatIndividual): number {
        let c_e = Config.speciation.excessCoef;
        let c_d = Config.speciation.disjointCoef;
        let c_m = Config.speciation.matchingCoef;
        let N = 0; // #genes in longer
        let D = 0; // #genes disjoint
        let E = 0; // #genes excess
        let W = 0; // average weight differences of matching genes W

        let M = 0; // #genes matching

        let thisArray = this.getConnectGenesAsArray();
        let thatArray = that.getConnectGenesAsArray();

        let i = 0;
        let j = 0;
        while(i < thisArray.length && j < thatArray.length) { // end when at the end of one
            let thisConn = thisArray[i];
            let thatConn = thatArray[j];

            if(thisConn.innov == thatConn.innov) { // same
                W += Math.abs(thisConn.weight - thatConn.weight);

                M++;
                i++;
                j++;
            } else { // disjoint

                D++;
                if(thisConn.innov < thatConn.innov) {
                    j++;
                } else  {
                    i++;
                }
            }

        }
        E = i < thisArray.length ? thisArray.length - i : (j < thatArray.length ? thatArray.length - j: 0); // excess
        N = thisArray.length > thatArray.length ? thisArray.length : thatArray.length;

        // count thresholds also as input connection
        for(let key in this.nodeGenes) {
            let thisNode = this.nodeGenes[key];
            let thatNode = that.nodeGenes[key];

            if(thatNode) { // same
                W += Math.abs(thisNode.threshold - thatNode.threshold);
                M++;
            }
        }

        W = W / M;

        return (c_e*E)/N + (c_d*D)/N + c_m*W;
    }


    //
    // GETTERS and SETTERS
    //

    public get id() {
        return this._id;
    }

    public get fitness(): number {
        return this._fitness;
    }

    public get nodeGenes(): NodeGenes {
        return this._genome[0];
    }

    public get connectGenes(): ConnectGenes {
        return this._genome[1];
    }

    public getConnectGenesAsArray(): Array<ConnectGene> {
        let res: Array<ConnectGene> = [];

        for (let key in this.connectGenes) {
            res.push(this.connectGenes[key]);
        }

        return res.sort((a, b) => {return b.innov - a.innov});
    }

    private get inputGenes(): Array<NodeGene> {
        return this._inputGenes;
    }

    private get outputGenes(): Array<NodeGene> {
        return this._outputGenes;
    }
}

export default BaseNeatIndividual;