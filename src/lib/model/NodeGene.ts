import ConnectGene from "./ConnectGene";

export type NodeGenes = { [key: number]: NodeGene; };

export default class NodeGene {

    private _id: number;
    private static _idCounter: number = 0;
    private _innov: number; // not necessary, better for vizualization and tracking copy of this node
    private static _innovCounter: number = 0;

    protected _type: NodeGeneType;
    protected _state: NodeGeneState;

    protected _inConnections: ConnectGene[];
    protected _activationFunc: actFuncType;
    public output: number;
    public lastOutput: number;
    public threshold: number; // bias


    constructor(nodeGene: NodeGene);
    constructor(type?: NodeGeneType, activationFunc?: actFuncType);
    constructor() {
        this._id = ++NodeGene._idCounter;

        if (arguments.length == 1 && arguments[0] instanceof NodeGene) {
            this.copyConstructor(arguments[0]);
        } else {
            this.normalConstructor(arguments[0], arguments[1]);
        }
    }

    private copyConstructor(that: NodeGene) {
        // does not copy connectGenes - copied from caller
        this._innov = that._innov;
        this._inConnections = [];

        this.threshold = that.threshold;
        this.output = that.output;
        this.lastOutput = that.lastOutput;

        this._state = that._state;
        this._type = that._type;

        this._activationFunc = that._activationFunc;
    }

    private normalConstructor(type?: NodeGeneType, activationFunc?: actFuncType) {
        this._innov = ++NodeGene._innovCounter;
        this._inConnections = [];

        this.threshold = 2*Math.random() - 1;
        this.output = null;
        this.lastOutput = 0;

        this._state = NodeGeneState.New;
        this._type = typeof type !== "undefined" ? arguments[0] : NodeGeneType.Hidden;

        this._activationFunc = typeof activationFunc !== "undefined" ? activationFunc : softStep;
    }

    public addInConnection(inConnection: ConnectGene) {
        this.inConnections.push(inConnection);
    }

    public evaluateOutput(input?: number): number {
        if(this.state == NodeGeneState.Open) { // recurrence
            return this.lastOutput;
        }

        if (this.state == NodeGeneState.Closed) { // already evaluated
            return this.output;
        }

        // other - evaluate recursively
        this._state = NodeGeneState.Open;

        let sum: number = 0;

        // input nodes - sum over inputs, then over recurrent connections
        if (this.type == NodeGeneType.Input && arguments.length == 1) {
            sum += input;

            sum += this.inConnections.reduce(
                (sum: number, current: ConnectGene) => {
                    if (current.enabled) {
                        return sum + current.inNode.lastOutput * current.weight; // NO recursion - recurrent connection
                    } else {
                        return sum;
                    }
                }, 0);

            sum += this.threshold;
        } else {
            // hidden, output nodes - sum over in connections
            sum += this.inConnections.reduce(
                (sum: number, current: ConnectGene) => {
                    if (current.enabled) {
                        return sum + current.inNode.evaluateOutput() * current.weight; // recursion
                    } else {
                        return sum;
                    }
                }, 0);
            sum += this.threshold;
        }

        this._state = NodeGeneState.Closed;
        this.output = this.activationFunc(sum);
        return this.output;
    }

    public resetOutput(): void {
        this.lastOutput = this.output;
        this.output = null;
        this._state = NodeGeneState.New;
    }

    public toString() {
        return "NodeGene = {id: " + this.id + "; innov: " + this.innov + "; type:" + this.type + "; threshold: " + this.threshold.toFixed(2) + "}";
    }

    //
    // GETTERS and SETTERS
    //

    public get id(): number {
        return this._id;
    }

    public get innov(): number {
        return this._innov;
    }

    public get type(): NodeGeneType {
        return this._type;
    }

    public get activationFunc(): actFuncType {
        return this._activationFunc;
    }

    protected get inConnections(): ConnectGene[] {
        return this._inConnections;
    }

    protected get state(): NodeGeneState {
        return this._state;
    }
}

export enum NodeGeneType {
    Input,
    Hidden,
    Output
}

export enum NodeGeneState {
    New,
    Open,
    Closed
}

export type actFuncType = (x: number) => number;

export function softStep(x: number): number {
    return 1 / (1 + Math.exp(-x));
}

export function binaryStep(x: number): number {
    return x >= 0 ? 1 : 0;
}