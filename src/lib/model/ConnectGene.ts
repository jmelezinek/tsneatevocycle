import NodeGene from "./NodeGene";

export type ConnectGenes = { [key: number]: ConnectGene; };

export default class ConnectGene {

    private _id: number;
    private static _idCounter: number = 0;
    private _innov: number;
    private static _innovCounter: number = 0;
    public enabled: boolean;
    // public inNode: NodeGene;
    // public outNode: NodeGene;
    // public weight: number;

    constructor(public inNode: NodeGene, public outNode: NodeGene, public weight?: number, enabled?: boolean, innov?: number, ) {
        this._id = ++ConnectGene._idCounter;
        this._innov = typeof innov != "undefined" ? innov : ++ConnectGene._innovCounter;
        this.enabled = typeof enabled != "undefined" ? enabled: true;
        this.weight = typeof weight != "undefined" ? weight : 2*Math.random() - 1;
        this.outNode.addInConnection(this);
    }

    public toString(): string {
        return "ConnecGene = {id: " + this.id + "; innov: " + this.innov + "; in: " + this.inNode.id + "; out: " + this.outNode.id + "; weight: " + this.weight.toFixed(2) + "; enabled: " + this.enabled + "}";
    }

    //
    // GETTERS and SETTERS
    //

    public get id(): number {
        return this._id;
    }

    public get innov(): number {
        return this._innov;
    }

}