export default class MyMath {

    private constructor() {}

    /**
     * @description returns a gaussian random function with the given mean and stdev.
     * @author http://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve#answer-35599181
     * @viz https://en.wikipedia.org/wiki/Marsaglia_polar_method
     * @param mean (mu)
     * @param stdev (sigma) standard deviation
     * @returns {()=>number}
     */
    public static gaussian(mean: number, stdev: number): () => number {
        var y2: number;
        var use_last: boolean = false;
        return function () {
            var y1: number;
            if (use_last) {
                y1 = y2;
                use_last = false;
            } else {
                var x1: number, x2: number, w: number;
                do {
                    x1 = 2.0 * Math.random() - 1.0;
                    x2 = 2.0 * Math.random() - 1.0;
                    w = x1 * x1 + x2 * x2;
                } while (w >= 1.0);
                w = Math.sqrt((-2.0 * Math.log(w)) / w);
                y1 = x1 * w;
                y2 = x2 * w;
                use_last = true;
            }

            var retval = mean + stdev * y1;
            // if(retval > 0)
            //     return retval;
            // return -retval;

            return retval;
        }
    }

    // public static randomNormal = MyMath.gaussian(0, 1);

    public static randomNormal(stdev:number) {
        return MyMath.gaussian(0, stdev);
    }
}
