// ==UserScript==
// @name         lib.controls.js
// @namespace    http://neat.melezinek.cz/
// @version      0.1
// @description  css, html and js to start lib.controls
// @author       Jakub Melezinek (melezjak)
// @match        http://slither.io/
// @matchDisabl  http://neat.melezinek.cz/dist/balancinggame/ // can be inserted winh on page button
// @grant        none
// @run-at       document-end
// @require      http://code.jquery.com/jquery-3.1.1.min.js
// @require      https://code.jquery.com/ui/1.12.1/jquery-ui.min.js
// @require      https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/angular-google-chart/0.1.0/ng-google-chart.min.js
// ==/UserScript==

// after DOM loaded
$(function(){

    $("head").append('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">');
    $("head").append('<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css"/>');

    $("head").append(`<style>{{controls.css}}</style>`);
    $("body").append(`{{controls.html}}`);

    var $controllerDivWrapper = $('#tsneatevocycle-controller-wrapper');
    var $controllerDiv = $('#tsneatevocycle-controller');

    $controllerDiv
        .position({
            of: $(window)
        })
        .draggable({
            handle: "h1"
        });

});