import {Individuals, default as Individual} from "../model/Individual";

/// <reference path="../../../node_modules/gojs/release/go.d.ts" />
declare var go: any;

/// <reference path="../../node_modules/@types/angular/index.d.ts" />
declare var angular: any; // not really importing, just need .d.ts here // included by EvoCycle.controls.tampermonkey.js

import EvoCycle from "../EvoCycle";
import Species from "../model/Species";
import {EvoCycleObserver} from "../EvoCycle";

export class EvoCycleControls implements EvoCycleObserver, angular.IController {

    static $inject = ["$scope", "$http", "$interval", "$timeout"];

    public model: EvoCycle;
    protected fitnessChartObject: any;
    protected speciesChartObject: any;

    public state = {
        current: {
            viewLong: false
        },
        population: {
            viewLong: [false] // viewLong[0] toggle all
        },
        offsprings: {
            viewLong: [false] // viewLong[0] toggle all
        },
        mutants: {
            viewLong: [false] // viewLong[0] toggle all
        }
    };

    constructor(private $scope: angular.IScope | any, private $http: angular.IHttpService, private $interval: angular.IIntervalService, private $timeout: angular.ITimeoutService) {

        this.fitnessChartObject = {};
        this.fitnessChartObject.type = "LineChart";
        this.fitnessChartObject.options = {
            title: "Fitness",
            legend: { position: 'bottom' }
        };
        this.fitnessChartObject.data = {
            cols: [{
                id: "generation",
                label: "Generation",
                type: "string"
            }, {
                id: "min",
                label: "min",
                type: "number"
            }, {
                id: "max",
                label: "max",
                type: "number"
            }, {
                id: "avg",
                label: "avg",
                type: "number"
            }],
            rows: [] // filled by EvoCycleControls::notifyDoneReduction
        };

        this.speciesChartObject = {};
        this.speciesChartObject.type = "AreaChart";
        this.speciesChartObject.options = {
            title: "Species",
            legend: { position: 'none' },
            isStacked: "true",
            fill: 20,
            displayExactValues: true
        };

        this.speciesChartObject.data = {
            cols: [
                {
                    id: "generation",
                    label: "Generation",
                    type: "string"
                }, {
                    id: "specie_0",
                    label: "Species #0",
                    type: "number"
                }
            ],
            rows: [] // set by setModel
        }; // rest filled by EvoCycleControls::notifyDoneSpeciation

    }

    /**
     * creates and starts ng app for EvoCycleControls
     */
    public static init(model: EvoCycle) {
        let tsneatEvoCycleControlsDiv = document.getElementById('tsneatevocycle-controller');
        let tsneatEvoCycleControlsApp =
            angular.module('tsneatevocycleControlsApp', ['googlechart'])
                .controller('tsNeatEvoCycleCtrl', EvoCycleControls);
                //.directive('individualGraph', IndividualGraph.Factory())

        // load angular app by hand not by ng-app attribute in case multiple angular app is running
        // WARNING there will be still problem if ng-app attr is on html or body because nested ng-app are not allowed // for possible solution see http://stackoverflow.com/questions/22548610/can-i-use-one-ng-app-inside-another-one-in-angularjs#answer-28030105
        angular.bootstrap(tsneatEvoCycleControlsDiv, ['tsneatevocycleControlsApp']);

        angular.element(tsneatEvoCycleControlsDiv).controller().setModel(model);
    }

    protected setModel(evoCycle: EvoCycle) {
        this.model = evoCycle;
        this.speciesChartObject.data.rows = [{
            c: [{
                v: '#0'
            },{
                v: this.model.population.length
            }]
        }];
        this.model.addObserver(this);
        this.refresh();
    }

    public saveModel() {
        // TODO serialize or save to local storage or ??
        // JSON.stringify(this.model.population); // circullar error
        throw "Not Implemented!";
    }

    public loadModel() {
        // TODO deserialize
        throw "Not Implemented!";
    }

    public refresh() {
        // this.$scope.$apply();
        this.$scope.$evalAsync();
    }

    // start or continue with next generation
    public continue(singleGeneration: boolean = false) {
        this.model.config.cycle.continue = !singleGeneration;
        this.model.continue(singleGeneration); // prevents multiple call
    }

    public notifyDoneReduction(evocycle: EvoCycle, population: Individuals) {
        this.log("population:", population);
        let min: number = Number.MAX_VALUE;
        let max: number = Number.MIN_VALUE;
        let avg = this.model.population.reduce((sum, curr) => {
            min = curr.fitness < min ? curr.fitness : min;
            max = curr.fitness > max ? curr.fitness : max;
            return sum + curr.fitness;
        }, 0) / this.model.population.length;

        this.updateFitnessGraph(min, max, avg);
        // this.refresh();
    }

    protected updateFitnessGraph(min: number, max: number, avg: number) {
        this.fitnessChartObject.data.rows.push({
            c: [{
                v: "#" + this.model.generationCounter
            }, {
                v: min
            }, {
                v: max
            }, {
                v: avg
            }]
        });
    }

    public notifyDoneSpeciation(evocycle: EvoCycle, species: Species[]) {
        this.log("species:", species);
        let maxId: number = -1;
        let speciesQuantity: number[] = [];

        species.forEach((specie) => {
            // [specId, specSize]
            maxId = specie.id > maxId ? specie.id : maxId;
            speciesQuantity[specie.id] = specie.individuals.reduce((sum: number, curr: Individual) => {
                return sum + 1;
            }, 0);
        });
        speciesQuantity.length = maxId +1;
        speciesQuantity.forEach((item, index, array) => {
            array[index] = typeof item == "undefined" ? 0 : item;
        });
        speciesQuantity[0] = null;

        this.updateSpeciesGraph(speciesQuantity);
    }

    private updateSpeciesGraph(speciesQuantity: number[]) {
        let cols = this.speciesChartObject.data.cols;
        let rows = this.speciesChartObject.data.rows;

        // rewrite column (titles) to contain all species
        cols = [
            {
                id: "generation",
                label: "Generation",
                type: "string"
            }
        ];
        for(let i = 1; i < speciesQuantity.length; i++) {
            cols.push({
                id: "specie_" + i,
                label: "Species #" + i,
                type: "number"
            });
        }
        this.speciesChartObject.data.cols = cols;

        // update rows (values) with last values
        rows.push({
            c: [{
                v: "#" + this.model.generationCounter
            }]
        });
        for (let i = 1; i < speciesQuantity.length; i++) {

            // for new also update one before current
            if (typeof rows[rows.length - 2] != "undefined"
                && typeof rows[rows.length - 2].c[i] == "undefined") {
                rows[rows.length - 2].c[i] =
                    {
                        v: 0
                    }
            }

            // current generation
            rows[rows.length - 1].c.push(
                {
                    v: speciesQuantity[i]
                }
            );

        }
    }

    public notifyEvaluatedNextFitness(evocycle: EvoCycle, individual: Individual) {
        this.log("individual:", individual);
        this.refresh();
    }

    public notifyDoneSelection(evocycle: EvoCycle, parents: Individuals[]): void {
        this.log("parents:", parents);
    }

    public notifyDoneCrossover(evocycle: EvoCycle, offsprings: Individuals[]): void {
        this.log("offsprings:", offsprings);
    }

    public notifyDoneMutation(evocycle: EvoCycle, mutants: Individuals): void {
        this.log("mutants:", mutants);
    }

    protected log(msg?: any, obj?: any) {
        let doLogs = false;

        if(doLogs) {
            console.log(msg)
            console.dir(obj);
        }
    }

}

/**
 * @deprecated
 */
export class IndividualGraph implements angular.IDirective {

    public restrict = 'E';
    public template = '<div></div>';
    public replace = true;
    public scope = {individual: "<individual"};
    public link: angular.IDirectiveLinkFn = (scope: ng.IScope | any,
                                             element: ng.IAugmentedJQuery,
                                             attrs: ng.IAttributes | any) => {

        let makeGO = go.GraphObject.make;

        let myDiagram = makeGO(go.Diagram, element[0],
            {
                isReadOnly: true, // disable all edits
                isEnabled: false, // no interaction
                "initialContentAlignment": go.Spot.Center, // center Diagram contents
                "toolManager.mouseWheelBehavior": go.ToolManager.WheelNone,
                "animationManager.isEnabled": false,
                "layout": new go.ForceDirectedLayout(),
                "InitialLayoutCompleted": function (e: any) {
                    // dynamic height for diagram
                    var dia = e.diagram;
                    dia.div.style.height = (dia.documentBounds.height + 8) + "px";
                }
            });

        myDiagram.model = go.Model.fromJson(scope.individual.toGOJS());

        // define the Node template
        myDiagram.nodeTemplate =
            makeGO(go.Node, "Auto",
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                // define the node's outer shape, which will surround the TextBlock
                makeGO(go.Shape, "RoundedRectangle",
                    {
                        parameter1: 20,  // the corner has a large radius
                        fill: makeGO(go.Brush, "Linear", {0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)"}),
                        stroke: null,
                        portId: "",  // this Shape is the Node's port, not the whole Node
                        fromLinkable: true, fromLinkableSelfNode: true, fromLinkableDuplicates: true,
                        toLinkable: true, toLinkableSelfNode: true, toLinkableDuplicates: true,
                        cursor: "pointer"
                    }),
                makeGO(go.TextBlock,
                    {
                        font: "9pt helvetica, arial, sans-serif",
                        editable: true  // editing the text automatically updates the model data
                    },
                    new go.Binding("text").makeTwoWay())
            );

        // replace the default Link template in the linkTemplateMap
        myDiagram.linkTemplate =
            makeGO(go.Link,  // the whole link panel
                {
                    curve: go.Link.Bezier, adjusting: go.Link.Stretch,
                    reshapable: true, relinkableFrom: true, relinkableTo: true,
                    toShortLength: 3
                },
                new go.Binding("points").makeTwoWay(),
                new go.Binding("curviness"),
                makeGO(go.Shape,  // the link shape
                    {strokeWidth: 1.5}),
                makeGO(go.Shape,  // the arrowhead
                    {toArrow: "standard", stroke: null}),
                makeGO(go.Panel, "Auto",
                    makeGO(go.Shape,  // the label background, which becomes transparent around the edges
                        {
                            fill: makeGO(go.Brush, "Radial",
                                {0: "rgb(240, 240, 240)", 0.3: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)"}),
                            stroke: null
                        }),
                    makeGO(go.TextBlock, "transition",  // the label text
                        {
                            textAlign: "center",
                            font: "9pt helvetica, arial, sans-serif",
                            margin: 4,
                            editable: true  // enable in-place editing
                        },
                        // editing the text automatically updates the model data
                        new go.Binding("text").makeTwoWay())
                )
            );
    };

    private constructor() {
    }

    public static Factory() {
        let directive = () => {
            return new IndividualGraph();
        };
        directive['$inject'] = [];
        return directive;
    }

}