export default class Config {
    public static log = {
        functions: false,
        cycle: false,
        generations: false,
        nextFitness: false,
    };

    public static cycle = {
        continue: false,
        running: false
    };

    public static general = {
        alwaysEvaluateFitness: false
    };

    public static mutationOptions = {
        mutateOffsprings: true,
        mutateByCloning: true,
        individualTopology: {
            chance: 0.33,
            addNode: {
                chance: 1
            },
            addConnection: {
                chance: 1
            },
            addNodeXORaddConnection: true,
        },
        individualWeights: {
            chance: 0.33,
            weights: {
                chance: 1,
                mutateSingle: {
                    chance: 0.8,
                    stdev: 1
                }
            },
            thresholds: {
                chance: 1,
                mutateSingle: {
                    chance: 0.8,
                    stdev: 1
                }
            }
        }
    };

    public static crossoverOptions = {
        offspringRatio: 0.33,
        tournamentRatio: 0.5,
        geneDisabled: {
            chance: 0.75
        }
    };

    /**
     * distance function coefficient
     * d = (c_e*E)/N + (c_d*D)/N + c_m*W;
     * 3.0 = 1.0 ...... 1.0 ...... 0.4 - values from NEAT paper capter 4.1
     */
    public static speciation = {
        excessCoef: 1,
        disjointCoef:  1,
        matchingCoef: 0.4,
        distanceThreshold: 0.8
    };

}
