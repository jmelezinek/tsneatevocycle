/**
 * Cart and pole (balance inverted pendulum) game closure
 */
(function() {

    /**
     * Cart and pole (balance inverted pendulum) game AngularJS app
     */
    var app = angular.module("balancingGame", ['ui.toggle']); // @author ui.toggle - http://ziscloud.github.io/angular-bootstrap-toggle/

    app.controller("gameController", function($interval) {

        this.log = false;

        // ---------- constructor ----------
        this.config = {
            trackLimit: 2.4,    // track length (to one side) in m; 1px = 1cm
            failureAngle: 12 * Math.PI / 180,  // 12° // failure angle in rad
            randomAngle: false, // initial angle of pole
            time: 0,            // time in ms       // WARNING for calculation needed in seconds, same for tstep
            tstep: 20,          // time step in ms  // WARNING this is read-only see page 4 of Jason Brownlee: THE POLE BALANCING PROBLEM, A Benchmark Control Theory Problem - http://researchbank.swinburne.edu.au/vital/access/services/Download/swin:7595/SOURCE1
            timeSpeed: 1,       // can slow down animation
            force: 5,           // force applied at update time to the cart in N
            forceDir: true,     // force direction; true = 1, false = -1
            g: 9.81             // gravitational acceleratin in m/s^2
        };

        this.cart = {
            pos: 0.0,           // position from the center (0.0) in m
            vel: 0.0,           // velocity in m/s
            acc: 0.0,           // acceleration m/s^2
            mass: 1.0           // mass of car in kg
        };

        this.pole = {
            angle: 0.0,         // pole angle in rad
            vel: 0.0,           // angular velocity in rad/s
            acc: 0.0,           // pole acceleration in rad/s^2
            length: 1.0,        // pole length in m
            mass: 0.1           // mass at the end of the rod in kg
        };

        this.graphics = new PendulumGraphic(this.config, this.cart, this.pole);

        this.resetValues = function() {
            this.config.time = 0.0;

            this.cart.pos = 0.0;
            this.cart.vel = 0.0;
            this.cart.acc = 0.0;

            this.pole.angle = !this.config.randomAngle ? 0.0 : this.randomPoleAngle();
            this.pole.vel = 0.0;
            this.pole.acc = 0.0;

            this.inout = null;
        };

        this.randomPoleAngle = function() {
            // return Math.random() * (this.config.failureAngle / 8) * (Math.random() > 0.5 ? -1 : 1);
            return (Math.random() > 0.5 ? -1 : 1) * 1 * Math.PI / 180; // 1° to the left or to the right
        };

        // ---------- CONTROL FUNCTION ----------
        this.timer;

        this.reset = function() {
            if(this.log) {
                console.log("reset");
            }
            this.stop();
            this.resetValues();
            this.graphics.update(this.config, this.cart, this.pole);
            this.graphics.gameover(false);
            this.graphics.gamewon(false);
        };

        this.start = function() {
            if(this.log) {
                console.log("start");
            }
            $interval.cancel(this.timer);
            this.timer = $interval(this.step.bind(this), this.config.tstep * (1 / this.config.timeSpeed));
        };

        this.step = function() {
            this.config.time += this.config.tstep;
            this.update();
            this.graphics.update(this.config, this.cart, this.pole);

            // reset inputs/inputs to be shown
            this.inout = null;

            if(!this.fulfillConstraints()) {
                this.gameover();
            }

            if(this.fulfillGame()) {
                this.gamewon();
            }
        };

        this.stop = function() {
            if(this.log) {
                console.log("stop");
            }
            $interval.cancel(this.timer);
        };

        this.gameover = function() {
            if(this.log) {
                console.log("GAME OVER");
            }
            this.stop();
            this.graphics.gameover();
        };

        this.gamewon = function() {
            if(this.log) {
                console.log("WINNER");
            }
            this.stop();
            this.graphics.gamewon();
        };

        this.update = function() {
            if(this.log) {
                console.log("update");
            }

            // Differential equation of motion of the pole
            var g = this.config.g;
            var F = this.config.force * (this.config.forceDir ? 1.0 : -1.0);
            var m_c = this.cart.mass;
            var m_p = this.pole.mass;
            var l = this.pole.length / 2;
            var O = this.pole.angle;
            var O_d2 = this.pole.acc * this.pole.acc;
            var sinO = Math.sin(O);
            var cosO = Math.cos(O);
            var cos2O = cosO * cosO;

            // equation
            var numerator = g * sinO + cosO * ((-F - m_p * l * O_d2 * sinO) / (m_c + m_p));
            var denominator = l * (4 / 3 - ((m_p * cos2O) / (m_c + m_p)));
            this.pole.acc = numerator / denominator;

            // Differential equation of motion of the cart
            var O_dd = this.pole.acc;

            // equation
            this.cart.acc = (F + m_p * l * (O_d2 * sinO - O_dd * cosO)) / (m_c + m_p);

            // update
            var step = this.config.tstep / 1000; // time step in s
            this.cart.pos = this.cart.pos + step * this.cart.vel;
            this.cart.vel = this.cart.vel + step * this.cart.acc;
            this.pole.angle = this.pole.angle + step * this.pole.vel;
            this.pole.vel = this.pole.vel + step * this.pole.acc;
        };

        this.fulfillConstraints = function() {
            if(Math.abs(this.cart.pos) > this.config.trackLimit) {
                return false;
            }
            if(Math.abs(this.pole.angle) > this.config.failureAngle) {
                return false;
            }
            return true;
        };

        this.fulfillGame = function() {
            return this.config.time >= 600000;
        };

        // ---------- INPUT OUTPUT FUNCTIONS ----------

        this.getCartPos = function() {
            return this.cart.pos;
        };
        this.getCartVel = function() {
            return this.cart.vel;
        };
        this.getPoleAngle = function() {
            return this.pole.angle;
        };
        this.getPoleVel = function() {
            return this.pole.vel;
        };
        this.pushLeft = function() {
            if(this.log) {
                console.log("pushLeft");
            }
            this.config.forceDir = false;
        };
        this.pushRight = function() {
            if(this.log) {
                console.log("pushRight");
            }
            this.config.forceDir = true;
        };
        this.pushSame = function() {
            if(this.log) {
                console.log("pushSame");
            }
            this.config.forceDir = this.config.forceDir;
        };
        this.getForceDir = function() {
            return this.config.forceDir ? 1.0 : -1.0;
        };

    });

    app.directive('jsonTable', function() {
        return {
            restrict: 'E',
            scope: {
                caption: "@",
                json: "=" // "=" both way binding
            },
            templateUrl: 'JSONTable.html'
        };
    });

    /**
     * Graphics (view) for Cart and pole (balance inverted pendulum) game
     *
     * assume this coordinate system:
     *
     *                       0*PI
     *               -PI/6 \  |  / +PI/6
     *                      \ | /
     *                       \|/
     * ____________________[CART]____________________
     *
     *                        y
     *                        ^
     *                        |
     *                        |
     *                      (0,0) ---> x
     * ____________________[CART]____________________
     *
     */
    PendulumGraphic = function(configData, cartData, poleData) {

        this.X_WIDTH = $('#canvas').width();
        this.Y_HEIGHT = $('#canvas').height();
        this.CART_W = 50;
        this.CART_H = 20;
        this.CART_H_OFFSET = this.CART_H + 5;
        this.CART_Y = this.Y_HEIGHT - this.CART_H_OFFSET;
        this.POLE_START_Y = this.Y_HEIGHT - this.CART_H_OFFSET - this.CART_H / 2;
        this.TRACK_Y = this.Y_HEIGHT - this.CART_H_OFFSET / 2;

        this.correctCoordinates = function() {
            // move everything relative to our cartesian center
            var cartLoc = this.cartGraphic.getLocation();
            var cartX = cartLoc.getX() + this.X_WIDTH / 2;
            this.cartGraphic.setLocationXY(cartX, this.CART_Y);

            var poleEnd = this.poleGraphic.getEndPoint();
            this.poleGraphic.setStartPointXY(cartX, this.POLE_START_Y);
            this.poleGraphic.setEndPointXY(cartX + poleEnd.getX(), -poleEnd.getY() + this.Y_HEIGHT - this.CART_H_OFFSET - this.CART_H / 2);

        };

        this.update = function(configData, cartData, poleData) {

            this.trackGraphic.setStartPointXY(-configData.trackLimit * 100 + this.X_WIDTH / 2, this.TRACK_Y); // already correct coordinates
            this.trackGraphic.setEndPointXY(configData.trackLimit * 100 + this.X_WIDTH / 2, this.TRACK_Y); // already correct coordinates

            this.cartGraphic.setX(cartData.pos * 100);
            this.cartGraphic.setY(0);

            // recalculate polar coordinates to our cartesian
            this.poleGraphic.setEndX(poleData.length * 100 * Math.cos(-poleData.angle + Math.PI / 2));
            this.poleGraphic.setEndY(poleData.length * 100 * Math.sin(-poleData.angle + Math.PI / 2));

            this.correctCoordinates();
        };

        this.gameover = function(gameover) {
            gameover = typeof(gameover) === 'undefined' ? true : gameover;
            this.gameoverText.setOpacity(gameover ? 1 : 0);
        };

        this.gamewon = function(gamewon) {
            gamewon = typeof(gamewon) === 'undefined' ? true : gamewon;
            this.gamewonText.setOpacity(gamewon ? 1 : 0);
        };

        // ---------- constructor ----------
        this.panel = new jsgl.Panel(document.getElementById('canvas'));

        this.cartGraphic = this.panel.createRectangle();
        this.cartGraphic.setSizeWH(this.CART_W, this.CART_H);
        this.cartGraphic.setHorizontalAnchor(jsgl.HorizontalAnchor.CENTER);
        this.cartGraphic.setVerticalAnchor(jsgl.VerticalAnchor.MIDDLE);
        this.panel.addElement(this.cartGraphic);

        this.poleGraphic = this.panel.createLine();
        this.panel.addElement(this.poleGraphic);

        this.trackGraphic = this.panel.createLine();
        var trackStroke = new jsgl.stroke.SolidStroke();
        trackStroke.setColor("blue");
        trackStroke.setWeight(3);
        trackStroke.setOpacity(0.66);
        this.trackGraphic.setStroke(trackStroke);
        this.panel.addElement(this.trackGraphic);

        this.gameoverText = this.panel.createLabel();
        this.gameoverText.setLocationXY(this.X_WIDTH / 2, this.Y_HEIGHT / 2);
        this.gameoverText.setHorizontalAnchor(jsgl.HorizontalAnchor.CENTER);
        this.gameoverText.setVerticalAnchor(jsgl.VerticalAnchor.MIDDLE);
        this.gameoverText.setText("GAME OVER");
        this.gameoverText.setFontSize(24);
        this.gameoverText.setFontColor("red");
        this.gameoverText.setBold(true);
        this.gameoverText.setOpacity(0);
        this.panel.addElement(this.gameoverText);

        this.gamewonText = this.panel.createLabel();
        this.gamewonText.setLocationXY(this.X_WIDTH / 2, this.Y_HEIGHT / 2);
        this.gamewonText.setHorizontalAnchor(jsgl.HorizontalAnchor.CENTER);
        this.gamewonText.setVerticalAnchor(jsgl.VerticalAnchor.MIDDLE);
        this.gamewonText.setText("WINNER");
        this.gamewonText.setFontSize(24);
        this.gamewonText.setFontColor("green");
        this.gamewonText.setBold(true);
        this.gamewonText.setOpacity(0);
        this.panel.addElement(this.gamewonText);

        this.update(configData, cartData, poleData);

    };

    /**
     *  key binding for Cart and pole (balance inverted pendulum) game
     */
    $(document).keydown(function(e) {
        switch(e.which) {
            case 37: // left
                $('#push-left').click();
                break;
            case 39: // right
                $('#push-right').click();
                break;
            case 82: // r
                $('#reset').click();
                break;
            case 84: // t
                $('#start').click();
                break;
            case 83: // s
                $('#stop').click();
                break;
            case 32:// space
                $('#step').click();
                break;
            default:
                return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll, move, etc.)
    });

})();