import SlitherioBot from "./SlitherioBot";
import {Point, default as SlitherioController} from "./SlitherioController";

export default class SlitherioBotSensorMove extends SlitherioBot {

    public static noOutputs: number = 13; // 12 move directions + acceleration

    protected outputsToDecision(outputs: number[]) {

        // acceleration
        let acc = outputs.pop() > 0 ? true : false; // remove and use last which is acceleration
        this.gameCtrl.setAcceleration(acc);

        // move to one of 12 possible direction
        let s = outputs.indexOf(Math.max(...outputs)); // first 12 values are sensors to move
        this.gameCtrl.setMouseCoor(this.sensorCenter(4+s));

    }

    private sensorCenter(sensorNumber: number): Point {
        let A = SlitherioController.wordToCanvasCoor(this.precalculated.sensors[sensorNumber].cornerA);
        let B = SlitherioController.wordToCanvasCoor(this.precalculated.sensors[sensorNumber].cornerB);
        let snkObj = this.gameCtrl.getSnake();
        let snkPt = this.gameCtrl.wordToCanvasCoor({x: snkObj.xx, y: snkObj.yy});

        let res = {
            x: ((A.x + B.x) / 2 - snkPt.x) / this.gameCtrl.getZoom(),
            y: ((A.y +B.y) / 2 - snkPt.y) / this.gameCtrl.getZoom()
        };
        return res;
    }
}
