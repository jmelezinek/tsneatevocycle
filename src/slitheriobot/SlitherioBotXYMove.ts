import SlitherioBot from "./SlitherioBot";

export default class SlitherioBotXYMove extends SlitherioBot {

    public static noOutputs: number = 3; // X, Y where to move + acceleration

    protected outputsToDecision(outputs: number[]) {

        let x = outputs[0];
        let y = outputs[1];
        let acc = outputs[2] > 0 ? true : false;

        // move direction
        let res = {x: x, y: y};
        console.log(res);
        this.gameCtrl.setMouseCoor({x: x, y: y});

        // acceleration
        this.gameCtrl.setAcceleration(acc);

    }

}
