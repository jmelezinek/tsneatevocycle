import {BaseNeatIndividual} from "../lib/model/BaseNeatIndividual";
import SlitherioController from "./SlitherioController";
import {Rect} from "./SlitherioController";
import {Point} from "./SlitherioController";
import {Positionable} from "./SlitherioController";
import {SnakePart} from "./SlitherioController";
import {Circle} from "./SlitherioController";


abstract class SlitherioBot extends BaseNeatIndividual {
    protected _timer: number;
    protected _gameCtrl = SlitherioController;
    public static sensorWidth: number = 50;
    public static noSensorLayers: number = 4;
    public static noSensors: number = 4 + (SlitherioBot.noSensorLayers - 1)*12; // 4 in first layer and 12 fore each other layyer
    public static noInputs: number = SlitherioBot.noSensors;
    public static noOutputs: number; // abstract to be rewritten
    protected _ticks: number = 0;

    protected _precalculated: {
        sensorWidth: number,
        sensorsWidth: number,
        sensorsRadius: number,
        sensors: Array<Rect>
    } = {
        sensorWidth: null,
        sensorsWidth: null,
        sensorsRadius: null,
        sensors: null
    };

    public evaluateFitness(): Promise<number> {
        return this.start()
            .then((res: number) => {
                this._fitness = res;
                return res;
            });
    }

    public start() : Promise<number> {

        let fitnessPromise = new Promise(
            (resolve, reject) => {
                this._timer = window.setInterval(this.update.bind(this, resolve), 100);
            });

        this.gameCtrl.startNewGame();

        return fitnessPromise;
    }

    protected update(resolve: (score: number) => void) {
        if(!this.gameCtrl.getSnake()) {
            console.warn("Cannot update: snake not available.");
            return;
        }

        if(this.gameCtrl.isGameOver()) {
            window.clearInterval(this._timer);
            this.gameCtrl.getGameScore().then((score) => resolve(score));
            return;
        }

        this.precalculate();
        this.makeDecision();

        this._ticks++;
    }

    /**
     * @desc Precalculates values repeatedly used in decision.
     */
    protected precalculate() {

        this._precalculated.sensorWidth = SlitherioBot.sensorWidth * this.gameCtrl.getZoom(); // length of patch in pixels
        this._precalculated.sensorsWidth = Math.pow(2,SlitherioBot.noSensorLayers-1) * this._precalculated.sensorWidth;
        this._precalculated.sensorsRadius = Math.sqrt(this._precalculated.sensorsWidth*this._precalculated.sensorsWidth + this._precalculated.sensorsWidth*this._precalculated.sensorsWidth);

        this._precalculated.sensors = [];
        for (let s = 0; s < SlitherioBot.noSensors; s++) {
            this._precalculated.sensors[s] = this.getSensor(s);
        }
    }

    protected makeDecision() {

        // transform game values to inputs
        let inputs: Array<number> = this.environmentToInputs();

        // brain
        let outputs = this.evaluateNetwork(inputs);

        // transform outputs to game moves
        this.outputsToDecision(outputs);

        this.drawSensors(inputs);
        // this.drawSnakeParts();
    }

    protected environmentToInputs(): number[] {

        let sensorValues: number[] = [];
        sensorValues.length = SlitherioBot.noSensors;
        sensorValues.fill(0, 0, sensorValues.length); // array full of 0

        // set sensor to amount of food and pray (by size) which is in it
        let foods = this.gameCtrl.getAllFoodsAndPreys();
        for(let i = 0; i < foods.length; i++) {
            let food = foods[i];
            if(this.isInRange(food)) { // fast decision if it even can be in any sensor
                for (let s = 0; s < SlitherioBot.noSensors; s++) {
                    if (this.isInSensor(s, food)) {
                        sensorValues[s] += food.sz;
                        continue; // can't be in multiple, only on edges // don't care
                    }
                }
            }
        }

        // set sensor to -1 if there is other snake part obstacle
        let snakes = this.gameCtrl.getAllOtherSnakes(); // cannot use squareDistance easily because snake is consisted of multiple parts
        for(let i = 0; i < snakes.length; i++) {
            let snake = snakes[i];

            let radius = Math.round(snake.sc * 29.0) / 2;
            if (this.isInRange(snake, radius)) { // fast decision if it even can be in any sensor
                for (let s = 0; s < SlitherioBot.noSensors; s++) {
                    if (this.isInSensor(s, snake, radius)) {
                        sensorValues[s] = -1;
                        // continue; // can be in multiple sensors
                    }
                }
            }

            for (let j = 0; j < snake.pts.length; j++) {
                let part = snake.pts[j];
                if (part != null && this.isInRange(part, radius)) { // fast decision if it even can be in any sensor
                    for (let s = 0; s < SlitherioBot.noSensors; s++) {
                        if (!part.dying && this.isInSensor(s, part, radius)) {
                            sensorValues[s] = -1;
                            // continue; // can be in multiple sensors
                        }
                    }
                }
            }
        }

        // set sensor to -1 if end of word
        if(SlitherioController.distance(this.gameCtrl.getSnake(), this.gameCtrl.word.center) + this._precalculated.sensorsRadius >= this.gameCtrl.word.radius) { // fast decision
            for(let s = 0; s < SlitherioBot.noSensors; s++) {
                if(this.isSensorOutOfMap(s)) {
                    sensorValues[s] = -1;
                }
            }
        }

        return sensorValues;
    }

    protected abstract outputsToDecision(outputs: number[]): void;

    protected drawSensors(sensorValues: number[]) {
        this.gameCtrl.setCustomRedraw(() => {
            let snk = this.gameCtrl.getSnake();
            if(!snk) {
                return;
            }

            // heading
            let headingCircle = {center: {x: snk.xx + this.gameCtrl.getMouseCoor().x, y: snk.yy + this.gameCtrl.getMouseCoor().y}, radius: 5};
            this.gameCtrl.drawCircle(headingCircle, '#ffffff', false, true, 0.66);

            // sensor grid
            for(let s = 0; s < SlitherioBot.noSensors; s++) {
                this.gameCtrl.drawRect(this.getSensor(s), '#000000', true, false, 1);
            }

            // sensor filling
            for(let s = 0; s < SlitherioBot.noSensors; s++) {
                let color: string;
                let alpha: number;
                let maxAlpha: number = 0.66;

                if(sensorValues[s] == -1) {
                    color = '#b20000';
                    alpha = maxAlpha;
                } else {
                    color = '#00b200';
                    alpha = Math.min(sensorValues[s] / 100, maxAlpha);
                }

                this.gameCtrl.drawRect(this.getSensor(s), color, false, true, alpha);
            }
        });
    }

    protected drawSnakeParts() {
        this.gameCtrl.setCustomRedraw(() => {
            let snakes = this.gameCtrl.getAllSnakes();
            for(let i = 0; i < snakes.length; i++) {

                let snk = snakes[i];
                this.gameCtrl.drawCircle({center: {x: snk.xx, y: snk.yy}, radius: 5}, '#000000', false, true, 1);

                for(let j = 0; j < snk.pts.length; j++) {
                    let prt = snk.pts[j];
                    if(!((<any>prt).dying)) {
                        this.gameCtrl.drawCircle({center: {x: prt.xx, y: prt.yy}, radius: 5}, '#000000', false, true, 1);
                    }
                }
            }
        });
    }

    protected isInSensor(sensorNumber: number, point: Positionable, radius: number = 0): boolean {
        let sensor = this._precalculated.sensors[sensorNumber];
        return SlitherioBot.isInRect(sensor.cornerA, sensor.cornerB, point, radius);
    }

    protected isSensorOutOfMap(sensorNumber: number) {
        let word: Circle = this.gameCtrl.word;
        let sensor: Rect = this._precalculated.sensors[sensorNumber];

        // Find the furthermost point to the circle within the rectangle
        let furthestX = SlitherioBot.clampRev(word.center.x, sensor.cornerA.x, sensor.cornerB.x);
        let furthestY = SlitherioBot.clampRev(word.center.y, sensor.cornerA.y, sensor.cornerB.y);

        // Calculate the distance between the circle's center and this closest point
        let distanceX = word.center.x - furthestX;
        let distanceY = word.center.y - furthestY;

        // make sure it is *not* in the circle
        return !((distanceX * distanceX) + (distanceY * distanceY) <= (word.radius * word.radius));
    }

    protected static clamp(value: number, minOrMax: number, maxorMin: number) {
        let min = Math.min(minOrMax, maxorMin);
        let max = Math.max(minOrMax, maxorMin);
        let res = value;

        if (res < min) {
            res = min;
        } else if (res > max) {
            res = max;
        }

        return res;
    };

    protected static clampRev(value: number, minOrMax: number, maxorMin: number) {
        let min = Math.min(minOrMax, maxorMin);
        let max = Math.max(minOrMax, maxorMin);
        let res = value;

        if (res < max) {
            res = max;
        } else if (res > min) {
            res = min;
        }

        return res;
    };

    protected getSensor(sensorNumber: number): Rect {
        let width = this._precalculated.sensorWidth;
        let xx = this.gameCtrl.getSnake().xx;
        let yy = this.gameCtrl.getSnake().yy;

        /**
         * First layer of 4 sensors
         */
        if(sensorNumber < 4) {
            switch (sensorNumber) {
                case 0: return {cornerA: {x: xx, y: yy}, cornerB: {x: xx + width, y: yy + width}};
                case 1: return {cornerA: {x: xx, y: yy}, cornerB: {x: xx - width, y: yy + width}};
                case 2: return {cornerA: {x: xx, y: yy}, cornerB: {x: xx - width, y: yy - width}};
                case 3: return {cornerA: {x: xx, y: yy}, cornerB: {x: xx + width, y: yy - width}};
            }
        }

        /**
         * Second and further layers of 12 sensors
         *
         * Imagine two rect (small, 2 times bigger) with same center
         * originRectPoint - corners of smaller rect - changes every 4
         * secondRectPoint - corners and halfs - changes like 012 234 456 678
         */
        for(let l = 0; l < SlitherioBot.noSensorLayers -1; l++) {
            let lenInLayer = width * Math.pow(2, l);
            let sensorNumberInLayer = sensorNumber - 4 - 12*l; // in a layer
            if(sensorNumberInLayer >= 12) {
                continue; // next layer
            }

            // changes every 4
            let originRectPoint: Point;
            switch(Math.floor(sensorNumberInLayer/3)) {
                case 0: originRectPoint = {x: xx + lenInLayer, y: yy + lenInLayer}; break;
                case 1: originRectPoint = {x: xx - lenInLayer, y: yy + lenInLayer}; break;
                case 2: originRectPoint = {x: xx - lenInLayer, y: yy - lenInLayer}; break;
                case 3: originRectPoint = {x: xx + lenInLayer, y: yy - lenInLayer}; break;
            }

            // changes like 012 234 456 678
            let secondRectPoint: Point = {
                x: xx + (SlitherioBot.secondPointFn(sensorNumberInLayer) * 2*lenInLayer),
                y: yy + (SlitherioBot.secondPointFn((sensorNumberInLayer + 12 - 3) % 12) * 2*lenInLayer)};

            return {cornerA: originRectPoint, cornerB: secondRectPoint};
        }
    }

    /**
     * @returns 0-11: 1,1,0,0,-1,-1, -1,-1,0,0,1,1
     */
    protected static secondPointFn(n:number): number {
        if(n > 5) {
            n = 11 - n;
        }
        return -(Math.floor(n/2)-1);
    }

    /**
     * Do not forget that canvas starts top left, x goes right, y goes down
     */
    protected static isInRect(cornerA: Point, cornerB: Point, point: Positionable, radius: number = 0): boolean {

        // sort by x axis
        if (cornerA.x > cornerB.x) {
            let tmpA = {x: cornerA.x, y: cornerA.y};
            cornerA = cornerB;
            cornerB = tmpA;
        }

        // one option in x axe
        if (point.xx >= cornerA.x - radius && point.xx <= cornerB.x + radius) {
            // two option in y axe
            if((point.yy >= cornerA.y - radius && point.yy <= cornerB.y + radius)
                || (point.yy >= cornerB.y - radius && point.yy <= cornerA.y + radius)) {
                return true;
            }
        }

        return false;
    }

    protected isInRange(point: Positionable, radius: number = 0) {
        return SlitherioController.distance(this.gameCtrl.getSnake(), point) <= this._precalculated.sensorsRadius + radius;
    }

    //
    // GETTERST and SETTERS
    //

    protected get gameCtrl() {
        return this._gameCtrl;
    }

    public get ticks() {
        return this._ticks;
    }


    protected get precalculated(): {sensorWidth: number; sensorsWidth: number; sensorsRadius: number; sensors: Array<Rect>} {
        return this._precalculated;
    }
}

export default SlitherioBot;