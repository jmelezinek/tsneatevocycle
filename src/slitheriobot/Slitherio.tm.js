// ==UserScript==
// @name         lib.slitherio.js
// @namespace    http://neat.melezinek.cz/
// @version      0.1
// @description  tsneatevocycle distribution for slither.io
// @author       Jakub Melezinek (melezjak)
// @match        http://slither.io/
// @grant        none
// @run-at       document-end
// ==/UserScript==

// after DOM loaded
$(function(){

    /* jshint ignore:start */ // necessary fot Tampermonkey due its too strict "compiler"
    {{lib.slitherio.bundle.js}}
    /* jshint ignore:end */

});