/// <reference path="../../node_modules/@types/angular/index.d.ts" />
declare var angular: any; // not really importing, just need .d.ts here // included by EvoCycle.controls.tampermonkey.js

export type Point = {x: number, y: number};
export type Circle = {center: Point, radius: number};
export type Rect = {cornerA: Point, cornerB: Point};
export type RectWH = {topLeft: Point, width: number, height: number}

export interface Positionable {
    id: number,
    xx: number,
    yy: number
}

function isPositionable(entity: any): entity is Positionable {
    return (<Positionable>entity).xx !== undefined && (<Positionable>entity).yy !== undefined;
}

function isPoint(entity: any): entity is Point {
    return (<Point>entity).x !== undefined && (<Point>entity).y !== undefined;
}

export interface FoodLike extends Positionable {
    sz: number, // size
}

export interface Snake extends Positionable {
    pts: Array<SnakePart>, // parts
    sc: number // scale
}

export interface SnakePart extends Positionable {
    dying: boolean;
}

var game: any = (<any>window);
var ctrl = class SlitherioController {

    protected static _checkScoreTimer: number;
    protected static _botPlaying: boolean = false;
    protected static _isInitialized: boolean = false;
    protected static _gameScoreChecked: boolean = false;
    protected static _isMouseMoveEnabled: boolean = false;
    protected static _isControlsVisible: boolean = true;

    public static word: Circle = {center: {x: 21600, y: 21600}, radius: 21160};

    public static init() {
        game.slitherioController = {gsc: game.gsc};
        game.origController = {};

        // low graphics for better performance
        game.render_mode = 1;
        game.want_quality = 0;
        game.high_quality = false;

        // additional custom redraw function
        game.origController.redraw = game.redraw;
        game.redraw = function () {
            game.origController.redraw();
            ctrl._customRedraw();
        };

        // additional eof function
        game.origController.oef = game.oef;
        game.oef = function() {
            game.origController.oef();

            // keep zoom
            game.gsc = game.slitherioController.gsc;
        };

        // additional onmousemove function
        game.origController.onmousemove = game.onmousemove;
        game.onmousemove = function(e:any) {
            if(ctrl._isMouseMoveEnabled) {
                game.origController.onmousemove(e);
            }
        };

        // additional onmousedown function
        game.origController.onmousedown = game.onmousedown;
        game.onmousedown = function(e:any) {
            if(ctrl._isMouseMoveEnabled) {
                game.origController.onmousedown(e);
            }
        };

        // additional custom key bindings
        game.origController.onkeydown = document.onkeydown;
        document.onkeydown = function(e) {
            ctrl._customOnkeydown(e); // custom first to disable space
            game.origController.onkeydown(e);
        };

        console.log("Custom shortcuts:");
        console.log("\t+/- zoom");
        console.log("\t D  debug [d]ump");
        console.log("\t M  toggle [m]ouse moving (default off)");
        console.log("\t H  toggle [h]idden/visible controls div (default visible)");

        ctrl._isInitialized = true;
    }

    public static startNewGame() {
        if(!ctrl._isInitialized) {
            ctrl.init();
        }

        (<any>document.getElementById("nick")).value = "playing...";
        (<any>document.getElementById("playh")).children[0].click();
        ctrl.setMouseCoor({x: 0, y: 0});
        ctrl._botPlaying = true;
    }

    public static isGameOver(): boolean {
        if(ctrl.isBotPlaying() && game.snake != null && game.snake.dead_amt >= 1 && game.snake.alive_amt >= 1) {
            ctrl.resetCustomRedraw();
            game.snake.dead_amt = 0; // IMPORTANT slither.io itself doesn't set these variables for new game immediately, takes old instead which mess up with this function
            game.snake.alive_amt = 0; // IMPORTANT slither.io itself doesn't set these variables for new game immediately, takes old instead which mess up with this function
            ctrl._botPlaying = false;
            ctrl._gameScoreChecked = false;
            return true;
        }

        return false;
    }

    protected static isBotPlaying(): boolean {
        return ctrl._botPlaying;
    }

    public static getGameScore(): Promise<number> {

        let scorePromise = new Promise(
            (resolve, reject) => {
                ctrl._checkScoreTimer = window.setInterval(ctrl.checkGameScore.bind(this, resolve), 100);
            });

        return scorePromise;
    }

    protected static checkGameScore(resolveScore: any) {
        // wait until score is there
        if(game.lastscore
            && game.lastscore.childNodes[1]
            && game.lastscore.childNodes[1].innerHTML != ""
            && ctrl._gameScoreChecked == false) {

            window.clearInterval(ctrl._checkScoreTimer);

            let score = parseInt(game.lastscore.childNodes[1].innerHTML);
            ctrl._gameScoreChecked = true; // otherwise next bot will read wrong score
            // game.lastscore.childNodes[1].innerHTML = ""; // otherwise next bot will read wrong score, also with waiting it solves problem of next bot thinking it is already isGameover == true

            // resolveScore(score);
            window.setTimeout(() => {resolveScore(score);}, 3000); // problems with playing again so wait a little bit
        }
    }

    public static getSnake(): Snake {
        return game.snake;
    }

    public static getAllFoodsAndPreys(): Array<FoodLike> {
        return [].concat(game.foods.concat(), game.preys.concat()).filter(ctrl.nullFilter);
    }

    public static getClosestSnakes(n?: number): Array<Snake> {
        n = (typeof n == "undefined") ? game.snakes.length : n;

        let res = game.snakes.concat()
            .filter(ctrl.nullFilter)
            .sort(ctrl.distanceCoparator);
        res.splice(n);
        res.shift(); // do not include game.snake

        return res;
    }

    public static getAllSnakes(): Array<Snake> {
        return game.snakes.concat();
    }

    public static getAllOtherSnakes(): Array<Snake> {
        return game.snakes.concat()
            .filter(ctrl.nullFilter)
            .filter((snk: Snake) => { return !(snk.id == game.snake.id); });
    }

    public static positionableToPoint(entity: Positionable | Point): Point {
        if(isPoint(entity)) {
            return entity;
        } else {
            return {x: entity.xx, y: entity.yy};
        }
    }

    public static distance(entityA: Positionable | Point, entityB: Positionable | Point) {
        let s1 = ctrl.positionableToPoint(entityA).x - ctrl.positionableToPoint(entityB).x;
        let s2 = ctrl.positionableToPoint(entityA).y - ctrl.positionableToPoint(entityB).y;

        return Math.sqrt(s1*s1 + s2*s2);
    }

    protected static distanceCoparator(entityA: Positionable, entityB: Positionable): number {
        return ctrl.distance(game.snake, entityA) - ctrl.distance(game.snake, entityB);
    }

    public static squareDistance(entityA: Positionable, entityB: Positionable) {
        let s1 = Math.abs(entityA.xx - entityB.xx);
        let s2 = Math.abs(entityA.yy - entityB.yy);

        return Math.sqrt(s1+s2);
    }

    protected static squareDistanceCoparator(entityA: Positionable, entityB: Positionable): number {
        return ctrl.squareDistance(game.snake, entityA) - ctrl.distance(game.snake, entityB);
    }

    protected static sizeComparator(entityA: Snake, entityB: Snake): number
    protected static sizeComparator(entityA: FoodLike, entityB: FoodLike): number
    protected static sizeComparator() {
        let entityA = arguments[0];
        let entityB = arguments[1];
        return entityB.sz - entityA.sz || entityB.pts.length - entityA.pts.length;
    }

    protected static nullFilter(entity: any) {
        return !(entity == null);
    }

    public static drawRect(rect: Rect, color: string, stroke: boolean = true, fill: boolean = true, alpha: number = 0.5) {

        let drawRect: RectWH = ctrl.rectToDrawRect(ctrl.rectToRectWH(rect));

        let ctx = ctrl.getCanvasContext();
        ctx.save();

        ctx.beginPath();
        ctx.globalAlpha = alpha;
        ctx.lineWidth=0.5;
        ctx.rect(drawRect.topLeft.x, drawRect.topLeft.y, drawRect.width, drawRect.height);
        if(stroke) {
            ctx.strokeStyle = color;
            ctx.stroke();
        }
        if (fill) {
            ctx.fillStyle = color;
            ctx.fill();
        }

        ctx.restore();
    }

    public static rectToRectWH(rect: Rect): RectWH {
        let w = Math.abs(rect.cornerA.x - rect.cornerB.x);
        let h = Math.abs(rect.cornerA.y - rect.cornerB.y);
        let tl = {
            x: rect.cornerA.x < rect.cornerB.x ? rect.cornerA.x : rect.cornerB.x ,
            y: rect.cornerA.y < rect.cornerB.y ? rect.cornerA.y : rect.cornerB.y ,
        };

        return {
            topLeft: tl,
            width: w,
            height: h
        };
    }

    public static rectToDrawRect(rect: RectWH): RectWH {
        return {
            topLeft: ctrl.wordToCanvasCoor(rect.topLeft),
            width: rect.width * game.gsc,
            height: rect.height * game.gsc,
        }
    }

    // Draw a circle on the canvas.
    public static drawCircle (circle: Circle, color:string, stroke:boolean = true,  fill:boolean = true, alpha:number = 0.5) {

        let drawCircle: Circle = ctrl.mapCircleToCanvas(circle);

        let ctx = ctrl.getCanvasContext();
        ctx.save();

        ctx.beginPath();
        ctx.globalAlpha = alpha;
        ctx.lineWidth=0.5;
        ctx.arc(drawCircle.center.x, drawCircle.center.y, drawCircle.radius, 0, Math.PI * 2);
        if(stroke) {
            ctx.strokeStyle = color;
            ctx.stroke();
        }
        if (fill) {
            ctx.fillStyle = color;
            ctx.fill();
        }

        ctx.restore();
    }

    /**
     * Maps and scales point.
     */
    public static wordToCanvasCoor(point: Point): Point {
        return {
            x: game.mww2 + (point.x - game.view_xx) * game.gsc,
            y: game.mhh2 + (point.y - game.view_yy) * game.gsc
        };
    }

    /**
     * Maps and scales circle.
     */
    public static mapCircleToCanvas (circle: Circle): Circle {
        return {
            center: ctrl.wordToCanvasCoor(circle.center),
            radius: circle.radius * game.gsc
        };
    }

    public static getCanvasContext(): CanvasRenderingContext2D {
        return game.mc.getContext('2d');
    }

    public static getMouseCoor(): Point {
        return {x: game.xm, y: game.ym};
    }

    /**
     * @desc [0,0] is s snake head and axes are like for canvas => x increasing right, y increasing down
     */
    public static setMouseCoor(point: Point): void {
        game.xm = point.x;
        game.ym = point.y;
    }

    private static accTrueInRow = 0;

    public static setAccelerationXX(value: 1 | 0): void {

        // prevents blocking going fast if hold even if not enouhgt mass
        // TODO check jestli je dost dlouhje a jenom v tu chvili opravdu boostovat

        if(value == 1) {
            ctrl.accTrueInRow++
        } else {
            ctrl.accTrueInRow = 0;
        }

        if(ctrl.accTrueInRow % 10) {
            game.setAcceleration(0);
        } else {
            game.setAcceleration(value);
        }


    }

    public static setAcceleration(value: boolean): void {

        // prevents blocking going fast if hold whole time and not big enought to accelerate
        if(ctrl.getSnake().pts.filter((item) => { return !item.dying; }).length < 3) {
            game.setAcceleration(0);
            return; // not long enough
        }

        if(value) {
            game.setAcceleration(1);
        } else {
            game.setAcceleration(0);
        }

    }

    public static getZoom(): number {
        return game.gsc;
    }

    protected static _customRedraw: () => void = function () {};

    public static setCustomRedraw(func: () => void) {
        ctrl._customRedraw = func;
    }

    public static resetCustomRedraw() {
        ctrl._customRedraw = function(){};
    }

    protected static _customOnkeydown = function (e:any) {
        switch (e.keyCode) { // and custom key binding
            // space for acceleration disabled
            case 32:
                break;
            // + increase zoom
            case 107:
                game.slitherioController.gsc += 0.1;
                console.log("zoom increased to " + game.slitherioController.gsc.toFixed(2));
                break;
            // - decrease zoom
            case 109:
                game.slitherioController.gsc -= 0.1;
                console.log("zoom decreased to " + game.slitherioController.gsc.toFixed(2));
                break;
            // D debug dump
            case 68:
                game.origController.dump = angular.element(document.getElementById('tsneatevocycle-controller')).controller().model;
                console.dir(game.origController.dump);
                console.log("debug dump saved to window.origController.dump:");
                break;
            // M toggle mouse control
            case 77:
                ctrl._isMouseMoveEnabled = !ctrl._isMouseMoveEnabled;
                console.log("mouse move " + (ctrl._isMouseMoveEnabled ? "enabled" : "disabled"));
                break;
            // H toggle hide
            case 72:
                ctrl._isControlsVisible = !ctrl._isControlsVisible;
                document.getElementById('tsneatevocycle-controller').style.display = ctrl._isControlsVisible ? 'block' : 'none';
                console.log("controls div " + (ctrl._isControlsVisible ? "visible" : "hidden"));
                break;
            default:
                console.warn("no key binding");
                break;
        }

    };

    public static getGame() {
        return game;
    }
};

var SlitherioController = ctrl;
export default SlitherioController;
