import EvoCycle from "../lib/EvoCycle";
import {EvoCycleControls} from "../lib/controls/EvoCycleControls";
import SlitherioBot from "./SlitherioBot";
import SlitherioController from "./SlitherioController";
import SlitherioBotXYMove from "./SlitherioBotXYMove";
import SlitherioBotSensorMove from "./SlitherioBotSensorMove";

export class SlitherioLoader {
    private constructor() {}

    /**
     * Make sure that lib.controls.js is loaded first.
     * Then (this code) lib.slitherio.js can be loaded.
     */
    public static init(): void {

        // create evocycle with init population
        // let evocycle = new EvoCycle(SlitherioBotXYMove.createInitPopulation(2, [SlitherioBotXYMove.noInputs, SlitherioBotXYMove.noOutputs]));
        let evocycle = new EvoCycle(SlitherioBotSensorMove.createInitPopulation(50, [SlitherioBotSensorMove.noInputs, SlitherioBotSensorMove.noOutputs]));

        // initialize SlitherioController
        SlitherioController.init();

        // creates and starts angular app
        EvoCycleControls.init(evocycle);

    }
}

/**
 * @description This is the starting point for Slitherio EvoCycle
 */
SlitherioLoader.init();