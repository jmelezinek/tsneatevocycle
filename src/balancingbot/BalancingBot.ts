import {BaseNeatIndividual} from "../lib/model/BaseNeatIndividual";
import BalancingGameController from "./BalancingController";

export abstract class BalancingBot extends BaseNeatIndividual {

    protected _timer: number;

    public evaluateFitness(): Promise<number> {

        return this.start()
            .then((res: number) => {
                this._fitness = res;
                return res;
            });

    }

    public start = function() : Promise<number> {
        window.clearInterval(this.timer);

        this.game.gameReset();

        let fitnessPromise = new Promise(
            (resolve, reject) => {
                this.timer = window.setInterval(this.update.bind(this, resolve), this.game.getTstep());
            });

        this.game.gameStart();

        return fitnessPromise;
    };

    private update = function(resolve: any) {
        if(this.game.isGameover() || this.game.isGamewon()) {
            window.clearInterval(this.timer);
            resolve(this.game.getTime());
        }
        this.game.setOutputs(this.evaluate());
    };

    protected abstract evaluate(): number[];

    //
    // GETTERST and SETTERS
    //

    protected get game(): BalancingGameController {
        return BalancingGameController.getInstance();
    }

    protected get timer(): number {
        return this._timer;
    }

    protected set timer(value: number) {
        this._timer = value;
    }

}

/**
 * @class
 * @description
 * used with sigmoid NodeGenes (output is (0;1))
 * 4 inputs - standard
 * 1 output - force and direction (value and sign)
 */
export class NeatSingleOutputBalancingBot extends BalancingBot {
    protected evaluate(): number[] {

        let outputs = this.evaluateNetwork(this.game.getInputs());

        // translate neat output to game output // 0 -> same; -1 -> left; +1 -> right
        return [outputs[0] - 0.5];
    }
}

/**
 * @class
 * @description
 * used with binaryStep NodeGenes (output is 0/1)
 * 4 inputs - standard
 * 2 outputs - which button to press
 */
export class NeatTwoOutputsBalancingBot extends BalancingBot {
    protected evaluate(): number[] {

        let outputs = this.evaluateNetwork(this.game.getInputs());

        // translate neat output to game output // 0 -> same; -1 -> left; +1 -> right
        if(outputs[0] == 1 && outputs[1] == 1 // both buttons down - incorrect output
            || outputs[0] == 0 && outputs[1] == 0) { // both buttons up - incorrect output
            return [0]; // keep same
        }
        if(outputs[0] == 1) {
            return [-1]; // left
        }
        if(outputs[1] == 1) {
            return [1]; // right
        }

        throw "Unregonized options";
    }
}