import EvoCycle from "../lib/EvoCycle";
import {EvoCycleControls} from "../lib/controls/EvoCycleControls";
import {NeatSingleOutputBalancingBot} from "./BalancingBot";

export class BalancingLoader {
    private constructor() {}

    /**
     * Make sure that lib.controls.js is loaded first.
     * Then (this code) lib.slitherio.js can be loaded.
     */
    public static init(): void {

        // create EvoCycle with init population
        let evocycle = new EvoCycle(NeatSingleOutputBalancingBot.createInitPopulation(100, [4, 1]));
        // let evocycle = new EvoCycle(NeatSingleOutputBalancingBot.createInitPopulation(10, [4, 1]));

        // creates and starts angular app
        EvoCycleControls.init(evocycle);

    }
}

/**
 * @description This is the starting point for Slitherio EvoCycle
 */
BalancingLoader.init();