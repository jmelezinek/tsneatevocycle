// ==UserScript==
// @name         lib.balancing.js
// @namespace    http://neat.melezinek.cz/
// @version      0.1
// @description  tsneatevocycle distribution for balancing game
// @author       Jakub Melezinek (melezjak)
// @match        http://neat.melezinek.cz/dist/balancinggame/
// @match        http://127.0.0.1:63342/neat-libappgame/dist/balancinggame/
// @grant        none
// @run-at       document-end
// ==/UserScript==

// after DOM loaded
$(function(){

    /* jshint ignore:start */ // necessary fot Tampermonkey due its too strict "compiler"
    {{lib.balancing.bundle.js}}
    /* jshint ignore:end */

});