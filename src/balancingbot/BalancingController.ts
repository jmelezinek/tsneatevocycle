
/// <reference path="../../node_modules/@types/angular/index.d.ts" />
declare var angular: any; // not really importing, just need .d.ts here

export default class BalancingController {

    protected static _instance: BalancingController;
    protected _angularCtrl: any;

    protected constructor() {
        this._angularCtrl = angular.element(document.getElementById('game')).controller();
    }

    public static getInstance() {
        if(!BalancingController._instance) {
            BalancingController._instance = new BalancingController();
        }
        return BalancingController._instance;
    }

    public getInputs(): Array<number> {
        return [this.angularCtrl.getCartPos(), this.angularCtrl.getCartVel(), this.angularCtrl.getPoleAngle(), this.angularCtrl.getPoleVel()];
    }

    /**
     * @description Translates outputs to actions.
     * @param values
     */
    public setOutputs(values: Array<number>) {
        if(values[0] < 0) {
            this.angularCtrl.pushLeft();
        } else if(values[0] == 0) {
            this.angularCtrl.pushSame();
        } else if(values[0] > 0) {
            this.angularCtrl.pushRight();
        }
    }

    public getTime(): number {
        return this.angularCtrl.config.time;
    }

    public getTstep(): number {
        return this.angularCtrl.config.tstep;
    }

    public isGameover(): boolean {
        return !this.angularCtrl.fulfillConstraints();
    }

    public isGamewon(): boolean {
        return this.angularCtrl.fulfillGame();
    }

    public gameStart() {
        this.angularCtrl.start();
    }

    public gameReset() {
        this.angularCtrl.reset();
    }

    //
    // GETTERS and SETTERS
    //

    protected get angularCtrl(): any {
        return this._angularCtrl;
    }

}